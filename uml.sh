#!/bin/bash

set -ex

docker-compose -f docker-compose-dev.yml run --rm django ./manage.py graph_models -X AbstractBaseUser,PermissionsMixin,Group,Permission,MPTTModel,Receipt members > uml.dot
dot -Tpdf uml.dot > uml.pdf
