.. _synchro:

Synchronisation temps réel
==========================

L':ref:`API<api>` permet de synchroniser ponctuellement un module avec le noyau Jéito (par exemple une fois par jour).
Pour garder une synchronisation en temps (quasi) réel, Jéito propose également un mécanisme d'envoi de message à chaque modification.

Le protocole utilisé est `AMQP <https://www.amqp.org/>`_, selon le mode ``topic``.
Chaque modification d'un objet de la base de données du noyau déclenche l'émission d'un message.
Ce message peut être routé grâce à une clé du type ``jeito.<model>``.
``<model>`` est le nom du :ref:`modèle de données<modeles>`, en anglais, en minuscules.

Le message est au format JSON et contient un dictionnaire avec les attributs suivants :

- ``"operation"`` : ``"CREATE_UPDATE"`` ou ``"DELETE"``,
- ``"uuid"`` : identifiant unique de l'objet (UUID),
- ``"attributes"`` : dictionnaire contenant les attributs de l'objet, selon le format documenté dans :ref:`l'API<api>` (uniquement pour l'opération create_update),

Le client doit :

- se connecter au serveur,
- créer un canal,
- déclarer un échange nommé ``synchro_v4`` selon le mode ``topic``,
- créer une ou plusieurs files,
- déclarer un routage des différentes clés vers ces files.

Le plus simple est de créer une unique file et d'y router tous les message en provenance du noyau c'est à dire
avec la clé ``jeito.*`` (utilisation d'un wildcard pour récupérer les messages de tous les modèles).
Mais il est possible de router des modèles spécifiques. Il est conseillé de ne créer qu'une seule file
pour ne pas casser l'ordre d'émission des messages et éviter d'avoir à gérer des références sur des objets non créés.

Il est ensuite possible de lire les message de la file.

Les informations de connection (domaine, port, activation de TLS, utilisateur et mot de passe) sont à demander aux administrateurs de Jéito.
Des exemples de code dans différents langages sont accessibles dans cette `documentation <https://www.rabbitmq.com/getstarted.html>`_.
dans la rubrique *5 topics*.

Au démarrage du module, pour ne perdre aucune modification, il est nécessaire de :

- créer la file (sans lire aucun message à ce stade),
- lancer un synchronisation totale via des appels à :ref:`l'API<api>` V4,
- lire les messages de synchronisation temps réel.

Les premiers messages pourront être redondants avec les données déjà crées ou supprimées.
Une double création pourra être traitée comme une mise à jour. Et la suppression d'un objet inexistant pourra être ignorée.
