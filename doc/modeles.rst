.. _modeles:

Modèles de données
==================

StructureType
-------------

Type de structure :

- association,
- région,
- groupe local,
- centre,
- service vacances,
- autre.


Structure
---------

Structure EEDF telle que définie dans les statuts/réglements.

Lié à une structure parente.


Person
------

Toute personne physique :

- adhérent·e,
- ancien·ne adhérent·e,
- salarié·e,
- parent,
- payeur·se,
- ou autre…

Si une personne a plusieurs rôle, il est important de ne pas dupliquer cet objet.


Adherent
--------

Toute personne ayant adhéré. Un numéro d'adhérent·e lui est attribué.

Lié à une personne.


Adhesion
--------

Adhésion annuelle finissant au 31 août. Il existe autant d'adhésions pour une personne qu'il y a eu de saisons sur lesquelle il ou elle a adhéré.

Lié à un·e adhérent·e et à la structure sur laquelle l'adhésion a été prise administrativement.
L'adhérent peut être lié à une ou plusieurs autres structures via les fonctions.


Employee
--------

Salarié·e au sens large (y compris services civiques et contrats d'engagement éducatif). Un numéro de de salarié lui est attribué.


Employment
----------

Contrat de travail.

Lié à un·e salarié·e.


TeamType
--------

Type d'équipe :

- équipe de gestion
- commission,
- format·eur·rices,
- ronde Lutin·e,
- cercle louveteau·tte,
- unité Éclé·e,
- unité Aîné·e,
- unité Nomade,
- activités complémentaires,
- activité adaptée,
- équipes Support et Développement,
- equipe nationale,
- CAF,
- commission de contrôle,


Team
----

Équipe.

Lié à un type d'équipe et à une structure.


FunctionType
------------

Type de fonction :

- participant·e (enfant, stagiaire, participant SV),
- membre (bénévole),
- responsable (bénévole), au sens responsable de structure ou d'unité, pas au sens responsable d'animation,
- trésorier·e (bénévole),
- délégué·e à l'AG (bénévole),
- salarié·e


Function
--------

Fonction.

Lié à un type de fonction, à une personne et à une équipe.
