.. Jéito documentation master file, created by
   sphinx-quickstart on Mon Nov 29 18:12:38 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenue sur la documentation technique de Jéito
=================================================

Jéito constitue le noyau du système d'information EEDF.
C'est un annuaire des personnes et structures de l'association.

Les autres modules du SI peuvent récupérer ces informations au travers d'une API REST,
se synchroniser en temps réel en souscrivant aux messages envoyés via un broker AMQP,
et authentifier les utilisateurs de manière unique avec OpenID Connect.

.. toctree::
   :maxdepth: 1

   modeles
   api
   synchro
   sso
   changements
