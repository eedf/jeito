.. _sso:

Authentification unique
=======================

Jéito met en oeuvre le protocole OAuth 2.0 / OpenID Connect afin de permettre l'authentification unique
des personnes enregistrées dans sa base.

Il existe différentes méthodes de connexion. La méthode recommandée et utilisée par les modules ROADS, AGORA
et renonciations est l' « Authorization code flow », avec l'extension PKCE.


Configuration du client
-----------------------

La configuration d'un nouveau client nécessite de le déclarer au préalable.
Pour cela, il faut en faire la demande à l'administrateur de Jéito en précisant :

* Votre URL vers laquelle rediriger après la connexion,
* Votre URL vers laquelle rediriger après la déconnexion.

Il est préférable de faire une demande séparée pour chaque client.

En retour vous recevrez un identifiant et un secret qu'il faudra configurer dans votre client,
ainsi que l'URL d'autoconfiguration `</oauth/.well-known/openid-configuration/>`_ (à préfixer
avec le protocole https:// et le nom de domaine de Jéito).

Si votre client ne gère pas l'autoconfiguration, les différentes URL sont :

* authorization_endpoint: `</oauth/authorize/>`_
* token_endpoint: `</oauth/token/>`_
* userinfo_endpoint: `</oauth/userinfo/>`_
* end_session_endpoint: `</oauth/logout/>`_

Le PKCE est requis. Le token est signé par une clé asymétrique dont la clé publique est disponible
sur `</oauth/.well-known/jwks.json>`_.

Il est obligatoire de réclamer le scope ``openid``. Le jeton JWT récupéré contiendra alors un
attribut de type chaine 'sub' qui contient un entier numérique entre 1 et 2^63-1.
Ce nombre est l'identifiant unique de la personne connectée. Cet identifiant
est garanti de ne pas changer au cours du temps, même si la personne change de nom ou d'email.

Le scope optionnel ``profile`` permet de récupérer également les attributs suivants :

* name: prénom et nom d'usage
* given_name: prénom
* family_name: nom d'usage

Enfin, le scope ``email`` permet de récupérer l'adresse email dans le jeton.
Cette adresse peut être commune à plusieurs utilisateurs, et elle
peut changer au cours du temps pour un même utilisateur.
Elle ne doit donc pas être utilisée pour identifier l'utilisateur.


Lien avec l'API
---------------

L'attribut 'sub' du jeton JWT est l'UUID de la personne dans l':ref:`API<api>` v4 Jéito.
Vous pouvez donc récupérer les informations de la personne avec :
``GET /api/v4/persons/<uuid>/``.

De même si vous avez synchronisé tout ou partie de la base de Jéito, vous pouvez accéder
à toutes les infos attachées à la personne (adhésion, fonctions, etc) en suivant les clés
étrangères à partir de l'identifiant de la personne.
