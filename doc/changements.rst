.. changements:

Journal des modifications
=========================

Version 2.8.13 (09/03/25)
-------------------------

* Amélioration de la détection des doublons
* [Fix] Ordre d'envoi des modifications temps réel lors de la fusion de personne.
* [Fix] Transfert de la connexion et des rôles lors de la fusion de personne.

Version 2.8.12 (04/03/25)
-------------------------

* Remplacement de la colonne « Téléphone » par « Tél. fixe » et « Tél. mobile » dans les imports Excel.
* [Fix] Création d'une personne sans numéro de téléphone dans l'API v5.

Version 2.8.11 (01/03/25)
-------------------------

* Ajout de la liste des personnes fusionnées à l'API.

Version 2.8.10 (28/02/25)
-------------------------

* [Fix] Crash lors du changement de mot de passe si le nom d'utilisteur n'est pas défini.

Version 2.8.9 (28/02/25)
------------------------

* [Fix] Crash lors de la prise d'adhésion d'une nouvelle personne.

Version 2.8.8 (27/02/25)
------------------------

* Vérification que les deux responsables légaux sont bien différents.

Version 2.8.7 (26/02/25)
------------------------

* Anonymisation avec des emails et numéros de téléphone différents.

Version 2.8.6 (26/02/25)
------------------------

* Vérification de l'unicité des personnes par rapport à leur nom + email et nom + numéro de téléphone.
* Vérification que les numéros mobiles commencent par 06 ou 07, et inversement pour les numéros fixes.
* Possibilité de choisir l'utilisateur·ice concerné·e lors de la réinitialisation de mot de passe lorsque plusieurs
  d'entre elle·ux partage une même adresse email.
* Possibilité de définir un nom d'utilisateur et/ou de choisir d'utiliser son adresse email comme identifiant.
* Renommage du nom de famille en nom de naissance.

Version 2.8.5 (24/02/25)
------------------------

* [Fix] Crash lors de la fusion de certaines personnes.

Version 2.8.4 (04/01/25)
------------------------

* Mise à jour du reçu fiscal.
* Ajout de la région de rattachement des structures nationales.

Version 2.8.3 (30/12/24)
------------------------

* Configuration de la génération automatique des reçus fiscaux.
* [Fix] Synchronisation temps réel des fonctions lors de la fusion de personnes.

Version 2.8.2 (16/12/24)
------------------------

* [Fix] Validation de la fonction lors de la modification d'une invitation.

Version 2.8.1 (10/12/24)
------------------------

* [Fix] Crash en cas de renouvellement de contrat de travail

Version 2.8.0 (03/12/24)
------------------------

* Synchro temps réel de l'API v5.
* API v5 pour créer/modifier les personnes.
* API v5 pour créer/consulter les invitations.
* Séparation pays/ville de naissance dans l'API v5.
* [Fix] Installation du logiciel.
* [Fix] Déconnexion de l'authentification unique depuis plusieurs appareils.
* [Fix] L'étiquettes de statut dans la liste des adhésion tient désormais compte de la saison.

Version 2.7.8 (04/10/24)
------------------------

* [Fix] Fonction de rappel de la banque en cas d'appels successifs.

Version 2.7.7 (03/10/24)
------------------------

* [Fix] Fusion de personnes.
* [Fix] Fonction de rappel de la banque en cas d'échec précédent.
* [Fix] Correction API v5.

Version 2.7.6 (19/09/24)
------------------------

* [Fix] Problème de sélection du payeur lors du paiement en ligne.

Version 2.7.5 (26/08/24)
------------------------

* [Fix] Import avec des communes de naissance en doublon.

Version 2.7.4 (18/07/24)
------------------------

* [Fix] Import des contrats de travail.
* [Fix] Sérialisation API v5.

Version 2.7.3 (11/07/24)
------------------------

* [Fix] Performances de l'API v3.

Version 2.7.2 (06/07/24)
------------------------

* [Fix] Performances de l'API.

Version 2.7.1 (05/07/24)
------------------------

* [Fix] Fusion de personnes.
* [Fix] Crash en cas de double import en parallèle.
* [Fix] Suppression d'un contrat de travail en CDI.

Version 2.7.0 (14/06/24)
------------------------

* Prélèvement des adhésion à la structure locale si le paiement en ligne n'a pas été effectué.
  Ce montant est ensuite remboursé à la structure locale lors du paiement en ligne.
* Import des contrats de travail.
* Identité du ou de la signataire de l'adhésion.
* Possibilité de supprimer les équipe vides non obligatoires.
* Pas d'envoi de la carte d'adhésion tant que le paiement n'est pas effectué.
* Téléchargement d'une attestation d'adhésion.
* [Fix] Recherche par Nom/Prénom/Numéro des adhésions impayées.
* [Fix] Interdiction de créer des équipes en doublon (même structure et même nom).
* [Fix] Vérification que la date de naissance n'est pas supprimée lors d'une fusion de personnes.

Version 2.6.7 (16/05/24)
------------------------

* [Fix] Crash lors du paiement en ligne de plusieurs adhésions en même temps.
* [Fix] Crash lors de la création d'une fonction déjà existante.
* [Fix] Certains paiements apparaissent en double dans le journal d'une personne.

Version 2.6.6 (11/05/24)
------------------------

* [Fix] Vérification de la cohérence entre la fonction et l'équipe lors de la création des
  invitations.
* [Fix] Autorisation d'une date de fin un an après la fin de l'adhésion pour les fonctions dans
  les commissions et à la CAF et deux ans au CD et à la commission de contrôle.
* [Fix] La colonne âge des exports affiche <bound method Person.age of …>
* [Fix] Suppression de la fonction de délégué·e AG.
* [Fix] Multiples synchronisation temps réel lorsque Jéito crashe.

Version 2.6.5 (30/04/24)
------------------------

* [Fix] Validation des dates lors de la création/modification d'anciennes fonctions.
* [Fix] Déconnexion.

Version 2.6.4 (29/04/24)
------------------------

* [Fix] Fusion de personnes lorsqu'une invitation est en cours.

Version 2.6.3 (29/04/24)
------------------------

* [Fix] Permissions pour l'API v5.

Version 2.6.2 (26/04/24)
------------------------

* [Fix] Optimisation des performances pour les rôles attribués sur l'association entière.

Version 2.6.1 (26/04/24)
------------------------

* [Fix] Crash pour afficher les personnes non adhérentes.

Version 2.6.0 (26/04/24)
------------------------

* API v5 (suppression des points d'accès structure_type, team_type et function_type).
* Arrêt automatique des fonctions salariées lors de la modification de la date de fin
  d'un contrat de travail.
* Suppression automatique des fonctions bénévole/participant non électives lors de l'annulation
  d'une adhésion.
* [Fix] Statut « Refus » pour les invitations refusées.
* [Fix] Ajout de fonction à un·e mineur·e sans reponsable légal.
* [Fix] Création d'un second contrat de travail.
* [Fix] Lien vers tous les contrats de travail dans le détail d'une personne.

Version 2.5.0 (22/04/24)
------------------------

* Liste des délégations de rôles.
* Ajout du menu « Aide ».
* Ajouts des statistiques des adhésions pour le SF.
* Amélioration de la recherche des adhérent·es, adhésions, salarié·es, contrats de travail,
  invitations et paiements par nom/prénom/numéro/email de la personne.
* Journalisation des prélèvements
* [Fix] Pas de prélèvement en cas de changement de tarif d'une adhésion en ligne non payée.
* [Fix] Lien « Voir toutes les adhésions » sur la fiche personne.
* [Fix] Crash lors de la suppression d'un paiement pris en charge par la structure.
* [Fix] Ajout des communes des collectivités d'outre-mer.
* [Fix] Vérification que la date de début de fonction ne soit pas nulle.
* [Fix] Blocage du paiement en ligne si l'adhésion a déjà été prélevée.
* [Fix] Calcul du montant du prélèvement en cas de tranformation d'adhésion.
* [Fix] Calcul du montant d'un second paiement prise en charge par la structure.
* [Fix] Blocage de l'affichage du détail d'une adhésion annulée.
* [Fix] Affichage des paiements dans le journal de la personne si il est effectué par un tiers.

Version 2.4.4 (09/04/24)
------------------------

* [Fix] Calcul de la date de fin des adhésions prises le 1er septembre.

Version 2.4.3 (08/04/24)
------------------------

* [Fix] Possibilité de prendre une adhésion au tarif enfant sur les centres.

Version 2.4.2 (05/04/24)
------------------------

* [Fix] Prise d'adhésion au tarif enfant.

Version 2.4.1 (05/04/24)
------------------------

* [Fix] Prise d'adhésion au tarif enfant.

Version 2.4.0 (04/04/24)
------------------------

* Possibilité de choisir la personne pouvant se connecter parmi celles partageant le même email.
* Affichage des noms de naissance et surnoms (entre parenthèses).
* Amélioration de la recherche sur les noms et prénoms composés.
* Affichage d'informations supplémentaires dans le détail des invitations.
* Affichage des invitations sur la page d'une personne.
* Bouton pour renvoyer l'email d'invitation.
* Lien pour refuser dans l'email d'invitation.
* Email d'invitation formaté en HTML.
* [Fix] Modification de fonction d'un·e salarié·e.
* [Fix] Permissions pour la délégation des rôles.

Version 2.3.4 (21/03/24)
------------------------

* [Fix] Transformation d'une adhésion découverte en adhésion pleine.
* [Fix] Perte de la redirection quand le mot de passe n'est pas valide.

Version 2.3.3 (19/03/24)
------------------------

* Affiche le nom du site qui demande l'authentification.
* [Fix] Crash en affichant le détail d'un prélèvement.

Version 2.3.2 (17/03/24)
------------------------

* Modifications d'API pour Galilée et pour les renonciations.
* [Fix] Supression de paiement.
* [Fix] Ajout d'un·e responsable légal·e à un·e adhérent·e existant·e.

Version 2.3.1 (15/03/24)
------------------------

* [Fix] Crash adhésion en ligne d'une personne déjà connue mais non adhérente.

Version 2.3.0 (14/03/24)
------------------------

* Possibilité de modifier uniquement les dates et le nom personnalisé d'une fonction existante.
* Correction de la validation des dates de fonction.
* Empêchement de modifier ou supprimer une fonction soumises à nomination et gérée par AGORA.
* Affichage du type de fonction si le nom de la fonction est personnalisé.
* Ajout d'un filtre sur le statut à l'API des adhérent·es.
* Révision du blocage des invitations/adhésions des adhérent·es exclu·es/suspendu·es.
* Ajout d'une date de fin de suspension.
* Ajout du nom de famille (facultatif, si différent du nom d'usage) et du surnom (facultatif).
* Sélection du pays de naissance et de la commune (pour la France) dans des listes déroulantes.
* Lieu de naissance obligatoire pour toutes les nouvelles adhésions ou les renouvellements.
* Fusion d'adhérent·es avec plusieurs adhésions sur la même saison.
* Adhésions découverte en ligne.
* [Fix] Correction du compteur des jeunes.

Version 2.2.17 (13/03/24)
-------------------------

* [Fix] Crash création d'un paiement pris en charge par la structure.

Version 2.2.16 (13/03/24)
-------------------------

* [Fix] Non affichage des paiements effectués par la structure.

Version 2.2.15 (11/03/24)
-------------------------

* [Fix] Crash récupération des structures sur l'API v1.

Version 2.2.14 (11/03/24)
-------------------------

* [Fix] Crash pour supprimer une invitation.

Version 2.2.13 (11/03/24)
-------------------------

* [Fix] Crash pour lister les invitations.

Version 2.2.12 (11/03/24)
-------------------------

* Ajout des colonnes structure et région à l'export des fonctions.
* Ajout de l'alerte RGPD pour l'export des salarié·e, des paiements et des fonctions.
* [Fix] Amélioration des performances.
* [Fix] Suppression des doublons dans la liste des paiements.

Version 2.2.11 (29/02/24)
-------------------------

* [Fix] Correction d'un crash les 29 février (années bissextiles).

Version 2.2.10 (05/02/24)
-------------------------

* [Fix] Correction d'un crash en cas de délégation de rôle à un·e salarié·e.

Version 2.2.9 (01/02/24)
------------------------

* [Fix] Génération de doublons par la synchronisation temps réel lors de l'import des adhésions.

Version 2.2.8 (30/01/24)
------------------------

* [Fix] Correction de la comptabilité des adhésions.
* [Fix] Correction des listes d'émargement.
* [Fix] Correction du retour à la page précédente après ajout/suppression/modification.

Version 2.2.7 (29/01/24)
------------------------

* Comptabilité des adhésions.
* Retour à la page précédente après ajout/suppression/modification.
* Ajout d'explications sur le quotient familial.
* Deux types d'export des adhérent·es/adhésions: contacts et complet.
* Ajout de l'autorisation de sortie à l'export des adhérent·es/adhésions.
* Confirmation RGPD pour l'export des adhérent·es/adhésions.
* Listes d'émargement par structure.

Version 2.2.6 (25/01/24)
------------------------

* Ajout des ID dans l'API v4.

Version 2.2.5 (11/01/24)
------------------------

* Annulation du reçu fiscal lors de l'édition d'un paiement.

Version 2.2.4 (21/12/23)
------------------------

* Possibilité de prendre une adhésion au tarif « Responsable » lorsqu'aucune fonction n'est attribuée.

Version 2.2.3 (20/12/23)
------------------------

* Lors de la suppression d'un fonction, termine à partir d'aujourd'hui et non du lendemain.
* [Fix] Mise à jour du statut « Salarié·e » lors de la prolongation d'un contrat de travail.

Version 2.2.2 (19/12/23)
------------------------

* Amélioration des performances de l'API v4.
* Ré-ouverture d'une structure mise en sommeil.
* Retour en arrière en cas d'annulation de la déconnexion de l'authentification unique.
* [Fix] Masque le droit à l'image pour les responsables légaux.
* [Fix] Vérification que la date de fin d'une fonction est postérieure à la date de début.
* [Fix] Crash dans les statistiques pour certains paramètres de filtre particuliers.

Version 2.2.1 (16/12/23)
------------------------

* Supprime l'invitation associée lors de l'annulation d'une adhésion.
* Unicité des invitations de la même personne sur la même structure.

Version 2.2.0 (15/12/23)
------------------------

* Pas d'attribution des fonctions électives (possible uniquement via AGORA).
* Pas de rattachement/détachement des structures (possible uniquement via AGORA).
* API v4 (UUIDs, modification des fonctions et structures pour AGORA).
* Pas de résultat dans la liste des personnes si aucun critère de recherche n'est précisé.
* Masquage des données au delà d'un certain délai après la dernière adhésion (RGDP).
* [Fix] Ajout de membres aux équipes personnalisées.
* [Fix] Suppression des équipes personnalisées.

Version 2.1.19 (28/11/23)
-------------------------

* [Fix] Configuration des permissions pour les reçus fiscaux (confusion entre adhésions et renonciations).

Version 2.1.18 (28/11/23)
-------------------------

* [Fix] Exclusion des fonctions non acceptées dans l'API.
* [Fix] Publication des permissions de configuration des rôles via le broker.
* [Fix] Distinction du module pour les permissions de même nom (par ex. reçu fiscal à la fois pour les adhésions et les renonciations).

Version 2.1.17 (27/11/23)
-------------------------

Version 2.1.16 (26/11/23)
-------------------------

* Suppression du scope OIDC seafile (inutile dans la version 11 de Seafile).

Version 2.1.15 (24/11/23)
-------------------------

* Construction de l'image Docker dans la CI.
* Documentation de l'authentification unique.
* Modification du claim OIDC sub (person_id au lieu de user_id).
* [Fix] Correction des date de fin décalées d'un jour dans l'API.

Version 2.1.14 (15/11/23)
-------------------------

* Possibilité de configurer les permissions pour des modules externes (ex. renonciations).
* [Fix] Ajout de l'attribut code_challenge_methods_supported dans l'auto-découverte OIDC (nécessaire pour AGORA).

Version 2.1.13 (07/11/23)
-------------------------

* Ajout d'un exemple de fichier d'import des adhésions.
* [Fix] Suppression des « {} » dans les noms de fonctions.
* En-tête du fichier d'import insensible aux accents.
* Amélioration de la doc de l'API.
* [Fix] Ignore les fonctions salariées lors de l'adhésion.
* [Fix] Crash si la banque ne valide pas l'adresse email.
* [Fix] Crash si lors de la prise d'adhésion si les données sont modifiées en parallèle dans une autre fenêtre.

Version 2.1.12 (20/10/23)
-------------------------

Version 2.1.11 (17/10/23)
-------------------------

* [Fix] Doublons apparaissants dans les listes.
* [Fix] Auto-complétion des adhérents filtrés par structure.
* [Fix] Crash lors de la prise d'adhésion en cas de double validation.

Version 2.1.10 (13/10/23)
-------------------------

* [Fix] Perte d'informations lors de la fusion de deux personnes.
* [Fix] Crash lors de la fusion.
* Ajout de l'attribut « management » à l'API des équipes.

Version 2.1.9 (12/10/23)
-------------------------

* Vérification de l'âge à la date de début de l'adhésion uniquement et non à la date de fin d'adhésion (permet l'adhésion d'un·e aîné·e qui aura 19 ans en fin de saison).
* Affichage des contacts pour le support sur la page d'accueil et dans les mails de notification.

Version 2.1.8 (10/10/23)
-------------------------

* [Fix] Anonymisation des invitations sur les sites de test/démo.

Version 2.1.7 (05/10/23)
-------------------------

* Filtrage par année civile dans l'API.
* [Fix] Salariés manquants dans l'API des personnes lorsqu'on filtre par saison.

Version 2.1.6 (04/10/23)
-------------------------

* [Fix] Possibilité de choisir une date de paiement antérieure à la date d'adhésion (nécessaire pour les adhésion découverte).

Version 2.1.5 (04/10/23)
-------------------------

* Ajout d'un message si aucun tarif n'est disponible.
* Filtrage des incohérence âge/fonction par saison.
* Page de détail des fonctions.

Version 2.1.4 (03/10/23)
-------------------------

* Ajout d'une vérification d'âge selon la fonction lors de l'adhésion en ligne.
* Suppression de la formule « Je soussigné ».

Version 2.1.3 (02/10/23)
-------------------------

* Ajout d'un filtre par catégorie de fonction dans les statistiques.
* [Fix] Filtrage par âge min/max dans les statistiques.
* [Fix] Lenteur lorsque le broker est down.

Version 2.1.2 (29/09/23)
-------------------------

* [Fix] Le broker n'envoie pas de heartbeat.

Version 2.1.1 (28/09/23)
-------------------------

* [Fix] Fermeture de connexion au broker.

Version 2.1.0 (28/09/23)
-------------------------

* Publication des modifications en temps réel via un broker AMQP.

Version 2.0.35 (27/09/23)
-------------------------

* Désactivation de l'export des personnes.
* [Fix] Crash des compteurs statistiques si la saison n'est pas précisés.

Version 2.0.34 (27/09/23)
-------------------------

* Export des personnes, adhérents, salariés, contrats de travail.
* [Fix] Export des adhésions lorsque la date de naissance est inconnue.

Version 2.0.33 (15/09/23)
-------------------------

* [Fix] Enregistrement d'une fonction sans date de fin.

Version 2.0.32 (14/09/23)
-------------------------

* [Fix] Filtrage des fonctions et des paiements.
* Validation de la date de paiement.
* Possibilité de modifier un paiement qui a déjà fait l'objet d'un reçu fiscal.
* Possibilité de déléguer un rôle depuis une personne non adhérente.

Version 2.0.31 (14/09/23)
-------------------------

* [Fix] Déléguation des rôles.
* Statistiques par département.
* Amélioration des export des paiements et des adhésions.

Version 2.0.30 (13/09/23)
-------------------------

* Lien vers les anciennes fonctions et adhésions sur la fiche d'une personne.
* Liste des paiements.

Version 2.0.29 (12/09/23)
-------------------------

* Bouton modifier sur le détail d'un contrat de travail.
* Blocage des rôles attribués manuellement si pas d'adhésion ou de contrat de travail en cours.
* Possibilité de renouveler sa propre adhésion en ligne si responsable des adhésions.

Version 2.0.28 (11/09/23)
-------------------------

* Ajout du filtre par type de structures dans la liste des fonctions.
* Journalisation des modifications.

Version 2.0.27 (07/09/23)
-------------------------

* Export des fonctions.
* [Fix] Formattage de l'export des adhésions.
* Export par un bouton sur la liste des adhésions.
* Ajout d'un filter « Plus de 16 ans ».

Version 2.0.26 (06/09/23)
-------------------------

* [Fix] Gestion de fonctions des salarié·es.

Version 2.0.25 (04/09/23)
-------------------------

* [Fix] Autocomplétion avec juste une espace.
* [Fix] Crash après création d'une fonction.
* [Fix] Crash lors de la modification de la structure racine.
* [Fix] Crash de la base de données par manque de RAM.

Version 2.0.24 (01/09/23)
-------------------------

* Tri des résultats d'auto-complétion des personnes par similarité.

Version 2.0.23 (01/09/23)
-------------------------

* Permissions liées aux structures selon les fonctions et non seulement l'adhésion.

Version 2.0.22 (01/09/23)
-------------------------

* [Fix] Modification des paiements communs pour deux adhésions.
* [Fix] Affichage des fonctions sur les invitations.

Version 2.0.21 (01/09/23)
-------------------------

* [Fix] Doublons lors de l'affichage des choix du tarif.

Version 2.0.20 (01/09/23)
-------------------------

* [Fix] Création des invitations.

Version 2.0.19 (01/09/23)
-------------------------

* [Fix] Calcul des tarifs disponibles.

Version 2.0.18 (31/08/23)
-------------------------

* Modification de la page RGPD.
* Suppression du terme « Parent » au profit de « Responsable légal·e ».

Version 2.0.17 (31/08/23)
-------------------------

* Adaptations au nouveau bulletin d'adhésion pour la saison 2023/2024.
* [Fix] Crash des imports au vieux format Excel .xls.

Version 2.0.16 (19/07/23)
-------------------------

* [Fix] Invitation d'une personne non adhérente.

Version 2.0.15 (18/07/23)
-------------------------

* [Fix] Auto-complétion des structures filtrées par type de parent.
* [Fix] Géolocalisation des structures avec un espace en suffixe.
* [Fix] Attribut region dans l'API v1.
* [Fix] Déduplication d'une adhésion annulée.
* [Fix] Annulation d'une adhésion
* [Fix] Ergonomie des auto-complétions.

Version 2.0.14 (14/07/23)
-------------------------

* [Fix] Permission pour télécharger les virements.

Version 2.0.13 (13/07/23)
-------------------------

* Vérification de l'âge par rapport à la date de début d'adhésion et non la date de début de saison.

Version 2.0.12 (12/07/23)
-------------------------

Version 2.0.11 (08/07/23)
-------------------------

* Affichage de tous les contrats de travail sur la page d'une personne.
* Filtrage de toutes les saisons sur la liste des contrats de travail.

Version 2.0.10 (07/07/23)
-------------------------

* [Fix] Fusion.
* [Fix] Suppression des contrats de travail.

Version 2.0.9 (07/07/23)
-------------------------

* [Fix] Permission de télécharger les cartes d'adhésion.

Version 2.0.8 (06/07/23)
-------------------------

* Suppression de la recherche par email lors du renouvellement d'adhésion.
* [Fix] Email du responsable légal pour les personnes sans date de naissance.
* Renommage des structures « Actives » en « Autonomes ».

Version 2.0.7 (04/07/23)
-------------------------

* [Fix] Création d'invitation de renouvellement.
* [Fix] Déconnexion OIDC.

Version 2.0.6 (03/07/23)
-------------------------

* [Fix] Nom de fonction par défaut lors de la création d'une nouvelle fonction.
* [Fix] Adhesion en ligne avec un nouveau payeur.

Version 2.0.5 (02/07/23)
-------------------------

* [Fix] Crash lors du téléchargement du reçu fiscal.
* [Fix] Crash du géocodage avec moins de 3 caractères.
* [Fix] Prise d'adhésion avec une personne déjà connue mais pas encore adhérente.
* [Fix] Filtrage des plus de 16 ans.

Version 2.0.4 (02/07/23)
-------------------------

* [Fix] Crash lors de la prise d'adhésion en inversant email parent/enfant.

Version 2.0.3 (02/07/23)
-------------------------

* Invitation en tant qu'ami·e (sans fonction).
* [Fix] Téléchargement du reçu fiscal par les responsables des adhésions.
* [Fix] Création des nouveaux utilisateurs.
* [Fix] Doublons d'email pour les utilisateurs.
* [Fix] API v1.

Version 2.0.2 (01/07/23)
-------------------------

* [Fix] Affichage des fonctions à prolonger lors de la prise d'adhésion.
* [Fix] Prise d'adhesion en ligne.
* Affichage des fonctions sur la carte d'adhérent·e.
* Masquage des tarifs découverte pour l'adhésion en ligne.
* [Fix] Téléchargement du reçu fiscal.
* Renommage de « Sous tutelle » en « Rattachée ».

Version 2.0.1 (30/06/23)
-------------------------

* Bouton pour ajouter des invitations.

Version 2.0.0 (30/06/23)
-------------------------

* Nouveau modèle de données selon l'étude « Noyau ».
