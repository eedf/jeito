.. _api:

API
===

Jeito propose 4 API REST/JSON.

Ces API sont accessible sur `</api/v1/>`_,  `</api/v3/>`_, `</api/v4/>`_ et `</api/v5/>`_.
Elles sont auto-documentée par `Swagger <https://swagger.io>`_.

L'accès aux données nécessite d'obtenir un token auprès des administrateurs de Jéito.
Ce token doit être passé dans les requêtes via une en-tête HTTP nommée ``Authorization`` et dont la valeur est préfixée par ``Token␣``.


API v5
------

**Dernière version**

Par rapport à l'API v4, cette version supprime les points d'accès structure_type, team_type et function_type.
Les ancien uuid pointant vers ces objets sont transformées en entiers et documentés par Swagger.

API v4
------

Par rapport à l'API v3, cette version utilise des UUIDs au lieu des IDs.
Elle permet la modification des fonctions électives ainsi que le rattachement des structures.


API v3
------

**Dépréciée**

Utilisée par `ROADS <https://roads.eedf.fr>`_ et `Galilée <https://galilee.eedf.fr>`_.


API v2
------

**Supprimée**


API v1
------

**Dépréciée**

Utilisée par `Prépa AG <https://prepa-ag.eedf.fr>`_ et `Résolutions <https://resolutions.eedf.fr>`_.
