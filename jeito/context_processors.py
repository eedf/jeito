# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from django.conf import settings
from . import __version__


def conf(request):
    return {
        'conf': {
            'test_site': settings.TEST_SITE,
            'test_site_banner': settings.TEST_SITE_BANNER,
            'version': __version__,
            'host': settings.HOST,
        },
    }
