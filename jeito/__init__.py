from pathlib import Path

try:
    __version__ = Path(__file__).parent.parent.joinpath('VERSION').read_text()
except FileNotFoundError:
    __version__ = "unknown"
