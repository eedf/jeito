# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from .base import *  # NOQA

DEBUG = True
SECRET_KEY = 'dev'
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
# EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
# EMAIL_FILE_PATH = '/code/emails'
INSTALLED_APPS.append('django_extensions')  # NOQA
DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': lambda request: DEBUG,
}
BROKER_ENABLED = False
SUPPORT_EMAIL = 'support@jeito.com'
SUPPORT_PHONE = '+33199999999'
