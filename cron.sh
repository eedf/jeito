#!/bin/bash

set -ex

echo "======== BEGIN `date` ========"

# Clear OIDC tokens
./manage.py cleartokens

# Clear sessions
./manage.py clearsessions

# Reset suspensions
./manage.py reset_suspensions

# Send reminder for not paid adhesions
./manage.py remind

# Send receipts for previous year
./manage.py receipts

# Expire persons afre RGPD delay
./manage.py rgpd

# Checks
./manage.py check_members

echo "======== END `date` ========"
