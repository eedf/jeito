.. _assistance:

Assistance
==========

En cas de difficulté ou si tu constates un bug, tu peux contacter Pascale Sousa et Céline Thominet au siège national.

- Par email : `adhesions@eedf.fr <mailto:adhesions@eedf.fr>`_
- Par téléphone : 01 48 15 17 66

N'oublie pas de dire qui tu es (nom, prénom, structure, fonction et, si possible, numéro d'adhérent).
Essaie de donner tous les détails nécessaires pour qu'on puisse bien comprendre ton problème pour t'apporter une solution au plus vite.
