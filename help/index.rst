.. Jéito documentation master file, created by
   sphinx-quickstart on Mon Nov 29 18:12:38 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenue sur le manuel d'utilisation de Jéito
==============================================

.. toctree::
   :maxdepth: 1

   connexion
   adhesions
   invitations
   assistance
