.. _invitations:

Invitations à adhérer en ligne
==============================

En tant que responsable des adhésions (ou responsable de SLA ou trésorier), il est désormais possible d'inviter
des personnes à adhérer et à payer leur cotisation en ligne.

La saisie manuelle des adhésions papier reste possible.
Personne ne peut adhérer à votre structure sans y avoir été invité au préalable.

Gestions des invitations
------------------------

La `liste des invitations </invitation/>`_ est accessible par le menu « Adhesions -> Invitations »,
ou par le bouton « Invitations à adhérer en ligne » sur la page d'accueil.

Un clic sur une adresse dans la colonne « email » permet de modifier l'invitation pour corriger l'adresse email ou la fonction.
Si l'adhésion n'a pas encore été prise il est également possible de supprimer une invitation en cliquant
sur l'adresse email dans la liste puis en cliquant sur le bouton « Supprimer ».

La colonne statut indique :

* Invité·e : l'invitation a été envoyée mais l'adhésion n'a pas encore et prise en ligne,
* Non payé : l'adhésion a été prise en ligne mais le paiement n'a pas fait ou a échoué,
* Adhérent·e : l'adhésion a été prise et payée.

Si le paiement en ligne s'avère impossible, il est possible de récupérer le payement au niveau de la SLA et de le saisir
sur la fiche de l'adhérent.

Création des invitations
------------------------

Pour accéder à la `création des invitations </invitation/create/>`_, cliquez sur le bouton
« + Nouvelle(s) invitation(s) » depuis la liste des invitations.

Il existe 3 cas de figure :

* Renouvellement : pour les personne ayant adhéré sur votre structure la saison précédente
* Ancien adhérent : pour une personne ayant déjà adhéré aux EEDF, mais pas la saison précédente ou pas sur votre structure.
* Nouveaux adhérents : pour une personne n'ayant jamais adhéré aux EEDF.

Dans tous les cas, il vous faudra renseigner une adresse email et une unité/fonction.
Dans le cas des renouvellement, un tableau est pré-rempli avec ces informations.
Il vous suffit de cocher les personnes concernées, de contrôler les informations et de les modifier si nécessaire
(mauvaise adresse email, changement de branche ou de fonction).

Paiement
--------

* Comme pour une saisie d'adhésion classique, la première partie demande à la personne invitée de saisir ses informations d'identité, et la seconde partie de payer en ligne.
* Il sera possible aux personnes invitées de saisir en ligne uniquement la première partie. Puis de payer par un autre moyen plus tard lors d'une activité par exemple.
* Le paiement en ligne par carte bancaire de plusieurs adhésions est possible en un seul paiement, par exemple pour une fratrie. Il faut ouvrir les différentes invitations successivement dans la même session de navigateur. Une fois l'ensemble des adhésions saisies, passer au paiement, la somme des différentes cotisations sera proposée au paiement.
* Le paiement de la cotisation sera reçu directement par le siège national et évitera ainsi les opérations de prélèvements de ces cotisations par le siège. (= moins de comptabilités) Pour les adhésions saisies par le groupe, aucun changement sur le système de prélèvement.
* Il est possible de rajouter une somme de don au paiement de la cotisation, don qui sera reversé par le siège national à la structure où adhère la personne.
* Les options de montant demandés seront dépendant de la fonction attendue de la personne invité (voir diapo suivante).

Fonctions
---------

* La fonction de la personne invitée (lutin·e/…/aîné·e/responsable) est définie lors de la création des invitations, la personne invitée n'a pas la main sur le choix de sa fonction. Une alerte sera émise lors de la création de l'invitation si l'âge de la personne ne correspond pas à fonction attendue.
* Pour les renouvellements il sera possible de générer et envoyer plusieurs invitations distinctes en même temps en sélectionnant les adhérents de l'année précédente.
* Pour l'invitation de personnes n'ayant jamais adhérées, vous pouvez inviter plusieurs personnes en une seule fois, sous réserve qu'elles aient la même fonction. (= une invitation pour les nouveau·elles lutin·es, puis une autre pour les nouveau·elles louveteau·ettes, et ainsi de suite.)
* Il sera possible d'inviter un·e ancien·ne adhérent·e appartenant précédemment à une autre structure EEDF, en le recherchant grâce à son nom ou prénom ou numéro d'adhérent·e.
