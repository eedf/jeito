.. _adhesions:

Adhésions
=========

Au préalable
------------

Si ce n'est pas déjà fait, commence d'abord par te :ref:`connecter<connexion>`.

La saisie des adhésions est réservée aux responsables régionaux, responsables de groupe, trésoriers et responsables des adhésions, ainsi qu'à certains salariés. Elle ne peut se faire que sur le périmètre de sa structure (y compris les SLA dans les régions).

Pour l'instant il n'est pas possible de demander sa nomination. En cas de problème de droit d'accès lié à une nomination, merci de contacter l':ref:`assistance<assistance>`.

Prise d'adhésion
----------------

Rends-toi sur la page de `prise d'adhésion </adhesion/pre_create/>`_, au choix

* en cliquant sur le bouton « Nouvelle adhésion » de la page d'accueil,
* en choisissant « Nouvelle adhésion » dans le menu « Adhésions »,

Recherche de doublon
~~~~~~~~~~~~~~~~~~~~

Il se peut que la personne ait déjà été adhérente par le passé. Ou bien que son adhésion ait déjà été saisie sans que tu le saches ou sans que tu t'en rappelles. Dans ce cas il est important de ne pas créer de doublon dans la base.

* Si tu sais que la personne a déjà été adhérente et que tu connais sont numéro d'adhérent·e, tu peux le saisir afin de retrouver directement sa fiche.
* Sinon, saisis ses nom, prénom et date de naissance (sous la forme mois/jour/année à 4 chiffres, par exemple 15/07/1978) afin d'effectuer une recherche. Celle-ci ne tient pas compte de la casse (majuscules) ni des accents. En revanche, il faut veiller à orthographier correctement le nom et le prénom.

Après avoir cliqué sur « Suivant », si une fiche est trouvée dans la base, il faudra confirmer qu'il s'agit bien de la bonne personne. Sinon il faudra confirmer qu'il ne s'agit pas d'un renouvellement.

Formulaire de saisie
~~~~~~~~~~~~~~~~~~~~

Il ne te reste plus qu'à remplir le formulaire et à le valider. Les champs suivis d'une astérisque sont obligatoires.

* Civilité : par rapport à Entréclés, le choix « Mademoiselle » a été supprimé.
* Nom d'usage : veille à l'orthographier correctement. Il est possible de mettre des accents. Le nom est automatiquement mis en majuscules quelle que soit la façon de le taper.
* Prénom : idem.
* Date de naissance : au format jj/mm/aaaa ou jj/mm/aa. Par exemple « 06/06/1944 ».
* Lieu de naissance : commune et département de naissance (ou pays si naissance à l'étranger). Par exemple : « Paris (75) ».
* Profession : optionnel.
* Pays : uniquement si à l'étranger.
* Email, Tél. : indispensable pour contacter les adhérents.
* Structure : pour les enfants, choisir la bonne unité (ronde, cercle, unité éclé, clan).
* Date d'adhésion : reporter la date de signature du bulletin.
* Tarif : la liste des tarifs disponibles dépend de la structure choisie. En cas de modification de structure, merci de vérifier le tarif.
* + Don : éventuel don versé en plus de la cotisation. Mets 0 si pas de don.
* Fonction : la liste des fonctions disponibles varie en fonction de la structure et du tarif choisis. En cas de modification de structure ou de tarif, merci de vérifier la fonction.
* Parents : pour les mineurs, il est obligatoire de saisir au moins un responsable légal. Le bouton « Copier depuis l'adhérent » permet de copier le nom et l'adresse pour éviter d'avoir à les resaisir.

Contrôle de la saisie
~~~~~~~~~~~~~~~~~~~~~

Après validation, un message signale que l'adhésion a bien été prise et donne le numéro d'adhérent. Si l'adresse a été saisie, l'adhérent le reçoit également directement par mail. Tu peux également contrôler la saisie dans la liste des adhérents ou avec l'export Excel.

Liste des adhérents
-------------------

Rends-toi sur la page qui liste les `adhérents </adhesion/>`_, au choix :

* en cliquant sur le bouton « Liste des adhérents » de la page d'accueil,
* en choisissant « Liste » dans le menu « Adhésions »,

Pagination
~~~~~~~~~~

Seuls les 25 premiers adhérents sont affichés. Pour voir les autres tu peux changer de page en cliquant sur les liens en bas du tableau. Si il y a beaucoup de pages, il est préférable de filtrer ou trier la liste.

Tri
~~~

En haut de la page, tu peux choisir l'ordre d'affichage :

* nom (puis prénom),
* prénom (puis nom),
* numéro d'adhérent,
* structure (puis nom/prénom).

Filtrage et recherche
~~~~~~~~~~~~~~~~~~~~~

Il est possible de filtrer la liste pour n'afficher qu'une structure ou qu'un branche. Il suffit de choisir la valeur correspondante dans les listes déroulantes. Il est également possible de chercher un nom, un prénom ou un numéro d'adhérent dans le champ « Nom/prénom/numéro». Tu peux chercher à la fois sur le nom et le prénom. La recherche ne tient pas compte des accents et des majuscules et elle effectue les correspondances sur des sous parties du nom. Par exemple, chercher « Noémie dup » permet de trouver « NOEMIE DUPONT ».

Détail d'une adhésion
~~~~~~~~~~~~~~~~~~~~~

A partir de la liste, il suffit de cliquer sur un nom pour afficher le détail.

Modification d'une adhésion
~~~~~~~~~~~~~~~~~~~~~~~~~~~

A partir de la fiche détail d'une adhésion, il suffit de cliquer sur le bouton « Modifier » en haut à droite. Il est également possible de passer par la procédure de nouvelle adhésion.

Si ça ne fonctionne pas
-----------------------

En cas de difficulté, ou si tu constate un bug, tu peux contacter l':ref:`assistance<assistance>`.