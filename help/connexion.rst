.. _connexion:

Connexion
=========

Initialiser le mot de passe
---------------------------

Avant de te connecter pour la première fois, tu dois initialiser ton mot de passe.
Pour cela, munis-toi de ton numéro d'adhérent·e et rentre le sur la page de réinitialisation.
Il te faudra ensuite suivre les instructions reçues par mail.

Je ne reçois pas de mail
------------------------

Si tu ne reçois pas de mail, attends encore quelques minutes et vérifie qu'il n'est pas tombé dans les spams.
Si cela ne fonctionne toujours pas, vérifie auprès de ton ou de ta responsable des adhésions que ton adresse mail est correctement enregistrée dans la base.

Je ne connais pas mon numéro d'adhérent·e
-----------------------------------------

Si tu ne connais pas ton numéro d'adhérent·e, demande le à ton ou ta responsable des adhésions.

Personne n'a accès à Jéito dans ma structure
--------------------------------------------

Il faut contacter l':ref:`assistance<assistance>`.

Je n'y arrive toujours pas
--------------------------

En cas de difficulté, tu peux contacter l':ref:`assistance<assistance>`.
