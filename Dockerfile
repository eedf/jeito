FROM python:3.12 AS base
WORKDIR /code
ENV DJANGO_SETTINGS_MODULE=jeito.settings.prod
ENV PYTHONUNBUFFERED=1
ENV VIRTUAL_ENV=/env
RUN python -m venv ${VIRTUAL_ENV}
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN pip install --no-cache-dir --upgrade pip

FROM base AS build
RUN pip install --no-cache-dir sphinx
COPY VERSION /code/
COPY doc /code/doc
COPY help /code/help
RUN make -C doc html && make -C help html

FROM base AS jeito
COPY requirements.txt /code/
RUN pip install --no-cache-dir -r requirements.txt
COPY VERSION pyproject.toml manage.py cron.sh .flake8 .coveragerc /code/
COPY jeito /code/jeito
COPY members /code/members
RUN ./manage.py collectstatic --noinput
COPY --from=build /code/doc/_build/html /code/html/doc
COPY --from=build /code/help/_build/html /code/html/aide
