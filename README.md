# Jéito

Jéito est un logiciel de gestion des membres pour les organisations scoutes.

Il est développé initialement par et pour l'association des Éclaireuses et Éclaireurs de France.
Mais il est générique et peut être configuré pour convenir à d'autres associations.

N'hésite pas à contribuer en créant une issue ou en proposant une merge request sur https://gitlab.com/eedf/jeito/.


# Dévelopment

## Installation

Sur une distribution GNU/Linux Ubuntu, dans un shell, exécute les commandes suivantes :

```bash
$ sudo apt install git docker.io docker-compose
$ git clone https://gitlab.com/eedf/www/jeito.git
$ cd jeito
$ sudo docker-compose -f docker-compose-dev.yml build
$ sudo docker-compose -f docker-compose-dev.yml run --rm django ./manage.py migrate
$ sudo docker-compose -f docker-compose-dev.yml run --rm django ./manage.py createsuperuser
```

Choisis le numéro d'adhérent du premier utilisateur, par exemple « 1 ».
Il aura tous les droits d'administration.

## Démarrage

Place toi dans le répertoire `jeito` puis exécute :

```bash
$ sudo docker-compose -f docker-compose-dev.yml up
```

Ouvre ton navigateur à l'adresse http://localhost:8000

## Configuration

Clique sur le menu en haut à droite, puis sur « Admin ».
Crée les structures, tarifs et fonctions.

Tu peux aussi charger une configuration par défaut avec :

```bash
sudo docker-compose -f docker-compose-dev.yml exec django ./manage.py loaddata config2022
```

## Mise à jour

Place toi dans le répertoire `jeito` puis exécute :

```bash
$ git pull
$ sudo docker-compose -f docker-compose-dev.yml build
$ sudo docker-compose -f docker-compose-dev.yml run --rm django ./manage.py migrate
```

## Changement de saison

Pour la saison commençant au 1er septembre de l'année N :
* Créer la saison N+1 avec le PDF de la carte et du bulletin dans l'admin (/admin/members/season/)
* Lancer `./manage.py copy_season <N+1>` et ajuster les tarifs
