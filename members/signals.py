from django.contrib.contenttypes.models import ContentType
from django.db.models.signals import post_save, post_delete, m2m_changed, post_migrate
from django.dispatch import receiver
from rest_framework.renderers import JSONRenderer
from .models import RoleConfigPermission, Permission
from . import serializers_v4, serializers_v5
from .publisher import publish


SERIALIZER_MODULES = {
    4: serializers_v4,
    5: serializers_v5
}


def force_publish_save(instance, **kwargs):
    for version in (4, 5):
        model_name = instance.__class__.__name__
        try:
            Serializer = getattr(SERIALIZER_MODULES[version], f"{model_name}Serializer{version}")
        except AttributeError:  # There is no serializer for this model
            return
        serializer = Serializer(instance)
        json = JSONRenderer().render({
            'operation': 'CREATE_UPDATE',
            'uuid': instance.uuid,
            'attributes': serializer.data,
        })
        publish(f'synchro_v{version}', f'jeito.{model_name.lower()}', json)


@receiver(post_save)
def publish_save(instance, update_fields, **kwargs):
    if getattr(instance, 'no_sync', False):
        return
    if update_fields in [('has_protected', ), ('has_payment', ), ('last_invitation', )]:
        return
    force_publish_save(instance, **kwargs)


def force_publish_delete(instance, **kwargs):
    for version in (4, 5):
        model_name = instance.__class__.__name__
        try:
            getattr(SERIALIZER_MODULES[version], f"{model_name}Serializer{version}")
        except AttributeError:  # There is no serializer for this model
            return
        json = JSONRenderer().render({
            'operation': 'DELETE',
            'uuid': instance.uuid,
        })
        publish(f'synchro_v{version}', f'jeito.{model_name.lower()}', json)


@receiver(post_delete)
def publish_delete(instance, **kwargs):
    if getattr(instance, 'no_sync', False):
        return
    force_publish_delete(instance, **kwargs)


@receiver(m2m_changed, sender=RoleConfigPermission)
def publish_roleconfigpermission_save(action, instance, pk_set, **kwargs):
    if action != 'post_add':
        return
    for pk in pk_set:
        obj = RoleConfigPermission.objects.get(roleconfig=instance, permission=pk)
        for version in (4, 5):
            Serializer = getattr(SERIALIZER_MODULES[version], f'RoleConfigPermissionSerializer{version}')
            serializer = Serializer(obj)
            json = JSONRenderer().render({
                'operation': 'CREATE_UPDATE',
                'uuid': obj.uuid,
                'attributes': serializer.data,
            })
            publish(f'synchro_v{version}', 'jeito.roleconfigpermission', json)


@receiver(post_migrate)
def rename_permissions(sender, verbosity, **kwargs):
    for model in sender.get_models(sender):
        content_type = ContentType.objects.get_for_model(model)
        for codename, name in tuple(model._meta.permissions) + tuple(getattr(model, 'renamed_permissions', ())):
            try:
                permission = Permission.objects.get(content_type=content_type, codename=codename)
            except Permission.DoesNotExist:
                continue
            if permission.name != name:
                if verbosity >= 2:
                    print(f"rename « {permission.name} » to « {name} »")
                permission.name = name
                permission.save()
