import sentry_sdk
import pika
import ssl
from django.conf import settings


def publish(exchange, key, message):
    if not settings.BROKER_ENABLED:
        return

    if hasattr(sentry_sdk.integrations, 'logging'):
        sentry_sdk.integrations.logging.ignore_logger('pika.adapters.blocking_connection')
        sentry_sdk.integrations.logging.ignore_logger('pika.adapters.utils.connection_workflow')
        sentry_sdk.integrations.logging.ignore_logger('pika.adapters.utils.io_services_utils')

    try:
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=settings.BROKER_HOST,
                port=settings.BROKER_PORT,
                ssl_options=pika.SSLOptions(ssl.create_default_context()) if settings.BROKER_TLS else None,
                virtual_host=settings.BROKER_VIRTUAL_HOST,
                credentials=pika.credentials.PlainCredentials(
                    username=settings.BROKER_USERNAME,
                    password=settings.BROKER_PASSWORD,
                ),
                connection_attempts=1,
                socket_timeout=1,
                stack_timeout=1,
            ),
        )
    except Exception as err:
        sentry_sdk.capture_exception(err)
        return

    try:
        channel = connection.channel()
    except Exception as err:
        sentry_sdk.capture_exception(err)
        connection.close()
        return

    try:
        channel.exchange_declare(exchange=exchange, exchange_type='topic')
        channel.basic_publish(exchange=exchange, routing_key=key, body=message)
    except Exception as err:
        sentry_sdk.capture_exception(err)

    channel.close()
    connection.close()
