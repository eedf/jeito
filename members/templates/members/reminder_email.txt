Bonjour {{ person.first_name }},

Tu as répondu à l'invitation à adhérer en ligne aux EEDF le {{ adhesion.begin|date:"d/m/Y" }} sous le numéro {{ adhesion.adherent.id_str }} auprès de la structure locale {{ adhesion.structure }}.
Cependant, à ce jour, et sauf erreur de notre part, nous n'avons pas enregistré {% if adhesion.successful_allocations %}la totalité du{% else %}le{% endif %} paiement de la cotisation{% if adhesion.balance_donation %} et du don{% endif %} pour un montant de {{ adhesion.amount }} €.
{% if adhesion.successful_allocations %}{% for allocation in adhesion.successful_allocations %}
Un paiement de {{ allocation.amount }} € a été enregistré le {{ allocation.payment.date|date:"d/m/Y" }}.
{% endfor %}Le solde restant à payer est de {{ adhesion.balance }} €.
{% endif %}
Si tu as déjà effectué le paiement en ligne sur le site https://jeito.eedf.fr, via le mail d'invitation que tu as reçu, merci de vérifier que tu as bien reçu un ticket de paiement par email et que la somme a bien été débitée sur ton compte. Si c'est le cas (c'est très rare mais ça peut arriver), merci de le signaler à {{ SUPPORT_EMAIL }}.

Si tu n'as pas encore payé, tu peux le faire en ligne grâce au lien suivant : {{ url }}

Si tu ne veux ou ne peut pas payer en ligne, tu peux te rapprocher de ta structure locale pour payer par virement, par chèque ou en espèces.

Si tu as déjà effectué le paiement par virement, chèque, espèces ou CB sur le site Hello Asso, celui-ci n'a pas encore été enregistré par ta structure locale. Signale le afin qu'ils fassent le nécessaire. A défaut, le reçu fiscal ne pourra être édité.

En cas de question ou de difficulté, tu peux contacter ta structure locale ou le support des adhésions par email à {{ SUPPORT_EMAIL }} ou par téléphone au {{ SUPPORT_PHONE }}. Merci de ne pas répondre directement à ce mail automatique.

Amicalement,

--
Les Éclaireuses Éclaireurs de France
