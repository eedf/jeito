# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

import re
from django.conf import settings
from django.contrib.auth.backends import ModelBackend
from django.db.models import Q

from members.models import User


class JeitoBackend(ModelBackend):
    def check_password(self, user, password):
        return user.check_password(password)

    def authenticate(self, request=None, username=None, password=None):
        if username is None or password is None:
            return
        match = re.fullmatch(r'([A-Z]{3})-([0-9]{6})', username)
        if username.isdigit():
            number = int(username)
        elif match:
            number = int(match.group(2))
        else:
            number = None
        if number:
            q = Q(person__adherent__id=number) | Q(person__employee__id=number)
        else:
            q = Q(person__email=username, person__email_as_login=True)
        try:
            user = User.objects.get(q)
        except User.DoesNotExist:
            # Run the default password hasher once to reduce the timing
            # difference between an existing and a nonexistent user (#20760).
            User().set_password(password)
        else:
            if self.check_password(user, password) and self.user_can_authenticate(user):
                return user


class TestBackend(JeitoBackend):
    def check_password(self, user, password):
        return settings.TEST_PASSWORD and password == settings.TEST_PASSWORD
