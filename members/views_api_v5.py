# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD & Max PIMM
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from datetime import date as Date
from drf_spectacular.utils import extend_schema_view, extend_schema, OpenApiParameter, OpenApiResponse
from psycopg2.extras import DateRange
from uuid import UUID
from django.core.exceptions import BadRequest, PermissionDenied
from django.db.models import Q
from django.shortcuts import get_object_or_404
from rest_framework import serializers
from rest_framework.mixins import CreateModelMixin, ListModelMixin, RetrieveModelMixin, UpdateModelMixin
from rest_framework.permissions import BasePermission, SAFE_METHODS
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet, GenericViewSet
from .models import (Adhesion, Adherent, Function, Person, Structure, Employee,
                     Team, Employment, FunctionStatus, RoleConfig, Role,
                     Attribution, RoleConfigPermission, Invitation, Country, Commune)
from .serializers_v5 import AdhesionSerializer5, StructureSerializer5, PersonSerializer5, \
                            TeamSerializer5, FunctionSerializer5, \
                            EmploymentSerializer5, AdherentSerializer5, EmployeeSerializer5, RoleConfigSerializer5, \
                            AttributionSerializer5, RoleSerializer5, PermissionSerializer5, Permission, \
                            RoleConfigPermissionSerializer5, StructureResponseSerializer5, StructureWriteSerializer5, \
                            FunctionResponseSerializer5, InvitationSerializer5, CountrySerializer5, CommuneSerializer5


class APIPermission5(BasePermission):
    def has_permission(self, request, view):
        meta = view.queryset.model._meta
        if request.method in SAFE_METHODS:
            return request.user.has_perm('{}.apiv5_read_{}'.format(meta.app_label, meta.model_name))
        else:
            return request.user.has_perm('{}.apiv5_write_{}'.format(meta.app_label, meta.model_name))


@extend_schema_view(
    create=extend_schema(
        description="Structures. Ce endpoint ne permet que de modifier le statut d'objets existants.",
        request=StructureWriteSerializer5(many=True),
        responses={200: OpenApiResponse(response=StructureResponseSerializer5)},
    ),
)
class StructureViewSet5(ListModelMixin, RetrieveModelMixin, UpdateModelMixin, GenericViewSet):
    """Structures."""

    lookup_field = 'uuid'
    # Order by lft so a parent is always before its children
    queryset = Structure.objects.select_related(
        'type', 'parent', 'region',
    ).only(
        'uuid', 'name', 'parent__uuid', 'region__uuid', 'type__uuid', 'status', 'place', 'public_email', 'public_phone',
    ).order_by('lft')
    permission_classes = [APIPermission5]

    def get_serializer_class(self):
        if self.action in ('create', 'update'):
            return StructureWriteSerializer5
        return StructureSerializer5

    def create(self, request, *args, **kwargs):
        bulk = isinstance(request.data, list)

        if not bulk:
            return super(StructureViewSet5, self).create(request, *args, **kwargs)

        for item in request.data:
            if 'uuid' not in item:
                raise serializers.ValidationError({'uuid': "Missing."})
            instance = get_object_or_404(self.queryset, uuid=UUID(item['uuid']))
            serializer = self.get_serializer(instance, data=item)
            serializer.is_valid(raise_exception=True)
            if instance.status == Structure.STATUS_CLOSED:
                raise serializers.ValidationError({'status': ["Une structure fermée ne peut être ré-ouverte."]})
            self.perform_update(serializer)

        return Response({'updated': len(request.data)})


class TeamViewSet5(ReadOnlyModelViewSet):
    """Équipes."""

    lookup_field = 'uuid'
    queryset = Team.objects.select_related(
        'structure', 'type',
    ).only(
        'uuid', 'structure__uuid', 'type__uuid', 'name', 'creation_date', 'deactivation_date',
    ).order_by('id')
    serializer_class = TeamSerializer5
    permission_classes = [APIPermission5]


class CountryViewSet5(ReadOnlyModelViewSet):
    """Pays."""

    queryset = Country.objects.order_by('id')
    serializer_class = CountrySerializer5
    permission_classes = [APIPermission5]


class CommuneViewSet5(ReadOnlyModelViewSet):
    """Communes."""

    queryset = Commune.objects.order_by('id')
    serializer_class = CommuneSerializer5
    permission_classes = [APIPermission5]


@extend_schema_view(
    list=extend_schema(
        parameters=[
            OpenApiParameter(
                name='season',
                required=False,
                type=int,
                description="Filtre les adhérent·es, responsables légaux et salariés entre le 01/09/n-1 et le 31/08/n."
            ),
            OpenApiParameter(
                name='year',
                required=False,
                type=int,
                description="Filtre les adhérent·es, responsables légaux et salariés sur l'année civile n."
            ),
        ]
    )
)
class PersonViewSet5(ModelViewSet):
    """Personnes (adhérent·es, salarié·es, responsables légaux, payeur·ses, …)"""

    lookup_field = 'uuid'
    # Sort persons without legal_guardian first so legal_guardians appear before children
    queryset = Person.objects.select_related(
        'legal_guardian1', 'legal_guardian2', 'birth_country', 'birth_commune',
    ).only(
        'uuid', 'gender', 'first_name', 'last_name', 'family_name', 'nickname', 'birthdate',
        'birth_country__uuid', 'birth_commune__uuid', 'birth_place',
        'gender', 'address1', 'address2', 'address3', 'postal_code', 'city', 'country',
        'email', 'home_phone', 'mobile_phone', 'birth_place', 'profession',
        'legal_guardian1__uuid', 'legal_guardian2__uuid', 'image_rights',
        'permission_alone', 'permission_with', 'merged_with',
    ).order_by('-legal_guardian1', '-legal_guardian2', 'id')
    serializer_class = PersonSerializer5
    permission_classes = [APIPermission5]

    def get_queryset(self):
        qs = self.queryset
        if 'season' in self.request.GET:
            try:
                season = int(self.request.GET['season'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'season'.")
            dates = DateRange(Date(season - 1, 9, 1), Date(season, 8, 31), '[]')
            qs = qs.filter(
                Q(adherent__adhesions__season=season) |
                Q(protected1__adherent__adhesions__season=season) |
                Q(protected2__adherent__adhesions__season=season) |
                Q(employee__employments__dates__overlap=dates)
            )
        if 'year' in self.request.GET:
            try:
                year = int(self.request.GET['year'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'year'.")
            dates = DateRange(Date(year, 1, 1), Date(year, 12, 31), '[]')
            qs = qs.filter(
                Q(adherent__adhesions__season__in=(year, year + 1)) |
                Q(protected1__adherent__adhesions__season__in=(year, year + 1)) |
                Q(protected2__adherent__adhesions__season__in=(year, year + 1)) |
                Q(employee__employments__dates__overlap=dates)
            )
        return qs

    def create(self, request, *args, **kwargs):
        bulk = isinstance(request.data, list)

        if not bulk:
            return super(PersonViewSet5, self).create(request, *args, **kwargs)

        nb_created = 0
        nb_updated = 0
        for item in request.data:
            if 'uuid' not in item:
                raise serializers.ValidationError({'uuid': "Missing."})
            uuid = UUID(item['uuid'])
            try:
                instance = self.queryset.get(uuid=uuid)
                create = False
            except Person.DoesNotExist:
                instance = Person(uuid=uuid, owner=self.request.user)
                create = True
            serializer = self.get_serializer(instance, data=item)
            serializer.is_valid(raise_exception=True)
            if create:
                self.perform_create(serializer)
                nb_created += 1
            else:
                self.perform_update(serializer)
                nb_updated += 1

        return Response({'created': nb_created, 'updated': nb_updated})


@extend_schema_view(
    list=extend_schema(
        parameters=[
            OpenApiParameter(
                name='season',
                required=False,
                type=int,
                description="Filtre les adhérent·es entre le 01/09/n-1 et le 31/08/n."
            ),
            OpenApiParameter(
                name='year',
                required=False,
                type=int,
                description="Filtre les adhérent·es sur l'année civile n."
            ),
            OpenApiParameter(
                name='status',
                required=False,
                type=str,
                description="Filtre les adhérent·es sur leur status (valeurs numériques). Il est possible de préciser "
                            "plusieurs valeurs séparées par des virgules, sans espace."
            ),
        ]
    )
)
class AdherentViewSet5(ReadOnlyModelViewSet):
    """Adhérent·es."""

    lookup_field = 'uuid'
    queryset = Adherent.objects.select_related(
        'person'
    ).only(
        'uuid', 'id', 'person__uuid', 'status', 'duplicates', 'merged_with',
    ).order_by('id')
    serializer_class = AdherentSerializer5
    permission_classes = [APIPermission5]

    def get_queryset(self):
        qs = self.queryset
        if 'season' in self.request.GET:
            try:
                season = int(self.request.GET['season'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'season'.")
            qs = qs.filter(adhesions__season=season)
        if 'year' in self.request.GET:
            try:
                year = int(self.request.GET['year'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'year'.")
            qs = qs.filter(adhesions__season__in=(year, year + 1))
        if 'status' in self.request.GET:
            try:
                statuses = [int(status) for status in self.request.GET['status'].split(',')]
            except ValueError:
                raise BadRequest("Invalid value for parameter 'status'.")
            qs = qs.filter(status__in=statuses)
        return qs


@extend_schema_view(
    list=extend_schema(
        parameters=[
            OpenApiParameter(
                name='season',
                required=False,
                type=int,
                description="Filtre les adhésions entre le 01/09/n-1 et le 31/08/n."
            ),
            OpenApiParameter(
                name='year',
                required=False,
                type=int,
                description="Filtre les adhésions sur l'année civile n."
            ),
        ]
    )
)
class AdhesionViewSet5(ReadOnlyModelViewSet):
    """Adhésions."""

    lookup_field = 'uuid'
    queryset = Adhesion.objects.select_related(
        'adherent', 'structure', 'rate',
    ).only(
        'uuid', 'adherent__uuid', 'season', 'dates', 'entry_date', 'structure__uuid', 'canceled',
        'rate__category', 'rate__duration',
    ).order_by('id')
    serializer_class = AdhesionSerializer5
    permission_classes = [APIPermission5]

    def get_queryset(self):
        qs = self.queryset
        if 'season' in self.request.GET:
            try:
                season = int(self.request.GET['season'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'season'.")
            qs = qs.filter(season=season)
        if 'year' in self.request.GET:
            try:
                year = int(self.request.GET['year'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'year'.")
            qs = qs.filter(season__in=(year, year + 1))
        return qs


@extend_schema_view(
    list=extend_schema(
        parameters=[
            OpenApiParameter(
                name='season',
                required=False,
                type=int,
                description="Filtre les invitations entre le 01/09/n-1 et le 31/08/n."
            ),
            OpenApiParameter(
                name='year',
                required=False,
                type=int,
                description="Filtre les invitations sur l'année civile n."
            ),
        ]
    )
)
class InvitationViewSet5(ModelViewSet):
    """Invitations."""

    lookup_field = 'token'
    queryset = Invitation.objects.select_related(
        'structure', 'person', 'adhesion', 'host', 'team',
    ).only(
        'id', 'uuid', 'token', 'season', 'email', 'structure__uuid', 'person__uuid', 'adhesion__uuid', 'date',
        'declined', 'host__uuid', 'entry_date', 'modification_date', 'team__uuid', 'function_config_id',
    ).order_by('date')
    serializer_class = InvitationSerializer5
    permission_classes = [APIPermission5]

    def get_queryset(self):
        qs = self.queryset
        if 'season' in self.request.GET:
            try:
                season = int(self.request.GET['season'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'season'.")
            qs = qs.filter(season=season)
        if 'year' in self.request.GET:
            try:
                year = int(self.request.GET['year'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'year'.")
            qs = qs.filter(season__in=(year, year + 1))
        return qs


@extend_schema_view(
    list=extend_schema(
        parameters=[
            OpenApiParameter(
                name='season',
                required=False,
                type=int,
                description="Filtre les salarié·es entre le 01/09/n-1 et le 31/08/n."
            ),
            OpenApiParameter(
                name='year',
                required=False,
                type=int,
                description="Filtre les salarié·es sur l'année civile n."
            ),
        ]
    )
)
class EmployeeViewSet5(ReadOnlyModelViewSet):
    """Salarié·es, CEE, SC."""

    lookup_field = 'uuid'
    queryset = Employee.objects.select_related(
        'person',
    ).only(
        'uuid', 'id', 'person__uuid', 'email', 'phone', 'merged_with',
    ).order_by('id')
    serializer_class = EmployeeSerializer5
    permission_classes = [APIPermission5]

    def get_queryset(self):
        qs = self.queryset
        if 'season' in self.request.GET:
            try:
                season = int(self.request.GET['season'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'season'.")
            qs = qs.for_season(season)
        if 'year' in self.request.GET:
            try:
                year = int(self.request.GET['year'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'year'.")
            qs = qs.for_year(year)
        return qs


@extend_schema_view(
    list=extend_schema(
        parameters=[
            OpenApiParameter(
                name='season',
                required=False,
                type=int,
                description="Filtre les salarié·es entre le 01/09/n-1 et le 31/08/n."
            ),
            OpenApiParameter(
                name='year',
                required=False,
                type=int,
                description="Filtre les salarié·es sur l'année civile n."
            ),
        ]
    )
)
class EmploymentViewSet5(ReadOnlyModelViewSet):
    """Contrats de travail."""

    lookup_field = 'uuid'
    queryset = Employment.objects.select_related(
        'employee',
    ).only(
        'uuid', 'employee__uuid', 'dates', 'name', 'type',
    ).order_by('id')
    serializer_class = EmploymentSerializer5
    permission_classes = [APIPermission5]

    def get_queryset(self):
        qs = self.queryset
        if 'season' in self.request.GET:
            try:
                season = int(self.request.GET['season'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'season'.")
            qs = qs.for_season(season=season)
        if 'year' in self.request.GET:
            try:
                year = int(self.request.GET['year'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'year'.")
            qs = qs.for_year(year=year)
        return qs


@extend_schema_view(
    list=extend_schema(
        parameters=[
            OpenApiParameter(
                name='season',
                required=False,
                type=int,
                description="Filtre les fonctions entre le 01/09/n-1 et le 31/08/n."
            ),
            OpenApiParameter(
                name='year',
                required=False,
                type=int,
                description="Filtre les fonctions sur l'année civile n."
            ),
        ]
    ),
    create=extend_schema(
        description="Fonctions. Il est possible de passer plusieurs objets dans une liste. "
                    "Si l'UUID existe, l'objet correspondant est modifié. Sinon il est créé.",
        request=FunctionSerializer5(many=True),
        responses={200: OpenApiResponse(response=FunctionResponseSerializer5)},
    ),
)
class FunctionViewSet5(ModelViewSet):
    """Fonctions."""

    lookup_field = 'uuid'
    queryset = Function.objects.filter(
        status=FunctionStatus.OK
    ).select_related(
        'person', 'team', 'function_config',
    ).only(
        'uuid', 'person__uuid', 'team__uuid', 'dates', 'name', 'function_config__function_type_id'
    ).order_by('id')
    serializer_class = FunctionSerializer5
    permission_classes = [APIPermission5]

    def get_queryset(self):
        qs = self.queryset.select_related('function_config')
        if 'season' in self.request.GET:
            try:
                season = int(self.request.GET['season'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'season'.")
            qs = qs.for_season(season)
        if 'year' in self.request.GET:
            try:
                year = int(self.request.GET['year'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'year'.")
            qs = qs.for_year(year)
        return qs

    def create(self, request, *args, **kwargs):
        bulk = isinstance(request.data, list)

        if not bulk:
            return super(FunctionViewSet5, self).create(request, *args, **kwargs)

        nb_created = 0
        nb_updated = 0
        for item in request.data:
            if 'uuid' not in item:
                raise serializers.ValidationError({'uuid': "Missing."})
            uuid = UUID(item['uuid'])
            try:
                instance = self.queryset.get(uuid=uuid)
                create = False
            except Function.DoesNotExist:
                instance = Function(uuid=uuid, owner=self.request.user)
                create = True
            serializer = self.get_serializer(instance, data=item)
            serializer.is_valid(raise_exception=True)
            if create:
                self.perform_create(serializer)
                nb_created += 1
            else:
                self.perform_update(serializer)
                nb_updated += 1

        return Response({'created': nb_created, 'updated': nb_updated})

    def get_object(self):
        if self.request.method in ('GET', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'):
            obj = super().get_object()
            if self.request.method in ('PATCH', 'DELETE') and obj.owner != self.request.user:
                raise PermissionDenied
            return obj

        uuid = UUID(self.kwargs['uuid'])
        try:
            obj = self.queryset.get(uuid=uuid)
        except Function.DoesNotExist:
            obj = Function(uuid=uuid, owner=self.request.user)

        if obj.owner != self.request.user:
            raise PermissionDenied

        self.check_object_permissions(self.request, obj)

        return obj


class RoleConfigViewSet5(ReadOnlyModelViewSet):
    """Configuration des rôles."""

    lookup_field = 'uuid'
    queryset = RoleConfig.objects.order_by('id')
    serializer_class = RoleConfigSerializer5
    permission_classes = [APIPermission5]


@extend_schema_view(
    list=extend_schema(
        parameters=[
            OpenApiParameter(
                name='app_label',
                required=False,
                type=str,
                description="Filtre les permissions sur les modèles de cette application. "
                            "Il est possible de spécifier plusieurs valeurs séparées par des virgules, sans espace."
            ),
        ]
    ),
)
class RoleConfigPermissionViewSet5(ReadOnlyModelViewSet):
    """Permissions pour la configuration des rôles."""

    lookup_field = 'uuid'
    queryset = RoleConfigPermission.objects.order_by('id')
    serializer_class = RoleConfigPermissionSerializer5
    permission_classes = [APIPermission5]

    def get_queryset(self):
        qs = self.queryset
        if 'app_label' in self.request.GET:
            app_labels = self.request.GET.get('app_label', '').split(',')
            qs = qs.filter(permission__content_type__app_label__in=app_labels)
        qs = qs.select_related(
            'roleconfig',
            'permission',
            'permission__content_type',
        )
        return qs


class AttributionViewSet5(ReadOnlyModelViewSet):
    """Attribution des rôles."""

    lookup_field = 'uuid'
    queryset = Attribution.objects.order_by('id')
    serializer_class = AttributionSerializer5
    permission_classes = [APIPermission5]


class RoleViewSet5(ReadOnlyModelViewSet):
    """Rôles."""

    lookup_field = 'uuid'
    queryset = Role.objects.order_by('id')
    serializer_class = RoleSerializer5
    permission_classes = [APIPermission5]


class PermissionViewSet5(CreateModelMixin, ListModelMixin, RetrieveModelMixin, GenericViewSet):
    """Permissions."""

    queryset = Permission.objects.exclude(content_type__external=None).order_by('id')
    serializer_class = PermissionSerializer5
    permission_classes = [APIPermission5]
