# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from datetime import timedelta as TimeDelta
from psycopg2.extras import DateRange
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.utils.functional import lazy
from members.models import age, Adhesion, FunctionType, Function, Person, AdherentStatus, Structure, StructureType, \
    TeamType, Team, Employment, EmploymentType, Adherent, Employee, Permission, Country, Commune, \
    RoleConfig, Attribution, Role, RoleConfigPermission, ExternalContentType, FunctionConfig, FunctionStatus, Invitation
from members.utils import current_season, today
from phonenumber_field.serializerfields import PhoneNumberField


def lazy_objects_for_model(model):
    return lazy(lambda: ", ".join(f"{item.id}={item.name}" for item in model.objects.order_by('id')), str)


class CountrySerializer5(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ('uuid', 'id', 'name', 'iso2', 'iso3')


class CommuneSerializer5(serializers.ModelSerializer):
    begin = serializers.DateField(label="Date de début")
    end = serializers.DateField(label="Date de fin")

    class Meta:
        model = Commune
        fields = ('uuid', 'id', 'code', 'name', 'begin', 'end')


class PersonSerializer5(serializers.ModelSerializer):
    gender = serializers.CharField(
        label="Genre",
        help_text=", ".join(f"{key}={val}" for key, val in Person.GENDER_CHOICES),
    )
    legal_guardian1_uuid = serializers.UUIDField(source='legal_guardian1.uuid', default=None, required=False)
    legal_guardian2_uuid = serializers.UUIDField(source='legal_guardian2.uuid', default=None, required=False)
    birth_country_uuid = serializers.UUIDField(source='birth_country.uuid', default=None, required=False)
    birth_commune_uuid = serializers.UUIDField(source='birth_commune.uuid', default=None, required=False)
    home_phone = PhoneNumberField(required=False, default='')
    mobile_phone = PhoneNumberField(required=False, default='')

    class Meta:
        model = Person
        fields = (
            'uuid', 'id', 'gender', 'first_name', 'last_name', 'family_name', 'nickname',
            'address1', 'address2', 'address3', 'postal_code', 'city', 'country',
            'email', 'home_phone', 'mobile_phone',
            'birthdate', 'birth_country_uuid', 'birth_commune_uuid', 'birth_place', 'birth_place',
            'legal_guardian1_uuid', 'legal_guardian2_uuid', 'image_rights', 'profession',
            'permission_alone', 'permission_with', 'merged_with',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email'].validators = [
            validator for validator in self.fields['email'].validators if not isinstance(validator, UniqueValidator)
        ]

    def validate_last_name(self, value):
        return value and value.upper()

    def validate_first_name(self, value):
        return value and value.upper()

    def validate_family_name(self, value):
        return value and value.upper()

    def validate_legal_guardian1_uuid(self, value):
        if value is None:
            return None
        try:
            return Person.objects.get(uuid=value)
        except Person.DoesNotExist:
            raise serializers.ValidationError({'legal_guardian1_uuid': ["Not found."]})

    def validate_legal_guardian2_uuid(self, value):
        if value is None:
            return None
        try:
            return Person.objects.get(uuid=value)
        except Person.DoesNotExist:
            raise serializers.ValidationError({'legal_guardian2_uuid': ["Not found."]})

    def validate_birth_country_uuid(self, value):
        if value is None:
            return None
        try:
            return Country.objects.get(uuid=value)
        except Country.DoesNotExist:
            raise serializers.ValidationError({'country_uuid': ["Not found."]})

    def validate_birth_commune_uuid(self, value):
        if value is None:
            return None
        try:
            return Commune.objects.get(uuid=value)
        except Commune.DoesNotExist:
            raise serializers.ValidationError({'commune_uuid': ["Not found."]})

    def validate_birthdate(self, value):
        if value and age(value) > 100:
            raise serializers.ValidationError("La personne est-elle réellement centenaire ?")
        if value and age(value) < 6:
            raise serializers.ValidationError(
                "La réglementation (Article L2324-1 du code de la santé publique) ne permet pas "
                "aux EEDF d'accueillir des enfants de moins de 6 ans."
            )
        return value

    def validate(self, data):
        def get_value(name):
            if name in data:
                if isinstance(data[name], dict):
                    return data[name]['uuid']
                return data[name]
            if self.instance:
                return getattr(self.instance, name)
            return None

        # Email against birthdate
        birthdate = get_value('birthdate')
        email = get_value('email')
        if birthdate and age(birthdate) >= 16 and not email:
            raise serializers.ValidationError({'email': "Obligatoire pour les personnes de plus de 16 ans."})
        # Birth commune against birthdate
        birth_commune = get_value('birth_commune')
        if birth_commune and birth_commune.dates.lower and birth_commune.dates.lower > birthdate:
            raise serializers.ValidationError(
                {'birth_commune_uuid': f"La commune n'existait pas encore au {birthdate.strftime('%d/%m/%Y')}."},
            )
        if birth_commune and birth_commune.dates.upper and birth_commune.dates.upper <= birthdate:
            raise serializers.ValidationError(
                {'birth_commune_uuid': f"La commune n'existait plus au {birthdate.strftime('%d/%m/%Y')}."},
            )
        # Birth commune against birth country
        birth_country = get_value('birth_country')
        if birth_country and birth_country.id == 99100 and not birth_commune:
            raise serializers.ValidationError(
                {'birth_commune_uuid': "La commune est obligatoire pour une naissance en France."},
            )
        if birth_country and birth_country.id != 99100 and birth_commune:
            raise serializers.ValidationError(
                {'birth_commune_uuid': "La commune doit être nulle pour une naissance à l'étranger."},
            )
        # Birth place against birth country
        birth_place = get_value('birth_place')
        if birth_country and birth_country.id != 99100 and not birth_place:
            raise serializers.ValidationError(
                {'birth_place': "La ville de naissance est obligatoire pour une naissance à l'étranger."},
            )
        if birth_country and birth_country.id != 99100 and birth_place:
            raise serializers.ValidationError(
                {'birth_place': "La ville de naissance doit être nulle pour une naissance en France."},
            )
        return data

    def create(self, validated_data):
        validated_data['legal_guardian1'] = validated_data['legal_guardian1']['uuid']
        validated_data['legal_guardian2'] = validated_data['legal_guardian2']['uuid']
        validated_data['birth_country'] = validated_data['birth_country']['uuid']
        validated_data['birth_commune'] = validated_data['birth_commune']['uuid']
        person = Person(**validated_data)
        person.save()
        return person

    def update(self, instance, validated_data):
        if 'legal_guardian1' in validated_data:
            validated_data['legal_guardian1'] = validated_data['legal_guardian1']['uuid']
        if 'legal_guardian2' in validated_data:
            validated_data['legal_guardian2'] = validated_data['legal_guardian2']['uuid']
        if 'birth_country' in validated_data:
            validated_data['birth_country'] = validated_data['birth_country']['uuid']
        if 'birth_commune' in validated_data:
            validated_data['birth_commune'] = validated_data['birth_commune']['uuid']
        for field, value in validated_data.items():
            setattr(instance, field, value)
        instance.save()
        return instance


class AdherentSerializer5(serializers.ModelSerializer):
    person_uuid = serializers.UUIDField(source='person.uuid')
    status = serializers.IntegerField(
        label="Statut",
        help_text=", ".join(f"{key}={val}" for key, val in AdherentStatus.choices),
    )

    class Meta:
        model = Adherent
        fields = (
            'uuid', 'id', 'person_uuid', 'status', 'duplicates', 'merged_with',
        )


class AdhesionSerializer5(serializers.ModelSerializer):
    adherent_uuid = serializers.UUIDField(source='adherent.uuid')
    begin = serializers.DateField(label="Date de début")
    end = serializers.DateField(label="Date de fin")
    structure_uuid = serializers.UUIDField(source='structure.uuid')

    class Meta:
        model = Adhesion
        fields = (
            'uuid', 'id', 'adherent_uuid', 'season', 'begin', 'end', 'entry_date', 'structure_uuid', 'canceled',
        )


class InvitationSerializer5(serializers.ModelSerializer):
    season = serializers.IntegerField(read_only=True)
    structure_uuid = serializers.UUIDField(source='structure.uuid')
    person_uuid = serializers.UUIDField(source='person.uuid', allow_null=True, required=False)
    adhesion_uuid = serializers.UUIDField(source='adhesion.uuid', allow_null=True, read_only=True)
    team_uuid = serializers.UUIDField(source='team.uuid', allow_null=True, required=False)
    function_type = serializers.IntegerField(
        source='function_config.function_type_id',
        label="Type de fonction",
        help_text=lazy_objects_for_model(FunctionType),
        required=False,
    )
    host_uuid = serializers.UUIDField(source='host.uuid', read_only=True)
    entry_date = serializers.DateField(read_only=True)
    modification_date = serializers.DateField(read_only=True)
    token = serializers.CharField(read_only=True)

    class Meta:
        model = Invitation
        fields = (
            'uuid', 'id', 'token', 'season', 'email', 'structure_uuid', 'person_uuid', 'adhesion_uuid', 'date',
            'declined', 'team', 'function_type', 'host_uuid', 'entry_date', 'modification_date', 'host_uuid',
            'team_uuid',
        )

    def create(self, data):
        if 'person' in data:
            try:
                person = Person.objects.get(uuid=data['person']['uuid'])
            except Person.DoesNotExist:
                raise serializers.ValidationError({'person_uuid': ["Not found."]})
        else:
            person = None
        if 'structure' in data:
            try:
                structure = Structure.objects.get(uuid=data['structure']['uuid'])
            except Structure.DoesNotExist:
                raise serializers.ValidationError({'structure_uuid': ["Not found."]})
        else:
            structure = None
        if 'team' in data:
            try:
                team = Team.objects.get(uuid=data['team']['uuid'])
            except Team.DoesNotExist:
                raise serializers.ValidationError({'team_uuid': ["Not found."]})
        else:
            team = None
        if 'function_type' in data:
            try:
                function_type = FunctionType.objects.get(id=data['function_type']['uuid'])
            except FunctionType.DoesNotExist:
                raise serializers.ValidationError({'function_type': ["Not found."]})
            try:
                function_config = FunctionConfig.objects.get(
                    Q(structure_type=team.structure.type) | Q(structure_type=None),
                    function_type=function_type,
                    team_type=team.type,
                )
            except FunctionConfig.DoesNotExist:
                raise serializers.ValidationError({'function_type': ["No function config found."]})
            except FunctionConfig.MultipleObjectsReturned:
                raise serializers.ValidationError({'type': ["Multiple function configs found."]})
        else:
            function_config = None
        return Invitation.objects.create(
            uuid=data['uuid'],
            person=person,
            season=current_season(),
            email=data['email'],
            structure=structure,
            team=team,
            function_config=function_config,
            rate=None,
            date=data.get('date'),
            entry_date=today(),
            modification_date=today(),
        )


class TeamSerializer5(serializers.ModelSerializer):
    structure_uuid = serializers.UUIDField(label="Structure", source='structure.uuid')
    type = serializers.IntegerField(
        source='type_id',
        label="Type",
        help_text=lazy_objects_for_model(TeamType),
    )

    class Meta:
        model = Team
        fields = ('uuid', 'id', 'structure_uuid', 'type', 'name', 'creation_date', 'deactivation_date')


class StructureSerializer5(serializers.ModelSerializer):
    parent_uuid = serializers.UUIDField(label="Structure parente", source='parent.uuid', default=None, read_only=True)
    region_uuid = serializers.UUIDField(label="Région", source='region.uuid', default=None, read_only=True)
    type = serializers.IntegerField(
        source='type_id',
        label="Type",
        help_text=lazy_objects_for_model(StructureType),
    )
    status = serializers.IntegerField(
        label="Statut",
        help_text=", ".join(f"{key}={val}" for key, val in Structure.STATUS_CHOICES),
    )

    class Meta:
        model = Structure
        fields = ('uuid', 'id', 'name', 'parent_uuid', 'region_uuid', 'type', 'status', 'place',
                  'public_email', 'public_phone')


class StructureWriteSerializer5(serializers.ModelSerializer):
    status = serializers.IntegerField(
        label="Statut",
        help_text=", ".join(f"{key}={val}" for key, val in Structure.STATUS_CHOICES if key in (1, 2)),
    )

    def validate_status(self, value):
        if value not in (Structure.STATUS_AUTONOMOUS, Structure.STATUS_GUARDIANSHIP):
            raise serializers.ValidationError("Seuls les statuts 1 (autonome) et 2 (rattachée) sont autorisés.")
        return value

    class Meta:
        model = Structure
        fields = ('uuid', 'id', 'status')


class FunctionSerializer5(serializers.ModelSerializer):
    person_uuid = serializers.UUIDField(source='person.uuid')
    team_uuid = serializers.UUIDField(source='team.uuid')
    begin = serializers.DateField(label="Date de début")
    end = serializers.DateField(label="Date de fin")
    type = serializers.IntegerField(
        source='function_config.function_type_id',
        label="Type",
        help_text=lazy_objects_for_model(FunctionType),
    )

    class Meta:
        model = Function
        fields = ('uuid', 'id', 'person_uuid', 'team_uuid', 'begin', 'end', 'name', 'type')

    def validate(self, data):
        response = {}
        if 'uuid' in data:
            response['uuid'] = data['uuid']
        if 'person' in data:
            try:
                response['person'] = Person.objects.get(uuid=data['person']['uuid'])
            except Person.DoesNotExist:
                raise serializers.ValidationError({'person_uuid': ["Not found."]})
        if 'team' in data:
            try:
                response['team'] = Team.objects.get(uuid=data['team']['uuid'])
            except Team.DoesNotExist:
                raise serializers.ValidationError({'team_uuid': ["Not found."]})
        if 'begin' in data:
            response['begin'] = data['begin']
        if 'end' in data:
            response['end'] = data['end'] + TimeDelta(days=1)
        if 'name' in data:
            response['name'] = data['name']
        if 'function_config' in data:
            try:
                response['type'] = FunctionType.objects.get(id=data['function_config']['function_type_id'])
            except FunctionType.DoesNotExist:
                raise serializers.ValidationError({'type': ["Invalid value."]})
        return response

    def create(self, validated_data):
        try:
            function_config = FunctionConfig.objects.get(
                Q(structure_type=validated_data['team'].structure.type) | Q(structure_type=None),
                function_type=validated_data['type'],
                team_type=validated_data['team'].type,
            )
        except FunctionConfig.DoesNotExist:
            raise serializers.ValidationError({'type': ["Not config found."]})
        except FunctionConfig.MultipleObjectsReturned:
            raise serializers.ValidationError({'type': ["Multiple configs found."]})
        if validated_data['end'] <= validated_data['begin']:
            raise serializers.ValidationError({'end': ["Should be after begin."]})
        return Function.objects.create(
            person=validated_data['person'],
            team=validated_data['team'],
            function_config=function_config,
            dates=DateRange(validated_data['begin'], validated_data['end']),
            name=validated_data['name'],
            status=FunctionStatus.OK,
            owner=self.context['request'].user
        )

    def update(self, instance, validated_data):
        if 'uuid' in validated_data and validated_data['uuid'] != instance.uuid:
            raise serializers.ValidationError({"uuid": ["Mismatch."]})
        if 'person' in validated_data:
            instance.person = validated_data['person']
        if 'team' in validated_data:
            team = validated_data['team']
            instance.team = team
        else:
            team = self.instance.team
        if 'type' in validated_data:
            try:
                instance.function_config = FunctionConfig.objects.get(
                    Q(structure_type=team.structure.type) | Q(structure_type=None),
                    function_type=validated_data['type'],
                    team_type=team.type,
                )
            except FunctionConfig.DoesNotExist:
                raise serializers.ValidationError({'type': ["Not config found."]})
            except FunctionConfig.MultipleObjectsReturned:
                raise serializers.ValidationError({'type': ["Multiple configs found."]})
        if 'begin' in validated_data:
            begin = validated_data['begin']
        else:
            begin = self.instance.begin
        if 'end' in validated_data:
            end = validated_data['end']
        else:
            end = self.instance.end + TimeDelta(days=1)
        if end <= begin:
            raise serializers.ValidationError({"end": ["Should be after begin."]})
        instance.dates = DateRange(begin, end)
        if 'name' in validated_data:
            instance.name = validated_data['name']
        instance.status = FunctionStatus.OK
        instance.save()
        return instance


class EmployeeSerializer5(serializers.ModelSerializer):
    person_uuid = serializers.UUIDField(source='person.uuid')

    class Meta:
        model = Employee
        fields = (
            'uuid', 'id', 'person_uuid', 'email', 'phone', 'merged_with',
        )


class EmploymentSerializer5(serializers.ModelSerializer):
    employee_uuid = serializers.UUIDField(source='employee.uuid')
    begin = serializers.DateField(label="Date de début")
    end = serializers.DateField(label="Date de fin")
    type = serializers.IntegerField(
        label="Type",
        help_text=lazy_objects_for_model(EmploymentType),
    )

    class Meta:
        model = Employment
        fields = ('uuid', 'id', 'employee_uuid', 'begin', 'end', 'name', 'type')


class RoleConfigSerializer5(serializers.ModelSerializer):
    permissions = serializers.SerializerMethodField()

    class Meta:
        model = RoleConfig
        fields = ('uuid', 'id', 'name', 'description', 'delegation', 'for_self', 'for_team', 'for_structure',
                  'for_sub_structures', 'for_all', 'only_electeds', 'only_employees', 'with_guardians',
                  'with_payers', 'permissions')

    def get_permissions(self, obj):
        return (f"{ct}.{name}" for ct, name in obj.permissions.values_list("content_type__app_label", "codename"))


class RoleConfigPermissionSerializer5(serializers.ModelSerializer):
    roleconfig_uuid = serializers.UUIDField(source='roleconfig.uuid')
    app_label = serializers.CharField(source='permission.content_type.app_label')
    model = serializers.CharField(source='permission.content_type.model')
    codename = serializers.CharField(source='permission.codename')

    class Meta:
        model = RoleConfigPermission
        fields = ('uuid', 'id', 'roleconfig_uuid', 'app_label', 'model', 'codename')


class AttributionSerializer5(serializers.ModelSerializer):
    role_config_uuid = serializers.UUIDField(source='role_config.uuid')
    function_type = serializers.IntegerField(
        source='function_type_id',
        label="Type de fonction",
        help_text=lazy_objects_for_model(FunctionType),
    )
    structure_type = serializers.IntegerField(
        source='structure_type_id',
        label="Type de structure",
        help_text=lazy_objects_for_model(StructureType),
    )
    team_type = serializers.IntegerField(
        source='team_type_id',
        label="Type d'équipe",
        help_text=lazy_objects_for_model(TeamType),
    )

    class Meta:
        model = Attribution
        fields = ('uuid', 'id', 'role_config_uuid', 'structure_type', 'team_type',
                  'function_type', 'function_category')


class RoleSerializer5(serializers.ModelSerializer):
    config_uuid = serializers.UUIDField(source='config.uuid')
    team_uuid = serializers.UUIDField(source='team.uuid')
    holder_uuid = serializers.UUIDField(source='holder.uuid', default=None)
    proxy_uuid = serializers.UUIDField(source='proxy.uuid')

    class Meta:
        model = Role
        fields = ('uuid', 'id', 'config_uuid', 'team_uuid', 'holder_uuid', 'proxy_uuid')


class PermissionSerializer5(serializers.ModelSerializer):
    app_label = serializers.CharField(source='content_type.app_label')
    model = serializers.CharField(source='content_type.model')
    model_verbose_name = serializers.CharField(source='content_type.external.verbose_name')

    class Meta:
        model = Permission
        fields = ('name', 'app_label', 'model', 'codename', 'model_verbose_name')

    def create(self, validated_data):
        content_type_data = validated_data.pop('content_type')
        external_data = content_type_data.pop('external')
        codename = validated_data.pop('codename')
        content_type, _ = ContentType.objects.get_or_create(**content_type_data)
        ExternalContentType.objects.get_or_create(content_type=content_type, defaults=external_data)
        permission, _ = Permission.objects.update_or_create(
            content_type=content_type,
            codename=codename,
            defaults=validated_data,
        )
        return permission


class StructureResponseSerializer5(serializers.Serializer):
    updated = serializers.IntegerField(label="Nombre d'objets mis à jour")


class FunctionResponseSerializer5(serializers.Serializer):
    created = serializers.IntegerField(label="Nombre d'objets créés")
    updated = serializers.IntegerField(label="Nombre d'objets mis à jour")
