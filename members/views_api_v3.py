# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from datetime import date as Date
from drf_spectacular.utils import extend_schema_view, extend_schema, OpenApiParameter
from psycopg2.extras import DateRange
from django.db.models import Q
from django.core.exceptions import BadRequest
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.permissions import BasePermission
from .models import (Adhesion, Adherent, Function, Person, Structure, StructureType, Employee,
                     Team, TeamType, FunctionType, Employment, FunctionStatus)
from .serializers_v3 import AdhesionSerializer3, StructureTypeSerializer3, StructureSerializer3, PersonSerializer3, \
                            TeamTypeSerializer3, TeamSerializer3, FunctionTypeSerializer3, FunctionSerializer3, \
                            EmploymentSerializer3, AdherentSerializer3, EmployeeSerializer3


class APIPermission3(BasePermission):
    def has_permission(self, request, view):
        meta = view.queryset.model._meta
        return request.user.has_perm('{}.apiv3_{}'.format(meta.app_label, meta.model_name))


class StructureTypeViewSet3(ReadOnlyModelViewSet):
    """Types de structures."""

    queryset = StructureType.objects.order_by('id')
    serializer_class = StructureTypeSerializer3
    permission_classes = [APIPermission3]


class StructureViewSet3(ReadOnlyModelViewSet):
    """Structures."""

    # Order by lft so a parent is always before its children
    queryset = Structure.objects.order_by('lft')
    serializer_class = StructureSerializer3
    permission_classes = [APIPermission3]


class TeamTypeViewSet3(ReadOnlyModelViewSet):
    """Types d'équipes."""

    queryset = TeamType.objects.order_by('id')
    serializer_class = TeamTypeSerializer3
    permission_classes = [APIPermission3]


class TeamViewSet3(ReadOnlyModelViewSet):
    """Équipes."""

    queryset = Team.objects.order_by('id')
    serializer_class = TeamSerializer3
    permission_classes = [APIPermission3]


@extend_schema_view(
    list=extend_schema(
        parameters=[
            OpenApiParameter(
                name='season',
                required=False,
                type=int,
                description="Filtre les adhérent·es, responsables légaux et salariés entre le 01/09/n-1 et le 31/08/n."
            ),
            OpenApiParameter(
                name='year',
                required=False,
                type=int,
                description="Filtre les adhérent·es, responsables légaux et salariés sur l'année civile n."
            ),
        ]
    )
)
class PersonViewSet3(ReadOnlyModelViewSet):
    """Personnes (adhérent·es, salarié·es, responsables légaux, payeur·ses, …)"""

    # Sort persons without legal_guardian first so legal_guardians appear before children
    queryset = Person.objects.order_by('-legal_guardian1', '-legal_guardian2', 'id')
    serializer_class = PersonSerializer3
    permission_classes = [APIPermission3]

    def get_queryset(self):
        qs = self.queryset.select_related('adherent', 'birth_country', 'birth_commune')
        if 'season' in self.request.GET:
            try:
                season = int(self.request.GET['season'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'season'.")
            dates = DateRange(Date(season - 1, 9, 1), Date(season, 8, 31), '[]')
            qs = qs.filter(
                Q(adherent__adhesions__season=season) |
                Q(protected1__adherent__adhesions__season=season) |
                Q(protected2__adherent__adhesions__season=season) |
                Q(employee__employments__dates__overlap=dates)
            )
        if 'year' in self.request.GET:
            try:
                year = int(self.request.GET['year'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'year'.")
            dates = DateRange(Date(year, 1, 1), Date(year, 12, 31), '[]')
            qs = qs.filter(
                Q(adherent__adhesions__season__in=(year, year + 1)) |
                Q(protected1__adherent__adhesions__season__in=(year, year + 1)) |
                Q(protected2__adherent__adhesions__season__in=(year, year + 1)) |
                Q(employee__employments__dates__overlap=dates)
            )
        return qs


@extend_schema_view(
    list=extend_schema(
        parameters=[
            OpenApiParameter(
                name='season',
                required=False,
                type=int,
                description="Filtre les adhérent·es entre le 01/09/n-1 et le 31/08/n."
            ),
            OpenApiParameter(
                name='year',
                required=False,
                type=int,
                description="Filtre les adhérent·es sur l'année civile n."
            ),
            OpenApiParameter(
                name='status',
                required=False,
                type=str,
                description="Filtre les adhérent·es sur leur status (valeurs numériques). Il est possible de préciser "
                            "plusieurs valeurs séparées par des virgules, sans espace."
            ),
        ]
    )
)
class AdherentViewSet3(ReadOnlyModelViewSet):
    """Adhérent·es."""

    queryset = Adherent.objects.order_by('id')
    serializer_class = AdherentSerializer3
    permission_classes = [APIPermission3]

    def get_queryset(self):
        qs = self.queryset
        if 'season' in self.request.GET:
            try:
                season = int(self.request.GET['season'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'season'.")
            qs = qs.filter(adhesions__season=season)
        if 'year' in self.request.GET:
            try:
                year = int(self.request.GET['year'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'year'.")
            qs = qs.filter(adhesions__season__in=(year, year + 1))
        if 'status' in self.request.GET:
            try:
                statuses = [int(status) for status in self.request.GET['status'].split(',')]
            except ValueError:
                raise BadRequest("Invalid value for parameter 'status'.")
            qs = qs.filter(status__in=statuses)
        return qs


@extend_schema_view(
    list=extend_schema(
        parameters=[
            OpenApiParameter(
                name='season',
                required=False,
                type=int,
                description="Filtre les adhésions entre le 01/09/n-1 et le 31/08/n."
            ),
            OpenApiParameter(
                name='year',
                required=False,
                type=int,
                description="Filtre les adhésions sur l'année civile n."
            ),
        ]
    )
)
class AdhesionViewSet3(ReadOnlyModelViewSet):
    """Adhésions."""

    queryset = Adhesion.objects.order_by('id')
    serializer_class = AdhesionSerializer3
    permission_classes = [APIPermission3]

    def get_queryset(self):
        qs = self.queryset
        if 'season' in self.request.GET:
            try:
                season = int(self.request.GET['season'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'season'.")
            qs = qs.filter(season=season)
        if 'year' in self.request.GET:
            try:
                year = int(self.request.GET['year'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'year'.")
            qs = qs.filter(season__in=(year, year + 1))
        return qs


@extend_schema_view(
    list=extend_schema(
        parameters=[
            OpenApiParameter(
                name='season',
                required=False,
                type=int,
                description="Filtre les salarié·es entre le 01/09/n-1 et le 31/08/n."
            ),
            OpenApiParameter(
                name='year',
                required=False,
                type=int,
                description="Filtre les salarié·es sur l'année civile n."
            ),
        ]
    )
)
class EmployeeViewSet3(ReadOnlyModelViewSet):
    """Salarié·es, CEE, SC."""

    queryset = Employee.objects.order_by('id')
    serializer_class = EmployeeSerializer3
    permission_classes = [APIPermission3]

    def get_queryset(self):
        qs = self.queryset
        if 'season' in self.request.GET:
            try:
                season = int(self.request.GET['season'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'season'.")
            qs = qs.for_season(season)
        if 'year' in self.request.GET:
            try:
                year = int(self.request.GET['year'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'year'.")
            qs = qs.for_year(year)
        return qs


@extend_schema_view(
    list=extend_schema(
        parameters=[
            OpenApiParameter(
                name='season',
                required=False,
                type=int,
                description="Filtre les salarié·es entre le 01/09/n-1 et le 31/08/n."
            ),
            OpenApiParameter(
                name='year',
                required=False,
                type=int,
                description="Filtre les salarié·es sur l'année civile n."
            ),
        ]
    )
)
class EmploymentViewSet3(ReadOnlyModelViewSet):
    """Contrats de travail."""

    queryset = Employment.objects.order_by('id')
    serializer_class = EmploymentSerializer3
    permission_classes = [APIPermission3]

    def get_queryset(self):
        qs = self.queryset
        if 'season' in self.request.GET:
            try:
                season = int(self.request.GET['season'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'season'.")
            qs = qs.for_season(season=season)
        if 'year' in self.request.GET:
            try:
                year = int(self.request.GET['year'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'year'.")
            qs = qs.for_year(year=year)
        return qs


@extend_schema_view(
    list=extend_schema(
        description="Types de fonction."
    ),
)
class FunctionTypeViewSet3(ReadOnlyModelViewSet):
    queryset = FunctionType.objects.order_by('id')
    serializer_class = FunctionTypeSerializer3
    permission_classes = [APIPermission3]


@extend_schema_view(
    list=extend_schema(
        parameters=[
            OpenApiParameter(
                name='season',
                required=False,
                type=int,
                description="Filtre les fonctions entre le 01/09/n-1 et le 31/08/n."
            ),
            OpenApiParameter(
                name='year',
                required=False,
                type=int,
                description="Filtre les fonctions sur l'année civile n."
            ),
        ]
    ),
)
class FunctionViewSet3(ReadOnlyModelViewSet):
    """Fonctions."""

    queryset = Function.objects.filter(status=FunctionStatus.OK).order_by('id')
    serializer_class = FunctionSerializer3
    permission_classes = [APIPermission3]

    def get_queryset(self):
        qs = self.queryset.select_related('function_config')
        if 'season' in self.request.GET:
            try:
                season = int(self.request.GET['season'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'season'.")
            qs = qs.for_season(season)
        if 'year' in self.request.GET:
            try:
                year = int(self.request.GET['year'])
            except ValueError:
                raise BadRequest("Invalid value for parameter 'year'.")
            qs = qs.for_year(year)
        return qs
