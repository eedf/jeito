# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from datetime import date as Date, datetime as DateTime
from decimal import Decimal
import factory
from psycopg2.extras import DateRange
from sequences import get_next_value
from django.utils.timezone import make_aware
from . import models


class StructureTypeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.StructureType

    name = factory.Sequence(lambda n: "Name %03d" % n)
    can_import = False


class StructureFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Structure

    name = factory.Sequence(lambda n: "Name %03d" % n)
    type = factory.SubFactory(StructureTypeFactory)
    status = models.Structure.STATUS_AUTONOMOUS


class RateFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Rate

    name = factory.Sequence(lambda n: "Rate %03d" % n)
    rate = 22
    season = 2015
    function_category = models.FunctionCategory.PARTICIPANT


class PersonFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Person

    first_name = factory.Sequence(lambda n: "Firstname %03d" % n)
    last_name = factory.Sequence(lambda n: "Lastname %03d" % n)
    normalized_first_name = factory.Sequence(lambda n: "Firstname %03d" % n)
    normalized_last_name = factory.Sequence(lambda n: "Lastname %03d" % n)
    birthdate = '1980-01-01'
    email = factory.Sequence(lambda n: "person%03d@toto.com" % n)


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.User

    person = factory.SubFactory(PersonFactory)


class AdherentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Adherent

    id = factory.LazyAttribute(lambda o: get_next_value('person_number'))
    person = factory.SubFactory(PersonFactory)


class AdhesionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Adhesion

    adherent = factory.SubFactory(AdherentFactory)
    season = 2015
    dates = DateRange(Date(2014, 10, 15), Date(2015, 9, 1))
    entry_date = Date(2014, 10, 15)
    rate = factory.SubFactory(RateFactory)
    structure = factory.SubFactory(StructureFactory)

    @factory.post_generation
    def last_adhesion(obj, create, extracted, **kwargs):
        obj.adherent.last_adhesion = obj
        if create:
            obj.adherent.save()


class EmployeeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Employee

    id = factory.LazyAttribute(lambda o: get_next_value('person_number'))
    person = factory.SubFactory(PersonFactory)


class EmploymentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Employment

    employee = factory.SubFactory(EmployeeFactory)
    type = models.EmploymentType.SAL
    name = factory.Sequence(lambda n: "Employment %03d" % n)
    dates = DateRange(Date(2014, 10, 14), Date(2015, 8, 31), '[]')

    @factory.post_generation
    def last_employment(obj, create, extracted, **kwargs):
        obj.employee.last_employment = obj
        if create:
            obj.employee.save()


class FunctionTypeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.FunctionType

    name = factory.Sequence(lambda n: "Function type %03d" % n)
    category = models.FunctionCategory.PARTICIPANT


class TeamTypeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.TeamType

    name = factory.Sequence(lambda n: "Team type %03d" % n)


class TeamFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Team

    structure = factory.SubFactory(StructureFactory)
    type = factory.SubFactory(TeamTypeFactory)
    name = factory.Sequence(lambda n: "Team %03d" % n)
    creation_date = make_aware(DateTime(2014, 10, 15))


class FunctionConfigFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.FunctionConfig

    function_type = factory.SubFactory(FunctionTypeFactory)
    team_type = factory.SubFactory(TeamTypeFactory)
    name = factory.Sequence(lambda n: "Function config %03d" % n)


class FunctionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Function

    person = factory.SubFactory(PersonFactory)
    team = factory.SubFactory(TeamFactory)
    function_config = factory.SubFactory(FunctionConfigFactory)
    dates = DateRange(Date(2014, 10, 14), Date(2015, 8, 31), '[]')
    name = factory.Sequence(lambda n: "Function %03d" % n)


class PaymentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Payment

    amount = Decimal('10.00')
    date = Date(2014, 10, 14)
    payer = factory.SubFactory(PersonFactory)
    method = 1
    reference = factory.Sequence(lambda n: "Reference %03d" % n)


class AllocationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Allocation

    adhesion = factory.SubFactory(AdhesionFactory)
    payment = factory.SubFactory(PaymentFactory)
    category = models.Allocation.CATEGORY_CONTRIBUTION
    amount = Decimal('10.00')


class InvitationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Invitation

    season = 2015
    email = 'toto@tata.com'
    structure = factory.SubFactory(StructureFactory)
    host = factory.SubFactory(PersonFactory)
