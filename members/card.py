# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from PyPDF2 import PdfWriter, PdfReader
from io import BytesIO
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from members.models import Season


def generate_card(adhesion, outputStream):
    functions = adhesion.functions.filter(team__structure=adhesion.structure)
    packet = BytesIO()
    # create a new PDF with Reportlab
    can = canvas.Canvas(packet, pagesize=A4)
    can.setFont('Helvetica', 9)
    can.drawString(235, 746, str(adhesion.adherent.id_str))
    can.drawString(235, 730, f"{adhesion.adherent.person.first_name} {adhesion.adherent.person.last_name}")
    can.drawString(235, 714, adhesion.adherent.person.birthdate.strftime("%d/%m/%Y"))
    can.drawString(235, 698, ', '.join([str(function) for function in functions]))
    can.drawString(235, 682, adhesion.structure.name)
    can.save()

    # move to the beginning of the StringIO buffer
    packet.seek(0)
    new_pdf = PdfReader(packet)
    # read your existing PDF
    season = Season.objects.get(year=adhesion.season)
    f = season.card.open(mode="rb")
    existing_pdf = PdfReader(f)
    output = PdfWriter()
    # add the "watermark" (which is the new pdf) on the existing page
    output.add_page(existing_pdf.pages[0])
    page = existing_pdf.pages[1]
    page.merge_page(new_pdf.pages[0])
    output.add_page(page)
    # finally, write "output" to a real file
    output.write(outputStream)
    f.close()
