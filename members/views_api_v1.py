# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from django.shortcuts import get_object_or_404
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.permissions import BasePermission
from .models import Adhesion, Structure, AdherentStatus
from .serializers_v1 import AdhesionListSerializer, AdhesionDetailSerializer, StructureSerializer
from .utils import current_season


class APIPermission1(BasePermission):
    def has_permission(self, request, view):
        meta = view.queryset.model._meta
        return request.user.has_perm('{}.apiv1_{}'.format(meta.app_label, meta.model_name))


class AdhesionRetrieveAPIView(RetrieveAPIView):
    queryset = Adhesion.objects.all()
    serializer_class = AdhesionDetailSerializer
    lookup_field = 'adherent__id'
    permission_classes = [APIPermission1]

    def get_queryset(self):
        season = self.request.GET.get('season') or current_season()
        return self.queryset.select_related('adherent', 'adherent__person', 'structure') \
            .filter(season=season, canceled=False, adherent__status=AdherentStatus.OK)


class AdhesionListAPIView(ListAPIView):
    queryset = Adhesion.objects.all()
    serializer_class = AdhesionListSerializer
    permission_classes = [APIPermission1]
    pagination_class = None

    def get_queryset(self):
        season = self.request.GET.get('season') or current_season()
        structure_id = self.request.GET.get('structure') or 1
        structure = get_object_or_404(Structure, id=structure_id)
        return self.queryset.select_related(
            'adherent',
            'adherent__person',
            'adherent__person__legal_guardian1',
            'adherent__person__legal_guardian2',
            'structure',
            'structure__region',
        ).only(
            'adherent__id',
            'adherent__person__first_name',
            'adherent__person__last_name',
            'adherent__person__birthdate',
            'adherent__person__email',
            'adherent__person__legal_guardian1__email',
            'adherent__person__legal_guardian2__email',
            'structure__name',
            'structure__region__name',
        ).filter(
            season=season,
            canceled=False,
            adherent__status=AdherentStatus.OK,
            structure__lft__gte=structure.lft,
            structure__lft__lte=structure.rght,
        )


class StructureListAPIView(ListAPIView):
    queryset = Structure.objects.all()
    serializer_class = StructureSerializer
    permission_classes = [APIPermission1]
    pagination_class = None
    pagination_class = None

    def get_queryset(self):
        season = self.request.GET.get('season') or current_season()
        return self.queryset.filter(adhesions__season=season).distinct()
