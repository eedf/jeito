# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from pathlib import Path
from io import StringIO
import hmac
import json
from freezegun import freeze_time
from datetime import date as Date
from psycopg2.extras import DateRange
from unittest.mock import MagicMock, patch
from django.conf import settings
from django.contrib.auth.hashers import make_password
from django.core import mail
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.management import call_command
from django.test import TestCase, TransactionTestCase
from django.test.utils import override_settings
from members import factories
from members import forms
from members import models
from members.utils import next_transfer_date


@override_settings(HOST='testserver')
@freeze_time("2015-01-01")
class ImportTests(TransactionTestCase):
    def setUp(self):
        # Create a user with permission to create adhesions
        root_structure = factories.StructureFactory()
        self.user = factories.UserFactory()
        self.person = self.user.person
        self.adhesion = factories.AdhesionFactory(
            adherent__person=self.person,
            structure__parent=root_structure,  # Entries are not created on root structure
            structure__iban='GB33BUKB20201555555555',
            structure__type__can_import=True,
        )
        self.person = self.adhesion.adherent.person
        self.structure = self.adhesion.structure
        self.team = factories.TeamFactory(
            name="Vacances adaptées",
            structure=self.adhesion.structure,
            structure__type=self.adhesion.structure.type,
            type__management=True,
        )
        self.function = factories.FunctionFactory(
            person=self.person,
            team=self.team,
            function_config__team_type=self.team.type,
        )
        self.adhesion.rate.structure_types.add(self.adhesion.structure.type)
        self.role_config = models.RoleConfig.objects.create(
            name="Gérer les adhésions d'une structure",
            for_structure=True,
        )
        models.Attribution.objects.create(
            role_config=self.role_config,
            team_type=self.team.type,
            function_type=self.function.function_config.function_type,
        )
        self.function_config = factories.FunctionConfigFactory(
            team_type=self.team.type,
            name="Participant·e vacances adaptées",
            function_type__category=models.FunctionCategory.EMPLOYEE,
        )
        self.rate = factories.RateFactory(
            name="Séjour vacances adaptées",
            season=2015,
            rate=30
        )
        self.country = models.Country.objects.create(name="Syldavie")
        self.commune = models.Commune.objects.create(name="Trifouilli", code=99999, dates=(None, None))
        self.client.force_login(self.person.user)

    @override_settings(IMPORT_RATE_NAME="Séjour vacances adaptées")
    def test_adhesion_import(self):
        self.role_config.permissions.set([
            models.Permission.objects.get(codename='import_adhesion', content_type__app_label='members'),
        ])
        with open(Path(__file__).parent / 'test_adhesion_import.xlsx', 'rb') as f:
            response = self.client.post('/adhesion/import/', {
                'structure': self.structure.id,
                'file': f
            }, follow=True)
        self.assertRedirects(response, '/adhesion/')
        self.assertContains(response, "1 adhésions ont bien été importées :")
        adhesion = models.Adhesion.objects.latest('pk')
        self.assertEqual(adhesion.season, 2015)
        self.assertEqual(adhesion.structure, self.structure)
        self.assertEqual(adhesion.rate, self.rate)
        adherent = adhesion.adherent
        self.assertContains(response, f"- DUPONT PIERRE [ADH-{adherent.id:06}] (Créée)")
        self.assertEqual(adherent.last_adhesion, adhesion)
        person = adherent.person
        self.assertEqual(person.last_name, "DUPONT")
        self.assertEqual(person.first_name, "PIERRE")
        self.assertEqual(person.gender, "M")
        self.assertEqual(person.address1, "Rue principale")
        function = person.functions.get()
        self.assertEqual(function.function_config, self.function_config)
        self.assertEqual(function.team, self.team)
        entry = adhesion.entry_set.get()
        self.assertEqual(entry.amount, 30)

    def test_employment_import(self):
        self.role_config.permissions.set([
            models.Permission.objects.get(codename='import_employment', content_type__app_label='members'),
            models.Permission.objects.get(codename='view_employment', content_type__app_label='members'),
        ])
        with open(Path(__file__).parent / 'test_employment_import.xlsx', 'rb') as f:
            response = self.client.post('/employment/import/', {
                'structure': self.structure.id,
                'file': f
            }, follow=True)
        self.assertRedirects(response, '/employment/')
        self.assertContains(response, "1 contrats de travail ont bien été importés :")
        employment = models.Employment.objects.latest('pk')
        self.assertEqual(employment.type, models.EmploymentType.CEE)
        self.assertEqual(employment.begin, Date(2015, 7, 1))
        self.assertEqual(employment.end, Date(2015, 7, 16))
        employee = employment.employee
        self.assertContains(response, f"- DUPONT PIERRE [CEE-{employee.id:06}] (Créée)")
        self.assertEqual(employee.last_employment, employment)
        person = employee.person
        self.assertEqual(person.last_name, "DUPONT")
        self.assertEqual(person.first_name, "PIERRE")
        self.assertEqual(person.gender, "M")
        self.assertEqual(person.address1, "Rue principale")
        function = person.functions.get()
        self.assertEqual(function.function_config, self.function_config)
        self.assertEqual(function.team, self.team)


class TransferTests(TestCase):
    def test_next_transfer_date(self):
        CASES = (
            ('2020-01-01', '2020-02-01'),
            ('2020-01-15', '2020-02-01'),
            ('2020-01-31', '2020-02-01'),
            ('2020-02-01', '2020-03-01'),
            ('2020-02-28', '2020-03-01'),
            ('2020-12-19', '2020-12-20'),
            ('2020-12-20', '2021-02-01'),
            ('2020-12-31', '2021-02-01'),
        )
        for today, expected in CASES:
            with freeze_time(today):
                self.assertEqual(next_transfer_date(), Date.fromisoformat(expected))


@override_settings(HOST='testserver')
class LoginTests(TestCase):
    def test_email(self):
        person = models.Person.objects.create(email='toto@tata.com', email_as_login=True)
        models.User.objects.create(person=person, password=make_password('toto'))
        logged = self.client.login(username='toto@tata.com', password='toto')
        self.assertTrue(logged)

    def test_non_login_email(self):
        factories.PersonFactory(email='toto@tata.com')
        person = factories.PersonFactory(email='toto@tata.com')
        models.User.objects.create(person=person, password=make_password('toto'))
        logged = self.client.login(username='toto@tata.com', password='toto')
        self.assertFalse(logged)

    def test_adherent_number(self):
        person = models.Person.objects.create()
        adherent = models.Adherent.objects.create(id=42, person=person)
        models.User.objects.create(person=person, password=make_password('toto'))
        logged = self.client.login(username=str(adherent.id), password='toto')
        self.assertTrue(logged)

    def test_employee_number(self):
        person = models.Person.objects.create()
        employee = models.Employee.objects.create(id=24, person=person)
        models.User.objects.create(person=person, password=make_password('toto'))
        logged = self.client.login(username=str(employee.id), password='toto')
        self.assertTrue(logged)

    def test_username(self):
        models.User.objects.create(username='tata', password=make_password('toto'))
        logged = self.client.login(username='tata', password='toto')
        self.assertTrue(logged)


@freeze_time("2015-01-01")
@override_settings(HOST='testserver')
class AdhesionWizardTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Create a user with permission to create adhesions
        cls.user = factories.UserFactory()
        cls.person = cls.user.person
        cls.adhesion = factories.AdhesionFactory(adherent__person=cls.person)
        team = factories.TeamFactory(
            structure=cls.adhesion.structure,
            structure__type=cls.adhesion.structure.type,
            type__management=True,
        )
        cls.function = factories.FunctionFactory(
            person=cls.person,
            team=team,
            function_config__team_type=team.type,
        )
        cls.adhesion.rate.structure_types.add(cls.adhesion.structure.type)
        role_config = models.RoleConfig.objects.create(
            name="Gestion des adhésions d'une structure",
            for_structure=True,
        )
        role_config.permissions.set([
            models.Permission.objects.get(codename='add_adhesion', content_type__app_label='members'),
        ])
        models.Attribution.objects.create(
            role_config=role_config,
            team_type=team.type,
            function_type=cls.function.function_config.function_type,
        )
        cls.country = models.Country.objects.create(name="France", id=99100)
        cls.commune = models.Commune.objects.create(name="Trifouilli", code=99999, dates=(None, None))

    def setUp(self):
        self.client.force_login(self.person.user)

    def test_minor(self):
        response = self.client.get('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=2005-01-01')
        self.assertContains(response, "Étape 1 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=2005-01-01', {
            'create-1617aae635e9e4d2d5d07ee64c1746c5-current_step': 'person',
            'person-gender': 'M',
            'person-last_name': 'TEST',
            'person-first_name': 'Person',
            'person-birthdate': '01/01/2005',
            'person-birth_country': self.country.pk,
            'person-birth_commune': self.commune.pk,
            'person-address1': 'Chemin du test',
            'person-postal_code': '12345',
            'person-city': 'Testville',
            'person-image_rights': 'True',
        })
        self.assertContains(response, "Étape 2 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=2005-01-01', {
            'create-1617aae635e9e4d2d5d07ee64c1746c5-current_step': 'search_guardian1',
            'search_guardian1-mode': 'New',
        })
        self.assertContains(response, "Étape 3 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=2005-01-01', {
            'create-1617aae635e9e4d2d5d07ee64c1746c5-current_step': 'guardian1',
            'guardian1-gender': 'F',
            'guardian1-last_name': 'TEST',
            'guardian1-first_name': 'Guardian',
            'guardian1-birthdate': '01/01/1985',
            'guardian1-address1': 'Chemin du test',
            'guardian1-postal_code': '12345',
            'guardian1-city': 'Testville',
            'guardian1-email': 'toto2@tata.com',
        })
        self.assertContains(response, "Étape 4 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=2005-01-01', {
            'create-1617aae635e9e4d2d5d07ee64c1746c5-current_step': 'search_guardian2',
            'search_guardian2-mode': 'N/A',
        })
        self.assertContains(response, "Étape 5 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=2005-01-01', {
            'create-1617aae635e9e4d2d5d07ee64c1746c5-current_step': 'permission',
            'permission-permission_alone': True,
            'permission-permission_with_bool': True,
            'permission-permission_with': "Pierre DUPOND",
        })
        self.assertContains(response, "Étape 6 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=2005-01-01', {
            'create-1617aae635e9e4d2d5d07ee64c1746c5-current_step': 'adhesion',
            'adhesion-structure': self.adhesion.structure.pk,
            'adhesion-begin': '15/12/2014',
        })
        self.assertContains(response, "Étape 7 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=2005-01-01', {
            'create-1617aae635e9e4d2d5d07ee64c1746c5-current_step': 'function',
            'function-team': self.function.team.pk,
            'function-function_config': self.function.function_config.pk,
        })
        self.assertContains(response, "Étape 8 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=2005-01-01', {
            'create-1617aae635e9e4d2d5d07ee64c1746c5-current_step': 'contribution',
            'contribution-rate': self.adhesion.rate.pk,
            'contribution-donation': '50',
        })
        self.assertContains(response, "Étape 9 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=2005-01-01', {
            'create-1617aae635e9e4d2d5d07ee64c1746c5-current_step': 'search_payer',
            'search_payer-mode': 'G1',
        })
        self.assertContains(response, "Étape 10 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=2005-01-01', {
            'create-1617aae635e9e4d2d5d07ee64c1746c5-current_step': 'payment',
            'payment-amount': '72',
            'payment-date': '15/12/2014',
            'payment-method': '1',
        })
        self.assertRedirects(response, '/adhesion/')
        adhesion = models.Adhesion.objects.latest('pk')
        self.assertEqual(adhesion.structure, self.adhesion.structure)
        self.assertEqual(adhesion.rate, self.adhesion.rate)
        self.assertEqual(adhesion.begin.isoformat(), '2014-12-15')
        self.assertEqual(adhesion.end.isoformat(), '2015-08-31')
        self.assertEqual(adhesion.donation, 50)
        self.assertTrue(adhesion.is_paid)
        adherent = adhesion.adherent
        self.assertEqual(adherent.last_adhesion, adhesion)
        person = adherent.person
        self.assertEqual(person.gender, 'M')
        self.assertEqual(person.last_name, 'TEST')
        self.assertEqual(person.first_name, 'PERSON')
        self.assertEqual(person.birthdate.isoformat(), '2005-01-01')
        self.assertEqual(person.birth_place_display, 'Trifouilli (99), France')
        self.assertEqual(person.address1, 'Chemin du test')
        self.assertEqual(person.postal_code, '12345')
        self.assertEqual(person.city, 'Testville')
        self.assertEqual(person.image_rights, True)
        self.assertIsNone(person.legal_guardian2)
        self.assertEqual(person.permission_alone, True)
        self.assertEqual(person.permission_with, "PIERRE DUPOND")
        self.assertFalse(person.has_protected)
        self.assertFalse(person.has_payment)
        self.assertFalse(person.expired)
        guardian = person.legal_guardian1
        self.assertEqual(guardian.gender, 'F')
        self.assertEqual(guardian.last_name, 'TEST')
        self.assertEqual(guardian.first_name, 'GUARDIAN')
        self.assertEqual(guardian.birthdate.isoformat(), '1985-01-01')
        self.assertEqual(guardian.address1, 'Chemin du test')
        self.assertEqual(guardian.postal_code, '12345')
        self.assertEqual(guardian.city, 'Testville')
        self.assertTrue(guardian.has_protected)
        self.assertTrue(guardian.has_payment)
        self.assertFalse(guardian.expired)
        function = person.functions.get()
        self.assertEqual(function.function_config, self.function.function_config)
        self.assertEqual(function.team, self.function.team)

    def test_major(self):
        response = self.client.get('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=1985-01-01')
        self.assertContains(response, "Étape 1 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=1985-01-01', {
            'create-caf800d3996213650853e3d35a3d295c-current_step': 'person',
            'person-gender': 'M',
            'person-last_name': 'TEST',
            'person-first_name': 'Person',
            'person-birthdate': '01/01/1985',
            'person-birth_country': self.country.pk,
            'person-birth_commune': self.commune.pk,
            'person-address1': 'Chemin du test',
            'person-postal_code': '12345',
            'person-city': 'Testville',
            'person-email': 'toto@tata.com',
            'person-image_rights': 'True',
        })
        self.assertContains(response, "Étape 2 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=1985-01-01', {
            'create-caf800d3996213650853e3d35a3d295c-current_step': 'adhesion',
            'adhesion-structure': self.adhesion.structure.pk,
            'adhesion-begin': '15/12/2014',
        })
        self.assertContains(response, "Étape 3 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=1985-01-01', {
            'create-caf800d3996213650853e3d35a3d295c-current_step': 'function',
            'function-team': self.function.team.pk,
            'function-function_config': self.function.function_config.pk,
        })
        self.assertContains(response, "Étape 4 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=1985-01-01', {
            'create-caf800d3996213650853e3d35a3d295c-current_step': 'contribution',
            'contribution-rate': self.adhesion.rate.pk,
            'contribution-donation': '50',
        })
        self.assertContains(response, "Étape 5 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=1985-01-01', {
            'create-caf800d3996213650853e3d35a3d295c-current_step': 'search_payer',
            'search_payer-mode': 'Adh',
        })
        self.assertContains(response, "Étape 6 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=1985-01-01', {
            'create-caf800d3996213650853e3d35a3d295c-current_step': 'payment',
            'payment-amount': '72',
            'payment-date': '15/12/2014',
            'payment-method': '1',
        })
        self.assertRedirects(response, '/adhesion/')
        adhesion = models.Adhesion.objects.latest('pk')
        self.assertEqual(adhesion.structure, self.adhesion.structure)
        self.assertEqual(adhesion.rate, self.adhesion.rate)
        self.assertEqual(adhesion.begin.isoformat(), '2014-12-15')
        self.assertEqual(adhesion.end.isoformat(), '2015-08-31')
        self.assertEqual(adhesion.donation, 50)
        self.assertTrue(adhesion.is_paid)
        person = adhesion.adherent.person
        self.assertEqual(person.gender, 'M')
        self.assertEqual(person.last_name, 'TEST')
        self.assertEqual(person.first_name, 'PERSON')
        self.assertEqual(person.birthdate.isoformat(), '1985-01-01')
        self.assertEqual(person.birth_place_display, 'Trifouilli (99), France')
        self.assertEqual(person.address1, 'Chemin du test')
        self.assertEqual(person.postal_code, '12345')
        self.assertEqual(person.city, 'Testville')
        self.assertEqual(person.image_rights, True)
        self.assertFalse(person.has_protected)
        self.assertTrue(person.has_payment)
        self.assertFalse(person.expired)
        function = adhesion.adherent.person.functions.get()
        self.assertEqual(function.function_config, self.function.function_config)
        self.assertEqual(function.team, self.function.team)

    def test_transform(self):
        rate = models.Rate.objects.create(
            name="Tarif découverte",
            rate=5,
            season=2015,
            function_category=models.FunctionCategory.PARTICIPANT,
            category=models.RateCategory.DISCOVERY,
            duration=2,
        )
        rate.structure_types.add(self.adhesion.structure.type)
        response = self.client.get('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=2005-01-01')
        self.assertContains(response, "Étape 1 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=2005-01-01', {
            'create-1617aae635e9e4d2d5d07ee64c1746c5-current_step': 'person',
            'person-gender': 'M',
            'person-last_name': 'TEST',
            'person-first_name': 'Person',
            'person-birthdate': '01/01/2005',
            'person-birth_country': self.country.pk,
            'person-birth_commune': self.commune.pk,
            'person-address1': 'Chemin du test',
            'person-postal_code': '12345',
            'person-city': 'Testville',
            'person-image_rights': 'True',
        })
        self.assertContains(response, "Étape 2 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=2005-01-01', {
            'create-1617aae635e9e4d2d5d07ee64c1746c5-current_step': 'search_guardian1',
            'search_guardian1-mode': 'New',
        })
        self.assertContains(response, "Étape 3 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=2005-01-01', {
            'create-1617aae635e9e4d2d5d07ee64c1746c5-current_step': 'guardian1',
            'guardian1-gender': 'F',
            'guardian1-last_name': 'TEST',
            'guardian1-first_name': 'Guardian',
            'guardian1-birthdate': '01/01/1985',
            'guardian1-address1': 'Chemin du test',
            'guardian1-postal_code': '12345',
            'guardian1-city': 'Testville',
            'guardian1-email': 'toto2@tata.com',
        })
        self.assertContains(response, "Étape 4 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=2005-01-01', {
            'create-1617aae635e9e4d2d5d07ee64c1746c5-current_step': 'search_guardian2',
            'search_guardian2-mode': 'N/A',
        })
        self.assertContains(response, "Étape 5 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=2005-01-01', {
            'create-1617aae635e9e4d2d5d07ee64c1746c5-current_step': 'permission',
            'permission-permission_alone': True,
            'permission-permission_with_bool': True,
            'permission-permission_with': "Pierre DUPOND",
        })
        self.assertContains(response, "Étape 6 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=2005-01-01', {
            'create-1617aae635e9e4d2d5d07ee64c1746c5-current_step': 'adhesion',
            'adhesion-structure': self.adhesion.structure.pk,
            'adhesion-begin': '15/12/2014',
        })
        self.assertContains(response, "Étape 7 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=2005-01-01', {
            'create-1617aae635e9e4d2d5d07ee64c1746c5-current_step': 'function',
            'function-team': self.function.team.pk,
            'function-function_config': self.function.function_config.pk,
        })
        self.assertContains(response, "Étape 8 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=2005-01-01', {
            'create-1617aae635e9e4d2d5d07ee64c1746c5-current_step': 'contribution',
            'contribution-rate': rate.pk,
            'contribution-donation': '0',
        })
        self.assertContains(response, "Étape 9 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=2005-01-01', {
            'create-1617aae635e9e4d2d5d07ee64c1746c5-current_step': 'search_payer',
            'search_payer-mode': 'G1',
        })
        self.assertContains(response, "Étape 10 :")
        response = self.client.post('/adhesion/create/?last_name=TEST&first_name=Person&birthdate=2005-01-01', {
            'create-1617aae635e9e4d2d5d07ee64c1746c5-current_step': 'payment',
            'payment-amount': '5',
            'payment-date': '15/12/2014',
            'payment-method': '1',
        })
        self.assertRedirects(response, '/adhesion/')
        adhesion = models.Adhesion.objects.latest('pk')
        self.assertEqual(adhesion.structure, self.adhesion.structure)
        self.assertEqual(adhesion.rate, rate)
        self.assertEqual(adhesion.begin.isoformat(), '2014-12-15')
        self.assertEqual(adhesion.end.isoformat(), '2014-12-16')
        self.assertEqual(adhesion.donation, 0)
        self.assertTrue(adhesion.is_paid)
        self.assertTrue(adhesion.is_discovery)
        adherent = adhesion.adherent
        self.assertEqual(adherent.last_adhesion, adhesion)
        person = adherent.person
        self.assertEqual(person.gender, 'M')
        self.assertEqual(person.last_name, 'TEST')
        self.assertEqual(person.first_name, 'PERSON')
        self.assertEqual(person.birthdate.isoformat(), '2005-01-01')
        self.assertEqual(person.birth_place_display, 'Trifouilli (99), France')
        self.assertEqual(person.address1, 'Chemin du test')
        self.assertEqual(person.postal_code, '12345')
        self.assertEqual(person.city, 'Testville')
        self.assertEqual(person.image_rights, True)
        self.assertIsNone(person.legal_guardian2)
        self.assertEqual(person.permission_alone, True)
        self.assertEqual(person.permission_with, "PIERRE DUPOND")
        self.assertFalse(person.has_protected)
        self.assertFalse(person.has_payment)
        self.assertFalse(person.expired)
        guardian = person.legal_guardian1
        self.assertEqual(guardian.gender, 'F')
        self.assertEqual(guardian.last_name, 'TEST')
        self.assertEqual(guardian.first_name, 'GUARDIAN')
        self.assertEqual(guardian.birthdate.isoformat(), '1985-01-01')
        self.assertEqual(guardian.address1, 'Chemin du test')
        self.assertEqual(guardian.postal_code, '12345')
        self.assertEqual(guardian.city, 'Testville')
        self.assertTrue(guardian.has_protected)
        self.assertTrue(guardian.has_payment)
        self.assertFalse(guardian.expired)
        function = person.functions.get()
        self.assertEqual(function.function_config, self.function.function_config)
        self.assertEqual(function.team, self.function.team)
        response = self.client.get(f'/adhesion/{person.pk}/renew/')
        self.assertContains(response, "Étape 1 :")
        response = self.client.post(f'/adhesion/{person.pk}/renew/', {
            f'renew{person.pk}-current_step': 'adhesion',
            'adhesion-structure': self.adhesion.structure.pk,
            'adhesion-begin': '15/12/2014',
        })
        self.assertContains(response, "Étape 2 :")
        response = self.client.post(f'/adhesion/{person.pk}/renew/', {
            f'renew{person.pk}-current_step': 'function',
            'function-team': self.function.team.pk,
            'function-function_config': self.function.function_config.pk,
        })
        self.assertContains(response, "Étape 3 :")
        response = self.client.post(f'/adhesion/{person.pk}/renew/', {
            f'renew{person.pk}-current_step': 'contribution',
            'contribution-rate': self.adhesion.rate.pk,
            'contribution-donation': '0',
        })
        self.assertContains(response, "Étape 4 :")
        response = self.client.post(f'/adhesion/{person.pk}/renew/', {
            f'renew{person.pk}-current_step': 'search_payer',
            'search_payer-mode': 'G1',
        })
        self.assertContains(response, "Étape 5 :")
        response = self.client.post(f'/adhesion/{person.pk}/renew/', {
            f'renew{person.pk}-current_step': 'payment',
            'payment-amount': '17',
            'payment-date': '15/12/2014',
            'payment-method': '1',
        })
        self.assertRedirects(response, '/adhesion/')
        adhesion = models.Adhesion.objects.latest('pk')
        self.assertEqual(adhesion.structure, self.adhesion.structure)
        self.assertEqual(adhesion.rate, self.adhesion.rate)
        self.assertEqual(adhesion.begin.isoformat(), '2014-12-15')
        self.assertEqual(adhesion.end.isoformat(), '2015-08-31')
        self.assertEqual(adhesion.donation, 0)
        self.assertTrue(adhesion.is_paid)
        self.assertFalse(adhesion.is_discovery)


@freeze_time("2015-01-01")
@override_settings(HOST='testserver', BRED_API_PWD='motdepassetest')
class OnlineAdhesionWizardTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        models.Season.objects.create(
            year=2015,
            card=SimpleUploadedFile('card.pdf', b''),
            form=SimpleUploadedFile('form.pdf', b''),
        )
        cls.team = factories.TeamFactory()
        cls.structure = cls.team.structure
        cls.function_config = factories.FunctionConfigFactory(team_type=cls.team.type)
        cls.rate = factories.RateFactory()
        cls.rate.structure_types.set([cls.structure.type])
        cls.invitation = factories.InvitationFactory(
            structure=cls.structure,
            team=cls.team,
            function_config=cls.function_config,
        )
        cls.country = models.Country.objects.create(name="France", id=99100)
        cls.commune = models.Commune.objects.create(name="Trifouilli", code=99999, dates=(None, None))

    @patch('members.views.requests')
    def test_new(self, mock_requests):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json.return_value = {
            'status': 'SUCCESS',
            'answer': {
                'formToken': 'testtoken',
            },
        }
        mock_requests.post.return_value = mock_response

        response = self.client.get(f'/adhesion/create/{self.invitation.token}/')
        self.assertContains(response, "Étape 1 :")
        self.assertContains(response, f"En tant que <b>{self.function_config.name}</b>")
        self.assertContains(response, f"Sur la structure locale de <b>{self.structure.name}</b>")
        self.assertContains(response, "Du <b>1 janvier 2015</b> au <b>31 août 2015</b>")
        response = self.client.post(f'/adhesion/create/{self.invitation.token}/', {
            'create-99914b932bd37a50b983c5e7c90ae93b-current_step': 'pre_confirm',
            'pre_confirm-last_name': 'TEST',
            'pre_confirm-first_name': 'Person',
            'pre_confirm-birthdate': '01/01/1985',
            'pre_confirm-confirm': 'True',
        })
        self.assertContains(response, "Étape 2 :")
        response = self.client.post(f'/adhesion/create/{self.invitation.token}/', {
            'create-99914b932bd37a50b983c5e7c90ae93b-current_step': 'person',
            'person-gender': 'M',
            'person-birthdate': '01/01/1985',
            'person-birth_country': self.country.pk,
            'person-birth_commune': self.commune.pk,
            'person-address1': 'Chemin du test',
            'person-postal_code': '12345',
            'person-city': 'Testville',
            'person-email': 'toto@tata.com',
            'person-image_rights': 'True',
        })
        self.assertContains(response, "Étape 3 :")
        response = self.client.post(f'/adhesion/create/{self.invitation.token}/', {
            'create-99914b932bd37a50b983c5e7c90ae93b-current_step': 'contribution',
            'contribution-rate': self.rate.pk,
            'contribution-donation': '0',
        })
        self.assertContains(response, "Étape 4 :")
        response = self.client.post(f'/adhesion/create/{self.invitation.token}/', {
            'create-99914b932bd37a50b983c5e7c90ae93b-current_step': 'post_confirm',
            'post_confirm-signatory': 'Moi-même',
            'post_confirm-confirm': 'True',
            'post_confirm-values': 'True',
        })
        self.assertRedirects(response, f'/adhesion/pre-pay/{self.invitation.token}/')
        adhesion = models.Adhesion.objects.get()
        self.assertEqual(adhesion.structure, self.structure)
        self.assertEqual(adhesion.rate, self.rate)
        self.assertEqual(adhesion.begin.isoformat(), '2015-01-01')
        self.assertEqual(adhesion.end.isoformat(), '2015-08-31')
        self.assertEqual(adhesion.donation, 0)
        self.assertEqual(adhesion.signatory, 'Moi-même')
        self.assertFalse(adhesion.is_paid)
        person = adhesion.adherent.person
        self.assertEqual(person.gender, 'M')
        self.assertEqual(person.last_name, 'TEST')
        self.assertEqual(person.first_name, 'PERSON')
        self.assertEqual(person.birthdate.isoformat(), '1985-01-01')
        self.assertEqual(person.birth_place_display, 'Trifouilli (99), France')
        self.assertEqual(person.address1, 'Chemin du test')
        self.assertEqual(person.postal_code, '12345')
        self.assertEqual(person.city, 'Testville')
        self.assertEqual(person.image_rights, True)
        self.assertFalse(person.has_protected)
        self.assertFalse(person.has_payment)
        self.assertFalse(person.expired)
        function = adhesion.adherent.person.functions.get()
        self.assertEqual(function.function_config, self.function_config)
        self.assertEqual(function.team, self.team)
        response = self.client.get(f'/adhesion/pre-pay/{self.invitation.token}/')
        self.assertContains(response, "Étape 1 :")
        response = self.client.post(f'/adhesion/pre-pay/{self.invitation.token}/', {
            'online_pre_pay_view-current_step': 'select_invitations',
            'select_invitations-invitations': str(self.invitation.pk),
        })
        self.assertContains(response, "Étape 2 :")
        response = self.client.post(f'/adhesion/pre-pay/{self.invitation.token}/', {
            'online_pre_pay_view-current_step': 'search_payer',
            'search_payer-mode': 'Adh',
        })
        self.assertContains(response, "kr-embedded")
        payment = models.Payment.objects.get()
        self.assertEqual(payment.amount, 22)
        self.assertEqual(payment.date.isoformat(), '2015-01-01')
        self.assertEqual(payment.payer, person)
        self.assertEqual(payment.method, 4)
        self.assertEqual(payment.online_status, models.PaymentStatus.IN_PROGRESS)
        self.assertTrue(payment.payer.has_payment)
        self.assertFalse(payment.allocation_set.all()[0].adhesion.is_paid)
        response = self.client.get(f'/adhesion/post-pay/{payment.pk}/')
        self.assertContains(response, "a bien été payée")
        answer = json.dumps({
            'orderDetails': {
                'orderId': str(payment.pk),
            },
            'orderCycle': 'CLOSED',
            'orderStatus': 'PAID',
        })
        hash = hmac.new(settings.BRED_API_PWD.encode('ascii'), answer.encode('utf8'), 'sha256').hexdigest()
        response = self.client.post('/ipn/', {
            'kr-answer': answer,
            'kr-hash': hash,
        })
        self.assertEqual(response.status_code, 200)
        payment = models.Payment.objects.get()
        self.assertEqual(payment.online_status, models.PaymentStatus.SUCCESS)
        adhesion = models.Adhesion.objects.get()
        self.assertTrue(adhesion.is_paid)


@freeze_time("2015-01-01")
@override_settings(HOST='testserver')
class AdhesionWizardPermissionsTests(TestCase):
    def test_create_anonymous(self):
        response = self.client.get('/adhesion/create/')
        self.assertRedirects(response, '/login/?next=/adhesion/create/')

    @override_settings(LOGGING={})
    def test_create(self):
        self.client.force_login(models.User.objects.create())
        response = self.client.get('/adhesion/create/')
        self.assertContains(response, "Tu n&#x27;es pas autorisé à gérer les adhésions", status_code=403)

    def test_renew_anonymous(self):
        response = self.client.get('/adhesion/999999/renew/')
        self.assertRedirects(response, '/login/?next=/adhesion/999999/renew/')

    def test_invitation(self):
        models.Season.objects.create(year=2015, form="form.pdf")
        invitation = factories.InvitationFactory()
        response = self.client.get(f'/adhesion/create/{invitation.token}/')
        self.assertContains(response, "Rejoignez-nous !")


@override_settings(HOST='testserver')
class ReceiptTest(TestCase):
    def test_receipt(self):
        payment = factories.AllocationFactory().payment
        receipt = models.Receipt.create(payment)
        receipt.send()
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, f"[EEDF] Votre Reçu fiscal n°J-{receipt.number}")


@freeze_time("2014-09-01")
@override_settings(HOST='testserver')
class GraphTest(TestCase):
    def test_first_day_of_season(self):
        self.client.force_login(factories.UserFactory())
        response = self.client.get('/graphique/data/', {
            'former': 'on',
            'season': '2015',
            'branch': '',
            'function': '',
            'rate': '',
        })
        self.assertContains(response, b"La courbe n\'est visible qu\'\\u00e0 partir du 2 septembre.")


@override_settings(HOST='testserver')
class RemindTest(TestCase):
    def test_remind(self):
        adhesion = factories.AdhesionFactory()
        factories.InvitationFactory(adhesion=adhesion)

        # Before 7 days
        out1 = StringIO()
        with freeze_time("2014-10-22"):
            call_command('remind', stdout=out1)
        self.assertNotIn(str(adhesion), out1.getvalue())

        # After 7 days
        out2 = StringIO()
        with freeze_time("2014-10-23"):
            call_command('remind', stdout=out2)
        self.assertIn(str(adhesion), out2.getvalue())

        # After first remind
        out3 = StringIO()
        with freeze_time("2014-11-15"):
            call_command('remind', stdout=out3)
        self.assertNotIn(str(adhesion), out3.getvalue())


@override_settings(HOST='testserver')
@freeze_time("2015-01-01")
class APIv1Tests(TestCase):
    maxDiff = None

    @classmethod
    def setUpTestData(cls):
        cls.user = models.User.objects.create(is_superuser=True)
        cls.adhesion = factories.AdhesionFactory.create()
        cls.adherent = cls.adhesion.adherent
        cls.structure = factories.StructureFactory.create()

    def setUp(self):
        self.client.force_login(self.user)

    def test_adhesion_list(self):
        response = self.client.get(f'/api/v1/adhesions/?structure={self.adhesion.structure.id}')
        self.assertJSONEqual(response.content, [{
            'birthdate': '1980-01-01',
            'email': 'person000@toto.com',
            'parent_emails': [],
            'first_name': 'Firstname 000',
            'last_name': 'Lastname 000',
            'number': '000001',
            'structure': 'Name 000',
            'functions': [],
        }])

    def test_adhesion_detail(self):
        response = self.client.get(f'/api/v1/adhesions/{self.adherent.id}/?structure={self.adhesion.structure.id}')
        self.assertJSONEqual(response.content, {
            'adhesions_resp_email': None,
            'birthdate': '1980-01-01',
            'email': 'person000@toto.com',
            'first_name': 'Firstname 000',
            'last_name': 'Lastname 000',
            'nominations': [],
            'number': '000001',
            'rate': 'Rate 000',
            'region': None,
            'structure': 'Name 000',
            'structure_resp_email': None,
            'structure_type': '',
        })

    def test_structure_list(self):
        response = self.client.get('/api/v1/structures/')
        self.assertJSONEqual(response.content, [{
            'headcount': 1,
            'name': 'Name 000',
            'region': None,
            'subtype': None,
            'type': '',
        }])


@override_settings(HOST='testserver')
@freeze_time("2015-01-01")
class APIv3Tests(TestCase):
    maxDiff = None

    @classmethod
    def setUpTestData(cls):
        cls.commune = models.Commune.objects.create(name="Trifouilly-les-Oies", code='99999', dates=(None, None))
        cls.country = models.Country.objects.create(name="France", id=99100)
        cls.guardian1 = models.Person.objects.create(
            first_name='Prénom maman test',
            gender='F',
        )
        cls.guardian2 = models.Person.objects.create(
            first_name='Prénom papa test',
            gender='M',
        )
        cls.person = models.Person.objects.create(
            address1='Adresse test 1',
            address2='Adresse test 2',
            address3='Adresse test 3',
            birth_country=cls.country,
            birth_commune=cls.commune,
            birthdate='2005-01-01',
            city='Ville test',
            country='Pays test',
            email='email@test.com',
            family_name='Nom de famille test',
            first_name='Prénom test',
            gender='F',
            home_phone='+33999999999',
            image_rights=True,
            last_name='Nom test',
            legal_guardian1=cls.guardian1,
            legal_guardian2=cls.guardian2,
            permission_alone=True,
            permission_with="Permission prénom nom test",
            nickname='Surnom test',
            mobile_phone='+33888888888',
            postal_code='99999',
            profession='Profession test',
        )
        cls.user = models.User.objects.create(
            is_superuser=True,
            person=cls.person,
        )
        cls.structure_type = models.StructureType.objects.create(
            echelon=models.Echelon.LOCAL,
            name="Type de structure test",
            can_import=False,
        )
        cls.structure = models.Structure.objects.create(
            name="Structure test",
            parent=None,
            type=cls.structure_type,
            status=models.Structure.STATUS_AUTONOMOUS,
            place='Localisation test',
            public_email='Email public test',
            public_phone='+33999999999',
        )
        cls.team_type = models.TeamType.objects.create(
            name="Type d'équipe test",
            management=True,
        )
        cls.team = models.Team.objects.create(
            structure=cls.structure,
            type=cls.team_type,
            name="Equipe test",
            creation_date='2012-10-01T12:00:00+02:00',
            deactivation_date=None,
        )
        cls.adherent = models.Adherent.objects.create(
            id=1,
            person=cls.person,
        )
        cls.rate = models.Rate.objects.create(
            name="Tarif test",
            season=2016,
            rate=20,
            category=1,
            function_category=models.FunctionCategory.PARTICIPANT,
            age_min=8,
            age_max=11,
        )
        cls.rate.structure_types.set([cls.structure_type]),
        cls.adhesion = models.Adhesion.objects.create(
            adherent=cls.adherent,
            season=2016,
            dates=DateRange(Date(2015, 9, 15), Date(2016, 9, 1)),
            entry_date='2015-09-16',
            rate=cls.rate,
            structure=cls.structure,
            donation=10,
        )
        cls.employee = models.Employee.objects.create(
            person=cls.person,
            email='mail.travail@test.com',
            phone='7777777777',
        )
        cls.employment = models.Employment.objects.create(
            employee=cls.employee,
            type=models.EmploymentType.SAL,
            name="Intitulé de poste test",
            dates=DateRange(Date(2015, 9, 15), Date(2016, 8, 31), '[]'),
        )
        cls.function_type = models.FunctionType.objects.create(
            name="Type de fonction test",
            category=models.FunctionCategory.PARTICIPANT,
        )
        cls.function_config = models.FunctionConfig.objects.create(
            function_type=cls.function_type,
            team_type=cls.team_type,
            structure_type=cls.structure_type,
            name="Configuration de fonction test",
            age_min=8,
            age_max=11,
        )
        cls.function = models.Function.objects.create(
            person=cls.person,
            team=cls.team,
            function_config=cls.function_config,
            dates=DateRange(Date(2015, 9, 15), Date(2016, 8, 31), '[]'),
            name="Fonction test",
            status=models.FunctionStatus.OK,
        )

    def setUp(self):
        self.client.force_login(self.person.user)

    def test_structure_type(self):
        response = self.client.get('/api/v3/structure_types/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'echelon': 3,
                'id': self.structure_type.id,
                'name': 'Type de structure test'
            }]
        })

    def test_structure(self):
        response = self.client.get('/api/v3/structures/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'id': self.structure.id,
                'name': 'Structure test',
                'parent_id': None,
                'type_id': self.structure_type.id,
                'status': 1,
                'place': 'Localisation test',
                'public_email': 'Email public test',
                'public_phone': '+33999999999',
            }]
        })

    def test_team_type(self):
        response = self.client.get('/api/v3/team_types/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'id': self.team_type.id,
                'name': 'Type d\'équipe test',
                'management': True,
            }]
        })

    def test_team(self):
        response = self.client.get('/api/v3/teams/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'id': self.team.id,
                'structure_id': self.structure.id,
                'type_id': self.team_type.id,
                'name': 'Equipe test',
                'creation_date': '2012-10-01T12:00:00+02:00',
                'deactivation_date': None,
            }]
        })

    def test_person(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v3/persons/')
        self.assertJSONEqual(response.content, {
            'count': 3,
            'next': None,
            'previous': None,
            'results': [{
                'id': self.guardian1.id,
                'address1': '',
                'address2': '',
                'address3': '',
                'birth_place': '',
                'birthdate': None,
                'city': '',
                'country': '',
                'email': '',
                'family_name': '',
                'first_name': 'Prénom maman test',
                'gender': 'F',
                'home_phone': '',
                'image_rights': None,
                'last_name': '',
                'legal_guardian1_id': None,
                'legal_guardian2_id': None,
                'mobile_phone': '',
                'nickname': '',
                'permission_alone': None,
                'permission_first_name': None,
                'permission_last_name': None,
                'permission_with': None,
                'postal_code': '',
                'profession': '',
            }, {
                'id': self.guardian2.id,
                'address1': '',
                'address2': '',
                'address3': '',
                'birth_place': '',
                'birthdate': None,
                'city': '',
                'country': '',
                'email': '',
                'family_name': '',
                'first_name': 'Prénom papa test',
                'gender': 'M',
                'home_phone': '',
                'image_rights': None,
                'last_name': '',
                'legal_guardian1_id': None,
                'legal_guardian2_id': None,
                'mobile_phone': '',
                'nickname': '',
                'permission_alone': None,
                'permission_first_name': None,
                'permission_last_name': None,
                'permission_with': None,
                'postal_code': '',
                'profession': '',
            }, {
                'id': self.person.id,
                'address1': 'Adresse test 1',
                'address2': 'Adresse test 2',
                'address3': 'Adresse test 3',
                'birth_place': 'Trifouilly-les-Oies (99), France',
                'birthdate': '2005-01-01',
                'city': 'Ville test',
                'country': 'Pays test',
                'email': 'email@test.com',
                'family_name': 'Nom de famille test',
                'first_name': 'Prénom test',
                'gender': 'F',
                'home_phone': '+33999999999',
                'image_rights': True,
                'last_name': 'Nom test',
                'legal_guardian1_id': self.guardian1.id,
                'legal_guardian2_id': self.guardian2.id,
                'mobile_phone': '+33888888888',
                'nickname': 'Surnom test',
                'permission_alone': True,
                'permission_first_name': "Permission",
                'permission_last_name': "prénom nom test",
                'permission_with': True,
                'postal_code': '99999',
                'profession': 'Profession test',
            }]
        })
        response = self.client.get('/api/v3/persons/?season=2015')
        self.assertEqual(response.json()['count'], 0)
        response = self.client.get('/api/v3/persons/?season=2016')
        self.assertEqual(response.json()['count'], 3)
        response = self.client.get('/api/v3/persons/?season=2017')
        self.assertEqual(response.json()['count'], 0)

    def test_adherent(self):
        response = self.client.get('/api/v3/adherents/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'duplicates': [],
                'id': self.adherent.id,
                'person_id': self.person.id,
                'status': 1
            }]
        })
        response = self.client.get('/api/v3/adherents/?season=2015')
        self.assertEqual(response.json()['count'], 0)
        response = self.client.get('/api/v3/adherents/?season=2016')
        self.assertEqual(response.json()['count'], 1)
        response = self.client.get('/api/v3/adherents/?season=2017')
        self.assertEqual(response.json()['count'], 0)

    def test_get_ok_adherents(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v3/adherents/?status=1')
        self.assertEqual(response.json()['count'], 1)

    def test_get_excluded_suspended_adherents(self):
        with self.assertNumQueries(3):
            response = self.client.get('/api/v3/adherents/?status=3,4')
        self.assertEqual(response.json()['count'], 0)

    def test_adhesion(self):
        response = self.client.get('/api/v3/adhesions/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'adherent_id': self.adherent.id,
                'begin': '2015-09-15',
                'canceled': False,
                'end': '2016-08-31',
                'entry_date': '2015-09-16',
                'id': self.adhesion.id,
                'season': 2016,
                'structure_id': self.structure.id
            }]
        })
        response = self.client.get('/api/v3/adhesions/?season=2015')
        self.assertEqual(response.json()['count'], 0)
        response = self.client.get('/api/v3/adhesions/?season=2016')
        self.assertEqual(response.json()['count'], 1)
        response = self.client.get('/api/v3/adhesions/?season=2017')
        self.assertEqual(response.json()['count'], 0)

    def test_employee(self):
        response = self.client.get('/api/v3/employees/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'email': 'mail.travail@test.com',
                'id': self.employee.id,
                'person_id': self.person.id,
                'phone': '7777777777'
            }]
        })
        response = self.client.get('/api/v3/employees/?season=2015')
        self.assertEqual(response.json()['count'], 0)
        response = self.client.get('/api/v3/employees/?season=2016')
        self.assertEqual(response.json()['count'], 1)
        response = self.client.get('/api/v3/employees/?season=2017')
        self.assertEqual(response.json()['count'], 0)

    def test_employment(self):
        response = self.client.get('/api/v3/employments/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'begin': '2015-09-15',
                'employee_id': self.employee.id,
                'end': '2016-08-31',
                'id': self.employment.id,
                'name': 'Intitulé de poste test',
                'type': 2
            }]
        })
        response = self.client.get('/api/v3/employments/?season=2015')
        self.assertEqual(response.json()['count'], 0)
        response = self.client.get('/api/v3/employments/?season=2016')
        self.assertEqual(response.json()['count'], 1)
        response = self.client.get('/api/v3/employments/?season=2017')
        self.assertEqual(response.json()['count'], 0)

    def test_function_type(self):
        response = self.client.get('/api/v3/function_types/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'category': 2,
                'id': self.function_type.id,
                'name': 'Type de fonction test',
            }]
        })

    def test_function(self):
        response = self.client.get('/api/v3/functions/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'begin': '2015-09-15',
                'end': '2016-08-31',
                'id': self.function.id,
                'name': 'Fonction test',
                'person_id': self.person.id,
                'team_id': self.team.id,
                'type_id': self.function_type.id
            }]
        })
        response = self.client.get('/api/v3/functions/?season=2015')
        self.assertEqual(response.json()['count'], 0)
        response = self.client.get('/api/v3/functions/?season=2016')
        self.assertEqual(response.json()['count'], 1)
        response = self.client.get('/api/v3/functions/?season=2017')
        self.assertEqual(response.json()['count'], 0)


@override_settings(HOST='testserver')
@freeze_time("2015-01-01")
class APIv4Tests(TestCase):
    maxDiff = None

    @classmethod
    def setUpTestData(cls):
        cls.commune = models.Commune.objects.create(name="Trifouilly-les-Oies", code='99999', dates=(None, None))
        cls.country = models.Country.objects.create(name="France", id=99100)
        cls.guardian1 = models.Person.objects.create(
            uuid='6b865252-7ed5-4eef-9444-e9352b4fa61d',
            first_name='Prénom maman test',
            gender='F',
        )
        cls.guardian2 = models.Person.objects.create(
            uuid='794ae0ba-a961-4b21-8c10-48c98e868f2a',
            first_name='Prénom papa test',
            gender='M',
        )
        cls.person = models.Person.objects.create(
            uuid='b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            address1='Adresse test 1',
            address2='Adresse test 2',
            address3='Adresse test 3',
            birth_country=cls.country,
            birth_commune=cls.commune,
            birthdate='2005-01-01',
            city='Ville test',
            country='Pays test',
            email='email@test.com',
            family_name='Nom de famille test',
            first_name='Prénom test',
            gender='F',
            home_phone='+33999999999',
            image_rights=True,
            last_name='Nom test',
            legal_guardian1=cls.guardian1,
            legal_guardian2=cls.guardian2,
            permission_alone=True,
            permission_with="Permission prénom nom test",
            mobile_phone='+33888888888',
            nickname='Surnom test',
            postal_code='99999',
            profession='Profession test',
        )
        cls.user = models.User.objects.create(
            is_superuser=True,
            person=cls.person,
        )
        cls.structure_type = models.StructureType.objects.create(
            uuid='fd8b90dc-c902-4fcc-98cf-1c8f8e40a3dd',
            echelon=models.Echelon.LOCAL,
            name="Type de structure test",
            can_import=False,
        )
        cls.parent_structure = models.Structure.objects.create(
            uuid='7303a2c4-1873-43e2-a31f-a8a0043623c1',
            name="Structure parente test",
            parent=None,
            type=cls.structure_type,
            status=models.Structure.STATUS_AUTONOMOUS,
        )
        cls.structure = models.Structure.objects.create(
            uuid='6303a2c4-1873-43e2-a31f-a8a0043623c1',
            name="Structure test",
            parent=cls.parent_structure,
            type=cls.structure_type,
            status=models.Structure.STATUS_AUTONOMOUS,
            place='Localisation test',
            public_email='Email public test',
            public_phone='+33999999999',
        )
        cls.team_type = models.TeamType.objects.create(
            uuid='d6e89333-d148-4479-8817-1498a2ac1c5a',
            name="Type d'équipe test",
            management=True,
        )
        cls.team = models.Team.objects.create(
            uuid='be1d9521-9d23-4887-9873-e7d8e2e245c6',
            structure=cls.structure,
            type=cls.team_type,
            name="Equipe test",
            creation_date='2012-10-01T12:00:00+02:00',
            deactivation_date=None,
        )
        cls.adherent = models.Adherent.objects.create(
            uuid='ba67115e-5fc3-4945-9577-a1425e97ed0d',
            id=1,
            person=cls.person,
        )
        cls.rate = models.Rate.objects.create(
            name="Tarif test",
            season=2016,
            rate=20,
            category=1,
            function_category=models.FunctionCategory.PARTICIPANT,
            age_min=8,
            age_max=11,
        )
        cls.rate.structure_types.set([cls.structure_type]),
        cls.adhesion = models.Adhesion.objects.create(
            uuid='54328081-4527-4c66-93b3-ae908bc67c0c',
            adherent=cls.adherent,
            season=2016,
            dates=DateRange(Date(2015, 9, 15), Date(2016, 9, 1)),
            entry_date='2015-09-16',
            rate=cls.rate,
            structure=cls.structure,
            donation=10,
        )
        cls.employee = models.Employee.objects.create(
            uuid='54c002e4-6671-46a6-9081-4869f6a7e5c1',
            person=cls.person,
            email='mail.travail@test.com',
            phone='7777777777',
        )
        cls.employment = models.Employment.objects.create(
            uuid='8ead35cc-fc10-4c42-9278-6e4671f4017f',
            employee=cls.employee,
            type=models.EmploymentType.SAL,
            name="Intitulé de poste test",
            dates=DateRange(Date(2015, 9, 15), Date(2016, 8, 31), '[]'),
        )
        cls.function_type = models.FunctionType.objects.create(
            uuid='80f7fa40-1410-4054-96a1-3f6fd25a9841',
            name="Type de fonction test",
            category=models.FunctionCategory.PARTICIPANT,
        )
        cls.function_config = models.FunctionConfig.objects.create(
            function_type=cls.function_type,
            team_type=cls.team_type,
            structure_type=cls.structure_type,
            name="Configuration de fonction test",
            age_min=8,
            age_max=11,
        )
        cls.function = models.Function.objects.create(
            uuid='2cc55278-e305-4215-bc6d-db5367e7efe7',
            person=cls.person,
            team=cls.team,
            function_config=cls.function_config,
            dates=DateRange(Date(2015, 9, 15), Date(2016, 8, 31), '[]'),
            name="Fonction test",
            status=models.FunctionStatus.OK,
            owner=cls.user,
        )
        cls.roleconfig = models.RoleConfig.objects.create(
            uuid='2cc55278-e305-4215-bc6d-db5367e7efe8',
            name='Roleconfig test',
            for_all=True,
        )
        cls.roleconfig_permission = models.RoleConfigPermission.objects.create(
            uuid='2cc55278-e305-4215-bc6d-db5367e7efea',
            roleconfig=cls.roleconfig,
            permission=models.Permission.objects.get(codename='view_person'),
        )

    def setUp(self):
        self.client.force_login(self.user)

    def test_get_structure_types(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v4/structure_type/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'echelon': 3,
                'uuid': 'fd8b90dc-c902-4fcc-98cf-1c8f8e40a3dd',
                'id': self.structure_type.id,
                'name': 'Type de structure test'
            }]
        })

    def test_get_structures(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v4/structure/')
        self.assertJSONEqual(response.content, {
            'count': 2,
            'next': None,
            'previous': None,
            'results': [{
                'uuid': '7303a2c4-1873-43e2-a31f-a8a0043623c1',
                'id': self.parent_structure.id,
                'name': 'Structure parente test',
                'parent_uuid': None,
                'region_uuid': None,
                'type_uuid': 'fd8b90dc-c902-4fcc-98cf-1c8f8e40a3dd',
                'status': 1,
                'place': '',
                'public_email': '',
                'public_phone': '',
            }, {
                'uuid': '6303a2c4-1873-43e2-a31f-a8a0043623c1',
                'id': self.structure.id,
                'name': 'Structure test',
                'parent_uuid': '7303a2c4-1873-43e2-a31f-a8a0043623c1',
                'region_uuid': None,
                'type_uuid': 'fd8b90dc-c902-4fcc-98cf-1c8f8e40a3dd',
                'status': 1,
                'place': 'Localisation test',
                'public_email': 'Email public test',
                'public_phone': '+33999999999',
            }]
        })

    def test_get_team_types(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v4/team_type/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'uuid': 'd6e89333-d148-4479-8817-1498a2ac1c5a',
                'id': self.team_type.id,
                'name': 'Type d\'équipe test',
                'management': True,
            }]
        })

    def test_get_teams(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v4/team/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
                'id': self.team.id,
                'structure_uuid': '6303a2c4-1873-43e2-a31f-a8a0043623c1',
                'type_uuid': 'd6e89333-d148-4479-8817-1498a2ac1c5a',
                'name': 'Equipe test',
                'creation_date': '2012-10-01T12:00:00+02:00',
                'deactivation_date': None,
            }]
        })

    def test_get_persons(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v4/person/')
        self.assertJSONEqual(response.content, {
            'count': 3,
            'next': None,
            'previous': None,
            'results': [{
                'uuid': '6b865252-7ed5-4eef-9444-e9352b4fa61d',
                'id': self.guardian1.id,
                'address1': '',
                'address2': '',
                'address3': '',
                'birth_place': '',
                'birthdate': None,
                'city': '',
                'country': '',
                'email': '',
                'family_name': '',
                'first_name': 'Prénom maman test',
                'gender': 'F',
                'home_phone': '',
                'image_rights': None,
                'last_name': '',
                'legal_guardian1_uuid': None,
                'legal_guardian2_uuid': None,
                'mobile_phone': '',
                'nickname': '',
                'permission_alone': None,
                'permission_first_name': None,
                'permission_last_name': None,
                'permission_with': None,
                'postal_code': '',
                'profession': '',
            }, {
                'uuid': '794ae0ba-a961-4b21-8c10-48c98e868f2a',
                'id': self.guardian2.id,
                'address1': '',
                'address2': '',
                'address3': '',
                'birth_place': '',
                'birthdate': None,
                'city': '',
                'country': '',
                'email': '',
                'family_name': '',
                'first_name': 'Prénom papa test',
                'gender': 'M',
                'home_phone': '',
                'image_rights': None,
                'last_name': '',
                'legal_guardian1_uuid': None,
                'legal_guardian2_uuid': None,
                'mobile_phone': '',
                'nickname': '',
                'permission_alone': None,
                'permission_first_name': None,
                'permission_last_name': None,
                'permission_with': None,
                'postal_code': '',
                'profession': '',
            }, {
                'uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
                'id': self.person.id,
                'address1': 'Adresse test 1',
                'address2': 'Adresse test 2',
                'address3': 'Adresse test 3',
                'birth_place': 'Trifouilly-les-Oies (99), France',
                'birthdate': '2005-01-01',
                'city': 'Ville test',
                'country': 'Pays test',
                'email': 'email@test.com',
                'first_name': 'Prénom test',
                'family_name': 'Nom de famille test',
                'gender': 'F',
                'home_phone': '+33999999999',
                'image_rights': True,
                'last_name': 'Nom test',
                'legal_guardian1_uuid': '6b865252-7ed5-4eef-9444-e9352b4fa61d',
                'legal_guardian2_uuid': '794ae0ba-a961-4b21-8c10-48c98e868f2a',
                'mobile_phone': '+33888888888',
                'nickname': 'Surnom test',
                'permission_alone': True,
                'permission_first_name': "Permission",
                'permission_last_name': "prénom nom test",
                'permission_with': True,
                'postal_code': '99999',
                'profession': 'Profession test',
            }]
        })
        # season parameter
        response = self.client.get('/api/v4/person/?season=2015')
        self.assertEqual(response.json()['count'], 0)
        response = self.client.get('/api/v4/person/?season=2016')
        self.assertEqual(response.json()['count'], 3)
        response = self.client.get('/api/v4/person/?season=2017')
        self.assertEqual(response.json()['count'], 0)
        response = self.client.get('/api/v4/person/?season=xxxx')
        self.assertEqual(response.status_code, 400)
        # year parameter
        response = self.client.get('/api/v4/person/?year=2014')
        self.assertEqual(response.json()['count'], 0)
        response = self.client.get('/api/v4/person/?year=2015')
        self.assertEqual(response.json()['count'], 3)
        response = self.client.get('/api/v4/person/?year=2016')
        self.assertEqual(response.json()['count'], 3)
        response = self.client.get('/api/v4/person/?year=2017')
        self.assertEqual(response.json()['count'], 0)
        response = self.client.get('/api/v4/person/?year=xxxx')
        self.assertEqual(response.status_code, 400)

    def test_get_adherents(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v4/adherent/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'duplicates': [],
                'id': self.adherent.id,
                'uuid': 'ba67115e-5fc3-4945-9577-a1425e97ed0d',
                'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
                'status': 1
            }]
        })
        response = self.client.get('/api/v4/adherent/?season=2015')
        self.assertEqual(response.json()['count'], 0)
        with self.assertNumQueries(4):
            response = self.client.get('/api/v4/adherent/?season=2016')
        self.assertEqual(response.json()['count'], 1)
        response = self.client.get('/api/v4/adherent/?season=2017')
        self.assertEqual(response.json()['count'], 0)

    def test_get_ok_adherents(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v4/adherent/?status=1')
        self.assertEqual(response.json()['count'], 1)

    def test_get_excluded_suspended_adherents(self):
        with self.assertNumQueries(3):
            response = self.client.get('/api/v4/adherent/?status=3,4')
        self.assertEqual(response.json()['count'], 0)

    def test_get_adhesions(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v4/adhesion/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'uuid': '54328081-4527-4c66-93b3-ae908bc67c0c',
                'id': self.adhesion.id,
                'adherent_uuid': 'ba67115e-5fc3-4945-9577-a1425e97ed0d',
                'begin': '2015-09-15',
                'canceled': False,
                'end': '2016-08-31',
                'entry_date': '2015-09-16',
                'season': 2016,
                'structure_uuid': '6303a2c4-1873-43e2-a31f-a8a0043623c1',
            }]
        })
        response = self.client.get('/api/v4/adhesion/?season=2015')
        self.assertEqual(response.json()['count'], 0)
        with self.assertNumQueries(4):
            response = self.client.get('/api/v4/adhesion/?season=2016')
        self.assertEqual(response.json()['count'], 1)
        response = self.client.get('/api/v4/adhesion/?season=2017')
        self.assertEqual(response.json()['count'], 0)

    def test_get_employees(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v4/employee/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'email': 'mail.travail@test.com',
                'id': self.employee.id,
                'uuid': '54c002e4-6671-46a6-9081-4869f6a7e5c1',
                'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
                'phone': '7777777777'
            }]
        })
        response = self.client.get('/api/v4/employee/?season=2015')
        self.assertEqual(response.json()['count'], 0)
        with self.assertNumQueries(4):
            response = self.client.get('/api/v4/employee/?season=2016')
        self.assertEqual(response.json()['count'], 1)
        response = self.client.get('/api/v4/employee/?season=2017')
        self.assertEqual(response.json()['count'], 0)

    def test_get_employments(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v4/employment/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'begin': '2015-09-15',
                'employee_uuid': '54c002e4-6671-46a6-9081-4869f6a7e5c1',
                'end': '2016-08-31',
                'uuid': '8ead35cc-fc10-4c42-9278-6e4671f4017f',
                'id': self.employment.id,
                'name': 'Intitulé de poste test',
                'type': 2
            }]
        })
        response = self.client.get('/api/v4/employment/?season=2015')
        self.assertEqual(response.json()['count'], 0)
        with self.assertNumQueries(4):
            response = self.client.get('/api/v4/employment/?season=2016')
        self.assertEqual(response.json()['count'], 1)
        response = self.client.get('/api/v4/employment/?season=2017')
        self.assertEqual(response.json()['count'], 0)

    def test_get_function_types(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v4/function_type/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'category': 2,
                'uuid': '80f7fa40-1410-4054-96a1-3f6fd25a9841',
                'id': self.function_type.id,
                'name': 'Type de fonction test',
            }]
        })

    def test_get_functions(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v4/function/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'begin': '2015-09-15',
                'end': '2016-08-31',
                'uuid': '2cc55278-e305-4215-bc6d-db5367e7efe7',
                'id': self.function.id,
                'name': 'Fonction test',
                'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
                'team_uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
                'type_uuid': '80f7fa40-1410-4054-96a1-3f6fd25a9841',
            }]
        })
        response = self.client.get('/api/v4/function/?season=2015')
        self.assertEqual(response.json()['count'], 0)
        with self.assertNumQueries(4):
            response = self.client.get('/api/v4/function/?season=2016')
        self.assertEqual(response.json()['count'], 1)
        response = self.client.get('/api/v4/function/?season=2017')
        self.assertEqual(response.json()['count'], 0)

    def test_get_existing_function(self):
        response = self.client.get('/api/v4/function/2cc55278-e305-4215-bc6d-db5367e7efe7/')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {
            'begin': '2015-09-15',
            'end': '2016-08-31',
            'name': 'Fonction test',
            'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'team_uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
            'type_uuid': '80f7fa40-1410-4054-96a1-3f6fd25a9841',
            'uuid': '2cc55278-e305-4215-bc6d-db5367e7efe7',
            'id': self.function.id,
        })

    def test_get_not_existing_function(self):
        response = self.client.get('/api/v4/function/2cc55278-e305-4215-bc6d-db5367e7efe8/')
        self.assertEqual(response.status_code, 404)

    def test_put_existing_function(self):
        response = self.client.put('/api/v4/function/2cc55278-e305-4215-bc6d-db5367e7efe7/', {
            'begin': '2017-09-15',
            'end': '2018-08-31',
            'name': 'Fonction test bis',
            'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'team_uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
            'type_uuid': '80f7fa40-1410-4054-96a1-3f6fd25a9841',
            'uuid': '2cc55278-e305-4215-bc6d-db5367e7efe7',
        }, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {
            'begin': '2017-09-15',
            'end': '2018-08-31',
            'name': 'Fonction test bis',
            'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'team_uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
            'type_uuid': '80f7fa40-1410-4054-96a1-3f6fd25a9841',
            'uuid': '2cc55278-e305-4215-bc6d-db5367e7efe7',
            'id': self.function.id,
        })

    def test_put_existing_function_wrong_uuid(self):
        response = self.client.put('/api/v4/function/2cc55278-e305-4215-bc6d-db5367e7efe7/', {
            'begin': '2017-09-15',
            'end': '2018-08-31',
            'name': 'Fonction test bis',
            'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'team_uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
            'type_uuid': '80f7fa40-1410-4054-96a1-3f6fd25a9841',
            'uuid': '2cc55278-e305-4215-bc6d-db5367e7efe8',
        }, content_type='application/json')
        self.assertEqual(response.status_code, 400)
        self.assertJSONEqual(response.content, {'uuid': ['Mismatch.']})

    def test_put_not_owner(self):
        self.function.owner = models.User.objects.create()
        self.function.save()
        response = self.client.put('/api/v4/function/2cc55278-e305-4215-bc6d-db5367e7efe7/', {
            'begin': '2017-09-15',
            'end': '2018-08-31',
            'name': 'Fonction test bis',
            'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'team_uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
            'type_uuid': '80f7fa40-1410-4054-96a1-3f6fd25a9841',
            'uuid': '2cc55278-e305-4215-bc6d-db5367e7efe7',
        }, content_type='application/json')
        self.assertEqual(response.status_code, 403)

    def test_put_unexisting_function(self):
        response = self.client.put('/api/v4/function/2cc55278-e305-4215-bc6d-db5367e7efe8/', {
            'begin': '2017-09-15',
            'end': '2018-08-31',
            'name': 'Fonction test bis',
            'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'team_uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
            'type_uuid': '80f7fa40-1410-4054-96a1-3f6fd25a9841',
            'uuid': '2cc55278-e305-4215-bc6d-db5367e7efe8',
        }, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        new_function = models.Function.objects.exclude(id=self.function.id).get()
        self.assertJSONEqual(response.content, {
            'begin': '2017-09-15',
            'end': '2018-08-31',
            'name': 'Fonction test bis',
            'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'team_uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
            'type_uuid': '80f7fa40-1410-4054-96a1-3f6fd25a9841',
            'uuid': '2cc55278-e305-4215-bc6d-db5367e7efe8',
            'id': new_function.id,
        })

    def test_patch_existing_function(self):
        response = self.client.patch('/api/v4/function/2cc55278-e305-4215-bc6d-db5367e7efe7/', {
            'begin': '2015-09-01',
        }, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {
            'begin': '2015-09-01',
            'end': '2016-08-31',
            'name': 'Fonction test',
            'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'team_uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
            'type_uuid': '80f7fa40-1410-4054-96a1-3f6fd25a9841',
            'uuid': '2cc55278-e305-4215-bc6d-db5367e7efe7',
            'id': self.function.id,
        })

    def test_patch_wrong_person(self):
        response = self.client.patch('/api/v4/function/2cc55278-e305-4215-bc6d-db5367e7efe7/', {
            'person_uuid': '2cc55278-e305-4215-bc6d-db5367e7efe8',
        }, content_type='application/json')
        self.assertEqual(response.status_code, 400)
        self.assertJSONEqual(response.content, {
            'person_uuid': ['Not found.'],
        })

    def test_post_functions(self):
        response = self.client.post('/api/v4/function/', [{
            'begin': '2017-09-15',
            'end': '2018-08-31',
            'name': 'Fonction test bis',
            'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'team_uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
            'type_uuid': '80f7fa40-1410-4054-96a1-3f6fd25a9841',
            'uuid': '2cc55278-e305-4215-bc6d-db5367e7efe7',
        }, {
            'begin': '2017-09-15',
            'end': '2018-08-31',
            'name': 'Fonction test ter',
            'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'team_uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
            'type_uuid': '80f7fa40-1410-4054-96a1-3f6fd25a9841',
            'uuid': '2cc55278-e305-4215-bc6d-db5367e7efe8',
        }], content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {'created': 1, 'updated': 1})
        self.assertQuerysetEqual(models.Function.objects.all(), [
            "Configuration de fonction test (Fonction test bis)",
            "Configuration de fonction test (Fonction test ter)",
        ], transform=str, ordered=False)

    def test_delete_existing_function(self):
        response = self.client.delete('/api/v4/function/2cc55278-e305-4215-bc6d-db5367e7efe7/')
        self.assertEqual(response.status_code, 204)

    def test_delete_not_existing_function(self):
        response = self.client.delete('/api/v4/function/2cc55278-e305-4215-bc6d-db5367e7efe8/')
        self.assertEqual(response.status_code, 404)

    def test_delete_function_not_owner(self):
        self.function.owner = models.User.objects.create()
        self.function.save()
        response = self.client.delete('/api/v4/function/2cc55278-e305-4215-bc6d-db5367e7efe7/')
        self.assertEqual(response.status_code, 403)

    def test_post_existing_structure(self):
        response = self.client.post('/api/v4/structure/', [{
            'uuid': '6303a2c4-1873-43e2-a31f-a8a0043623c1',
            'status': models.Structure.STATUS_GUARDIANSHIP,
        }], content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {'updated': 1})
        self.assertEqual(
            models.Structure.objects.get(uuid='6303a2c4-1873-43e2-a31f-a8a0043623c1').status,
            models.Structure.STATUS_GUARDIANSHIP,
        )

    def test_post_not_existing_structure(self):
        response = self.client.post('/api/v4/structure/', [{
            'uuid': '6303a2c4-1873-43e2-a31f-a8a0043623c2',
            'status': models.Structure.STATUS_GUARDIANSHIP,
        }], content_type='application/json')
        self.assertEqual(response.status_code, 404)

    def test_post_structure_wrong_value(self):
        response = self.client.post('/api/v4/structure/', [{
            'uuid': '6303a2c4-1873-43e2-a31f-a8a0043623c1',
            'status': models.Structure.STATUS_CLOSED,
        }], content_type='application/json')
        self.assertEqual(response.status_code, 400)
        self.assertJSONEqual(response.content, {
            'status': ["Seuls les statuts 1 (autonome) et 2 (rattachée) sont autorisés."],
        })

    def test_post_closed_structure(self):
        self.structure.status = models.Structure.STATUS_CLOSED
        self.structure.save()
        response = self.client.post('/api/v4/structure/', [{
            'uuid': '6303a2c4-1873-43e2-a31f-a8a0043623c1',
            'status': models.Structure.STATUS_AUTONOMOUS,
        }], content_type='application/json')
        self.assertEqual(response.status_code, 400)
        self.assertJSONEqual(response.content, {
            'status': ["Une structure fermée ne peut être ré-ouverte."],
        })

    def test_post_structure_wrong_field(self):
        response = self.client.post('/api/v4/structure/', [{
            'uuid': '6303a2c4-1873-43e2-a31f-a8a0043623c1',
            'status': models.Structure.STATUS_GUARDIANSHIP,
            'name': 'Toto',
        }], content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            models.Structure.objects.get(uuid='6303a2c4-1873-43e2-a31f-a8a0043623c1').name,
            "Structure test",
        )

    def test_delete_structure(self):
        response = self.client.delete('/api/v4/structure/6303a2c4-1873-43e2-a31f-a8a0043623c1/')
        self.assertEqual(response.status_code, 405)

    def test_patch_existing_structure(self):
        response = self.client.patch('/api/v4/structure/6303a2c4-1873-43e2-a31f-a8a0043623c1/', {
            'status': models.Structure.STATUS_GUARDIANSHIP,
        }, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            models.Structure.objects.get(uuid='6303a2c4-1873-43e2-a31f-a8a0043623c1').status,
            models.Structure.STATUS_GUARDIANSHIP,
        )

    def test_patch_not_existing_structure(self):
        response = self.client.patch('/api/v4/structure/6303a2c4-1873-43e2-a31f-a8a0043623c2/', {
            'status': models.Structure.STATUS_GUARDIANSHIP,
        }, content_type='application/json')
        self.assertEqual(response.status_code, 404)

    def test_get_roleconfig_permissions(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v4/role_config_permission/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'app_label': 'members',
                'codename': 'view_person',
                'id': self.roleconfig_permission.pk,
                'model': 'person',
                'roleconfig_uuid': '2cc55278-e305-4215-bc6d-db5367e7efe8',
                'uuid': '2cc55278-e305-4215-bc6d-db5367e7efea'
            }]
        })

    def test_get_roleconfig_permissions_with_app_label(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v4/role_config_permission/?app_label=members')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'app_label': 'members',
                'codename': 'view_person',
                'id': self.roleconfig_permission.pk,
                'model': 'person',
                'roleconfig_uuid': '2cc55278-e305-4215-bc6d-db5367e7efe8',
                'uuid': '2cc55278-e305-4215-bc6d-db5367e7efea'
            }]
        })

    def test_get_roleconfig_permissions_with_other_app_label(self):
        with self.assertNumQueries(3):
            response = self.client.get('/api/v4/role_config_permission/?app_label=auth')
        self.assertJSONEqual(response.content, {
            'count': 0,
            'next': None,
            'previous': None,
            'results': []
        })

    def test_get_roleconfig_permissions_with_multiple_app_labels(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v4/role_config_permission/?app_label=auth,members')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'app_label': 'members',
                'codename': 'view_person',
                'id': self.roleconfig_permission.pk,
                'model': 'person',
                'roleconfig_uuid': '2cc55278-e305-4215-bc6d-db5367e7efe8',
                'uuid': '2cc55278-e305-4215-bc6d-db5367e7efea'
            }]
        })


@override_settings(HOST='testserver')
@freeze_time("2015-01-01")
class APIv5Tests(TestCase):
    maxDiff = None

    @classmethod
    def setUpTestData(cls):
        cls.country = models.Country.objects.create(
            uuid='6b865252-7ed5-4eef-9444-e9352b4ff61d',
            name="France",
            id=99100,
        )
        cls.commune = models.Commune.objects.create(
            uuid='6b865252-7ed5-4eef-9444-e9352b4ff61e',
            name="Trifouilly-les-Oies",
            code='99999',
            dates=(None, None),
        )
        cls.guardian1 = factories.PersonFactory(
            uuid='6b865252-7ed5-4eef-9444-e9352b4fa61d',
            first_name='Prénom maman test',
            gender='F',
            birthdate=None,
            email='',
            last_name='',
        )
        cls.guardian2 = factories.PersonFactory(
            uuid='794ae0ba-a961-4b21-8c10-48c98e868f2a',
            first_name='Prénom papa test',
            gender='M',
            birthdate=None,
            email='',
            last_name='',
        )
        cls.person = factories.PersonFactory(
            uuid='b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            address1='Adresse test 1',
            address2='Adresse test 2',
            address3='Adresse test 3',
            birth_country=cls.country,
            birth_commune=cls.commune,
            birthdate='2005-01-01',
            city='Ville test',
            country='Pays test',
            email='email@test.com',
            family_name='Nom de famille test',
            first_name='Prénom test',
            gender='F',
            home_phone='+33999999999',
            image_rights=True,
            last_name='Nom test',
            legal_guardian1=cls.guardian1,
            legal_guardian2=cls.guardian2,
            permission_alone=True,
            permission_with="Permission prénom nom test",
            mobile_phone='+33666666666',
            nickname='Surnom test',
            postal_code='99999',
            profession='Profession test',
        )
        cls.user = models.User.objects.create(
            is_superuser=True,
            person=cls.person,
        )
        cls.structure_type = models.StructureType.objects.create(
            uuid='fd8b90dc-c902-4fcc-98cf-1c8f8e40a3dd',
            echelon=models.Echelon.LOCAL,
            name="Type de structure test",
            can_import=False,
        )
        cls.parent_structure = models.Structure.objects.create(
            uuid='7303a2c4-1873-43e2-a31f-a8a0043623c1',
            name="Structure parente test",
            parent=None,
            type=cls.structure_type,
            status=models.Structure.STATUS_AUTONOMOUS,
        )
        cls.structure = models.Structure.objects.create(
            uuid='6303a2c4-1873-43e2-a31f-a8a0043623c1',
            name="Structure test",
            parent=cls.parent_structure,
            type=cls.structure_type,
            status=models.Structure.STATUS_AUTONOMOUS,
            place='Localisation test',
            public_email='Email public test',
            public_phone='+33999999999',
        )
        cls.team_type = models.TeamType.objects.create(
            uuid='d6e89333-d148-4479-8817-1498a2ac1c5a',
            name="Type d'équipe test",
            management=True,
        )
        cls.team = models.Team.objects.create(
            uuid='be1d9521-9d23-4887-9873-e7d8e2e245c6',
            structure=cls.structure,
            type=cls.team_type,
            name="Equipe test",
            creation_date='2012-10-01T12:00:00+02:00',
            deactivation_date=None,
        )
        cls.adherent = models.Adherent.objects.create(
            uuid='ba67115e-5fc3-4945-9577-a1425e97ed0d',
            id=1,
            person=cls.person,
        )
        cls.rate = models.Rate.objects.create(
            name="Tarif test",
            season=2016,
            rate=20,
            category=1,
            function_category=models.FunctionCategory.PARTICIPANT,
            age_min=8,
            age_max=11,
        )
        cls.rate.structure_types.set([cls.structure_type]),
        cls.adhesion = models.Adhesion.objects.create(
            uuid='54328081-4527-4c66-93b3-ae908bc67c0c',
            adherent=cls.adherent,
            season=2016,
            dates=DateRange(Date(2015, 9, 15), Date(2016, 9, 1)),
            entry_date='2015-09-16',
            rate=cls.rate,
            structure=cls.structure,
            donation=10,
        )
        cls.invitation = models.Invitation.objects.create(
            token='5ee46443038e9af373add03f2d9473e7e76f02bf',
            season=2016,
            email=cls.adhesion.adherent.person.email,
            structure=cls.adhesion.structure,
            person=cls.adhesion.adherent.person,
            adhesion=cls.adhesion,
            host=cls.guardian1,
            date=Date(2015, 10, 9),
        )
        cls.employee = models.Employee.objects.create(
            uuid='54c002e4-6671-46a6-9081-4869f6a7e5c1',
            person=cls.person,
            email='mail.travail@test.com',
            phone='7777777777',
        )
        cls.employment = models.Employment.objects.create(
            uuid='8ead35cc-fc10-4c42-9278-6e4671f4017f',
            employee=cls.employee,
            type=models.EmploymentType.SAL,
            name="Intitulé de poste test",
            dates=DateRange(Date(2015, 9, 15), Date(2016, 8, 31), '[]'),
        )
        cls.function_type = models.FunctionType.objects.create(
            uuid='80f7fa40-1410-4054-96a1-3f6fd25a9841',
            name="Type de fonction test",
            category=models.FunctionCategory.PARTICIPANT,
        )
        cls.function_config = models.FunctionConfig.objects.create(
            function_type=cls.function_type,
            team_type=cls.team_type,
            structure_type=cls.structure_type,
            name="Configuration de fonction test",
            age_min=8,
            age_max=11,
        )
        cls.function = models.Function.objects.create(
            uuid='2cc55278-e305-4215-bc6d-db5367e7efe7',
            person=cls.person,
            team=cls.team,
            function_config=cls.function_config,
            dates=DateRange(Date(2015, 9, 15), Date(2016, 8, 31), '[]'),
            name="Fonction test",
            status=models.FunctionStatus.OK,
            owner=cls.user,
        )
        cls.roleconfig = models.RoleConfig.objects.create(
            uuid='2cc55278-e305-4215-bc6d-db5367e7efe8',
            name='Roleconfig test',
            for_all=True,
        )
        cls.roleconfig_permission = models.RoleConfigPermission.objects.create(
            uuid='2cc55278-e305-4215-bc6d-db5367e7efea',
            roleconfig=cls.roleconfig,
            permission=models.Permission.objects.get(codename='view_person'),
        )

    def setUp(self):
        self.client.force_login(self.user)

    def test_get_countries(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v5/country/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'uuid': str(self.country.uuid),
                'id': 99100,
                'name': 'France',
                'iso2': '',
                'iso3': '',
            }]
        })

    def test_get_communes(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v5/commune/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'uuid': str(self.commune.uuid),
                'id': self.commune.id,
                'name': 'Trifouilly-les-Oies',
                'code': '99999',
                'begin': None,
                'end': None,
            }]
        })

    def test_get_structure_types(self):
        response = self.client.get('/api/v5/structure_type/')
        self.assertEqual(response.status_code, 404)

    def test_get_structures(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v5/structure/')
        self.assertJSONEqual(response.content, {
            'count': 2,
            'next': None,
            'previous': None,
            'results': [{
                'uuid': '7303a2c4-1873-43e2-a31f-a8a0043623c1',
                'id': self.parent_structure.id,
                'name': 'Structure parente test',
                'parent_uuid': None,
                'region_uuid': None,
                'type': self.structure_type.id,
                'status': 1,
                'place': '',
                'public_email': '',
                'public_phone': '',
            }, {
                'uuid': '6303a2c4-1873-43e2-a31f-a8a0043623c1',
                'id': self.structure.id,
                'name': 'Structure test',
                'parent_uuid': '7303a2c4-1873-43e2-a31f-a8a0043623c1',
                'region_uuid': None,
                'type': self.structure_type.id,
                'status': 1,
                'place': 'Localisation test',
                'public_email': 'Email public test',
                'public_phone': '+33999999999',
            }]
        })

    def test_get_team_types(self):
        response = self.client.get('/api/v5/team_type/')
        self.assertEqual(response.status_code, 404)

    def test_get_teams(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v5/team/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
                'id': self.team.id,
                'structure_uuid': '6303a2c4-1873-43e2-a31f-a8a0043623c1',
                'type': self.team_type.id,
                'name': 'Equipe test',
                'creation_date': '2012-10-01T12:00:00+02:00',
                'deactivation_date': None,
            }]
        })

    def test_get_persons(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v5/person/')
        self.assertJSONEqual(response.content, {
            'count': 3,
            'next': None,
            'previous': None,
            'results': [{
                'uuid': '6b865252-7ed5-4eef-9444-e9352b4fa61d',
                'id': self.guardian1.id,
                'address1': '',
                'address2': '',
                'address3': '',
                'birth_country_uuid': None,
                'birth_commune_uuid': None,
                'birth_place': '',
                'birthdate': None,
                'city': '',
                'country': '',
                'email': '',
                'family_name': '',
                'first_name': 'Prénom maman test',
                'gender': 'F',
                'home_phone': '',
                'image_rights': None,
                'last_name': '',
                'legal_guardian1_uuid': None,
                'legal_guardian2_uuid': None,
                'merged_with': [],
                'mobile_phone': '',
                'nickname': '',
                'permission_alone': False,
                'permission_with': '',
                'postal_code': '',
                'profession': '',
            }, {
                'uuid': '794ae0ba-a961-4b21-8c10-48c98e868f2a',
                'id': self.guardian2.id,
                'address1': '',
                'address2': '',
                'address3': '',
                'birth_country_uuid': None,
                'birth_commune_uuid': None,
                'birth_place': '',
                'birthdate': None,
                'city': '',
                'country': '',
                'email': '',
                'family_name': '',
                'first_name': 'Prénom papa test',
                'gender': 'M',
                'home_phone': '',
                'image_rights': None,
                'last_name': '',
                'legal_guardian1_uuid': None,
                'legal_guardian2_uuid': None,
                'merged_with': [],
                'mobile_phone': '',
                'nickname': '',
                'permission_alone': False,
                'permission_with': '',
                'postal_code': '',
                'profession': '',
            }, {
                'uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
                'id': self.person.id,
                'address1': 'Adresse test 1',
                'address2': 'Adresse test 2',
                'address3': 'Adresse test 3',
                'birth_country_uuid': '6b865252-7ed5-4eef-9444-e9352b4ff61d',
                'birth_commune_uuid': '6b865252-7ed5-4eef-9444-e9352b4ff61e',
                'birth_place': '',
                'birthdate': '2005-01-01',
                'city': 'Ville test',
                'country': 'Pays test',
                'email': 'email@test.com',
                'first_name': 'Prénom test',
                'family_name': 'Nom de famille test',
                'gender': 'F',
                'home_phone': '+33999999999',
                'image_rights': True,
                'last_name': 'Nom test',
                'legal_guardian1_uuid': '6b865252-7ed5-4eef-9444-e9352b4fa61d',
                'legal_guardian2_uuid': '794ae0ba-a961-4b21-8c10-48c98e868f2a',
                'merged_with': [],
                'mobile_phone': '+33666666666',
                'nickname': 'Surnom test',
                'permission_alone': True,
                'permission_with': "Permission prénom nom test",
                'postal_code': '99999',
                'profession': 'Profession test',
            }]
        })
        # season parameter
        response = self.client.get('/api/v5/person/?season=2015')
        self.assertEqual(response.json()['count'], 0)
        response = self.client.get('/api/v5/person/?season=2016')
        self.assertEqual(response.json()['count'], 3)
        response = self.client.get('/api/v5/person/?season=2017')
        self.assertEqual(response.json()['count'], 0)
        response = self.client.get('/api/v5/person/?season=xxxx')
        self.assertEqual(response.status_code, 400)
        # year parameter
        response = self.client.get('/api/v5/person/?year=2014')
        self.assertEqual(response.json()['count'], 0)
        response = self.client.get('/api/v5/person/?year=2015')
        self.assertEqual(response.json()['count'], 3)
        response = self.client.get('/api/v5/person/?year=2016')
        self.assertEqual(response.json()['count'], 3)
        response = self.client.get('/api/v5/person/?year=2017')
        self.assertEqual(response.json()['count'], 0)
        response = self.client.get('/api/v5/person/?year=xxxx')
        self.assertEqual(response.status_code, 400)

    def test_get_adherents(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v5/adherent/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'duplicates': [],
                'id': self.adherent.id,
                'merged_with': [],
                'uuid': 'ba67115e-5fc3-4945-9577-a1425e97ed0d',
                'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
                'status': 1
            }]
        })
        response = self.client.get('/api/v5/adherent/?season=2015')
        self.assertEqual(response.json()['count'], 0)
        with self.assertNumQueries(4):
            response = self.client.get('/api/v5/adherent/?season=2016')
        self.assertEqual(response.json()['count'], 1)
        response = self.client.get('/api/v5/adherent/?season=2017')
        self.assertEqual(response.json()['count'], 0)

    def test_get_ok_adherents(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v5/adherent/?status=1')
        self.assertEqual(response.json()['count'], 1)

    def test_get_excluded_suspended_adherents(self):
        with self.assertNumQueries(3):
            response = self.client.get('/api/v5/adherent/?status=3,4')
        self.assertEqual(response.json()['count'], 0)

    def test_get_adhesions(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v5/adhesion/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'uuid': '54328081-4527-4c66-93b3-ae908bc67c0c',
                'id': self.adhesion.id,
                'adherent_uuid': 'ba67115e-5fc3-4945-9577-a1425e97ed0d',
                'begin': '2015-09-15',
                'canceled': False,
                'end': '2016-08-31',
                'entry_date': '2015-09-16',
                'season': 2016,
                'structure_uuid': '6303a2c4-1873-43e2-a31f-a8a0043623c1',
            }]
        })
        response = self.client.get('/api/v5/adhesion/?season=2015')
        self.assertEqual(response.json()['count'], 0)
        with self.assertNumQueries(4):
            response = self.client.get('/api/v5/adhesion/?season=2016')
        self.assertEqual(response.json()['count'], 1)
        response = self.client.get('/api/v5/adhesion/?season=2017')
        self.assertEqual(response.json()['count'], 0)

    def test_get_invitations(self):
        expected = {
            'token': '5ee46443038e9af373add03f2d9473e7e76f02bf',
            'season': 2016,
            'email': 'email@test.com',
            'structure_uuid': '6303a2c4-1873-43e2-a31f-a8a0043623c1',
            'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'adhesion_uuid': '54328081-4527-4c66-93b3-ae908bc67c0c',
            'date': '2015-10-09',
            'declined': False,
            'entry_date': None,
            'host_uuid': '6b865252-7ed5-4eef-9444-e9352b4fa61d',
            'id': self.invitation.id,
            'modification_date': None,
            'team': None,
            'team_uuid': None,
            'uuid': str(self.invitation.uuid),
        }
        with self.assertNumQueries(4):
            response = self.client.get('/api/v5/invitation/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [expected]
        })
        response = self.client.get('/api/v5/invitation/' + expected['token'] + '/')
        self.assertJSONEqual(response.content, expected)
        response = self.client.get('/api/v5/invitation/?season=2016')
        self.assertEqual(response.json()['count'], 1)
        response = self.client.get('/api/v5/invitation/?season=2017')
        self.assertEqual(response.json()['count'], 0)
        response = self.client.get('/api/v5/invitation/?year=2015')
        self.assertEqual(response.json()['count'], 1)
        response = self.client.get('/api/v5/invitation/?year=2016')
        self.assertEqual(response.json()['count'], 1)
        response = self.client.get('/api/v5/invitation/?year=2017')
        self.assertEqual(response.json()['count'], 0)

    def test_post_invitation(self):
        response = self.client.post('/api/v5/invitation/', {
            'uuid': 'c93602a5-fa99-47a9-b762-fb33fd3788e9',
            'structure_uuid': '6303a2c4-1873-43e2-a31f-a8a0043623c1',
            'email': 'invite@test.com',
        }, content_type='application/json')
        self.assertEqual(response.status_code, 201)
        new_invitation = models.Invitation.objects.exclude(id=self.invitation.id).get()
        self.assertJSONEqual(response.content, {
            'adhesion_uuid': None,
            'date': None,
            'declined': False,
            'email': 'invite@test.com',
            'entry_date': '2015-01-01',
            'id': new_invitation.id,
            'modification_date': '2015-01-01',
            'person_uuid': None,
            'season': 2015,
            'structure_uuid': '6303a2c4-1873-43e2-a31f-a8a0043623c1',
            'team': None,
            'team_uuid': None,
            'token': new_invitation.token,
            'uuid': 'c93602a5-fa99-47a9-b762-fb33fd3788e9',
        })

    def test_get_employees(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v5/employee/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'email': 'mail.travail@test.com',
                'id': self.employee.id,
                'merged_with': [],
                'uuid': '54c002e4-6671-46a6-9081-4869f6a7e5c1',
                'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
                'phone': '7777777777'
            }]
        })
        response = self.client.get('/api/v5/employee/?season=2015')
        self.assertEqual(response.json()['count'], 0)
        with self.assertNumQueries(4):
            response = self.client.get('/api/v5/employee/?season=2016')
        self.assertEqual(response.json()['count'], 1)
        response = self.client.get('/api/v5/employee/?season=2017')
        self.assertEqual(response.json()['count'], 0)

    def test_get_employments(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v5/employment/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'begin': '2015-09-15',
                'employee_uuid': '54c002e4-6671-46a6-9081-4869f6a7e5c1',
                'end': '2016-08-31',
                'uuid': '8ead35cc-fc10-4c42-9278-6e4671f4017f',
                'id': self.employment.id,
                'name': 'Intitulé de poste test',
                'type': 2
            }]
        })
        response = self.client.get('/api/v5/employment/?season=2015')
        self.assertEqual(response.json()['count'], 0)
        with self.assertNumQueries(4):
            response = self.client.get('/api/v5/employment/?season=2016')
        self.assertEqual(response.json()['count'], 1)
        response = self.client.get('/api/v5/employment/?season=2017')
        self.assertEqual(response.json()['count'], 0)

    def test_get_function_types(self):
        response = self.client.get('/api/v5/function_type/')
        self.assertEqual(response.status_code, 404)

    def test_get_functions(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v5/function/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'begin': '2015-09-15',
                'end': '2016-08-31',
                'uuid': '2cc55278-e305-4215-bc6d-db5367e7efe7',
                'id': self.function.id,
                'name': 'Fonction test',
                'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
                'team_uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
                'type': self.function_type.id,
            }]
        })
        response = self.client.get('/api/v5/function/?season=2015')
        self.assertEqual(response.json()['count'], 0)
        with self.assertNumQueries(4):
            response = self.client.get('/api/v5/function/?season=2016')
        self.assertEqual(response.json()['count'], 1)
        response = self.client.get('/api/v5/function/?season=2017')
        self.assertEqual(response.json()['count'], 0)

    def test_get_existing_function(self):
        response = self.client.get('/api/v5/function/2cc55278-e305-4215-bc6d-db5367e7efe7/')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {
            'begin': '2015-09-15',
            'end': '2016-08-31',
            'name': 'Fonction test',
            'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'team_uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
            'type': self.function_type.id,
            'uuid': '2cc55278-e305-4215-bc6d-db5367e7efe7',
            'id': self.function.id,
        })

    def test_get_not_existing_function(self):
        response = self.client.get('/api/v5/function/2cc55278-e305-4215-bc6d-db5367e7efe8/')
        self.assertEqual(response.status_code, 404)

    def test_put_existing_function(self):
        response = self.client.put('/api/v5/function/2cc55278-e305-4215-bc6d-db5367e7efe7/', {
            'begin': '2017-09-15',
            'end': '2018-08-31',
            'name': 'Fonction test bis',
            'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'team_uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
            'type': self.function_type.id,
            'uuid': '2cc55278-e305-4215-bc6d-db5367e7efe7',
        }, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {
            'begin': '2017-09-15',
            'end': '2018-08-31',
            'name': 'Fonction test bis',
            'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'team_uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
            'type': self.function_type.id,
            'uuid': '2cc55278-e305-4215-bc6d-db5367e7efe7',
            'id': self.function.id,
        })

    def test_put_existing_function_wrong_uuid(self):
        response = self.client.put('/api/v5/function/2cc55278-e305-4215-bc6d-db5367e7efe7/', {
            'begin': '2017-09-15',
            'end': '2018-08-31',
            'name': 'Fonction test bis',
            'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'team_uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
            'type': self.function_type.id,
            'uuid': '2cc55278-e305-4215-bc6d-db5367e7efe8',
        }, content_type='application/json')
        self.assertEqual(response.status_code, 400)
        self.assertJSONEqual(response.content, {'uuid': ['Mismatch.']})

    def test_put_not_owner(self):
        self.function.owner = models.User.objects.create()
        self.function.save()
        response = self.client.put('/api/v5/function/2cc55278-e305-4215-bc6d-db5367e7efe7/', {
            'begin': '2017-09-15',
            'end': '2018-08-31',
            'name': 'Fonction test bis',
            'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'team_uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
            'type': self.function_type.id,
            'uuid': '2cc55278-e305-4215-bc6d-db5367e7efe7',
        }, content_type='application/json')
        self.assertEqual(response.status_code, 403)

    def test_put_unexisting_function(self):
        response = self.client.put('/api/v5/function/2cc55278-e305-4215-bc6d-db5367e7efe8/', {
            'begin': '2017-09-15',
            'end': '2018-08-31',
            'name': 'Fonction test bis',
            'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'team_uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
            'type': self.function_type.id,
            'uuid': '2cc55278-e305-4215-bc6d-db5367e7efe8',
        }, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        new_function = models.Function.objects.exclude(id=self.function.id).get()
        self.assertJSONEqual(response.content, {
            'begin': '2017-09-15',
            'end': '2018-08-31',
            'name': 'Fonction test bis',
            'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'team_uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
            'type': self.function_type.id,
            'uuid': '2cc55278-e305-4215-bc6d-db5367e7efe8',
            'id': new_function.id,
        })

    def test_patch_existing_function(self):
        response = self.client.patch('/api/v5/function/2cc55278-e305-4215-bc6d-db5367e7efe7/', {
            'begin': '2015-09-01',
        }, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {
            'begin': '2015-09-01',
            'end': '2016-08-31',
            'name': 'Fonction test',
            'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'team_uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
            'type': self.function_type.id,
            'uuid': '2cc55278-e305-4215-bc6d-db5367e7efe7',
            'id': self.function.id,
        })

    def test_patch_wrong_person(self):
        response = self.client.patch('/api/v5/function/2cc55278-e305-4215-bc6d-db5367e7efe7/', {
            'person_uuid': '2cc55278-e305-4215-bc6d-db5367e7efe8',
        }, content_type='application/json')
        self.assertEqual(response.status_code, 400)
        self.assertJSONEqual(response.content, {
            'person_uuid': ['Not found.'],
        })

    def test_post_functions(self):
        response = self.client.post('/api/v5/function/', [{
            'begin': '2017-09-15',
            'end': '2018-08-31',
            'name': 'Fonction test bis',
            'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'team_uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
            'type': self.function_type.id,
            'uuid': '2cc55278-e305-4215-bc6d-db5367e7efe7',
        }, {
            'begin': '2017-09-15',
            'end': '2018-08-31',
            'name': 'Fonction test ter',
            'person_uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'team_uuid': 'be1d9521-9d23-4887-9873-e7d8e2e245c6',
            'type': self.function_type.id,
            'uuid': '2cc55278-e305-4215-bc6d-db5367e7efe8',
        }], content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {'created': 1, 'updated': 1})
        self.assertQuerysetEqual(models.Function.objects.all(), [
            "Configuration de fonction test (Fonction test bis)",
            "Configuration de fonction test (Fonction test ter)",
        ], transform=str, ordered=False)

    def test_delete_existing_function(self):
        response = self.client.delete('/api/v5/function/2cc55278-e305-4215-bc6d-db5367e7efe7/')
        self.assertEqual(response.status_code, 204)

    def test_delete_not_existing_function(self):
        response = self.client.delete('/api/v5/function/2cc55278-e305-4215-bc6d-db5367e7efe8/')
        self.assertEqual(response.status_code, 404)

    def test_delete_function_not_owner(self):
        self.function.owner = models.User.objects.create()
        self.function.save()
        response = self.client.delete('/api/v5/function/2cc55278-e305-4215-bc6d-db5367e7efe7/')
        self.assertEqual(response.status_code, 403)

    def test_post_existing_structure(self):
        response = self.client.post('/api/v5/structure/', [{
            'uuid': '6303a2c4-1873-43e2-a31f-a8a0043623c1',
            'status': models.Structure.STATUS_GUARDIANSHIP,
        }], content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {'updated': 1})
        self.assertEqual(
            models.Structure.objects.get(uuid='6303a2c4-1873-43e2-a31f-a8a0043623c1').status,
            models.Structure.STATUS_GUARDIANSHIP,
        )

    def test_post_not_existing_structure(self):
        response = self.client.post('/api/v5/structure/', [{
            'uuid': '6303a2c4-1873-43e2-a31f-a8a0043623c2',
            'status': models.Structure.STATUS_GUARDIANSHIP,
        }], content_type='application/json')
        self.assertEqual(response.status_code, 404)

    def test_post_structure_wrong_value(self):
        response = self.client.post('/api/v5/structure/', [{
            'uuid': '6303a2c4-1873-43e2-a31f-a8a0043623c1',
            'status': models.Structure.STATUS_CLOSED,
        }], content_type='application/json')
        self.assertEqual(response.status_code, 400)
        self.assertJSONEqual(response.content, {
            'status': ["Seuls les statuts 1 (autonome) et 2 (rattachée) sont autorisés."],
        })

    def test_post_closed_structure(self):
        self.structure.status = models.Structure.STATUS_CLOSED
        self.structure.save()
        response = self.client.post('/api/v5/structure/', [{
            'uuid': '6303a2c4-1873-43e2-a31f-a8a0043623c1',
            'status': models.Structure.STATUS_AUTONOMOUS,
        }], content_type='application/json')
        self.assertEqual(response.status_code, 400)
        self.assertJSONEqual(response.content, {
            'status': ["Une structure fermée ne peut être ré-ouverte."],
        })

    def test_post_structure_wrong_field(self):
        response = self.client.post('/api/v5/structure/', [{
            'uuid': '6303a2c4-1873-43e2-a31f-a8a0043623c1',
            'status': models.Structure.STATUS_GUARDIANSHIP,
            'name': 'Toto',
        }], content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            models.Structure.objects.get(uuid='6303a2c4-1873-43e2-a31f-a8a0043623c1').name,
            "Structure test",
        )

    def test_delete_structure(self):
        response = self.client.delete('/api/v5/structure/6303a2c4-1873-43e2-a31f-a8a0043623c1/')
        self.assertEqual(response.status_code, 405)

    def test_patch_existing_structure(self):
        response = self.client.patch('/api/v5/structure/6303a2c4-1873-43e2-a31f-a8a0043623c1/', {
            'status': models.Structure.STATUS_GUARDIANSHIP,
        }, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            models.Structure.objects.get(uuid='6303a2c4-1873-43e2-a31f-a8a0043623c1').status,
            models.Structure.STATUS_GUARDIANSHIP,
        )

    def test_patch_not_existing_structure(self):
        response = self.client.patch('/api/v5/structure/6303a2c4-1873-43e2-a31f-a8a0043623c2/', {
            'status': models.Structure.STATUS_GUARDIANSHIP,
        }, content_type='application/json')
        self.assertEqual(response.status_code, 404)

    def test_get_roleconfig_permissions(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v5/role_config_permission/')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'app_label': 'members',
                'codename': 'view_person',
                'id': self.roleconfig_permission.pk,
                'model': 'person',
                'roleconfig_uuid': '2cc55278-e305-4215-bc6d-db5367e7efe8',
                'uuid': '2cc55278-e305-4215-bc6d-db5367e7efea'
            }]
        })

    def test_get_roleconfig_permissions_with_app_label(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v5/role_config_permission/?app_label=members')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'app_label': 'members',
                'codename': 'view_person',
                'id': self.roleconfig_permission.pk,
                'model': 'person',
                'roleconfig_uuid': '2cc55278-e305-4215-bc6d-db5367e7efe8',
                'uuid': '2cc55278-e305-4215-bc6d-db5367e7efea'
            }]
        })

    def test_get_roleconfig_permissions_with_other_app_label(self):
        with self.assertNumQueries(3):
            response = self.client.get('/api/v5/role_config_permission/?app_label=auth')
        self.assertJSONEqual(response.content, {
            'count': 0,
            'next': None,
            'previous': None,
            'results': []
        })

    def test_get_roleconfig_permissions_with_multiple_app_labels(self):
        with self.assertNumQueries(4):
            response = self.client.get('/api/v5/role_config_permission/?app_label=auth,members')
        self.assertJSONEqual(response.content, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'app_label': 'members',
                'codename': 'view_person',
                'id': self.roleconfig_permission.pk,
                'model': 'person',
                'roleconfig_uuid': '2cc55278-e305-4215-bc6d-db5367e7efe8',
                'uuid': '2cc55278-e305-4215-bc6d-db5367e7efea'
            }]
        })

    def test_put_existing_person(self):
        response = self.client.put('/api/v5/person/b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48/', {
            'uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'address1': 'Adresse test 1',
            'address2': 'Adresse test 2',
            'address3': 'Adresse test 3',
            'birth_country_uuid': '6b865252-7ed5-4eef-9444-e9352b4ff61d',
            'birth_commune_uuid': '6b865252-7ed5-4eef-9444-e9352b4ff61e',
            'birth_place': '',
            'birthdate': '2005-01-01',
            'city': 'Ville test',
            'country': 'Pays test',
            'email': 'email@test.com',
            'first_name': 'Prénom test',
            'family_name': 'Nom de famille test',
            'gender': 'F',
            'home_phone': '0999999999',
            'image_rights': True,
            'last_name': 'Nom test',
            'legal_guardian1_uuid': '6b865252-7ed5-4eef-9444-e9352b4fa61d',
            'legal_guardian2_uuid': '794ae0ba-a961-4b21-8c10-48c98e868f2a',
            'merged_with': [],
            'mobile_phone': '0666666666',
            'nickname': 'Surnom test',
            'permission_alone': True,
            'permission_with': "Permission prénom nom test",
            'postal_code': '99999',
            'profession': 'Profession test',
        }, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {
            'uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'id': self.person.id,
            'address1': 'Adresse test 1',
            'address2': 'Adresse test 2',
            'address3': 'Adresse test 3',
            'birth_country_uuid': '6b865252-7ed5-4eef-9444-e9352b4ff61d',
            'birth_commune_uuid': '6b865252-7ed5-4eef-9444-e9352b4ff61e',
            'birth_place': '',
            'birthdate': '2005-01-01',
            'city': 'Ville test',
            'country': 'Pays test',
            'email': 'email@test.com',
            'first_name': 'PRÉNOM TEST',
            'family_name': 'NOM DE FAMILLE TEST',
            'gender': 'F',
            'home_phone': '+33999999999',
            'image_rights': True,
            'last_name': 'NOM TEST',
            'legal_guardian1_uuid': '6b865252-7ed5-4eef-9444-e9352b4fa61d',
            'legal_guardian2_uuid': '794ae0ba-a961-4b21-8c10-48c98e868f2a',
            'merged_with': [],
            'mobile_phone': '+33666666666',
            'nickname': 'Surnom test',
            'permission_alone': True,
            'permission_with': "Permission prénom nom test",
            'postal_code': '99999',
            'profession': 'Profession test',
        })

    def test_patch_person(self):
        response = self.client.patch('/api/v5/person/b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48/', {
            'address1': 'Adresse test 1 modifiée',
        }, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {
            'uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a48',
            'id': self.person.id,
            'address1': 'Adresse test 1 modifiée',
            'address2': 'Adresse test 2',
            'address3': 'Adresse test 3',
            'birth_country_uuid': '6b865252-7ed5-4eef-9444-e9352b4ff61d',
            'birth_commune_uuid': '6b865252-7ed5-4eef-9444-e9352b4ff61e',
            'birth_place': '',
            'birthdate': '2005-01-01',
            'city': 'Ville test',
            'country': 'Pays test',
            'email': 'email@test.com',
            'first_name': 'Prénom test',
            'family_name': 'Nom de famille test',
            'gender': 'F',
            'home_phone': '+33999999999',
            'image_rights': True,
            'last_name': 'Nom test',
            'legal_guardian1_uuid': '6b865252-7ed5-4eef-9444-e9352b4fa61d',
            'legal_guardian2_uuid': '794ae0ba-a961-4b21-8c10-48c98e868f2a',
            'merged_with': [],
            'mobile_phone': '+33666666666',
            'nickname': 'Surnom test',
            'permission_alone': True,
            'permission_with': "Permission prénom nom test",
            'postal_code': '99999',
            'profession': 'Profession test',
        })

    def test_post_person(self):
        response = self.client.post('/api/v5/person/', {
            'uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a49',
            'address1': 'Adresse test 1',
            'address2': 'Adresse test 2',
            'address3': 'Adresse test 3',
            'birth_country_uuid': '6b865252-7ed5-4eef-9444-e9352b4ff61d',
            'birth_commune_uuid': '6b865252-7ed5-4eef-9444-e9352b4ff61e',
            'birth_place': '',
            'birthdate': '2005-01-01',
            'city': 'Ville test',
            'country': 'Pays test',
            'email': 'email@test.com',
            'first_name': 'PRÉNOM TEST',
            'family_name': 'Nom de famille test',
            'gender': 'F',
            'home_phone': '0999999999',
            'image_rights': True,
            'last_name': 'NOM TEST POST',
            'legal_guardian1_uuid': '6b865252-7ed5-4eef-9444-e9352b4fa61d',
            'legal_guardian2_uuid': '794ae0ba-a961-4b21-8c10-48c98e868f2a',
            'merged_with': [],
            'mobile_phone': '0666666666',
            'nickname': 'Surnom test',
            'permission_alone': True,
            'permission_with': "Permission prénom nom test",
            'postal_code': '99999',
            'profession': 'Profession test',
        }, content_type='application/json')
        self.assertEqual(response.status_code, 201)
        new_person = models.Person.objects.exclude(
            id__in=(self.guardian1.id, self.guardian2.id, self.person.id)
        ).get()
        self.assertJSONEqual(response.content, {
            'uuid': 'b3c2e90e-9670-4e15-b5b5-85dbfd2f9a49',
            'id': new_person.id,
            'address1': 'Adresse test 1',
            'address2': 'Adresse test 2',
            'address3': 'Adresse test 3',
            'birth_country_uuid': '6b865252-7ed5-4eef-9444-e9352b4ff61d',
            'birth_commune_uuid': '6b865252-7ed5-4eef-9444-e9352b4ff61e',
            'birth_place': '',
            'birthdate': '2005-01-01',
            'city': 'Ville test',
            'country': 'Pays test',
            'email': 'email@test.com',
            'first_name': 'PRÉNOM TEST',
            'family_name': 'NOM DE FAMILLE TEST',
            'gender': 'F',
            'home_phone': '+33999999999',
            'image_rights': True,
            'last_name': 'NOM TEST POST',
            'legal_guardian1_uuid': '6b865252-7ed5-4eef-9444-e9352b4fa61d',
            'legal_guardian2_uuid': '794ae0ba-a961-4b21-8c10-48c98e868f2a',
            'merged_with': [],
            'mobile_phone': '+33666666666',
            'nickname': 'Surnom test',
            'permission_alone': True,
            'permission_with': "Permission prénom nom test",
            'postal_code': '99999',
            'profession': 'Profession test',
        })


@freeze_time("2015-01-01")
@override_settings(HOST='testserver')
class PaymentTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = models.User.objects.create(is_superuser=True)

    def setUp(self):
        self.client.force_login(self.user)

    def test_delete(self):
        allocation = factories.AllocationFactory.create()
        payment = allocation.payment
        response = self.client.get(f'/payment/{payment.pk}/delete/')
        self.assertContains(response, "Supprimer le paiment ?")
        response = self.client.post(f'/payment/{payment.pk}/delete/')
        self.assertRedirects(response, '/payment/')
        self.assertFalse(models.Payment.objects.filter(pk=payment.pk).exists())


class AdhesionTests(TestCase):
    def test_set_dates_first_day(self):
        adhesion = models.Adhesion()
        adhesion.set_dates(Date(2015, 9, 1))
        self.assertEqual(adhesion.dates.upper, Date(2016, 9, 1))

    def test_set_dates_last_day(self):
        adhesion = models.Adhesion()
        adhesion.set_dates(Date(2015, 8, 31))
        self.assertEqual(adhesion.dates.upper, Date(2015, 9, 1))


@freeze_time("2015-01-01")
class FunctionFormAdherentTests(TestCase):
    maxDiff = None

    @classmethod
    def setUpTestData(cls):
        cls.adhesion = factories.AdhesionFactory.create()
        cls.person = cls.adhesion.adherent.person
        cls.structure = cls.adhesion.structure
        cls.user = factories.UserFactory.create(person=cls.person)
        cls.team = factories.TeamFactory.create(structure=cls.structure)
        cls.function_config = factories.FunctionConfigFactory.create(team_type=cls.team.type)
        role_config = models.RoleConfig.objects.create(
            name="Gérer les adhésions d'une structure",
            for_all=True,
        )
        role_config.permissions.set([
            models.Permission.objects.get(codename='add_function', content_type__app_label='members'),
            models.Permission.objects.get(codename='view_person', content_type__app_label='members'),
        ])
        models.Role.objects.create(config=role_config, team=cls.team, proxy=cls.user.person)

    def test_adherent(self):
        form = forms.FunctionForm(
            user=self.user,
            force_age=False,
            data={
                'structure': str(self.structure.pk),
                'team': str(self.team.pk),
                'person': str(self.person.pk),
                'function_config': str(self.function_config.pk),
                'name': 'Test name',
                'begin': '31/10/2014',
                'end': '31/08/2015',
            },
        )
        self.assertTrue(form.is_valid(), f"{form.errors=}")

    def test_end_before_begin(self):
        form = forms.FunctionForm(
            user=self.user,
            force_age=False,
            data={
                'structure': str(self.structure.pk),
                'team': str(self.team.pk),
                'person': str(self.person.pk),
                'function_config': str(self.function_config.pk),
                'name': 'Test name',
                'begin': '31/10/2014',
                'end': '30/10/2014',
            },
        )
        self.assertFormError(form, 'end', ["La date de fin ne peut être antérieure à la date de début."])

    def test_end_after_adhesion(self):
        form = forms.FunctionForm(
            user=self.user,
            force_age=False,
            data={
                'structure': str(self.structure.pk),
                'team': str(self.team.pk),
                'person': str(self.person.pk),
                'function_config': str(self.function_config.pk),
                'name': 'Test name',
                'begin': '31/10/2014',
                'end': '01/09/2015',
            },
        )
        self.assertFormError(form, 'end', ["Merci de choisir une date avant la fin de l'adhésion en cours à la prise "
                                           "de fonction (31/08/2015)."])

    def test_missing_person(self):
        form = forms.FunctionForm(
            user=self.user,
            force_age=False,
            data={
                'structure': str(self.structure.pk),
                'team': str(self.team.pk),
                'person': '',
                'function_config': str(self.function_config.pk),
                'name': 'Test name',
                'begin': '31/10/2014',
                'end': '31/08/2015',
            },
        )
        self.assertFormError(form, 'person', ["Ce champ est obligatoire."])

    def test_missing_end(self):
        form = forms.FunctionForm(
            user=self.user,
            force_age=False,
            data={
                'structure': str(self.structure.pk),
                'team': str(self.team.pk),
                'person': str(self.person.pk),
                'function_config': str(self.function_config.pk),
                'name': 'Test name',
                'begin': '31/10/2014',
                'end': '',
            },
        )
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['end'], Date(2015, 8, 31))

    def test_not_employee(self):
        function_config = factories.FunctionConfigFactory.create(
            team_type=self.team.type,
            function_type__category=models.FunctionCategory.EMPLOYEE,
        )
        form = forms.FunctionForm(
            user=self.user,
            force_age=False,
            data={
                'structure': str(self.structure.pk),
                'team': str(self.team.pk),
                'outside': 'on',
                'person': str(self.person.pk),
                'function_config': str(function_config.pk),
                'name': 'Test name',
                'begin': '31/10/2014',
                'end': '31/08/2015',
            },
        )
        self.assertFormError(form, 'begin', ["Il faut un contrat de travail à cette date."])


@freeze_time("2015-01-01")
class FunctionFormEmployeeTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.employment = factories.EmploymentFactory.create()
        cls.person = cls.employment.employee.person
        cls.structure = factories.StructureFactory.create()
        cls.user = factories.UserFactory.create(person=cls.person)
        cls.team = factories.TeamFactory.create(structure=cls.structure)
        cls.function_config = factories.FunctionConfigFactory.create(
            team_type=cls.team.type,
            function_type__category=models.FunctionCategory.EMPLOYEE,
        )
        role_config = models.RoleConfig.objects.create(
            name="Gérer les adhésions d'une structure",
            for_all=True,
        )
        role_config.permissions.set([
            models.Permission.objects.get(codename='add_function', content_type__app_label='members'),
            models.Permission.objects.get(codename='view_person', content_type__app_label='members'),
        ])
        models.Role.objects.create(config=role_config, team=cls.team, proxy=cls.user.person)

    def test_employee(self):
        form = forms.FunctionForm(
            user=self.user,
            force_age=False,
            data={
                'structure': str(self.structure.pk),
                'team': str(self.team.pk),
                'person': str(self.person.pk),
                'function_config': str(self.function_config.pk),
                'name': 'Test name',
                'begin': '31/10/2014',
                'end': '31/08/2015',
            },
        )
        self.assertTrue(form.is_valid(), f"{form.errors=}")

    def test_end_after_employment(self):
        form = forms.FunctionForm(
            user=self.user,
            force_age=False,
            data={
                'structure': str(self.structure.pk),
                'team': str(self.team.pk),
                'outside': 'on',
                'person': str(self.person.pk),
                'function_config': str(self.function_config.pk),
                'name': 'Test name',
                'begin': '31/10/2014',
                'end': '01/09/2016',
            },
        )
        self.assertFormError(form, 'end', [
            "La date de fin doit être antérieure à la date du dernier contrat de travail (31/08/2015)."
        ])

    def test_employment_hole(self):
        factories.EmploymentFactory.create(
            employee=self.employment.employee,
            dates=DateRange(Date(2015, 10, 1), Date(2015, 11, 1)),
        )
        form = forms.FunctionForm(
            user=self.user,
            force_age=False,
            data={
                'structure': str(self.structure.pk),
                'team': str(self.team.pk),
                'person': str(self.person.pk),
                'function_config': str(self.function_config.pk),
                'name': 'Test name',
                'begin': '31/10/2014',
                'end': '01/09/2018',
            },
        )
        self.assertFormError(form, 'end', [
            "Les contrats de travail sont interrompus au 01/09/2015."
        ])

    def test_missing_end(self):
        form = forms.FunctionForm(
            user=self.user,
            force_age=False,
            data={
                'structure': str(self.structure.pk),
                'team': str(self.team.pk),
                'person': str(self.person.pk),
                'function_config': str(self.function_config.pk),
                'name': 'Test name',
                'begin': '31/10/2014',
                'end': '',
            },
        )
        self.assertFormError(form, 'end', [
            "La date de fin doit être précisée lorsque le contrat de travail n'est pas un CDI.",
        ])

    def test_not_adherent(self):
        function_config = factories.FunctionConfigFactory.create(
            team_type=self.team.type,
        )
        form = forms.FunctionForm(
            user=self.user,
            force_age=False,
            data={
                'structure': str(self.structure.pk),
                'team': str(self.team.pk),
                'outside': 'on',
                'person': str(self.person.pk),
                'function_config': str(function_config.pk),
                'name': 'Test name',
                'begin': '31/10/2014',
                'end': '31/08/2015',
            },
        )
        self.assertFormError(form, 'begin', ["Il faut une adhésion à cette date."])


class PersonTests(TestCase):
    def test_email_as_login_default(self):
        person = models.Person.objects.create(last_name="Dupont", first_name="Pierre")
        self.assertEqual(person.email_as_login, True)

    def test_email_as_login_true(self):
        person = models.Person.objects.create(last_name="Dupont", first_name="Pierre", email_as_login=True)
        self.assertEqual(person.email_as_login, True)

    def test_email_as_login_false(self):
        person = models.Person.objects.create(last_name="Dupont", first_name="Pierre", email_as_login=False)
        self.assertEqual(person.email_as_login, True)

    def test_email_as_login_default_after_true(self):
        person1 = models.Person.objects.create(last_name="Dupont", first_name="Pierre")
        self.assertEqual(person1.email_as_login, True)
        person2 = models.Person.objects.create(last_name="Durand", first_name="Paul")
        self.assertEqual(person2.email_as_login, False)

    def test_email_as_login_true_after_true(self):
        person1 = models.Person.objects.create(last_name="Dupont", first_name="Pierre")
        self.assertEqual(person1.email_as_login, True)
        person2 = models.Person.objects.create(last_name="Durand", first_name="Paul", email_as_login=True)
        self.assertEqual(person2.email_as_login, False)

    def test_email_as_login_false_after_true(self):
        person1 = models.Person.objects.create(last_name="Dupont", first_name="Pierre")
        self.assertEqual(person1.email_as_login, True)
        person2 = models.Person.objects.create(last_name="Durand", first_name="Paul", email_as_login=False)
        self.assertEqual(person2.email_as_login, False)
