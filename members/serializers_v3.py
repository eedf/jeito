# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from rest_framework import serializers
from members.models import Adhesion, FunctionType, Function, Person, AdherentStatus, Structure, StructureType, \
    TeamType, Team, Echelon, Employment, EmploymentType, FunctionCategory, Adherent, Employee


class PersonSerializer3(serializers.ModelSerializer):
    gender = serializers.CharField(
        label="Genre",
        help_text=", ".join(f"{key}={val}" for key, val in Person.GENDER_CHOICES),
    )
    birth_place = serializers.CharField(source='birth_place_display', label="Lieu de naissance")
    permission_alone = serializers.BooleanField(source='adherent.permission_alone',
                                                label="Permission de rentrer seul·e")
    permission_alone = serializers.SerializerMethodField(label="Permission de rentrer seul·e")
    permission_with = serializers.SerializerMethodField(label="Permission de rentrer accompagné·e")
    permission_last_name = serializers.SerializerMethodField(label="Permission de rentrer accompagné·e par (nom)")
    permission_first_name = serializers.SerializerMethodField(label="Permission de rentrer accompagné·e par (prénom)")

    class Meta:
        model = Person
        fields = (
            'id', 'gender', 'first_name', 'last_name', 'family_name', 'nickname', 'birthdate', 'gender',
            'address1', 'address2', 'address3', 'postal_code', 'city', 'country',
            'email', 'home_phone', 'mobile_phone', 'birth_place', 'profession',
            'legal_guardian1_id', 'legal_guardian2_id', 'image_rights',
            'permission_alone', 'permission_with', 'permission_last_name', 'permission_first_name',
        )

    def get_permission_alone(self, obj):
        try:
            obj.adherent
        except Adherent.DoesNotExist:
            return None
        return obj.permission_with != ""

    def get_permission_with(self, obj):
        try:
            obj.adherent
        except Adherent.DoesNotExist:
            return None
        return obj.permission_with != ""

    def get_permission_first_name(self, obj):
        try:
            obj.adherent
        except Adherent.DoesNotExist:
            return None
        if not obj.permission_with:
            return ""
        return obj.permission_with.split()[0]

    def get_permission_last_name(self, obj):
        try:
            obj.adherent
        except Adherent.DoesNotExist:
            return None
        try:
            return obj.permission_with.split(maxsplit=1)[1]
        except IndexError:
            return ""


class AdherentSerializer3(serializers.ModelSerializer):
    status = serializers.IntegerField(
        label="Statut",
        help_text=", ".join(f"{key}={val}" for key, val in AdherentStatus.choices),
    )

    class Meta:
        model = Adherent
        fields = (
            'id', 'person_id', 'status', 'duplicates',
        )


class AdhesionSerializer3(serializers.ModelSerializer):
    begin = serializers.DateField(label="Date de début")
    end = serializers.DateField(label="Date de fin")
    structure_id = serializers.PrimaryKeyRelatedField(label="Structure", read_only=True)

    class Meta:
        model = Adhesion
        fields = (
            'id', 'adherent_id', 'season', 'begin', 'end', 'entry_date', 'structure_id', 'canceled',
        )


class TeamTypeSerializer3(serializers.ModelSerializer):
    class Meta:
        model = TeamType
        fields = ('id', 'name', 'management')


class TeamSerializer3(serializers.ModelSerializer):
    structure_id = serializers.PrimaryKeyRelatedField(label="Structure", read_only=True)
    type_id = serializers.PrimaryKeyRelatedField(label="Type d'équipe", help_text="Null si équipe personnalisée.",
                                                 read_only=True)

    class Meta:
        model = Team
        fields = ('id', 'structure_id', 'type_id', 'name', 'creation_date', 'deactivation_date')


class StructureTypeSerializer3(serializers.ModelSerializer):
    echelon = serializers.IntegerField(
        label="Échelon",
        help_text=", ".join(f"{key}={val}" for key, val in Echelon.choices),
    )

    class Meta:
        model = StructureType
        fields = ('id', 'name', 'echelon')


class StructureSerializer3(serializers.ModelSerializer):
    parent_id = serializers.PrimaryKeyRelatedField(label="Structure parente", read_only=True)
    type_id = serializers.PrimaryKeyRelatedField(label="Type de structure", read_only=True)
    status = serializers.IntegerField(
        label="Statut",
        help_text=", ".join(f"{key}={val}" for key, val in Structure.STATUS_CHOICES),
    )

    class Meta:
        model = Structure
        fields = ('id', 'name', 'parent_id', 'type_id', 'status', 'place', 'public_email', 'public_phone')


class FunctionTypeSerializer3(serializers.ModelSerializer):
    category = serializers.IntegerField(
        label="Catégorie",
        help_text=", ".join(f"{key}={val}" for key, val in FunctionCategory.choices),
    )

    class Meta:
        model = FunctionType
        fields = ('id', 'name', 'category')


class FunctionSerializer3(serializers.ModelSerializer):
    begin = serializers.DateField(label="Date de début")
    end = serializers.DateField(label="Date de fin")
    type_id = serializers.PrimaryKeyRelatedField(source='function_config.function_type_id', read_only=True)

    class Meta:
        model = Function
        fields = ('id', 'person_id', 'team_id', 'begin', 'end', 'name', 'type_id')


class EmployeeSerializer3(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = (
            'id', 'person_id', 'email', 'phone',
        )


class EmploymentSerializer3(serializers.ModelSerializer):
    begin = serializers.DateField(label="Date de début")
    end = serializers.DateField(label="Date de fin")
    type = serializers.IntegerField(
        label="Type",
        help_text=", ".join(f"{type.value}={type.name}={type.label}" for type in EmploymentType),
    )

    class Meta:
        model = Employment
        fields = ('id', 'employee_id', 'begin', 'end', 'name', 'type')
