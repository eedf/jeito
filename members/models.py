# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from binascii import hexlify
from copy import deepcopy
from datetime import date as Date, timedelta as TimeDelta
from fintech import sepa
from functools import reduce, cached_property
import html2text
from itertools import chain
from localflavor.generic.models import IBANField
from num2words import num2words
from operator import or_
from os import makedirs, urandom
from os.path import dirname, join
import phonenumbers
from psycopg2.extras import DateRange
import re
from secrets import token_urlsafe
from uuid import uuid4, uuid5, NAMESPACE_DNS
from weasyprint import HTML

from django.db import models
from django.db.models import Q, Sum, Case, When, F, Value, Func, Count, Max
from django.db.models.functions import ExtractYear
from django.db import transaction
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin, Permission as BasePermission
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.contrib.contenttypes.models import ContentType
# from django.contrib.postgres.constraints import ExclusionConstraint
from django.contrib.postgres.indexes import GinIndex
from django.contrib.postgres.fields import DateRangeField, ArrayField
from django.contrib.postgres.search import TrigramSimilarity, TrigramStrictWordSimilarity
from django.core.exceptions import ValidationError
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.core.validators import RegexValidator, MinValueValidator
from django.template.loader import get_template
from django.urls import reverse_lazy
from django.utils.html import mark_safe, escape
from django.utils.timezone import now
from django.utils.translation import get_language

from auditlog.models import LogEntry as BaseLogEntry
from auditlog.registry import auditlog
from mptt.models import MPTTModel, TreeForeignKey, TreeManager
from mptt.querysets import TreeQuerySet
from phonenumber_field.modelfields import PhoneNumberField
from sequences import get_next_value

from .utils import current_season, next_transfer_date, today, build_url, remove_accents, normalize_name, tokenize_name


text_maker = html2text.HTML2Text()
text_maker.ignore_links = True


def q_or(a, b):
    """Or between Q objects. If on of the two is empty, the result is empty."""
    if a and b:
        return a | b
    return Q()


def q_add_prefix(q, prefix):
    if isinstance(q, Q):
        q.children = [q_add_prefix(child, prefix) for child in q.children]
        return q
    else:
        key, val = q
        return (prefix + key, val)


def validate_strictly_positive(value):
    if value <= 0:
        raise ValidationError("Cette valeur doit être strictement positive")


class Html5EmailValidator(RegexValidator):
    regex = r"^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}" \
        r"[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
    message = "Saisissez une adresse de courriel valide."


class NameValidator(RegexValidator):
    regex = r"^([aàâbcçdeéèêëfghiîïjklmnoôpqrstuùûüvwxyÿzæœAÀÂBCÇDEÉÈÊËFGHIÎÏJKLMNOÔPQRSTUÙÛÜVWXYŸZÆŒ]+( |'|-)?)+$"
    message = "Ne doit comporter que des lettres. Les noms composés ne doivent être séparés que par " \
        "une seule espace, un seul apostrophe ou un seul tiret."


class PermissionQuerySetMixin:
    with_guardians_prefixes = ()
    with_payers_prefixes = ()

    def for_perm(self, perm, user, only_active=True):
        if user.is_superuser:
            return self
        if user.is_anonymous or not user.person:
            return self.none()
        return self.for_person_perm(perm, user.person, only_active)

    def for_person_perm(self, perm, person, only_active=True):
        q_list = []
        for config, team in person.perms(only_active).get(perm, ()):
            q = Q()
            if config.for_self:
                q |= self._q_for_self(person)
                for child in Person.objects.filter(Q(legal_guardian1=person) | Q(legal_guardian2=person)):
                    q |= self._q_for_self(child)
            if config.for_team:
                q |= self._q_for_team(team)
            if config.for_structure:
                q |= self._q_for_structure(team.structure)
            if config.for_sub_structures:
                q |= self._q_for_sub_structures(team.structure)
            if config.for_all or config.for_structure and config.for_sub_structures and team.structure.is_root_node():
                q = Q()
            elif not q:  # no for_* is set
                continue
            with_q = Q()
            if q and config.with_guardians:
                for prefix in self.with_guardians_prefixes:
                    with_q |= q_add_prefix(deepcopy(q), prefix)
            if q and config.with_payers:
                for prefix in self.with_payers_prefixes:
                    with_q |= q_add_prefix(deepcopy(q), prefix)
            q |= with_q
            if config.only_electeds:
                q &= self._q_only_electeds()
            if config.only_employees:
                q &= self._q_only_employees()
            if config.rgpd:
                q &= self._q_rgpd_delay()
            q_list.append(q)
        if not q_list:
            return self.none()
        q = reduce(q_or, q_list)
        return self.filter(q).distinct()


class GlobalPermissionQuerySetMixin:
    def for_perm(self, perm, user, only_active=True):
        if user.is_superuser:
            return self
        if not user.person:
            return self.none()
        for config, team in user.person.perms(only_active).get(perm, ()):
            if config.for_all:
                return self
        return self.none()


class Season(models.Model):
    year = models.IntegerField(
        primary_key=True,
        verbose_name="Année",
        help_text="Année au 1er janvier de la saison (ex. 2023 pour la saison 2022/2023)"
    )
    card = models.FileField(
        upload_to='cards',
        verbose_name="Carte",
        help_text="Fond de carte d'adhésion au format PDF"
    )
    form = models.FileField(
        upload_to='forms',
        verbose_name="Bulletin d'adhésion",
    )

    class Meta:
        verbose_name = "Saison"

    def __str__(self):
        return f"{self.year - 1}/{self.year}"


class UserManager(BaseUserManager):
    def create_user(self, password=None, **kwargs):
        user = self.model(**kwargs)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, **kwargs):
        return self.create_user(is_superuser=True, **kwargs)


class PersonQuerySet(PermissionQuerySetMixin, models.QuerySet):
    with_guardians_prefixes = ('protected1__', 'protected2__')
    with_payers_prefixes = ('payment__allocation__adhesion__adherent__person__', )

    def _q_for_self(self, person):
        return Q(pk=person.pk)

    def _q_for_team(self, team):
        return Q(
            functions__team=team,
            functions__dates__contains=today(),
            functions__status=FunctionStatus.OK,
        )

    def _q_for_structure(self, structure):
        return Q(
            functions__team__structure=structure,
            functions__dates__contains=today(),
            functions__status=FunctionStatus.OK,
        ) | Q(
            adherent__last_adhesion__structure=structure,
        )

    def _q_for_sub_structures(self, structure):
        return Q(
            functions__team__structure__lft__gt=structure.lft,
            functions__team__structure__lft__lt=structure.rght,
            functions__dates__contains=today(),
            functions__status=FunctionStatus.OK,
        ) | Q(
            adherent__last_adhesion__structure__lft__gt=structure.lft,
            adherent__last_adhesion__structure__lft__lt=structure.rght,
        )

    def _q_only_electeds(self):
        return Q(
            functions__function_config__subject_to_nomination=True,
            functions__dates__contains=today(),
            functions__status=FunctionStatus.OK,
        )

    def _q_only_employees(self):
        return Q(employee__employments__dates__contains=today())

    def _q_rgpd_delay(self):
        return Q(expired=False)

    def search(self, q):
        if q.isdigit():
            return self.filter(Q(adherent__id=q) | Q(employee__id=q)).annotate(similarity=Value(1))
        match = re.fullmatch(r'([a-zA-Z]{3})-([0-9]{6})', q)
        if match:
            return self.filter(
                Q(adherent__id=match.group(2)) | Q(employee__id=match.group(2))
            ).annotate(similarity=Value(1))
        if "@" in q:
            return self.filter(Q(email=q) | Q(employee__email=q)).annotate(similarity=Value(1))
        tokens = tokenize_name(q)
        return self.filter(
            reduce(or_, [Q(tokens__token__trigram_similar=token) for token in tokens])
        ).annotate(
            similarity=sum([Max(TrigramSimilarity('tokens__token', token)) for token in tokens]),
        ).filter(
            similarity__gte=0.4,
        )

    def current(self, initial=None):
        q = Q(adherent__last_adhesion__season=current_season())
        q |= Q(employee__last_employment__dates__contains=today())
        if initial:
            q |= Q(pk=initial.pk)
        return self.filter(q)


class StructureQuerySet(PermissionQuerySetMixin, TreeQuerySet):
    def _q_for_self(self, person):
        return Q(pk=None)

    def _q_for_team(self, team):
        return Q(pk=None)

    def _q_for_structure(self, structure):
        return Q(pk=structure.pk)

    def _q_for_sub_structures(self, structure):
        return Q(lft__gt=structure.lft, lft__lt=structure.rght)

    def _q_only_electeds(self):
        return Q(pk=None)

    def _q_only_employees(self):
        return Q()

    def _q_rgpd_delay(self):
        return Q()

    def centers(self):
        return self.filter(type=15)

    def is_active(self):
        return self.filter(status__in=(Structure.STATUS_AUTONOMOUS, Structure.STATUS_GUARDIANSHIP))

    def is_inactive(self):
        return self.filter(status__in=(Structure.STATUS_DORMANT, Structure.STATUS_CLOSED))

    def search(self, q):
        q = remove_accents(q)
        return self.annotate(
            similarity=TrigramSimilarity('name__unaccent', q),
            word_similarity=TrigramStrictWordSimilarity(q, 'name__unaccent'),
        ).filter(
            word_similarity__gte=0.4,
        )


class StructureTypeManager(models.Manager):
    def get_by_natural_key(self, name):
        return self.get(name=name)


class Echelon(models.IntegerChoices):
    NATIONAL = 1, "National"
    REGIONAL = 2, "Régional"
    LOCAL = 3, "Local"


class StructureType(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid4)
    name = models.CharField("Nom", max_length=100, unique=True)
    echelon = models.IntegerField("Échelon", choices=Echelon.choices, default=Echelon.LOCAL)
    can_import = models.BooleanField("Peut importer les adhérent·es")
    has_iban = models.BooleanField("Possède un compte bancaire", default=True)
    management_team_name = models.CharField("Nom de l'équipe de gestion", max_length=100, blank=True)
    old_name = models.CharField("Ancien nom (API v1)", max_length=100, blank=True)
    old_subtype_name = models.CharField("Ancien nom du sous-type (API v1)", max_length=100, blank=True, null=True)

    objects = StructureTypeManager()

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.name, )

    class Meta:
        verbose_name = "Type de structure"
        verbose_name_plural = "Types de structure"
        permissions = [
            ('apiv1_structuretype', "Can view StructureType with API v1"),
            ('apiv3_structuretype', "Can read StructureType with API v3"),
            ('apiv4_read_structuretype', "Can read StructureType with API v4"),
        ]
        ordering = ('name', )


class Structure(MPTTModel):
    STATUS_AUTONOMOUS = 1
    STATUS_GUARDIANSHIP = 2
    STATUS_DORMANT = 3
    STATUS_CLOSED = 4
    STATUS_CHOICES = (
        (STATUS_AUTONOMOUS, "Autonome"),
        (STATUS_GUARDIANSHIP, "Rattachée"),
        (STATUS_DORMANT, "En sommeil"),
        (STATUS_CLOSED, "Fermée"),
    )

    uuid = models.UUIDField(unique=True, default=uuid4)
    name = models.CharField("Nom", max_length=100)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children',
                            db_index=True, on_delete=models.PROTECT)
    region = models.ForeignKey('self', verbose_name="Région", null=True, blank=True, on_delete=models.PROTECT)
    type = models.ForeignKey(StructureType, on_delete=models.PROTECT)
    status = models.IntegerField("Statut", choices=STATUS_CHOICES, default=STATUS_GUARDIANSHIP)
    iban = IBANField(verbose_name="IBAN", blank=True)
    place = models.CharField("Localisation", max_length=200, blank=True,
                             help_text="Adresse du local si il existe ou simple nom de la commune")
    public_email = models.EmailField("Email public", blank=True, help_text="Cette adresse sera affichée publiquement. "
                                     "Merci de vérifier que son propriétaire en a été informé et a donné son accord.",
                                     validators=[Html5EmailValidator()])
    public_phone = PhoneNumberField("Téléphone public", blank=True, help_text="Ce numéro sera affiché publiquement. "
                                    "Merci de vérifier que son propriétaire en a été informé et a donné son accord.")

    objects = TreeManager.from_queryset(StructureQuerySet)()

    def __str__(self):
        return self.name

    @property
    def status_display(self):
        return self.get_status_display()

    @property
    def public_phone_display(self):
        if not self.public_phone:
            return ''
        elif self.public_phone.country_code == 33:
            return self.public_phone.as_national
        else:
            return self.public_phone.as_international

    @property
    def is_active(self):
        return self.status in (Structure.STATUS_AUTONOMOUS, Structure.STATUS_GUARDIANSHIP)

    @property
    def is_dormant(self):
        return self.status == Structure.STATUS_DORMANT

    @property
    def is_closed(self):
        return self.status == Structure.STATUS_CLOSED

    @property
    def status_label(self):
        if self.status == self.STATUS_AUTONOMOUS:
            color = 'success'
        elif self.status == self.STATUS_GUARDIANSHIP:
            color = 'primary'
        elif self.status == self.STATUS_DORMANT:
            color = 'warning'
        elif self.status == self.STATUS_CLOSED:
            color = 'danger'
        return mark_safe(f'<span class="label label-{color}">{self.get_status_display()}</span>')

    @property
    def headcount(self, season=None):
        season = season or current_season()
        return self.adhesions.filter(season=season).count()

    @property
    def iban_structure(self):
        for structure in self.get_ancestors(ascending=True, include_self=True):
            if structure.iban and not structure.is_root_node():
                return structure
        return None

    def functions(self):
        return Function.objects.current().filter(
            team__structure=self,
            team__type__management=True,
            function_config__subject_to_nomination=True,
        ).select_related(
            'person',
        ).order_by(
            'name',
        )

    class Meta:
        verbose_name = "Structure"
        permissions = [
            ('view_structure_iban', "Voir également l'IBAN des structures"),
            ('view_inactive_structure', "Voir également les structures inactives et fermées"),
            ('change_structure_iban', "Modifier également les IBAN des structures"),
            ('change_structure_core', "Modifier également les noms/parents/type et statuts des structures"),
            ('apiv1_structure', "Can view Structure with API v1"),
            ('apiv3_structure', "Can read Structure with API v3"),
            ('apiv4_read_structure', "Can read Structure with API v4"),
            ('apiv4_write_structure', "Can write Structure with API v4"),
            ('apiv5_read_structure', "Can read Structure with API v5"),
            ('apiv5_write_structure', "Can write Structure with API v5"),
        ]
        ordering = ['lft']

    renamed_permissions = [
        ('view_structure', "Voir les données publiques des structures actives et rattachées"),
        ('add_structure', "Ajouter une structure"),
        ('change_structure', "Modifier les contacts/localisations des structures"),
        ('delete_structure', "Supprimer une structure"),
    ]

    class MPTTMeta:
        order_insertion_by = ['name']


class FunctionCategory(models.IntegerChoices):
    VOLUNTEER = 1, "Bénévole"
    PARTICIPANT = 2, "Participant·e"
    EMPLOYEE = 3, "Salarié·e, CEE, SC"


class FunctionType(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid4)
    name = models.CharField("Nom", max_length=100)
    category = models.IntegerField("Catégorie", choices=FunctionCategory.choices, default=FunctionCategory.VOLUNTEER)

    class Meta:
        verbose_name = 'Type de fonction'
        verbose_name_plural = 'Types de fonction'
        permissions = [
            ('apiv1_functiontype', "Can view FunctionType with API v1"),
            ('apiv3_functiontype', "Can read FunctionType with API v3"),
            ('apiv4_read_functiontype', "Can read FunctionType with API v4"),
        ]
        ordering = ('name', )

    def __str__(self):
        return self.name


class FunctionConfig(models.Model):
    function_type = models.ForeignKey(FunctionType, verbose_name="Type de fonction", on_delete=models.PROTECT)
    team_type = models.ForeignKey("TeamType", verbose_name="Type d'équipe", on_delete=models.PROTECT)
    structure_type = models.ForeignKey(StructureType, verbose_name="Type de structure", null=True, blank=True,
                                       on_delete=models.PROTECT)
    name = models.CharField("Nom", max_length=100)
    age_min = models.IntegerField("Âge minimum", null=True, blank=True, help_text="Au 1er septembre")
    age_max = models.IntegerField("Âge maximum", null=True, blank=True, help_text="Au 31 août")
    subject_to_nomination = models.BooleanField("Soumis à nomination", default=False,
                                                help_text="Géré par un module externe.")
    duration = models.IntegerField("Durée", default=0, help_text="Nombre d'années après la fin de l'adhésion en cours.")

    class Meta:
        verbose_name = "Configuration d'une fonction"
        verbose_name_plural = "Configuration des fonctions"
        ordering = ('name', )

    def __str__(self):
        return self.name

    def max_end(self, begin):
        if self. function_type.category == FunctionCategory.EMPLOYEE:
            return None
        end = begin.replace(month=8, day=31)
        if end < begin:
            end = end.replace(year=end.year + 1)
        end = end.replace(year=end.year + self.duration)
        return end

    def default_end(self, begin, format=None):
        if self. function_type.category == FunctionCategory.EMPLOYEE:
            return None
        if self.duration:
            end = begin.replace(year=begin.year + self.duration)
        else:
            end = begin.replace(month=8, day=31)
            if end < begin:
                end = end.replace(year=end.year + 1)
        if format:
            end = end.strftime(format)
        return end


class RateManager(models.Manager):
    def get_by_natural_key(self, season, name):
        return self.get(season=season, name=name)


class RateCategory(models.IntegerChoices):
    CHILD = 1, "Enfant"
    RESPONSIBLE = 2, "Responsable"
    SUPPORT = 3, "Soutien"
    TRAINEE = 4, "Stagiaire"
    IMPORT = 5, "Import SV/CP"
    DISCOVERY = 6, "Découverte"


class Rate(models.Model):
    name = models.CharField("Nom", max_length=256)
    season = models.IntegerField("Saison")
    rate = models.DecimalField("Tarif", max_digits=5, decimal_places=2, null=True, blank=True)
    rate_max = models.DecimalField("Tarif max", max_digits=5, decimal_places=2, null=True, blank=True)
    rate_after_tax_ex = models.DecimalField(
        "Tarif après défiscalisation", max_digits=5, decimal_places=2,
        null=True, blank=True)
    next_child = models.BooleanField("Enfants suivants", default=False)
    qf_min = models.IntegerField("QF min", null=True, blank=True)
    qf_max = models.IntegerField("QF max", null=True, blank=True)
    bracket = models.CharField("Tranche", max_length=100, blank=True)
    category = models.IntegerField("Catégorie", choices=RateCategory.choices, null=True, blank=True)
    function_category = models.IntegerField(verbose_name="Statut", choices=FunctionCategory.choices,
                                            default=FunctionCategory.PARTICIPANT)
    structure_types = models.ManyToManyField(StructureType, verbose_name="Associé aux structures")
    order = models.IntegerField(verbose_name="Numéro d'ordre", null=True, blank=True)
    can_be_free = models.BooleanField("Prise en charge", default=True,
                                      help_text="La prise en charge par la structure EEDF est autorisée")
    send_confirmation = models.BooleanField("Envoyer une confirmation", default=True)
    age_min = models.IntegerField("Âge minimum", null=True, blank=True, help_text="Au 1er septembre")
    age_max = models.IntegerField("Âge maximum", null=True, blank=True, help_text="Au 31 août")
    duration = models.IntegerField("Durée", null=True, blank=True,
                                   help_text="Nombre de jours de validité pour les adhésions découverte")

    objects = RateManager()

    def __str__(self):
        return f"{self.season} - {self.name}"

    def label(self):
        if self.rate_max:
            return f"{self.name} ({self.rate} à {self.rate_max} €)"
        elif self.rate:
            return f"{self.name} ({self.rate} €)"
        else:
            return self.name

    def natural_key(self):
        return (self.season, self.name)

    class Meta:
        verbose_name = "Tarif"
        unique_together = ('name', 'season')
        ordering = ('-season', 'order', 'name')


def age(birthdate, reference=None):
    if not birthdate:
        return None
    if not reference:
        reference = today()
    years = reference.year - birthdate.year
    if (reference.month, reference.day) < (birthdate.month, birthdate.day):
        years -= 1
    return years


class AdherentStatus(models.IntegerChoices):
    # Ids should be sorted from weakest to strongest in order to make comparisons
    OK = 1, "OK"
    LEFT = 2, "Parti·e"
    SUSPENDED = 3, "Suspendu·e"
    EXCLUDED = 4, "Exclu·e"


class NotOnlyDigitsValidator(RegexValidator):
    regex = r"[^0-9]"
    message = "Ne doit pas contenir uniquement des chiffres"
    flags = 0


class UserQuerySet(PermissionQuerySetMixin, models.QuerySet):
    with_guardians_prefixes = ('person__protected1__', 'person__protected2__')
    with_payers_prefixes = ('person__payment__allocation__adhesion__adherent__person__', )

    def _q_for_self(self, person):
        return Q(person=person)

    def _q_for_team(self, team):
        return Q(
            person__functions__team=team,
            person__functions__dates__contains=today(),
            person__functions__status=FunctionStatus.OK,
        )

    def _q_for_structure(self, structure):
        return Q(
            person__functions__team__structure=structure,
            person__functions__dates__contains=today(),
            person__functions__status=FunctionStatus.OK,
        ) | Q(
            person__adherent__last_adhesion__structure=structure,
        )

    def _q_for_sub_structures(self, structure):
        return Q(
            person__functions__team__structure__lft__gt=structure.lft,
            person__functions__team__structure__lft__lt=structure.rght,
            person__functions__dates__contains=today(),
            person__functions__status=FunctionStatus.OK,
        ) | Q(
            person__adherent__last_adhesion__lft__gt=structure.lft,
            person__adherent__last_adhesion__lft__lt=structure.rght,
        )

    def _q_only_electeds(self):
        return Q(
            person__functions__function_config__subject_to_nomination=True,
            person__functions__dates__contains=today(),
            person__functions__status=FunctionStatus.OK,
        )

    def _q_only_employees(self):
        return Q(person__employee__employments__dates__contains=today())

    def _q_rgpd_delay(self):
        return Q(person__expired=False)


class User(PermissionsMixin, AbstractBaseUser):
    person = models.OneToOneField('Person', verbose_name="Personne", null=True, blank=True, on_delete=models.PROTECT)
    username = models.CharField("Nom d'utilisateur", max_length=150, null=True, unique=True, blank=True,
                                help_text="Lettres (au moins une), chiffres et caractères ./+/-/_ seulement.",
                                validators=[UnicodeUsernameValidator(), NotOnlyDigitsValidator()],
                                error_messages={"unique": "Un utilisateur avec ce nom existe déjà."})

    objects = UserManager.from_queryset(UserQuerySet)()

    USERNAME_FIELD = 'username'
    EMAIL_FIELD = 'email'

    class Meta:
        verbose_name = 'Utilisateur'

    def __str__(self):
        if self.person:
            return str(self.person)
        return self.username or ''

    @property
    def email(self):
        if not self.person:
            return None
        return self.person.email

    def get_short_name(self):
        if self.person:
            return self.person.first_name.title()
        return self.username

    def get_full_name(self):
        return str(self)

    @property
    def is_active(self):
        return True

    @property
    def is_staff(self):
        return self.is_superuser

    def has_perm(self, perm, obj=None, only_active=True):
        if super().has_perm(perm, obj):
            return True
        if not self.person:
            return False
        if perm not in self.person.perms(only_active):
            return False
        if not obj:
            return True
        return obj.__class__.objects.for_perm(perm, self, only_active).filter(pk=obj.pk).exists()

    @property
    def structure(self):
        if not self.person:
            return None
        try:
            adherent = self.person.adherent
        except Adherent.DoesNotExist:
            return None
        if not adherent.last_adhesion:
            return None
        if not adherent.last_adhesion.structure.is_active:
            return None
        return adherent.last_adhesion.structure


class CountryQuerySet(models.QuerySet):
    def search(self, q):
        q = remove_accents(q)
        return self.annotate(
            similarity=TrigramSimilarity('name__unaccent', q),
            word_similarity=TrigramStrictWordSimilarity(q, 'name__unaccent'),
        ).filter(
            word_similarity__gte=0.4,
        )


class Country(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid4)
    name = models.CharField("Nom", max_length=70)
    iso2 = models.CharField("Code ISO à 2 lettres", max_length=2, blank=True)
    iso3 = models.CharField("Code ISO à 3 lettres", max_length=3, blank=True)

    objects = models.Manager.from_queryset(CountryQuerySet)()

    class Meta:
        verbose_name = "Pays"
        verbose_name_plural = "Pays"
        ordering = ('name', )
        permissions = [
            ('apiv5_read_country', "Can read Country with API v5"),
        ]

    def __str__(self):
        return self.name


class CommuneQuerySet(models.QuerySet):
    def search(self, q):
        q = remove_accents(q)
        return self.annotate(
            similarity=TrigramSimilarity('name__unaccent', q),
            word_similarity=TrigramStrictWordSimilarity(q, 'name__unaccent'),
        ).filter(
            word_similarity__gte=0.4,
        )


class Commune(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid4)
    code = models.CharField("Code", max_length=5)
    name = models.CharField("Nom", max_length=200)
    dates = DateRangeField("Dates")

    objects = models.Manager.from_queryset(CommuneQuerySet)()

    class Meta:
        verbose_name = "Commune"
        ordering = ('name', )
        permissions = [
            ('apiv5_read_commune', "Can read Commune with API v5"),
        ]

    @property
    def code_departement(self):
        n = 3 if self.code[:2] in ('97', '98') else 2
        return self.code[:n]

    def __str__(self):
        return f"{self.name} ({self.code_departement})"

    @property
    def begin(self):
        if not self.dates.lower:
            return None
        lower = self.dates.lower
        if not self.dates.lower_inc:
            lower += TimeDelta(days=1)
        return lower

    @property
    def end(self):
        if not self.dates.upper:
            return None
        upper = self.dates.upper
        if not self.dates.upper_inc:
            upper -= TimeDelta(days=1)
        return upper


class Person(models.Model):
    GENDER_CHOICES = (
        ('M', "Masculin"),
        ('F', "Féminin"),
        ('-', "Autre"),
    )

    uuid = models.UUIDField(unique=True, default=uuid4)
    gender = models.CharField("Genre", max_length=1, choices=GENDER_CHOICES)
    first_name = models.CharField("Prénom", max_length=100, validators=[NameValidator()])
    last_name = models.CharField("Nom d'usage", max_length=100, validators=[NameValidator()])
    family_name = models.CharField("Nom de naissance", max_length=100, validators=[NameValidator()], blank=True)
    nickname = models.CharField("Surnom", max_length=100, validators=[NameValidator()], blank=True)
    email = models.EmailField("Email", db_collation="case_insensitive", blank=True, validators=[Html5EmailValidator()])
    email_as_login = models.BooleanField("Email comme identifiant", default=False,
                                         help_text="L'adresse email peut être utilisée comme identifiant de connexion.")
    birthdate = models.DateField("Date de naissance", null=True, blank=True)
    address1 = models.CharField("Adresse ligne 1", max_length=100, blank=True)
    address2 = models.CharField("Adresse ligne 2", max_length=100, blank=True)
    address3 = models.CharField("Adresse ligne 3", max_length=100, blank=True)
    postal_code = models.CharField("Code postal", max_length=100, blank=True)
    city = models.CharField("Ville", max_length=100, blank=True)
    country = models.CharField("Pays", max_length=100, blank=True)
    home_phone = PhoneNumberField("Tél. fixe", blank=True)
    mobile_phone = PhoneNumberField("Tél. mobile", blank=True)
    birth_country = models.ForeignKey(Country, verbose_name="Pays de naissance", null=True, blank=True,
                                      on_delete=models.PROTECT)
    birth_commune = models.ForeignKey(Commune, verbose_name="Commune de naissance", null=True, blank=True,
                                      on_delete=models.PROTECT)
    birth_place = models.CharField("Ville de naissance", max_length=100, blank=True)
    profession = models.CharField("Profession", max_length=100, blank=True)
    image_rights = models.BooleanField("Droit à l'image", blank=True, null=True)
    legal_guardian1 = models.ForeignKey('Person', blank=True, null=True, related_name='protected1',
                                        on_delete=models.PROTECT, verbose_name="Responsable légal 1")
    legal_guardian2 = models.ForeignKey('Person', blank=True, null=True, related_name='protected2',
                                        on_delete=models.PROTECT, verbose_name="Responsable légal 2")
    permission_alone = models.BooleanField(verbose_name="Rentrer seul·e", default=False)
    permission_with = models.CharField(verbose_name="Rentrer accompagné·e par", max_length=200, blank=True)
    username = models.CharField("Nom d'utilisateur", max_length=150, null=True, unique=True,
                                help_text="Lettres (au moins une), chiffres et caractères ./+/-/_ seulement.",
                                validators=[UnicodeUsernameValidator(), NotOnlyDigitsValidator()],
                                error_messages={"unique": "Un utilisateur avec ce nom existe déjà."})
    # Last not used and not declined invitation
    last_invitation = models.IntegerField("Dernière invitation en cours", null=True, blank=True, default=None)
    has_protected = models.BooleanField("A un enfant", default=False)
    has_payment = models.BooleanField("A effectué un paiement", default=False)
    expired = models.BooleanField("Expiré", default=False, help_text="A dépassé le délai RGPD")
    merged_with = ArrayField(models.UUIDField(), verbose_name='Fusionné avec', default=list, blank=True)
    normalized_first_name = models.CharField("Prénom normalisé", max_length=100)
    normalized_last_name = models.CharField("Nom d'usage normalisé", max_length=100)

    objects = models.Manager.from_queryset(PersonQuerySet)()

    class Meta:
        verbose_name = "Personne"
        permissions = [
            ('view_person_email', "Consulter également l'adresse email des personnes"),
            ('view_person_phone', "Consulter également les numéros de téléphone des personnes"),
            ('view_person_birth', "Consulter également les date/lieu de naissance des personnes"),
            ('view_person_image_rights', "Consulter également le droit à l'image des personnes"),
            ('view_person_guardian', "Consulter également les responsables légaux ou les enfants"),
            ('add_person_guardian', "Ajouter un·e responsable légal·e"),
            ('change_person_guardian', "Modifier les responsables légaux·ales"),
            ('delete_person_guardian', "Supprimer les responsables légaux·ales"),
            ('masquerade_person', "Passer pour une autre personne"),
            ('merge_person', "Fusionner deux personnes"),
            ('apiv3_person', "Can read Person with API v3"),
            ('apiv4_read_person', "Can read Person with API v4"),
            ('apiv5_read_person', "Can read Person with API v5"),
            ('apiv5_write_person', "Can write Person with API v5"),
        ]
        constraints = [
            models.UniqueConstraint(
                fields=['email'],
                condition=Q(email_as_login=True),
                name='person_unique_email',
            ),
            models.UniqueConstraint(
                fields=['normalized_last_name', 'normalized_first_name', 'birthdate'],
                condition=~Q(birthdate=None),
                name='unique_name_and_birthdate',
                violation_error_message="Une personne avec les mêmes nom, prénom et date de naissance existe déjà "
                "dans la base de données. Merci de ne pas créer de doublon. Si vous avez sélectionné « Une nouvelle "
                "personne » à l'étape précédente, merci de revenir en arrière et de chercher la personne existante. "
                "Sinon, merci de contacter votre structure locale ou le support des adhésions.",
            ),
            models.UniqueConstraint(
                fields=['normalized_last_name', 'normalized_first_name', 'email'],
                condition=~Q(email=''),
                name='unique_name_and_email',
                violation_error_message="Une personne avec les mêmes nom, prénom et email existe déjà "
                "dans la base de données. Merci de ne pas créer de doublon. Si vous avez sélectionné « Une nouvelle "
                "personne » à l'étape précédente, merci de revenir en arrière et de chercher la personne existante. "
                "Sinon, merci de contacter votre structure locale ou le support des adhésions.",
            ),
            models.UniqueConstraint(
                fields=['normalized_last_name', 'normalized_first_name', 'home_phone'],
                condition=~Q(home_phone=''),
                name='unique_name_and_home_phone',
                violation_error_message="Une personne avec les mêmes nom, prénom et tel. fixe existe déjà "
                "dans la base de données. Merci de ne pas créer de doublon. Si vous avez sélectionné « Une nouvelle "
                "personne » à l'étape précédente, merci de revenir en arrière et de chercher la personne existante. "
                "Sinon, merci de contacter votre structure locale ou le support des adhésions.",
            ),
            models.UniqueConstraint(
                fields=['normalized_last_name', 'normalized_first_name', 'mobile_phone'],
                condition=~Q(mobile_phone=''),
                name='unique_name_and_mobile_phone',
                violation_error_message="Une personne avec les mêmes nom, prénom et tél. mobile existe déjà "
                "dans la base de données. Merci de ne pas créer de doublon. Si vous avez sélectionné « Une nouvelle "
                "personne » à l'étape précédente, merci de revenir en arrière et de chercher la personne existante. "
                "Sinon, merci de contacter votre structure locale ou le support des adhésions.",
            ),
            models.CheckConstraint(
                check=~Q(legal_guardian1=F('id')),
                name='check_legal_guardian1_not_self',
            ),
            models.CheckConstraint(
                check=~Q(legal_guardian2=F('id')),
                name='check_legal_guardian2_not_self',
            ),
            models.CheckConstraint(
                check=Q(legal_guardian2=None) | ~Q(legal_guardian2=F('legal_guardian1')),
                name='check_different_guardians',
                violation_error_message="Les deux responsables légaux doivent être différents.",
            ),
        ]

    renamed_permissions = [
        ('view_person', "Consulter l'identité (nom/prénom/genre) des personnes"),
        ('add_person', "Ajouter une personne"),
        ('change_person', "Modifier les personnes"),
        ('delete_person', "Supprimer les personnes"),
    ]

    def natural_key(self):
        return self.uuid

    def __str__(self):
        id_str = self.id_str
        if id_str:
            id_str = f" [{id_str}]"

        return f"{self.full_name}{id_str}"

    @property
    def full_name(self):
        return f"{self.last_name} {self.first_name}"

    @property
    def id_str(self):
        numbers = []
        try:
            numbers.append(self.employee.id_str)
        except Employee.DoesNotExist:
            pass
        try:
            numbers.append(self.adherent.id_str)
        except Adherent.DoesNotExist:
            pass
        return ', '.join(numbers)

    @property
    def last_and_family_name(self):
        if self.family_name and self.family_name != self.last_name:
            return f"{self.last_name} ({self.family_name})"
        return self.last_name

    @property
    def first_and_nick_name(self):
        if self.nickname and self.nickname != self.first_name:
            return f"{self.first_name} ({self.nickname})"
        return self.first_name

    @property
    def gender_oidc(self):
        return {'M': "male", 'F': "female", '-': ""}

    @property
    def is_adherent(self):
        try:
            self.adherent
        except Adherent.DoesNotExist:
            return False
        return True

    @property
    def is_employee(self):
        try:
            self.employee
        except Employee.DoesNotExist:
            return False
        return True

    @property
    def last_adhesion(self):
        try:
            adherent = self.adherent
        except Adherent.DoesNotExist:
            return None
        return adherent.last_adhesion

    @property
    def last_employment(self):
        try:
            employee = self.employee
        except Employee.DoesNotExist:
            return None
        return employee.last_employment

    @property
    def birth_place_display(self):
        if not self.birth_country:
            return self.birth_place
        if self.birth_country_id == 99100:
            return f"{self.birth_commune}, France"
        return f"{self.birth_place}, {self.birth_country}"

    @property
    def home_phone_display(self):
        if not self.home_phone:
            return ''
        elif self.home_phone.country_code == 33:
            return self.home_phone.as_national
        else:
            return self.home_phone.as_international

    @property
    def mobile_phone_display(self):
        if not self.mobile_phone:
            return ''
        elif self.mobile_phone.country_code == 33:
            return self.mobile_phone.as_national
        else:
            return self.mobile_phone.as_international

    @property
    def permission_display(self):
        if self.permission_alone:
            return "Seul·e"
        if self.permission_with:
            return f"Avec {self.permission_with}"
        return "Non"

    @property
    def guardian_email(self):
        if self.is_minor:
            if self.legal_guardian1 and self.legal_guardian1.email:
                return self.legal_guardian1.email
            if self.legal_guardian2 and self.legal_guardian2.email:
                return self.legal_guardian2.email
        return self.email

    @property
    def emails_with_guardians(self):
        emails = set()
        if self.email:
            emails.add(self.email)
        if self.is_minor:
            if self.legal_guardian1 and self.legal_guardian1.email:
                emails.add(self.legal_guardian1.email)
            if self.legal_guardian2 and self.legal_guardian2.email:
                emails.add(self.legal_guardian2.email)
        return list(emails)

    def guess_main_function(self):
        last_function = self.functions.overlap(
            Date(current_season() - 2, 9, 1),
            today()
        ).order_by('-dates')[:1]
        if last_function:
            team = last_function[0].team
            try:
                config = FunctionConfig.objects.get(
                    Q(
                        structure_type=team.structure.type,
                        team_type=team.type,
                        function_type=last_function[0].function_config.function_type,
                    ) | Q(
                        structure_type=None,
                        team_type=team.type,
                        function_type=last_function[0].function_config.function_type,
                    )
                )
            except FunctionConfig.DoesNotExist:
                config = None
            return team, config
        else:
            return None, None

    @property
    def adhesions(self):
        try:
            return self.adherent.adhesions.all()
        except Adherent.DoesNotExist:
            return Adhesion.objects.none()

    @property
    def employments(self):
        try:
            return self.employee.employments.all()
        except Employee.DoesNotExist:
            return Employment.objects.none()

    @property
    def current_adhesion(self):
        try:
            last_adhesion = self.adherent.last_adhesion
        except Adherent.DoesNotExist:
            return None
        if not last_adhesion:
            return None
        if last_adhesion.season != current_season():
            return None
        if self.adherent.status != AdherentStatus.OK:
            return None
        assert not last_adhesion.canceled
        return last_adhesion

    @property
    def adhesion(self):
        try:
            last_adhesion = self.adherent.last_adhesion
        except Adherent.DoesNotExist:
            return None
        if not last_adhesion:
            return None
        if last_adhesion.season != current_season():
            return None
        assert not last_adhesion.canceled
        return last_adhesion

    @property
    def employment(self):
        try:
            return self.employee.employments.current().get()
        except (Employee.DoesNotExist, Employment.DoesNotExist):
            return None

    @property
    def structure(self):
        adhesion = self.adhesion
        return adhesion and adhesion.structure

    @property
    def address(self):
        ligne = self.address1
        if self.address2:
            ligne += f", {self.address2}"
        if self.address3:
            ligne += f", {self.address3}"
        ligne += f", {self.postal_code} {self.city}"
        if self.country and self.country.upper() != "FRANCE":
            ligne += f", {self.country}"
        return ligne

    def age(self, reference=None):
        return age(self.birthdate, reference)

    @property
    def is_minor(self):
        return self.age() and self.age() < 18

    @property
    def protecteds(self):
        if not self.id:
            return Person.objects.none()
        return Person.objects.filter(
            Q(legal_guardian1=self) | Q(legal_guardian2=self)
        ).order_by('last_name', 'first_name')

    @property
    def status_label(self):
        # Current employee
        try:
            employee = self.employee
        except Employee.DoesNotExist:
            employee = None
        else:
            if employee.employment:
                return employee.status_label
        # Adherent
        try:
            adherent = self.adherent
        except Adherent.DoesNotExist:
            pass
        else:
            label = adherent.status_label
            if label:
                return label
        # Former employee
        if employee:
            return employee.status_label
        # Invité
        if self.last_invitation == current_season():
            return mark_safe('<span class="label label-warning">Invité·e</span>')
        # Responsable légal·e
        if self.has_protected:
            return mark_safe('<span class="label label-info">Resp. légal·e</span>')
        # Payeur
        if self.has_payment:
            return mark_safe('<span class="label label-info">Payeur</span>')
        return ''

    @property
    def status_text(self):
        # Current employee
        try:
            employee = self.employee
        except Employee.DoesNotExist:
            employee = None
        else:
            if employee.employment:
                return employee.status_text
        # Adherent
        try:
            adherent = self.adherent
        except Adherent.DoesNotExist:
            pass
        else:
            label = adherent.status_text
            if label:
                return label
        # Former employee
        if employee:
            return employee.status_text
        # Invité
        if self.last_invitation == current_season():
            return "Invité·e"
        # Responsable légal·e
        if self.has_protected:
            return "Resp. légal·e"
        # Payeur
        if self.has_payment:
            return "Payeur"
        return ''

    @property
    def phone_display(self):
        if not self.phone:
            return ''
        elif self.phone.country_code == 33:
            return self.phone.as_national
        else:
            return self.phone.as_international

    def with_guardians(self):
        persons = [self]
        if self.is_minor:
            if self.legal_guardian1:
                persons.append(self.legal_guardian1)
            if self.legal_guardian2:
                persons.append(self.legal_guardian2)
        return persons

    @property
    def holder_roles(self):
        """Return {(config, team, active): fixed_roles}"""
        if not hasattr(self, '_holder_roles_cache'):
            roles = {}
            for config in RoleConfig.objects.for_no_function():
                roles.setdefault((config, None, True), set())
            for function in Function.objects.filter(person=self).active().select_related(
                'team', 'team__type',
                'function_config', 'function_config__function_type',
                'team__structure', 'team__structure__type',
            ):
                if function.function_config.function_type.category == FunctionCategory.EMPLOYEE:
                    active = bool(self.employment)
                else:
                    active = bool(self.current_adhesion)
                for config in RoleConfig.objects.for_function(function):
                    structure_related = config.for_team or config.for_structure or config.for_sub_structures
                    team = function.team if structure_related else None
                    roles.setdefault((config, team, active), set())
                    roles[(config, team, active)] |= set(self.hold.filter(config=config, team=function.team))
            self._holder_roles_cache = roles
        return self._holder_roles_cache

    @property
    def proxy_roles(self):
        """Return {(config, team, active): fixed_roles}"""
        if not hasattr(self, '_proxy_roles_cache'):
            active = bool(self.current_adhesion or self.employment)
            roles = {}
            for role in self.proxied.all():
                if (
                    not role.holder
                    or (role.config, role.team, True) in role.holder.holder_roles
                    or (role.config, role.team, False) in role.holder.holder_roles
                ):
                    key = (role.config, role.team, active)
                    roles.setdefault(key, set())
                    roles[key].add(role)
            self._proxy_roles_cache = roles
        return self._proxy_roles_cache

    @property
    def roles(self):
        """Return {(config, team, active): fixed_roles}"""
        if not hasattr(self, '_roles_cache'):
            self._roles_cache = {}
            for key in chain(self.holder_roles.keys(), self.proxy_roles.keys()):
                self._roles_cache[key] = self.holder_roles.get(key, set()) | self.proxy_roles.get(key, set())
        return self._roles_cache

    def perms(self, only_active=True):
        if not hasattr(self, '_perms_cache'):
            self._perms_cache = [{}, {}]
            for config, team, active in self.roles:
                for permission in config.permissions.select_related('content_type'):
                    perm = f'{permission.content_type.app_label}.{permission.codename}'
                    self._perms_cache[False].setdefault(perm, set())
                    self._perms_cache[False][perm].add((config, team))
                    if active:
                        self._perms_cache[True].setdefault(perm, set())
                        self._perms_cache[True][perm].add((config, team))
        return self._perms_cache[only_active]

    def structures(self):
        return Structure.objects.is_active().filter(
            teams__function__person=self,
            teams__function__dates__contains=today(),
        ).distinct()

    @property
    def can_invite_itself(self):
        last_adhesion = self.last_adhesion
        if last_adhesion and last_adhesion.season == current_season() - 1:
            return self.user.has_perm('members.add_invitation', last_adhesion.structure, only_active=False)
        return False

    def has_perm(self, perm, obj=None, only_active=True):
        if perm not in self.perms(only_active):
            return False
        if not obj:
            return True
        return obj.__class__.objects.for_person_perm(perm, self, only_active).filter(pk=obj.pk).exists()

    def update_has_protected(self, commit=True):
        has_protected = self.protecteds.exists()
        if self.has_protected != has_protected:
            self.has_protected = has_protected
            if commit:
                self.save()

    def save(self, *args, **kwargs):
        assert not ((self.legal_guardian1 or self.legal_guardian2) and self.protecteds)

        # Set email_as_login
        with transaction.atomic():
            duplicate_email = Person.objects \
                .select_for_update() \
                .exclude(pk=self.pk) \
                .filter(email=self.email, email_as_login=True) \
                .exists()
            if self.email_as_login and duplicate_email:
                self.email_as_login = False
            if not self.email_as_login and not duplicate_email:
                self.email_as_login = True
            super().save(*args, **kwargs)

        # Normalize names
        self.normalized_first_name = normalize_name(self.first_name)
        self.normalized_last_name = normalize_name(self.last_name)

        # Update name tokens
        last_tokens = tokenize_name(self.last_name) | tokenize_name(self.family_name)
        PersonToken.objects.filter(person=self, part=PersonTokenPart.LAST).exclude(token__in=last_tokens).delete()
        PersonToken.objects.bulk_create(
            [PersonToken(token=token, person=self, part=PersonTokenPart.LAST) for token in last_tokens],
            ignore_conflicts=True,
        )
        first_tokens = tokenize_name(self.first_name) | tokenize_name(self.nickname)
        PersonToken.objects.filter(person=self, part=PersonTokenPart.FIRST).exclude(token__in=first_tokens).delete()
        PersonToken.objects.bulk_create(
            [PersonToken(token=token, person=self, part=PersonTokenPart.FIRST) for token in first_tokens],
            ignore_conflicts=True,
        )


class PersonTokenPart(models.IntegerChoices):
    FIRST = 1, "First"
    LAST = 2, "Last"


class PersonToken(models.Model):
    token = models.CharField(max_length=100, validators=[NameValidator()])
    person = models.ForeignKey(Person, verbose_name="Personne", on_delete=models.CASCADE, related_name='tokens')
    part = models.IntegerField("Partie", choices=PersonTokenPart.choices)

    class Meta:
        unique_together = ('token', 'person', 'part')
        indexes = [
            GinIndex(name='token_gin_idx', fields=['token'], opclasses=['gin_trgm_ops']),
            GinIndex(name='token_first_gin_idx', fields=['token'], opclasses=['gin_trgm_ops'],
                     condition=Q(part=PersonTokenPart.FIRST)),
            GinIndex(name='token_last_gin_idx', fields=['token'], opclasses=['gin_trgm_ops'],
                     condition=Q(part=PersonTokenPart.LAST)),
        ]
        ordering = ('token', )

    def __str__(self):
        return f"{self.token} → {self.get_part_display()}({self.person})"


class AdherentQuerySet(PermissionQuerySetMixin, models.QuerySet):
    def _q_for_self(self, person):
        return Q(person=person)

    def _q_for_team(self, team):
        return Q(
            person__functions__team=team,
            person__functions__dates__contains=today(),
            person__functions__status=FunctionStatus.OK,
        )

    def _q_for_structure(self, structure):
        return Q(
            person__functions__team__structure=structure,
            person__functions__dates__contains=today(),
            person__functions__status=FunctionStatus.OK,
        ) | Q(
            last_adhesion__structure=structure,
        )

    def _q_for_sub_structures(self, structure):
        return Q(
            person__functions__team__structure__lft__gt=structure.lft,
            person__functions__team__structure__lft__lt=structure.rght,
            person__functions__dates__contains=today(),
            person__functions__status=FunctionStatus.OK,
        ) | Q(
            last_adhesion__structure__lft__gt=structure.lft,
            last_adhesion__structure__lft__lt=structure.rght,
        )

    def _q_only_electeds(self):
        return Q(
            person__functions__function_config__subject_to_nomination=True,
            person__functions__dates__contains=today(),
            person__functions__status=FunctionStatus.OK,
        )

    def _q_only_employees(self):
        return Q(person__employee__employments__dates__contains=today())

    def _q_rgpd_delay(self):
        return Q(person__expired=False)

    def search(self, q):
        if q.isdigit():
            return self.filter(id=q).annotate(similarity=Value(1))
        match = re.fullmatch(r'([a-zA-Z]{3})-([0-9]{6})', q)
        if match:
            return self.filter(id=match.group(2)).annotate(similarity=Value(1))
        if "@" in q:
            return self.filter(person__email=q).annotate(similarity=Value(1))
        tokens = tokenize_name(q)
        return self.filter(
            reduce(or_, [Q(person__tokens__token__trigram_similar=token) for token in tokens])
        ).annotate(
            similarity=sum([Max(TrigramSimilarity('person__tokens__token', token)) for token in tokens]),
        ).filter(
            similarity__gte=0.4,
        )


class Adherent(models.Model):
    id = models.IntegerField("Numéro", primary_key=True, validators=[MinValueValidator(1)])
    uuid = models.UUIDField(unique=True, default=uuid4)
    person = models.OneToOneField(Person, verbose_name="Personne", on_delete=models.PROTECT)
    status = models.IntegerField("Statut", choices=AdherentStatus.choices, default=AdherentStatus.OK)
    last_adhesion = models.ForeignKey('Adhesion', verbose_name="Dernière adhesion", null=True, blank=True,
                                      related_name='+', on_delete=models.PROTECT)
    duplicates = ArrayField(models.IntegerField(validators=[MinValueValidator(1)]), verbose_name='Numéros doublons',
                            default=list, blank=True)
    suspension_deadline = models.DateField("Date de fin de suspension", blank=True, null=True)
    merged_with = ArrayField(models.UUIDField(), verbose_name='Fusionné avec', default=list, blank=True)

    objects = models.Manager.from_queryset(AdherentQuerySet)()

    class Meta:
        verbose_name = "Adhérent·e"
        permissions = [
            ('change_adherent_status', "Suspendre ou radier les adhérents"),
            ('apiv3_adherent', "Can read Adherent with API v3"),
            ('apiv4_read_adherent', "Can read Adherent with API v4"),
            ('apiv5_read_adherent', "Can read Adherent with API v5"),
        ]

    renamed_permissions = [
        ('view_adherent', "Consulter les adhérents"),
        ('add_adherent', "Ajouter un adhérent"),
        ('change_adherent', "Modifier les adhérents"),
        ('delete_adherent', "Supprimer les adhérents"),
    ]

    def __str__(self):
        return f"{self.person.full_name} [{self.id_str}]"

    @property
    def id_str(self):
        if not self.id:
            return "ADH-?"
        return f"ADH-{self.id:06d}"

    def duplicates_display(self):
        return ", ".join(f"{id:6d}" for id in self.duplicates)

    @property
    def status_text(self):
        adhesion = self.adhesion
        if self.status != AdherentStatus.OK:
            return self.get_status_display()
        if not adhesion:
            if self.person.last_invitation == current_season():
                return "Invité·e"
            else:
                return "Ancien·ne"
        if not adhesion.is_paid:
            return "Non payé"
        elif adhesion.is_discovery:
            if adhesion.begin > today():
                return "Future découverte"
            elif adhesion.end < today():
                return "Ex découverte"
            else:
                return "Découverte"
        else:
            return "Adhérent·e"

    @property
    def adhesion(self):
        if not self.last_adhesion:
            return None
        if self.last_adhesion.season != current_season():
            return None
        assert not self.last_adhesion.canceled
        return self.last_adhesion

    @property
    def status_label(self):
        adhesion = self.adhesion
        if self.status != AdherentStatus.OK:
            color = {
                AdherentStatus.LEFT: 'default',
                AdherentStatus.SUSPENDED: 'danger',
                AdherentStatus.EXCLUDED: 'danger',
            }[self.status]
            text = self.get_status_display()
        elif not adhesion:
            if self.person.last_invitation == current_season():
                color = 'warning'
                text = "Invité·e"
            else:
                color = 'default'
                text = "Ancien·ne"
        elif not adhesion.is_paid:
            color = 'danger'
            text = "Non payé"
        elif adhesion.is_discovery:
            if self.person.last_invitation == current_season():
                color = 'warning'
                text = "Invité·e"
            elif adhesion.begin > today():
                color = 'warning'
                text = "Future découverte"
            elif adhesion.end < today():
                color = 'danger'
                text = "Ex découverte"
            else:
                color = 'success'
                text = "Découverte"
        else:
            color = 'success'
            text = "Adhérent·e"
        return mark_safe(f'<span class="label label-{color}">{text}</span>')

    def get_additional_data(self):
        """Keep link between object and person for log"""
        return {'person_id': [self.person.pk]}

    def is_blocked(self):
        return self.status not in (AdherentStatus.OK, AdherentStatus.LEFT)


class EmployeeQuerySet(PermissionQuerySetMixin, models.QuerySet):
    def _q_for_self(self, person):
        return Q(person=person)

    def _q_for_team(self, team):
        return Q(
            person__functions__team=team,
            person__functions__dates__contains=today(),
            person__functions__status=FunctionStatus.OK,
        )

    def _q_for_structure(self, structure):
        return Q(
            person__functions__team__structure=structure,
            person__functions__dates__contains=today(),
            person__functions__status=FunctionStatus.OK,
        )

    def _q_for_sub_structures(self, structure):
        return Q(
            person__functions__team__structure__lft__gt=structure.lft,
            person__functions__team__structure__lft__lt=structure.rght,
            person__functions__dates__contains=today(),
            person__functions__status=FunctionStatus.OK,
        )

    def _q_only_electeds(self):
        return Q(
            person__functions__function_config__subject_to_nomination=True,
            person__functions__dates__contains=today(),
            person__functions__status=FunctionStatus.OK,
        )

    def _q_only_employees(self):
        return Q()

    def _q_rgpd_delay(self):
        return Q(person__expired=False)

    def overlap(self, begin, end):
        return self.filter(
            employments__dates__overlap=DateRange(begin, end, '[]')
        )

    def for_season(self, season):
        return self.overlap(
            Date(season - 1, 9, 1),
            Date(season, 8, 31),
        )

    def for_year(self, year):
        return self.overlap(
            Date(year, 1, 1),
            Date(year, 12, 31),
        )

    def current(self):
        return self.filter(last_employment__dates__contains=today())

    def search(self, q):
        if q.isdigit():
            return self.filter(id=q).annotate(similarity=Value(1))
        match = re.fullmatch(r'([a-zA-Z]{3})-([0-9]{6})', q)
        if match:
            return self.filter(id=match.group(2)).annotate(similarity=Value(1))
        if "@" in q:
            return self.filter(Q(email=q) | Q(person__email=q)).annotate(similarity=Value(1))
        tokens = tokenize_name(q)
        return self.filter(
            reduce(or_, [Q(person__tokens__token__trigram_similar=token) for token in tokens])
        ).annotate(
            similarity=sum([Max(TrigramSimilarity('person__tokens__token', token)) for token in tokens]),
        ).filter(
            similarity__gte=0.4,
        )


class Employee(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid4)
    person = models.OneToOneField(Person, verbose_name="Personne", on_delete=models.PROTECT)
    email = models.EmailField("Email professionel", db_collation="case_insensitive", blank=True,
                              validators=[Html5EmailValidator()])
    phone = PhoneNumberField("Tél. professionnel", blank=True)
    last_employment = models.ForeignKey('Employment', verbose_name="Dernier contrat", null=True, blank=True,
                                        related_name='+', on_delete=models.PROTECT)
    merged_with = ArrayField(models.UUIDField(), verbose_name='Fusionné avec', default=list, blank=True)

    objects = models.Manager.from_queryset(EmployeeQuerySet)()

    class Meta:
        verbose_name = "Salarié·e, CEE, SC"
        permissions = [
            ('apiv3_employee', "Can read Employee with API v3"),
            ('apiv4_read_employee', "Can read Employee with API v4"),
            ('apiv5_read_employee', "Can read Employee with API v5"),
        ]

    renamed_permissions = [
        ('view_employee', "Consulter les salariés, CEE, SC"),
        ('add_employee', "Ajouter un·e salarié·es, CEE, SC"),
        ('change_employee', "Modifier les salariés, CEE, SC"),
        ('delete_employee', "Supprimer les salariés, CEE, SC"),
    ]

    def __str__(self):
        return f"{self.person.full_name} [{self.id_str}]"

    @property
    def id_str(self):
        if self.last_employment:
            return f"{EmploymentType(self.last_employment.type).name}-{self.id:06d}"
        else:
            return f"SAL-{self.id:06d}"

    @property
    def employment(self):
        if not self.last_employment:
            return None
        if self.last_employment.begin > today():
            return None
        if self.last_employment.end and self.last_employment.end < today():
            return None
        return self.last_employment

    @property
    def status_text(self):
        employment = self.employment
        if not employment:
            return "Ancien·ne"
        return employment.get_type_display()

    @property
    def status_label(self):
        employment = self.employment
        if not employment:
            return mark_safe('<span class="label label-default">Ancien·ne</span>')
        return mark_safe(f'<span class="label label-primary">{employment.get_type_display()}</span>')

    @property
    def phone_display(self):
        if not self.phone:
            return ''
        elif self.phone.country_code == 33:
            return self.phone.as_national
        else:
            return self.phone.as_international

    def get_additional_data(self):
        """Keep link between object and person for log"""
        return {'person_id': [self.person.pk]}


class AdhesionQuerySet(PermissionQuerySetMixin, models.QuerySet):
    def _q_for_self(self, person):
        return Q(adherent__person=person)

    def _q_for_team(self, team):
        return Q(
            adherent__person__functions__team=team,
            adherent__person__functions__dates__contains=today(),
            adherent__person__functions__status=FunctionStatus.OK,
        )

    def _q_for_structure(self, structure):
        return Q(
            adherent__person__functions__team__structure=structure,
            adherent__person__functions__dates__contains=today(),
            adherent__person__functions__status=FunctionStatus.OK,
        ) | Q(
            structure=structure,
        )

    def _q_for_sub_structures(self, structure):
        return Q(
            adherent__person__functions__team__structure__lft__gt=structure.lft,
            adherent__person__functions__team__structure__lft__lt=structure.rght,
            adherent__person__functions__dates__contains=today(),
            adherent__person__functions__status=FunctionStatus.OK,
        ) | Q(
            structure__lft__gt=structure.lft,
            structure__lft__lt=structure.rght,
        )

    def _q_only_electeds(self):
        return Q(
            adherent__person__functions__function_config__subject_to_nomination=True,
            adherent__person__functions__dates__contains=today(),
            adherent__person__functions__status=FunctionStatus.OK,
        )

    def _q_only_employees(self):
        return Q(pk=None)

    def _q_rgpd_delay(self):
        return Q(adherent__person__expired=False)

    def with_effective_rate(self):
        return self.annotate(
            qs_effective_rate=Case(
                When(rate__rate_max=None, then=F('rate__rate')),
                default=F('free_rate')
            )
        )

    def with_age(self, date=None):
        if not date:
            date = today()
        return self.annotate(qs_age=ExtractYear(
            Func(Value(date), F('adherent__person__birthdate'), function='age'),
            output_field=models.IntegerField()
        ))

    def is_active(self):
        return self.filter(season=current_season(), canceled=False)

    def filter_parent_structure(self, structure):
        return self.filter(
            structure__lft__gte=structure.lft,
            structure__lft__lte=structure.rght,
        )

    def search(self, q):
        if q.isdigit():
            return self.filter(adherent__id=q).annotate(similarity=Value(1))
        match = re.fullmatch(r'([a-zA-Z]{3})-([0-9]{6})', q)
        if match:
            return self.filter(adherent__id=match.group(2)).annotate(similarity=Value(1))
        if "@" in q:
            return self.filter(adherent__person__email=q).annotate(similarity=Value(1))
        tokens = tokenize_name(q)
        return self.filter(
            reduce(or_, [Q(adherent__person__tokens__token__trigram_similar=token) for token in tokens])
        ).annotate(
            similarity=sum([Max(TrigramSimilarity('adherent__person__tokens__token', token)) for token in tokens]),
        ).filter(
            similarity__gte=0.4,
        )


class Adhesion(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid4)
    adherent = models.ForeignKey(Adherent, verbose_name="Adhérent", related_name='adhesions', on_delete=models.PROTECT)
    season = models.IntegerField("Saison")
    dates = DateRangeField(verbose_name="Dates")
    entry_date = models.DateField("Date de saisie")
    rate = models.ForeignKey(Rate, verbose_name="Tarif", on_delete=models.PROTECT)
    free_rate = models.DecimalField(verbose_name="Montant libre", max_digits=5, decimal_places=2,
                                    validators=(validate_strictly_positive, ), null=True, blank=True)
    structure = models.ForeignKey(Structure, verbose_name="Structure",
                                  related_name='adhesions', on_delete=models.PROTECT)
    donation = models.PositiveIntegerField("+ Don (€)", default=0)
    token = models.CharField(max_length=50)
    canceled = models.BooleanField("Annulée", default=False)
    signatory = models.CharField("Signataire", max_length=200, blank=True)
    is_paid = models.BooleanField("Payée", default=False)
    is_discovery = models.BooleanField("Découverte", default=False)

    objects = models.Manager.from_queryset(AdhesionQuerySet)()

    class Meta:
        verbose_name = "Adhésion"
        constraints = [
            models.UniqueConstraint(
                fields=['adherent', 'season'],
                condition=Q(canceled=False),
                name='adhesion_unique_adherent_season',
            ),
            models.CheckConstraint(
                check=~Q(dates__startswith=None) & ~Q(dates__endswith=None),
                name='adhesion_dates_not_null',
            )
        ]
        permissions = [
            ('view_unpaid_adhesion', "Consulter les adhésions non payées"),
            ('download_adhesion_card', "Télécharger les cartes d'adhésion"),
            ('import_adhesion', "Importer des adhésions"),
            ('change_old_adhesion', "Modifier également les adhésions passées"),
            ('apiv1_adhesion', "Can view Adhesion with API v1"),
            ('apiv3_adhesion', "Can read Adhesion with API v3"),
            ('apiv4_read_adhesion', "Can read Adhesion with API v4"),
            ('apiv5_read_adhesion', "Can read Adhesion with API v5"),
        ]

    renamed_permissions = [
        ('view_adhesion', "Consulter les adhésions"),
        ('add_adhesion', "Prendre les adhésions"),
        ('change_adhesion', "Modifier les adhésions"),
        ('delete_adhesion', "Annuler les adhésions"),
    ]

    def __str__(self):
        try:
            adherent = self.adherent
        except Adherent.DoesNotExist:
            adherent = None
        if not self.season:
            return f"None/None {adherent}"
        return f"{self.season - 1}/{self.season} {adherent}"

    @property
    def status_label(self):
        if self.adherent.status != AdherentStatus.OK:
            color = {
                AdherentStatus.LEFT: 'default',
                AdherentStatus.SUSPENDED: 'danger',
                AdherentStatus.EXCLUDED: 'danger',
            }[self.adherent.status]
            text = self.adherent.get_status_display()
        elif self.canceled:
            color = 'danger'
            text = "Annulée"
        elif not self.is_paid:
            color = 'danger'
            text = "Non payé"
        elif self.is_discovery:
            if self.adherent.person.last_invitation == current_season():
                color = 'warning'
                text = "Invité·e"
            elif self.begin > today():
                color = 'warning'
                text = "Future découverte"
            elif self.end < today():
                color = 'danger'
                text = "Ex découverte"
            else:
                color = 'success'
                text = "Découverte"
        else:
            color = 'success'
            text = "Adhérent·e"
        return mark_safe(f'<span class="label label-{color}">{text}</span>')

    @property
    def functions(self):
        return self.adherent.person.functions.for_season(self.season)

    @property
    def structure_functions(self):
        return self.adherent.person.functions.for_season(self.season).filter(team__structure=self.structure)

    @property
    def season_display(self):
        return f"{self.season - 1}/{self.season}"

    @property
    def effective_rate(self):
        if not self.rate:
            return None
        if self.rate.rate_max:
            return self.free_rate
        else:
            return self.rate.rate

    @property
    def amount(self):
        return self.effective_rate + self.donation

    @property
    def paid(self):
        return sum([
            allocation.amount for allocation in self.successful_allocations
        ])

    @property
    def successful_allocations(self):
        return self.allocation_set.filter(payment__online_status__in=(PaymentStatus.MANUAL, PaymentStatus.SUCCESS))

    @property
    def balance_contribution(self):
        allocations = self.successful_allocations.filter(category=Allocation.CATEGORY_CONTRIBUTION)
        return self.effective_rate - sum(allocation.amount for allocation in allocations)

    @property
    def balance_donation(self):
        allocations = self.successful_allocations.filter(category=Allocation.CATEGORY_DONATION)
        return self.donation - sum(allocation.amount for allocation in allocations)

    @property
    def balance(self):
        return self.balance_contribution + self.balance_donation

    @property
    def receipt(self):
        try:
            return Receipt.objects.get(payment__allocation__adhesion=self, canceled=False)
        except Receipt.DoesNotExist:
            return None

    @property
    def begin(self):
        lower = self.dates.lower
        if not self.dates.lower_inc:
            lower += TimeDelta(days=1)
        return lower

    @property
    def end(self):
        if not self.dates.upper:
            return None
        upper = self.dates.upper
        if not self.dates.upper_inc:
            upper -= TimeDelta(days=1)
        return upper

    def set_dates(self, begin):
        try:
            rate = self.rate
        except Rate.DoesNotExist:
            rate = None
        if rate and rate.duration:
            end = begin + TimeDelta(days=rate.duration)
        else:
            end = begin.replace(month=9, day=1)
            if end <= begin:
                end = end.replace(year=end.year + 1)
        self.dates = DateRange(begin, end)

    @property
    def is_current(self):
        return self.season == current_season() and not self.canceled

    @property
    def is_current_full_rate(self):
        return self.is_current and self.rate.category != RateCategory.DISCOVERY

    def save(self, *args, **kwargs):
        if not self.token:
            self.token = hexlify(urandom(20)).decode()
        super().save(*args, **kwargs)

    def send_email(self, request=None, connection=None):
        emails = self.adherent.person.emails_with_guardians
        if not emails:
            return
        if not self.rate.send_confirmation:
            return
        context = {
            'adhesion': self,
            'person': self.adherent.person,
            'structure': self.structure,
            'url': build_url(reverse_lazy('adhesion_card', args=[self.pk]), request) + f'?token={self.token}',
            'SUPPORT_EMAIL': settings.SUPPORT_EMAIL,
            'SUPPORT_PHONE': phonenumbers.parse(settings.SUPPORT_PHONE),
        }
        template = get_template('members/adhesion_email.txt')
        email = EmailMessage(
            subject="[EEDF] Ton adhésion",
            body=template.render(context),
            reply_to=[self.structure.public_email or settings.SUPPORT_EMAIL],
            to=emails,
            connection=connection
        )
        email.send()

    def get_additional_data(self):
        """Keep link between object and person for log"""
        return {'person_id': [self.adherent.person.pk]}

    def update_is_paid(self, commit=True):
        is_paid = self.paid >= self.amount
        if self.is_paid != is_paid:
            self.is_paid = is_paid
            if commit:
                self.save()


class TeamType(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid4)
    name = models.CharField("Nom", max_length=100)
    unique = models.BooleanField("Unique", default=False, help_text="Une seule équipe par structure")
    management = models.BooleanField("Équipe de gestion", default=False)
    custom = models.BooleanField("Personnalisé", default=False)
    order = models.IntegerField(verbose_name="Numéro d'ordre", null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Type d'équipe"
        verbose_name_plural = "Types d'équipe"
        permissions = [
            ('apiv1_teamtype', "Can view TeamType with API v1"),
            ('apiv3_teamtype', "Can read TeamType with API v3"),
            ('apiv4_read_teamtype', "Can read TeamType with API v4"),
        ]
        ordering = ('order', 'name')


class TeamConfigQuerySet(models.QuerySet):
    def with_team_name(self):
        return self.annotate(qs_team_name=Case(
            When(Q(team_type__management=True), then=F('structure_type__management_team_name')),
            default=F('team_type__name'),
        ))

    def search(self, q):
        return self.filter(qs_team_name__unaccent__icontains=q)


class TeamConfig(models.Model):
    team_type = models.ForeignKey(TeamType, verbose_name="Type d'équipe", on_delete=models.PROTECT,
                                  related_name="config")
    structure_type = models.ForeignKey(StructureType, verbose_name="Type de structure", on_delete=models.PROTECT)
    by_default = models.BooleanField("Par défaut", default=False)

    objects = models.Manager.from_queryset(TeamConfigQuerySet)()

    class Meta:
        verbose_name = "Configuration d'équipe"
        verbose_name_plural = "Configurations des équipes"
        permissions = [
            ('apiv1_team', "Can view Team with API v1"),
            ('apiv3_team', "Can read Team with API v3"),
            ('apiv4_read_team', "Can read Team with API v4"),
            ('apiv5_read_team', "Can read Team with API v5"),
        ]

    def __str__(self):
        return self.team_name

    @property
    def team_name(self):
        if self.team_type.management:
            return self.structure_type.management_team_name
        else:
            return self.team_type.name


class TeamQuerySet(PermissionQuerySetMixin, models.QuerySet):
    def _q_for_self(self, person):
        return Q(pk=None)

    def _q_for_team(self, team):
        return Q(pk=team.pk)

    def _q_for_structure(self, structure):
        return Q(structure=structure)

    def _q_for_sub_structures(self, structure):
        return Q(structure__lft__gt=structure.lft, structure__lft__lt=structure.rght)

    def _q_only_electeds(self):
        return Q(pk=None)

    def _q_only_employees(self):
        return Q(pk=None)

    def _q_rgpd_delay(self):
        return Q()

    def is_active(self):
        return self.filter(deactivation_date=None)

    def is_inactive(self):
        return self.exclude(deactivation_date=None)

    def with_headcount(self, season):
        begin = Date(season - 1, 9, 1)
        end = Date(season, 8, 31)
        return self.annotate(qs_headcount=Count('function', filter=Q(
            function__dates__overlap=DateRange(begin, end, '[]'),
        )))

    def for_adherent(self):
        return self.filter(type__functionconfig__function_type__category__in=(
            FunctionCategory.PARTICIPANT,
            FunctionCategory.VOLUNTEER,
        )).distinct()


class Team(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid4)
    structure = models.ForeignKey(Structure, verbose_name="Structure",
                                  related_name='teams', on_delete=models.PROTECT)
    type = models.ForeignKey(TeamType, verbose_name="Type", related_name='teams', on_delete=models.PROTECT)
    name = models.CharField("Nom", max_length=100)
    creation_date = models.DateTimeField("Date de création")
    deactivation_date = models.DateTimeField("Date de désactivation", null=True, blank=True)

    objects = models.Manager.from_queryset(TeamQuerySet)()

    def __str__(self):
        return self.name

    @property
    def type_display(self):
        return self.type.name if self.type else "Personnalisé"

    @property
    def status_text(self):
        if self.deactivation_date:
            return "Inactive"
        else:
            return "Active"

    @property
    def status_label(self):
        if self.deactivation_date:
            return mark_safe('<span class="label label-danger">Inactive</span>')
        else:
            return mark_safe('<span class="label label-success">Active</span>')

    @property
    def is_active(self):
        return self.deactivation_date is None

    @cached_property
    def headcount(self, season=None):
        return self.function_set.for_season(season or current_season()).count()

    class Meta:
        verbose_name = "Équipe"
        constraints = [
            models.UniqueConstraint('structure', 'name', name='unique_structure_name'),
        ]

    renamed_permissions = [
        ('view_team', "Consulter les équipes"),
        ('add_team', "Créer une équipe"),
        ('change_team', "Modifier les équipes"),
        ('delete_team', "Supprimer les équipes"),
    ]


class FunctionQuerySet(PermissionQuerySetMixin, models.QuerySet):
    def _q_for_self(self, person):
        return Q(person=person)

    def _q_for_team(self, team):
        return Q(team=team)

    def _q_for_structure(self, structure):
        return Q(team__structure=structure)

    def _q_for_sub_structures(self, structure):
        return Q(team__structure__lft__gt=structure.lft, team__structure__lft__lt=structure.rght)

    def _q_only_electeds(self):
        return Q(function_config__subject_to_nomination=True)

    def _q_only_employees(self):
        return Q(function_config__function_type__category=FunctionCategory.EMPLOYEE)

    def _q_rgpd_delay(self):
        return Q(person__expired=False)

    def overlap(self, begin, end):
        return self.filter(
            dates__overlap=DateRange(begin, end, '[]')
        )

    def for_season(self, season):
        return self.overlap(
            Date(season - 1, 9, 1),
            Date(season, 8, 31),
        )

    def for_year(self, year):
        return self.overlap(
            Date(year, 1, 1),
            Date(year, 12, 31),
        )

    def current(self):
        return self.filter(dates__contains=today())

    def active(self):
        return self.filter(
            status=FunctionStatus.OK,
            dates__contains=today(),
            # FIXME check adhesion or employment
        )

    def search(self, q):
        q = remove_accents(q)
        return self.annotate(
            similarity=TrigramSimilarity('name__unaccent', q),
            word_similarity=TrigramStrictWordSimilarity(q, 'name__unaccent'),
        ).filter(
            word_similarity__gte=0.4,
        )


class FunctionStatus(models.IntegerChoices):
    OK = 1, "OK"
    REQUESTED = 2, "Demandé"
    REFUSED = 3, "Refusé"


class Function(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid4)
    person = models.ForeignKey(Person, verbose_name="Personne", on_delete=models.PROTECT, related_name='functions')
    team = models.ForeignKey(Team, verbose_name="Équipe", on_delete=models.PROTECT)
    function_config = models.ForeignKey(FunctionConfig, verbose_name="Configuration de la fonction",
                                        on_delete=models.PROTECT)
    dates = DateRangeField(verbose_name="Dates")
    name = models.CharField("Nom", max_length=100, blank=True)
    token = models.CharField(max_length=50)
    status = models.IntegerField("Statut", choices=FunctionStatus.choices, default=FunctionStatus.OK)
    owner = models.ForeignKey(User, verbose_name="Module propriétaire", null=True, blank=True, on_delete=models.PROTECT)

    objects = models.Manager.from_queryset(FunctionQuerySet)()

    class Meta:
        verbose_name = "Fonction"
        permissions = [
            ('apiv1_function', "Can view Function with API v1"),
            ('apiv3_function', "Can read Function with API v3"),
            ('apiv4_read_function', "Can read Function with API v4"),
            ('apiv4_write_function', "Can write Function with API v4"),
            ('apiv5_read_function', "Can read Function with API v5"),
            ('apiv5_write_function', "Can write Function with API v5"),
            ('view_inconsistencies', "Voir les incohérences âge/fonction"),
        ]
        ordering = ('name', )
        constraints = [
            models.CheckConstraint(
                check=~Q(dates__startswith=None),
                name='function_begin_not_null',
            ),
            # ExclusionConstraint(name='function_unicity', expressions=(
            #     ('person', RangeOperators.EQUAL),
            #     ('team', RangeOperators.EQUAL),
            #     ('type', RangeOperators.EQUAL),
            #     ('dates', RangeOperators.OVERLAPS),
            # )),
        ]

    renamed_permissions = [
        ('view_function', "Consulter les fonctions"),
        ('add_function', "Ajouter une fonction"),
        ('change_function', "Modifier les fonctions"),
        ('delete_function', "Supprimer les fonctions"),
    ]

    def __str__(self):
        if self.name == self.function_config.name:
            return self.name
        return "{self.function_config.name} ({self.name})".format(self=self)

    @property
    def begin(self):
        lower = self.dates.lower
        if not self.dates.lower_inc:
            lower += TimeDelta(days=1)
        return lower

    @property
    def end(self):
        if not self.dates.upper:
            return None
        upper = self.dates.upper
        if not self.dates.upper_inc:
            upper -= TimeDelta(days=1)
        return upper

    def dates_display(self):
        if not self.end:
            return f"à partir du {self.begin.strftime('%d/%m/%Y')}"
        return f"entre le {self.begin.strftime('%d/%m/%Y')} et le {self.end.strftime('%d/%m/%Y')}"

    @property
    def status_label(self):
        if self.status == FunctionStatus.OK:
            color = 'success'
        elif self.status == FunctionStatus.REQUESTED:
            color = 'warning'
        elif self.status == FunctionStatus.REFUSED:
            color = 'danger'
        return mark_safe(f'<span class="label label-{color}">{self.get_status_display()}</span>')

    def save(self, *args, **kwargs):
        if not self.token:
            self.token = hexlify(urandom(20)).decode()
        super().save(*args, **kwargs)

    def send_email(self, request):
        assert self.status == FunctionStatus.REQUESTED
        emails = self.person.emails_with_guardians
        context = {
            'user': request.user,
            'function': self,
            'url': build_url(reverse_lazy('function_reply', args=[self.token]), request),
            'SUPPORT_EMAIL': settings.SUPPORT_EMAIL,
            'SUPPORT_PHONE': phonenumbers.parse(settings.SUPPORT_PHONE),
        }
        template = get_template('members/function_request_email.txt')
        email = EmailMessage(
            subject="[EEDF] Demande de confirmation d'une nouvelle fonction",
            body=template.render(context),
            reply_to=[settings.SUPPORT_EMAIL],
            to=emails,
        )
        email.send()

    def get_additional_data(self):
        """Keep link between object and person for log"""
        return {
            'person_id': [self.person.pk],
            'team_id': [self.team.pk],
        }


class PaymentQuerySet(PermissionQuerySetMixin, models.QuerySet):
    def _q_for_self(self, person):
        return Q(allocation__adhesion__adherent__person=person)

    def _q_for_team(self, team):
        return Q(
            allocation__adhesion__adherent__person__functions__team=team,
            allocation__adhesion__adherent__person__functions__dates__contains=today(),
            allocation__adhesion__adherent__person__functions__status=FunctionStatus.OK,
        )

    def _q_for_structure(self, structure):
        return Q(
            allocation__adhesion__adherent__person__functions__team__structure=structure,
            allocation__adhesion__adherent__person__functions__dates__contains=today(),
            allocation__adhesion__adherent__person__functions__status=FunctionStatus.OK,
        ) | Q(
            allocation__adhesion__structure=structure,
        )

    def _q_for_sub_structures(self, structure):
        return Q(
            allocation__adhesion__adherent__person__functions__team__structure__lft__gt=structure.lft,
            allocation__adhesion__adherent__person__functions__team__structure__lft__lt=structure.rght,
            allocation__adhesion__adherent__person__functions__dates__contains=today(),
            allocation__adhesion__adherent__person__functions__status=FunctionStatus.OK,
        ) | Q(
            allocation__adhesion__structure__lft__gt=structure.lft,
            allocation__adhesion__structure__lft__lt=structure.rght,
        )

    def _q_only_electeds(self):
        return Q(pk=None)

    def _q_only_employees(self):
        return Q(pk=None)

    def _q_rgpd_delay(self):
        return Q(payer=None) | Q(payer__expired=False)

    def for_season(self, season):
        return self.filter(allocation__adhesion__season=season).distinct()


class PaymentStatus(models.IntegerChoices):
    MANUAL = 1, "Manuel"
    IN_PROGRESS = 2, "En cours"
    FAILURE = 3, "Échec"
    SUCCESS = 4, "Succès"


class Payment(models.Model):
    METHOD_CHOICES = (
        (1, "Chèque"),
        (2, "Espèces"),
        (3, "Virement"),
        (4, "Carte bancaire"),
        (6, "Helloasso"),
        (5, "Autre (ANCV, CAF…)"),
    )

    amount = models.DecimalField(verbose_name="Montant", max_digits=5, decimal_places=2,
                                 validators=(validate_strictly_positive, ))
    date = models.DateField(verbose_name="Date")
    payer = models.ForeignKey(Person, verbose_name="Payeur", null=True, blank=True, on_delete=models.PROTECT)
    method = models.IntegerField(verbose_name="Mode de réglement", choices=METHOD_CHOICES)
    reference = models.CharField(verbose_name="Référence (ex. n° de chèque)", max_length=100, blank=True)
    online_status = models.IntegerField(verbose_name="Statut", choices=PaymentStatus, default=PaymentStatus.MANUAL)

    objects = models.Manager.from_queryset(PaymentQuerySet)()

    class Meta:
        verbose_name = "Paiement"
        permissions = [
            ('add_old_payment', "Ajouter également un paiement sur les saisons fermées."),
            ('change_old_payment', "Modifier également les paiements sur les saisons fermées."),
            ('delete_old_payment', "Supprimer également les paiements sur les saisons fermées."),
        ]
        constraints = [
            models.CheckConstraint(check=Q(method=5) | ~Q(payer=None), name='payment_payer_or_method5'),
        ]

    renamed_permissions = [
        ('view_payment', "Consulter les paiements"),
        ('add_payment', "Ajouter un paiement"),
        ('change_payment', "Modifier les paiements"),
        ('delete_payment', "Supprimer les paiements"),
    ]

    def __str__(self):
        payer = self.payer or " la structure EEDF"
        return f"{self.amount} € le {self.date.strftime('%d/%m/%Y')} par {payer}"

    @property
    def method_display(self):
        return self.get_method_display()

    @property
    def adherents_display(self):
        allocations = self.allocation_set.select_related('adhesion__adherent__person')
        return ", ".join(set(str(allocation.adhesion.adherent) for allocation in allocations))

    @property
    def seasons_display(self):
        allocations = self.allocation_set.select_related('adhesion')
        return ", ".join(set(allocation.adhesion.season_display for allocation in allocations))

    @property
    def adhesions(self):
        return Adhesion.objects.filter(
            allocation__payment=self
        ).distinct().order_by(
            'adherent__person__last_name',
            'adherent__person__first_name'
        )

    def create_allocations(self, adhesion):
        amount_contribution = min(adhesion.balance_contribution, self.amount)
        amount_donation = min(adhesion.balance_donation, self.amount - amount_contribution)
        assert amount_contribution + amount_donation <= self.amount
        if amount_contribution:
            Allocation.objects.create(
                payment=self,
                adhesion=adhesion,
                amount=amount_contribution,
                category=Allocation.CATEGORY_CONTRIBUTION,
            )
        if amount_donation:
            assert self.payer
            Allocation.objects.create(
                payment=self,
                adhesion=adhesion,
                amount=amount_donation,
                category=Allocation.CATEGORY_DONATION,
            )
        adhesion.update_is_paid()

    def get_additional_data(self):
        """Keep link between object and person for log"""
        person_ids = set(allocation.adhesion.adherent.person.id for allocation in self.allocation_set.all())
        if self.payer:
            person_ids.add(self.payer.id)
        return {'person_id': list(person_ids)}


class Allocation(models.Model):
    CATEGORY_CONTRIBUTION = 1
    CATEGORY_DONATION = 2
    CATEGORY_CHOICES = (
        (CATEGORY_CONTRIBUTION, "Cotisation"),
        (CATEGORY_DONATION, "Don"),
    )

    payment = models.ForeignKey(Payment, verbose_name="Paiement", on_delete=models.PROTECT)
    adhesion = models.ForeignKey(Adhesion, verbose_name="Adhésion", on_delete=models.PROTECT)
    amount = models.DecimalField(verbose_name="Montant", max_digits=5, decimal_places=2,
                                 validators=(validate_strictly_positive, ))
    category = models.IntegerField(verbose_name="Catégorie", choices=CATEGORY_CHOICES)

    class Meta:
        verbose_name = "Allocation"

    def __str__(self):
        return f"{self.get_category_display()} {self.amount} € @ {self.adhesion}"

    def get_additional_data(self):
        """Keep link between object and person for log"""
        person_ids = [self.adhesion.adherent.person.id]
        if self.payment.payer:
            person_ids.append(self.payment.payer.id)
        return {'person_id': person_ids}


class ReceiptQuerySet(PermissionQuerySetMixin, models.QuerySet):
    def _q_for_self(self, person):
        return Q(payment__allocation__adhesion__adherent__person=person)

    def _q_for_team(self, team):
        return Q(
            payment__allocation__adhesion__adherent__person__functions__team=team,
            payment__allocation__adhesion__adherent__person__functions__dates__contains=today(),
            payment__allocation__adhesion__adherent__person__functions__status=FunctionStatus.OK,
        )

    def _q_for_structure(self, structure):
        return Q(
            payment__allocation__adhesion__adherent__person__functions__team__structure=structure,
            payment__allocation__adhesion__adherent__person__functions__dates__contains=today(),
            payment__allocation__adhesion__adherent__person__functions__status=FunctionStatus.OK,
        ) | Q(
            payment__allocation__adhesion__structure=structure,
        )

    def _q_for_sub_structures(self, structure):
        return Q(
            payment__allocation__adhesion__adherent__person__functions__team__structure__lft__gt=structure.lft,
            payment__allocation__adhesion__adherent__person__functions__team__structure__lft__lt=structure.rght,
            payment__allocation__adhesion__adherent__person__functions__dates__contains=today(),
            payment__allocation__adhesion__adherent__person__functions__status=FunctionStatus.OK,
        ) | Q(
            payment__allocation__adhesion__structure__lft__gt=structure.lft,
            payment__allocation__adhesion__structure__lft__lt=structure.rght,
        )

    def _q_only_electeds(self):
        return Q(
            payment__allocation__adhesion__adherent__person__functions__function_config__subject_to_nomination=True,
            payment__allocation__adhesion__adherent__person__functions__dates__contains=today(),
            payment__allocation__adhesion__adherent__person__functions__status=FunctionStatus.OK,
        )

    def _q_only_employees(self):
        return Q(pk=None)

    def _q_rgpd_delay(self):
        return Q(payment__payer__expired=False)


class Receipt(models.Model):
    payment = models.OneToOneField(Payment, verbose_name="Paiement", on_delete=models.PROTECT, null=True)
    number = models.CharField(verbose_name="Numéro", max_length=32)
    name = models.CharField(verbose_name="Nom", max_length=200)
    email = models.EmailField("Email", db_collation="case_insensitive", blank=True,
                              validators=[Html5EmailValidator()])
    address = models.CharField(verbose_name="Adresse", max_length=200)
    amount = models.DecimalField(verbose_name="Montant", max_digits=5, decimal_places=2)
    date = models.DateField(verbose_name="Date")
    canceled = models.BooleanField(default=False)
    token = models.CharField(verbose_name="Jeton", max_length=64)
    sending = models.DateTimeField(verbose_name="Envoi", blank=True, null=True)
    download = models.DateTimeField(verbose_name="Téléchargement", blank=True, null=True)

    objects = models.Manager.from_queryset(ReceiptQuerySet)()

    class Meta:
        verbose_name = "Reçu fiscal (adhésions)"
        verbose_name_plural = "Reçus fiscaux (adhésions)"
        constraints = [
            models.CheckConstraint(check=~Q(payment=None) | Q(canceled=True), name='receipt_payment_not_null'),
        ]

    renamed_permissions = [
        ('view_receipt', "Consulter les reçus fiscaux (adhésions)"),
        ('add_receipt', "Ajouter un reçu fiscal (adhésions)"),
        ('change_receipt', "Modifier les reçus fiscaux (adhésions)"),
        ('delete_receipt', "Supprimer les reçus fiscaux (adhésions)"),
    ]

    def __str__(self):
        return self.number

    @property
    def filename(self):
        return join(settings.RECEIPT_ROOT, f'{self.date.year}', f'{self.number}.pdf')

    @property
    def amount_letters(self):
        return num2words(self.amount, lang=get_language(), to='currency')

    @classmethod
    def create(cls, payment):
        assert payment.amount > 0
        assert payment.method != 5
        assert payment.payer
        assert payment.online_status != PaymentStatus.IN_PROGRESS
        assert payment.online_status != PaymentStatus.FAILURE
        with transaction.atomic():
            number = get_next_value(f'receipt-{payment.date.year}')
            receipt = cls.objects.create(
                payment=payment,
                number=f'{payment.date.year}-{number}',
                name=f"{payment.payer.last_name} {payment.payer.first_name}",
                email=payment.payer.email,
                address=payment.payer.address,
                amount=payment.amount,
                date=payment.date,
                token=token_urlsafe(nbytes=32),
            )
        filename = receipt.filename
        makedirs(dirname(filename), exist_ok=True)
        template = get_template('members/receipt_pdf.html')
        markup = template.render({'receipt': receipt, 'today': today()})
        HTML(string=markup, base_url=settings.BASE_DIR).write_pdf(filename)
        return receipt

    def send(self, connection=None):
        assert self.email
        assert self.token
        url = reverse_lazy('receipt_download_by_token', args=[self.token])
        uri = f'https://{settings.HOST}{url}'
        structure = self.payment.allocation_set.first().adhesion.structure
        email = EmailMessage(
            subject=f"[EEDF] Votre Reçu fiscal n°J-{self.number}",
            body=f"""Bonjour,

Votre adhésion aux Eclaireuses Eclaireurs de France pour l'année passée vous a fait bénéficier d'avantages fiscaux.
Conformément à la législation en vigueur, nous avons le plaisir de vous transmettre le Reçu Fiscal correspondant.

Vous pouvez le télécharger en cliquant sur le lien ci-dessous et l'imprimer directement chez vous :
{uri}

Pour toute question concernant les reçus fiscaux, merci de contacter en priorité
votre structure locale :
{structure.type} {structure.name} - \
Email : {structure.public_email} - \
Tél. : {structure.public_phone_display}

A défaut, il est également possible de contacter le secrétariat national :
Email : {settings.SUPPORT_EMAIL} - Tél. : {phonenumbers.parse(settings.SUPPORT_PHONE)}

Amicalement,

Les Éclaireuses Éclaireurs de France""",
            reply_to=[structure.public_email or settings.SUPPORT_EMAIL],
            to=[self.email],
            connection=connection,
        )
        if email.send():
            self.sending = now()
            self.save()

    def cancel(self):
        structure = self.payment.allocation_set.first().adhesion.structure
        self.canceled = True
        self.payment = None
        self.save()
        if not self.sending:
            return
        email = EmailMessage(
            subject=f"[EEDF] Annulation du Reçu fiscal n°J-{self.number}",
            body=f"""Bonjour,

Le reçu fiscal n°J-{self.number} vous a été délivré par erreur et a été annulé.
Nous vous prions de ne pas l'utiliser pour votre déclaration.

S'il s'agissait d'une erreur de bénéficiaire, le nouveau bénéficaire recevra un nouveau reçu à son nom.
S'il s'agissait d'une erreur de montant, vous recevrez un nouveau reçu.

Pour toute question concernant les reçus fiscaux, merci de contacter en priorité
votre structure locale :
{structure.type} {structure.name} - \
Email : {structure.public_email} - \
Tél. : {structure.public_phone_display}

A défaut, il est également possible de contacter le secrétariat national :
Email : {settings.SUPPORT_EMAIL} - Tél. : {phonenumbers.parse(settings.SUPPORT_PHONE)}

Amicalement,

Les Éclaireuses Éclaireurs de France""",
            reply_to=[structure.public_email or settings.SUPPORT_EMAIL],
            to=[self.email],
        )
        email.send()

    def get_additional_data(self):
        """Keep link between object and person for log"""
        if not self.payment:
            return {'person_id': []}
        person_ids = [allocation.adhesion.adherent.person.id for allocation in self.payment.allocation_set.all()]
        if self.payment.payer:
            person_ids.append(self.payment.payer.id)
        return {'person_id': person_ids}


class TransferOrder(models.Model):
    date = models.DateField(verbose_name="Date", unique=True)

    class Meta:
        verbose_name = "Ordre de virement/prélèvement"
        verbose_name_plural = "Ordres de virement/prélèvement"
        ordering = ('-date', )
        permissions = [
            ('export_transferorder', "Exporter les ordres de virement/prélèvement"),
        ]

    renamed_permissions = [
        ('view_transferorder', "Consulter les ordres de virement/prélèvement"),
    ]

    def __str__(self):
        return self.date.strftime('%d/%m/%Y')

    @property
    def outstanding(self):
        return self.date > today()

    def sepa_debit(self):
        # Create the creditor account from an IBAN
        root_structure = Structure.objects.root_nodes().get()
        creditor = sepa.Account(root_structure.iban, root_structure.name)
        creditor.set_originator_id(settings.CREDITOR_ID)
        # Create a SEPADirectDebit instance
        sct = sepa.SEPADirectDebit(creditor)
        for transfer in self.transfer_set.filter(contribution__gt=0):
            # Create the creditor account from a tuple (IBAN, BIC)
            debitor = sepa.Account(transfer.iban, f"EEDF {transfer.structure.name}")
            debitor.set_mandate(f'I-{transfer.iban}', '2014-01-31', recurrent=True)
            # Add the transaction
            sct.add_transaction(
                debitor,
                sepa.Amount(transfer.contribution, 'EUR'),
                f"Cotisations {transfer.structure.name} au {self.date.strftime('%d/%m/%Y')}",
                due_date=self.date
            )
        for transfer in self.transfer_set.filter(donation__gt=0):
            # Create the creditor account from a tuple (IBAN, BIC)
            debitor = sepa.Account(transfer.iban, f"EEDF {transfer.structure.name}")
            debitor.set_mandate(f'I-{transfer.iban}', '2014-01-31', recurrent=True)
            # Add the transaction
            sct.add_transaction(
                debitor,
                sepa.Amount(transfer.donation, 'EUR'),
                f"Dons {transfer.structure.name} au {self.date.strftime('%d/%m/%Y')}",
                due_date=self.date
            )
        return sct.render().decode('ascii')

    def sepa_credit(self):
        # Create the debitor account from an IBAN
        root_structure = Structure.objects.root_nodes().get()
        debitor = sepa.Account(root_structure.iban, root_structure.name)
        # Create a SEPACreditTransfer instance
        sct = sepa.SEPACreditTransfer(debitor)
        for transfer in self.transfer_set.filter(contribution__lt=0):
            # Create the creditor account from a tuple (IBAN, BIC)
            creditor = sepa.Account(transfer.iban, f"EEDF {transfer.structure.name}")
            # Add the transaction
            sct.add_transaction(
                creditor,
                sepa.Amount(-transfer.contribution, 'EUR'),
                f"Cotisations {transfer.structure.name} au {self.date.strftime('%d/%m/%Y')}",
                due_date=self.date
            )
        for transfer in self.transfer_set.filter(donation__lt=0):
            # Create the creditor account from a tuple (IBAN, BIC)
            creditor = sepa.Account(transfer.iban, f"EEDF {transfer.structure.name}")
            # Add the transaction
            sct.add_transaction(
                creditor,
                sepa.Amount(transfer.donation_positive, 'EUR'),
                f"Dons {transfer.structure.name} au {self.date.strftime('%d/%m/%Y')}",
                due_date=self.date
            )
        return sct.render().decode('ascii')

    @property
    def contribution_debit(self):
        return sum([
            transfert.contribution_debit for transfert in self.transfer_set.all()
        ])

    @property
    def contribution_credit(self):
        return sum([
            transfert.contribution_credit for transfert in self.transfer_set.all()
        ])

    @property
    def donation_positive(self):
        return -self.transfer_set.aggregate(sum=Sum('donation'))['sum']

    @property
    def debit(self):
        balance = self.transfer_set.aggregate(sum=Sum('amount'))['sum']
        return max(balance, 0)

    @property
    def credit(self):
        balance = self.transfer_set.aggregate(sum=Sum('amount'))['sum']
        return -min(balance, 0)


class TransferQuerySet(PermissionQuerySetMixin, models.QuerySet):
    def _q_for_self(self, person):
        return Q(pk=None)

    def _q_for_team(self, team):
        return Q(pk=None)

    def _q_for_structure(self, structure):
        return Q(structure=structure)

    def _q_for_sub_structures(self, structure):
        return Q(structure__lft__gt=structure.lft, structure__lft__lt=structure.rght)

    def _q_only_electeds(self):
        return Q(pk=None)

    def _q_only_employees(self):
        return Q(pk=None)

    def _q_rgpd_delay(self):
        return Q()


class Transfer(models.Model):
    order = models.ForeignKey(TransferOrder, verbose_name="Ordre", on_delete=models.CASCADE)
    structure = models.ForeignKey(Structure, verbose_name="Structure", on_delete=models.PROTECT)
    iban = IBANField(verbose_name="IBAN", blank=True)
    carry = models.DecimalField(verbose_name="Report", max_digits=8, decimal_places=2, default=0)
    amount = models.DecimalField(verbose_name="Montant", max_digits=8, decimal_places=2, default=0)
    contribution = models.DecimalField(verbose_name="Cotisations", max_digits=8, decimal_places=2, default=0)
    donation = models.DecimalField(verbose_name="Dons", max_digits=8, decimal_places=2, default=0)

    objects = models.Manager.from_queryset(TransferQuerySet)()

    class Meta:
        verbose_name = "Virement/prélèvement"
        verbose_name_plural = "Virements/prélèvements"
        ordering = ('structure__name', )

    renamed_permissions = [
        ('view_transfer', "Consulter les virements/prélèvements"),
    ]

    def __str__(self):
        return f"{self.structure} {self.order}"

    @property
    def contribution_debit(self):
        return self.contribution if self.contribution > 0 else 0

    @property
    def contribution_credit(self):
        return -self.contribution if self.contribution < 0 else 0

    @property
    def donation_positive(self):
        return -self.donation

    @property
    def carry_positive(self):
        return -self.carry

    @property
    def debit(self):
        if self.amount > 0:
            return self.amount

    @property
    def credit(self):
        if self.amount < 0:
            return -self.amount


class Entry(models.Model):
    CATEGORY_CONTRIBUTION = 1
    CATEGORY_DONATION = 2
    CATEGORY_CHOICES = (
        (CATEGORY_CONTRIBUTION, "Cotisation"),
        (CATEGORY_DONATION, "Don"),
    )
    transfer = models.ForeignKey(Transfer, verbose_name="Virement/prélèvement", on_delete=models.CASCADE)
    adhesion = models.ForeignKey(Adhesion, verbose_name="Adhésion", on_delete=models.PROTECT)
    amount = models.DecimalField(verbose_name="Montant", max_digits=8, decimal_places=2)
    category = models.IntegerField(verbose_name="Catégorie", choices=CATEGORY_CHOICES)

    class Meta:
        verbose_name = "Écriture"

    def __str__(self):
        return f"{self.adhesion} {self.transfer}"

    @property
    def debit(self):
        if self.amount > 0:
            return self.amount

    @property
    def credit(self):
        if self.amount < 0:
            return -self.amount

    @property
    def contribution(self):
        if self.category == self.CATEGORY_CONTRIBUTION:
            return self.amount
        else:
            return 0

    @property
    def contribution_debit(self):
        return self.contribution if self.contribution > 0 else 0

    @property
    def contribution_credit(self):
        return -self.contribution if self.contribution < 0 else 0

    @property
    def donation_positive(self):
        if self.category == self.CATEGORY_DONATION:
            return -self.amount

    @classmethod
    def create(cls, adhesion, amount, category):
        assert amount != 0
        order, _ = TransferOrder.objects.get_or_create(date=next_transfer_date())
        for structure in adhesion.structure.get_ancestors(ascending=True, include_self=True):
            if structure.iban and not structure.is_root_node():
                transfer, _ = Transfer.objects.get_or_create(
                    order=order,
                    structure=structure,
                    defaults={'iban': structure.iban}
                )
                entry = cls.objects.create(
                    transfer=transfer,
                    adhesion=adhesion,
                    amount=amount,
                    category=category,
                )
                transfer.amount += amount
                if category == cls.CATEGORY_CONTRIBUTION:
                    transfer.contribution += amount
                if category == cls.CATEGORY_DONATION:
                    transfer.donation += amount
                transfer.save()
                return entry
        return None

    def get_additional_data(self):
        """Keep link between object and person for log"""
        return {'person_id': [self.adhesion.adherent.person.id]}


class InvitationQuerySet(PermissionQuerySetMixin, models.QuerySet):
    def _q_for_self(self, person):
        return Q(person=person) | Q(adhesion__adherent__person=person)

    def _q_for_team(self, team):
        return Q(
            person__functions__team=team,
            person__functions__dates__contains=today(),
            person__functions__status=FunctionStatus.OK,
        )

    def _q_for_structure(self, structure):
        return Q(structure=structure)

    def _q_for_sub_structures(self, structure):
        return Q(structure__lft__gt=structure.lft, structure__lft__lt=structure.rght)

    def _q_only_electeds(self):
        return Q()

    def _q_only_employees(self):
        return Q()

    def _q_rgpd_delay(self):
        min_season = current_season() - settings.RGPD_DELAY
        return Q(season__gte=min_season)


class Invitation(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid4)
    token = models.CharField(max_length=50)
    season = models.IntegerField(verbose_name="Saison")
    person = models.ForeignKey(Person, verbose_name="Personne", blank=True, null=True, on_delete=models.PROTECT)
    email = models.EmailField("Email", db_collation="case_insensitive")
    structure = models.ForeignKey(Structure, verbose_name="Structure",
                                  related_name='invitations', on_delete=models.PROTECT)
    team = models.ForeignKey(Team, verbose_name="Équipe", null=True, blank=True, on_delete=models.PROTECT)
    function_config = models.ForeignKey(FunctionConfig, verbose_name="Fonction", null=True, blank=True,
                                        on_delete=models.PROTECT)
    adhesion = models.OneToOneField(Adhesion, verbose_name="Adhésion", on_delete=models.PROTECT,
                                    related_name='invitation', blank=True, null=True)
    host = models.ForeignKey(Person, verbose_name="Hôte", on_delete=models.PROTECT, related_name='+',
                             null=True, blank=True)
    rate = models.ForeignKey(Rate, verbose_name="Tarif", null=True, blank=True, on_delete=models.PROTECT)
    date = models.DateField("Date de début", null=True, blank=True)
    entry_date = models.DateField("Date de saisie", null=True, blank=True)
    modification_date = models.DateField("Date de modification", null=True, blank=True)
    declined = models.BooleanField("Refusée", default=False)

    objects = models.Manager.from_queryset(InvitationQuerySet)()

    class Meta:
        verbose_name = "Invitation"
        permissions = [
            ('apiv5_read_invitation', "Can read Invitation with API v5"),
            ('apiv5_write_invitation', "Can write Invitation with API v5"),
        ]
        constraints = [
            models.UniqueConstraint(
                fields=('season', 'person', 'structure'),
                name='invitation_unique_season_person_structure',
                condition=~Q(person=None) & Q(declined=False),
            ),
        ]

    renamed_permissions = [
        ('view_invitation', "Consulter les invitations"),
        ('add_invitation', "Ajouter une invitation"),
        ('change_invitation', "Modifier les invitations"),
        ('delete_invitation', "Supprimer les invitations"),
    ]

    def __str__(self):
        return f"{self.email} {self.season} {self.structure}"

    @property
    def status_label(self):
        if self.adhesion and self.adhesion.adherent:
            return self.adhesion.adherent.status_label
        if self.declined:
            return mark_safe('<span class="label label-danger">Refus</span>')
        if self.person:
            return self.person.status_label
        return mark_safe('<span class="label label-warning">Invité·e</span>')

    @property
    def functions_display(self):
        functions = set()
        if self.person:
            functions |= set(self.person.functions.current().values_list('name', flat=True))
        if self.function_config:
            functions.add(str(self.function_config))
        if not functions:
            return "Ami·e ou responsable"
        return ", ".join(functions)

    @property
    def begin(self):
        return self.date or today()

    @property
    def end(self):
        if self.date:
            return self.date + TimeDelta(days=self.rate.duration - 1)
        return Date(self.season, 8, 31)

    @property
    def indirect_adhesion(self):
        if self.adhesion:
            return self.adhesion
        if not self.person:
            return None
        adhesion = self.person.last_adhesion
        if not adhesion:
            return None
        if adhesion.season != self.season:
            return None
        return adhesion

    @property
    def is_adherent(self):
        adhesion = self.indirect_adhesion
        if not adhesion:
            return False
        return adhesion.rate.category != RateCategory.DISCOVERY or adhesion.rate == self.rate

    def save(self, *args, **kwargs):
        if not self.token:
            self.token = hexlify(urandom(20)).decode()
        super().save(*args, **kwargs)

    def url(self, request=None):
        return build_url(reverse_lazy('adhesion_create', args=[self.token]), request)

    def decline_url(self, request=None):
        return build_url(reverse_lazy('invitation_decline', args=[self.token]), request)

    def send_email(self, request, connection=None):
        subject = "[EEDF] Adhésion en ligne"
        if self.person:
            subject += f" de {self.person}"
        template = get_template('members/invitation_email.html')
        context = {
            'invitation': self,
            'person': self.person,
            'host': self.host,
            'structure': self.structure,
            'team': self.team,
            'functions_display': self.functions_display,
            'url': self.url(request),
            'decline_url': self.decline_url(request),
            'SUPPORT_EMAIL': settings.SUPPORT_EMAIL,
            'SUPPORT_PHONE': phonenumbers.parse(settings.SUPPORT_PHONE),
        }
        html = template.render(context)
        text = text_maker.handle(html)
        email = EmailMultiAlternatives(
            subject=subject,
            body=text,
            reply_to=[self.structure.public_email or settings.SUPPORT_EMAIL],
            to=[self.email],
            connection=connection
        )
        email.attach_alternative(html, "text/html")
        email.send()

    def get_reminder_email_context(self, request=None):
        template = get_template('members/reminder_email.txt')
        adhesion = self.indirect_adhesion
        assert adhesion
        assert not adhesion.canceled
        to = adhesion.adherent.person.email or self.email
        if adhesion.donation:
            subject = "[EEDF] Paiement de ta cotisation et de ton don"
        else:
            subject = "[EEDF] Paiement de ta cotisation"
        context = {
            'adhesion': adhesion,
            'person': adhesion.adherent.person,
            'to': to,
            'subject': subject,
            'url': self.url(request),
            'SUPPORT_EMAIL': settings.SUPPORT_EMAIL,
            'SUPPORT_PHONE': phonenumbers.parse(settings.SUPPORT_PHONE),
        }
        context['message'] = template.render(context)
        return context

    def send_reminder_email(self, request=None, connection=None):
        context = self.get_reminder_email_context(request)
        email = EmailMessage(
            subject=context['subject'],
            body=context['message'],
            reply_to=[settings.SUPPORT_EMAIL],
            to=[context['to']],
            bcc=[self.host.email] if self.host else [],
            connection=connection
        )
        email.send()
        Notification.objects.create(
            adhesion=context['adhesion'],
            date=now(),
        )

    def get_additional_data(self):
        """Keep link between object and person for log"""
        if not self.person:
            return None
        return {'person_id': [self.person_id]}


class Notification(models.Model):
    date = models.DateTimeField("Date")
    adhesion = models.ForeignKey(Adhesion, verbose_name="Adhésion", on_delete=models.PROTECT)

    def get_additional_data(self):
        """Keep link between object and person for log"""
        return {'person_id': [self.adhesion.adherent.person.id]}


class EmploymentType(models.IntegerChoices):
    CTR = 1, "Autre"
    SAL = 2, "Salarié·e"
    CEE = 4, "CEE"
    STA = 5, "Stagiaire"
    SCV = 6, "Volontaire"


class EmploymentQuerySet(PermissionQuerySetMixin, models.QuerySet):
    def _q_for_self(self, person):
        return Q(employee__person=person)

    def _q_for_team(self, team):
        return Q(
            employee__person__functions__team=team,
            employee__person__functions__dates__contains=today(),
            employee__person__functions__status=FunctionStatus.OK,
        )

    def _q_for_structure(self, structure):
        return Q(
            employee__person__functions__team__structure=structure,
            employee__person__functions__dates__contains=today(),
            employee__person__functions__status=FunctionStatus.OK,
        )

    def _q_for_sub_structures(self, structure):
        return Q(
            employee__person__functions__team__structure__lft__gt=structure.lft,
            employee__person__functions__team__structure__lft__lt=structure.rght,
            employee__person__functions__dates__contains=today(),
            employee__person__functions__status=FunctionStatus.OK,
        )

    def _q_only_electeds(self):
        return Q(pk=None)

    def _q_only_employees(self):
        return Q()

    def _q_rgpd_delay(self):
        return Q(employee__person__expired=False)

    def overlap(self, begin, end):
        return self.filter(
            dates__overlap=DateRange(begin, end, '[]')
        )

    def for_season(self, season):
        return self.overlap(
            Date(season - 1, 9, 1),
            Date(season, 8, 31),
        )

    def for_year(self, year):
        return self.overlap(
            Date(year, 1, 1),
            Date(year, 12, 31),
        )

    def current(self):
        return self.filter(dates__contains=today())


class Employment(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid4)
    employee = models.ForeignKey(Employee, verbose_name="Personne", related_name='employments',
                                 on_delete=models.PROTECT)
    type = models.IntegerField("Type", choices=EmploymentType.choices)
    name = models.CharField("Intitulé de poste", max_length=100, blank=True)
    dates = DateRangeField(verbose_name="Dates")

    objects = models.Manager.from_queryset(EmploymentQuerySet)()

    class Meta:
        verbose_name = "Contrat de travail"
        verbose_name_plural = "Contrats de travail"
        permissions = [
            ('apiv1_employment', "Can view Employment with API v1"),
            ('apiv3_employment', "Can read Employment with API v3"),
            ('apiv4_read_employment', "Can read Employment with API v4"),
            ('apiv5_read_employment', "Can read Employment with API v5"),
            ('import_employment', "Importer des contrats de travail"),
        ]

    renamed_permissions = [
        ('view_employment', "Consulter les contrats de travail"),
        ('add_employment', "Ajouter un contrat de travail"),
        ('change_employment', "Modifier les contrats de travail"),
        ('delete_employment', "Supprimer les contrats de travail"),
    ]

    def __str__(self):
        return self.name

    @property
    def type_display(self):
        return self.get_type_display()

    @property
    def begin(self):
        lower = self.dates.lower
        if not self.dates.lower_inc:
            lower += TimeDelta(days=1)
        return lower

    @property
    def end(self):
        if not self.dates.upper:
            return None
        upper = self.dates.upper
        if not self.dates.upper_inc:
            upper -= TimeDelta(days=1)
        return upper

    def dates_display(self):
        if not self.end:
            return f"à partir du {self.begin.strftime('%d/%m/%Y')}"
        return f"entre le {self.begin.strftime('%d/%m/%Y')} et le {self.end.strftime('%d/%m/%Y')}"

    def send_email(self, request=None, connection=None):
        if not self.employee.email:
            return
        if self.end:
            dates = f"pour la période du {self.begin.strftime('%d/%m/%Y')} au {self.end.strftime('%d/%m/%Y')}"
        else:
            dates = f"à partir du {self.begin.strftime('%d/%m/%Y')}"
        email = EmailMessage(
            subject="[EEDF] Ton contrat de travail",
            body=f"""Bonjour {self.employee.person.first_name},

Ton contrat de travail {dates} vient d'être enregistré sur Jéito.

Amicalement,

Les Éclaireuses Éclaireurs de France""",
            reply_to=[settings.SUPPORT_EMAIL],
            to=[self.employee.email],
            connection=connection
        )
        email.send()

    def get_additional_data(self):
        """Keep link between object and person for log"""
        return {'person_id': [self.employee.person.pk]}


class RoleConfigQuerySet(GlobalPermissionQuerySetMixin, models.QuerySet):
    def for_function(self, function):
        return self.filter(
            Q(attributions__structure_type=None) |
            Q(attributions__structure_type=function.team.structure.type),
            Q(attributions__team_type=None) |
            Q(attributions__team_type=function.team.type),
            Q(attributions__function_type=None) |
            Q(attributions__function_type=function.function_config.function_type),
            Q(attributions__function_category=None) |
            Q(attributions__function_category=function.function_config.function_type.category),
            attributions__isnull=False,
        )

    def for_no_function(self):
        return self.filter(
            attributions__structure_type=None,
            attributions__team_type=None,
            attributions__function_type=None,
            attributions__function_category=None,
            attributions__isnull=False,
        )


class Permission(BasePermission):
    uuid_namespace = uuid5(NAMESPACE_DNS, 'jeito.eedf.fr')

    class Meta:
        proxy = True
        permissions = [
            ('apiv3_permission', "Can view/create/update Permission with API v3"),
            ('apiv4_read_permission', "Can read Permission with API v4"),
            ('apiv4_write_permission', "Can write Permission with API v4"),
            ('apiv5_read_permission', "Can read Permission with API v5"),
            ('apiv5_write_permission', "Can write Permission with API v5"),
        ]

    @property
    def uuid(self):
        uuid5(self.uuid_namespace, f'permission/{self.id}')


class RoleConfigPermission(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid4)
    roleconfig = models.ForeignKey("RoleConfig", verbose_name="Configuration du rôle", on_delete=models.PROTECT)
    permission = models.ForeignKey(Permission, verbose_name="Permission", on_delete=models.PROTECT)

    class Meta:
        verbose_name = "Permission pour la configuration du rôle"
        verbose_name_plural = "Permissions pour la configuration des rôles"
        permissions = [
            ('apiv3_roleconfigpermission', "Can read RoleConfigPermission with API v3"),
            ('apiv4_read_roleconfigpermission', "Can read RoleConfigPermission with API v4"),
            ('apiv5_read_roleconfigpermission', "Can read RoleConfigPermission with API v5"),
        ]


class RoleConfig(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid4)
    name = models.CharField("Nom", max_length=100)
    description = models.TextField("Description")
    delegation = models.BooleanField("Délégable", default=False)
    for_self = models.BooleanField("Pour soi", default=False)
    for_team = models.BooleanField("Pour son équipe", default=False)
    for_structure = models.BooleanField("Pour sa structure", default=False)
    for_sub_structures = models.BooleanField("Pour ses sous-structures", default=False)
    for_all = models.BooleanField("Pour l'association", default=False)
    only_electeds = models.BooleanField("Seulement les élus", default=False)
    only_employees = models.BooleanField("Seulement les salariés", default=False)
    with_guardians = models.BooleanField("Avec les responsables légaux", default=False)
    with_payers = models.BooleanField("Avec les payeurs", default=False)
    permissions = models.ManyToManyField(Permission, verbose_name="Permissions", through=RoleConfigPermission)
    rgpd = models.BooleanField("Délai RGPD", default=True, help_text="Ne peut pas consulter les anciennes données.")

    objects = models.Manager.from_queryset(RoleConfigQuerySet)()

    class Meta:
        verbose_name = "Configuration du rôle"
        verbose_name_plural = "Configuration des rôles"
        permissions = [
            ('apiv3_roleconfig', "Can read RoleConfig with API v3"),
            ('apiv4_read_roleconfig', "Can read RoleConfig with API v4"),
            ('apiv5_read_roleconfig', "Can read RoleConfig with API v5"),
        ]
        constraints = [
            models.CheckConstraint(
                check=(
                    Q(for_self=True) |
                    Q(for_team=True) |
                    Q(for_structure=True) |
                    Q(for_sub_structures=True) |
                    Q(for_all=True)
                ),
                name='roleconfig_perimeter',
            ),
        ]
        ordering = ('name', )

    renamed_permissions = [
        ('view_roleconfig', "Consulter la configuration des rôles"),
        ('add_roleconfig', "Ajouter la configuration d'un rôle"),
        ('change_roleconfig', "Modifier la configuration des rôles"),
        ('delete_roleconfig', "Supprimer la configuration des rôles"),
    ]

    def __str__(self):
        return self.name

    def xxx_display(self, start, cut, prefix, separator):
        for_items = []
        for field in self._meta.get_fields():
            if field.name.startswith(start) and getattr(self, field.name):
                for_items.append(prefix + escape(field.verbose_name[len(cut):].capitalize()))
        return mark_safe(separator.join(for_items))

    def for_display(self):
        return self.xxx_display('for_', "Pour ", "", ", ")

    def only_display(self):
        return self.xxx_display('only_', "Seulement ", "", ", ")

    def with_display(self):
        return self.xxx_display('with_', "Avec ", "", ", ")

    def for_display_br(self):
        return self.xxx_display('for_', "Pour ", "- ", "<br>")

    def only_display_br(self):
        return self.xxx_display('only_', "Seulement ", "- ", "<br>")

    def with_display_br(self):
        return self.xxx_display('with_', "Avec ", "- ", "<br>")

    def attributions_display(self):
        return mark_safe("<br>".join(("- " + escape(attribution.display()) for attribution in self.attributions.all())))


class Attribution(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid4)
    role_config = models.ForeignKey(RoleConfig, verbose_name="Configuration du rôle", on_delete=models.PROTECT,
                                    related_name='attributions')
    structure_type = models.ForeignKey(StructureType, verbose_name="Type de structure", null=True,
                                       blank=True, on_delete=models.PROTECT)
    team_type = models.ForeignKey(TeamType, verbose_name="Type d'équipe", null=True, blank=True,
                                  on_delete=models.PROTECT)
    function_type = models.ForeignKey(FunctionType, verbose_name="Type de fonction", null=True, blank=True,
                                      on_delete=models.PROTECT)
    function_category = models.IntegerField(verbose_name="Catégorie de fonction", null=True, blank=True,
                                            choices=FunctionCategory.choices)

    class Meta:
        verbose_name = "Attribution"
        permissions = [
            ('apiv3_attribution', "Can read Attribution with API v3"),
            ('apiv4_read_attribution', "Can read Attribution with API v4"),
            ('apiv5_read_attribution', "Can read Attribution with API v5"),
        ]

    def __str__(self):
        return f"{self.role_config}:" \
            f"structure_type={self.structure_type or '*'}," \
            f"team_type={self.team_type or '*'}," \
            f"function_type={self.function_type or '*'}," \
            f"function_category={self.get_function_category_display() or '*'}"

    def display(self):
        if not self.structure_type and not self.team_type and not self.function_type and not self.function_category:
            return "Tous"
        if self.function_type:
            attribution = self.function_type.name
        elif self.function_category:
            attribution = self.function_category.name
        else:
            attribution = ""
        if self.team_type:
            attribution += f" d'{self.team_type.name}"
        if self.structure_type:
            attribution += f" de {self.structure_type.name}"
        return attribution


class RoleQuerySet(PermissionQuerySetMixin, models.QuerySet):
    def _q_for_self(self, person):
        return Q(proxy=person)

    def _q_for_team(self, team):
        return Q(team=team)

    def _q_for_structure(self, structure):
        return Q(team__structure=structure)

    def _q_for_sub_structures(self, structure):
        return Q(
            team__structure__lft__gt=structure.lft,
            team__structure__lft__lt=structure.rght,
        )

    def _q_only_electeds(self):
        return Q(pk=0)

    def _q_only_employees(self):
        return Q(pk=0)

    def _q_rgpd_delay(self):
        return Q()

    def for_holder_perm(self, perm, user, only_active=True):
        if user.is_superuser:
            return self
        if not user.person:
            return self.none()
        q = Q()
        for config, team in user.person.perms(only_active).get(perm, ()):
            if config.for_self:
                q |= Q(holder=user.person)
            if config.for_team:
                q |= Q(
                    holder__functions__team=team,
                    holder__functions__dates__contains=today(),
                    holder__functions__status=FunctionStatus.OK,
                )
            if config.for_structure:
                q |= Q(
                    holder__functions__team__structure=team.structure,
                    holder__functions__dates__contains=today(),
                    holder__functions__status=FunctionStatus.OK,
                )
            if config.for_sub_structures:
                q |= Q(
                    holder__functions__team__structure__lft__gt=team.structure.lft,
                    holder__functions__team__structure__lft__lt=team.structure.rght,
                    holder__functions__dates__contains=today(),
                    holder__functions__status=FunctionStatus.OK,
                )
            if config.for_all:
                return self
        if not q:
            return self.none()
        return self.filter(q)

    def for_proxy_perm(self, perm, user, only_active=True):
        if user.is_superuser:
            return self
        if not user.person:
            return self.none()
        q = Q()
        for config, team in user.person.perms(only_active).get(perm, ()):
            if config.for_self:
                q |= Q(proxy=user.person)
            if config.for_team:
                q |= Q(
                    proxy__functions__team=team,
                    proxy__functions__dates__contains=today(),
                    proxy__functions__status=FunctionStatus.OK,
                )
            if config.for_structure:
                q |= Q(
                    proxy__functions__team__structure=team.structure,
                    proxy__functions__dates__contains=today(),
                    proxy__functions__status=FunctionStatus.OK,
                )
            if config.for_sub_structures:
                q |= Q(
                    proxy__functions__team__structure__lft__gt=team.structure.lft,
                    proxy__functions__team__structure__lft__lt=team.structure.rght,
                    proxy__functions__dates__contains=today(),
                    proxy__functions__status=FunctionStatus.OK,
                )
            if config.for_all:
                return self
        if not q:
            return self.none()
        return self.filter(q)


class Role(models.Model):
    """
    Delegation: holder is not None
    Manual role: holder is null
    """
    uuid = models.UUIDField(unique=True, default=uuid4)
    config = models.ForeignKey(RoleConfig, verbose_name="Configuration du rôle", related_name='roles',
                               on_delete=models.PROTECT)
    team = models.ForeignKey(Team, verbose_name="Équipe", related_name='roles', on_delete=models.PROTECT)
    holder = models.ForeignKey(Person, verbose_name="Mandant", help_text="Personne qui donne le rôle",
                               null=True, blank=True, related_name='hold', on_delete=models.PROTECT)
    proxy = models.ForeignKey(Person, verbose_name="Madataire", help_text="Personne qui reçoit le rôle",
                              related_name='proxied', on_delete=models.PROTECT)

    objects = models.Manager.from_queryset(RoleQuerySet)()

    class Meta:
        verbose_name = "Rôle"
        unique_together = ('config', 'team', 'holder', 'proxy')
        permissions = [
            ('add_delegation', "Déléguer ses rôles"),
            ('delete_delegation', "Supprimer ses délégations de rôles"),
            ('apiv3_role', "Can read Role with API v3"),
            ('apiv4_read_role', "Can read Role with API v4"),
            ('apiv5_read_role', "Can read Role with API v5"),
        ]

    renamed_permissions = [
        ('view_role', "Consulter les rôles"),
        ('add_role', "Attribuer un rôle manuellement"),
        ('change_role', "Modifier les rôles attribués manuellement"),
        ('delete_role', "Supprimer les rôles attribués manuellement"),
    ]

    def __str__(self):
        return f"{self.config}:{self.team}:{self.holder}:{self.proxy}"

    def get_additional_data(self):
        """Keep link between object and person for log"""
        person_ids = [self.proxy.id]
        if self.holder:
            person_ids.append(self.holder.id)
        return {'person_id': person_ids}


class LogEntry(BaseLogEntry):
    renamed_permissions = [
        ('view_logentry', "Consulter le journal"),
    ]

    class Meta:
        verbose_name = "Journal"


class ExternalContentType(models.Model):
    content_type = models.OneToOneField(ContentType, verbose_name="Type de contenu", on_delete=models.PROTECT,
                                        related_name='external')
    verbose_name = models.CharField(verbose_name="Nom", max_length=100)

    class Meta:
        verbose_name = "Type de contenu externe"
        verbose_name_plural = "Types de contenu externes"


auditlog.register(Person, exclude_fields=['id', 'uuid', 'hold', 'proxied', 'reports', 'adherent', 'functions',
                                          'protected1', 'protected2', 'countersignatures',
                                          'last_invitation', 'has_protected', 'has_payment', 'expired'])
auditlog.register(Adherent, exclude_fields=['id', 'uuid', 'last_adhesion', 'adhesions'])
auditlog.register(Adhesion, exclude_fields=['id', 'uuid', 'token', 'is_paid', 'is_discovery'])
auditlog.register(Payment, exclude_fields=['id'])
auditlog.register(Function, exclude_fields=['id', 'uuid', 'token'])
auditlog.register(Employee, exclude_fields=['id', 'uuid', 'last_employment'])
auditlog.register(Employment, exclude_fields=['id', 'uuid'])
auditlog.register(Role, exclude_fields=['id', 'uuid'])
auditlog.register(Structure, exclude_fields=['id', 'uuid', 'lft', 'rght', 'level', 'tree_id', 'children', 'teams',
                                             'adherents', 'invitations'])
auditlog.register(Team, exclude_fields=['id', 'uuid', 'roles'])
auditlog.register(Invitation, exclude_fields=['id', 'token'])
auditlog.register(Notification, exclude_fields=['id'])
auditlog.register(Entry, exclude_fields=['id'])
