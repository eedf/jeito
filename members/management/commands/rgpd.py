from django.conf import settings
from django.core.management.base import BaseCommand
from django.db.models import Q
from members.models import Person
from members.utils import current_season, today, replace_year


class Command(BaseCommand):
    def handle(self, *args, **options):
        min_season = current_season() - settings.RGPD_DELAY
        min_date = replace_year(today(), today().year - settings.RGPD_DELAY)
        persons = Person.objects.filter(
            Q(adherent__last_adhesion=None) | Q(adherent__last_adhesion__season__lt=min_season),
            Q(protected1__adherent__last_adhesion=None) | Q(protected1__adherent__last_adhesion__season__lt=min_season),
            Q(protected2__adherent__last_adhesion=None) | Q(protected2__adherent__last_adhesion__season__lt=min_season),
            Q(employee__last_employment=None) | Q(employee__last_employment__dates__endswith__lt=min_date),
            Q(invitation=None) | Q(invitation__season__lt=min_season),
            Q(payment=None) | Q(payment__date__lt=min_date),
            expired=False,
        )
        persons.update(expired=True)
        self.stdout.write("Newly expired after RGPD delay:")
        for person in persons:
            self.stdout.write(f"- {person}")
        self.stdout.write(self.style.SUCCESS("Done"))
