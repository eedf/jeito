# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from datetime import date
from django.core.management.base import BaseCommand
from django.db.models import Q
from ...models import Person
from ...utils import current_season


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-n', '--no-save', action='store_true')

    def handle(self, *args, **options):
        birthdate = date(current_season() - 19, 9, 1)
        persons = Person.objects.filter(
            ~Q(legal_guardian1=None) | ~Q(legal_guardian2=None),
            birthdate__lt=birthdate,
        )
        self.stdout.write(
            f"Suppression des responsable légaux·ales des {persons.count()} personnes "
            f"nées avant le {birthdate.strftime('%d/%m/%Y')}"
        )
        if not options['no_save']:
            persons.update(legal_guardian1=None, legal_guardian2=None)
            self.stdout.write(self.style.SUCCESS("Done"))
