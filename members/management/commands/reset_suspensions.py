from django.core.management.base import BaseCommand
from members.models import Adherent, AdherentStatus
from members.utils import today


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write("Reset suspensions for:")
        for adherent in Adherent.objects.filter(status=AdherentStatus.SUSPENDED, suspension_deadline__lt=today()):
            adherent.status = AdherentStatus.OK
            adherent.suspension_deadline = None
            adherent.save()
            self.stdout.write(str(adherent))
        self.stdout.write(self.style.SUCCESS("Done"))
