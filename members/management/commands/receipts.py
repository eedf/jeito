# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from time import sleep
from django.conf import settings
from django.core import mail
from django.core.management.base import BaseCommand
from members.utils import current_year
from members.models import Payment, Receipt, PaymentStatus


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--sleep', type=int, default=settings.DEFAULT_RECEIPTS_SLEEP)
        parser.add_argument('--max', type=int)
        parser.add_argument('--year', type=int, default=settings.DEFAULT_RECEIPTS_YEAR or current_year() - 1)

    def handle(self, *args, **options):
        self.stdout.write(f"Send receipts for year {options['year']}…")

        # Generate
        payments = Payment.objects.filter(
            date__year=options['year'],
            amount__gt=0,
            method__in=(1, 2, 3, 4, 6),
            online_status__in=(PaymentStatus.MANUAL, PaymentStatus.SUCCESS),
            payer__isnull=False,
            receipt=None,
        )
        payments = payments.order_by('date')
        nb = payments.count()
        for i, payment in enumerate(payments):
            self.stdout.write(
                f"Generate {i + 1}/{nb} - {payment.payer} - {payment.amount:.2f} € - "
                f"{payment.date.strftime('%d/%m/%Y')} - {payment.payer.email}"
            )
            Receipt.create(payment)

        # Send
        receipts = Receipt.objects.filter(
            date__year=options['year'],
            canceled=False,
            sending=None,
        ).exclude(
            email="",
        )
        if options['max']:
            receipts = receipts[:options['max']]
        nb = receipts.count()
        with mail.get_connection() as connection:
            for i, receipt in enumerate(receipts):
                self.stdout.write(
                    f"Send {i + 1}/{nb} - {receipt.name} - {receipt.amount:.2f} € - "
                    f"{receipt.date.strftime('%d/%m/%Y')} - {receipt.email}"
                )
                receipt.send(connection)
                if options['sleep']:
                    sleep(options['sleep'])

        # End
        self.stdout.write(self.style.SUCCESS("Done"))
