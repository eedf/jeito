# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from django.core.management.base import BaseCommand
from ...models import Rate


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('season', type=int)

    def handle(self, *args, **options):
        new_season = options['season']
        old_season = new_season - 1
        for rate in Rate.objects.filter(season=old_season):
            structure_types = rate.structure_types.all()
            rate.pk = None
            rate._state.adding = True
            rate.season = new_season
            rate.save()
            rate.structure_types.set(structure_types)
            self.stdout.write(str(rate))
