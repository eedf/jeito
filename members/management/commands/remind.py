from datetime import timedelta
from django.conf import settings
from django.core import mail
from django.core.management.base import BaseCommand
from django.utils.timezone import now
from members.models import Adhesion
from members.utils import current_season


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write("Send reminders to:")
        with mail.get_connection() as connection:
            threshold = now() - timedelta(days=7)
            adhesions = Adhesion.objects.exclude(
                invitation=None,
            ).filter(
                entry_date__lt=threshold,
                notification=None,
                season=current_season(),
                canceled=False,
                is_paid=False,
            )
            for adhesion in adhesions:
                invitation = adhesion.invitation
                if not settings.TEST_SITE:
                    invitation.send_reminder_email(connection=connection)
                self.stdout.write(str(adhesion))
        self.stdout.write(self.style.SUCCESS("Done"))
