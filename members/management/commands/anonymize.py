# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from datetime import date
from itertools import islice
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from random import randrange
from ...models import Person, Employee, Receipt, Invitation

BATCH_SIZE = 100


# FIXME: when upgrading to python 3.12, replace by stdlib version
def batched(iterable, n):
    "Batch data into tuples of length n. The last batch may be shorter."
    # batched('ABCDEFG', 3) --> ABC DEF G
    if n < 1:
        raise ValueError('n must be at least one')
    it = iter(iterable)
    while batch := tuple(islice(it, n)):
        yield batch


def generate_email():
    return ''.join(['abcedfghijklmnopqrstuvwxyz'[randrange(0, 26)] for i in range(16)]) + '@eedf.test'


def generate_home_phone():
    return '+3380' + str(randrange(1000000, 9999999))


def generate_mobile_phone():
    return '+337' + str(randrange(10000000, 39999999))


def generate_postal_code():
    return ''.join(['0123456789'[randrange(0, 10)] for i in range(5)])


class Command(BaseCommand):
    def handle(self, *args, **options):
        if not settings.TEST_SITE:
            raise CommandError("Ce n'est pas un site de test. Refus de modifier les données.")

        verbose = options['verbosity'] >= 1

        nb = Person.objects.count()
        for i, persons in enumerate(batched(Person.objects.only(
            'id',
            'birthdate',
            'birth_country_id',
            'birth_commune_id',
            'birth_place',
            'profession',
        ), BATCH_SIZE)):
            if verbose:
                print(f"{i * BATCH_SIZE}/{nb} persons")
            for person in persons:
                if person.birthdate:
                    person.birthdate = date(person.birthdate.year, 1, 1)
                person.email = generate_email()
                person.email_as_login = False
                person.address1 = "Anonymisé"
                person.address2 = ""
                person.address3 = ""
                person.postal_code = "99999"
                person.city = "Anonymisé"
                person.country = "France"
                person.home_phone = generate_home_phone()
                person.mobile_phone = generate_mobile_phone()
                if person.birth_country_id:
                    person.birth_country_id = 99100
                if person.birth_commune_id:
                    person.birth_commune_id = 2403
                if person.birth_place:
                    person.birth_place = "Anonymisé"
                if person.profession:
                    person.profession = "Anonymisé"
            Person.objects.bulk_update(persons, (
                'email',
                'email_as_login',
                'address1',
                'address2',
                'address3',
                'postal_code',
                'city',
                'country',
                'home_phone',
                'mobile_phone',
                'birth_place',
                'profession',
            ))

        nb = Employee.objects.count()
        for i, employees in enumerate(batched(Employee.objects.only('id'), BATCH_SIZE)):
            if verbose:
                print(f"{i * BATCH_SIZE}/{nb} employees")
            for employee in employees:
                employee.email = generate_email()
                employee.phone = generate_mobile_phone()
            Employee.objects.bulk_update(employees, ('email', 'phone'))

        nb = Receipt.objects.count()
        for i, receipts in enumerate(batched(Receipt.objects.only('id'), BATCH_SIZE)):
            if verbose:
                print(f"{i * BATCH_SIZE}/{nb} receipts")
            for receipt in receipts:
                receipt.email = generate_email()
                receipt.address = "Anonymisé"
            Receipt.objects.bulk_update(receipts, ('email', 'address'))

        nb = Invitation.objects.count()
        for i, invitations in enumerate(batched(Invitation.objects.only('id'), BATCH_SIZE)):
            if verbose:
                print(f"{i * BATCH_SIZE}/{nb} invitations")
            for invitation in invitations:
                invitation.email = generate_email()
            Invitation.objects.bulk_update(invitations, ('email', ))
