from datetime import timedelta as TimeDelta
from decimal import Decimal
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q, Case, When, F, Sum, Value, Subquery, OuterRef
from django.db.models.functions import Coalesce
from django.urls import reverse
from members.models import Adhesion, Person, RateCategory, Allocation, Entry, Payment, Function, PaymentStatus
from members.utils import current_season, today, build_url


class Command(BaseCommand):
    def handle(self, *args, **options):
        nb_errors = 0

        # Check function configs
        functions = Function.objects.exclude(
            function_config__team_type=F('team__type'),
        ).exclude(
            id__in=settings.EXCLUDE_CHECK.get('function_team', []),
        ).order_by('id')
        for function in functions:
            nb_errors += 1
            self.stdout.write(
                f"Function {build_url(reverse('function_detail', args=[function.id]))} : "
                "type is not consistent with team type"
            )
        functions = Function.objects.exclude(
            Q(function_config__structure_type=None) | Q(function_config__structure_type=F('team__structure__type')),
        ).exclude(
            id__in=settings.EXCLUDE_CHECK.get('function_structure', []),
        ).order_by('id')
        for function in functions:
            nb_errors += 1
            self.stdout.write(
                f"Function {build_url(reverse('function_detail', args=[function.id]))} : "
                "type is not consistent with structure type"
            )

        # Check functions without adhesion/employment
        functions = Function.objects.current().filter(
            ~Q(person__adherent__adhesions__season=2024, person__adherent__adhesions__canceled=False) &
            ~Q(person__employee__employments__dates__contains=today()),
            function_config__subject_to_nomination=False,
        ).exclude(
            id__in=settings.EXCLUDE_CHECK.get('function_inactive', []),
        ).order_by('id')
        for function in functions:
            nb_errors += 1
            self.stdout.write(
                f"Function {build_url(reverse('function_detail', args=[function.id]))} : "
                "no current adhesion or employment"
            )

        # Payment allocations
        payments = Payment.objects.annotate(
            sum=Sum('allocation__amount')
        ).exclude(
            id__in=settings.EXCLUDE_CHECK.get('payment_allocation', []),
        ).filter(amount__lt=F('sum'))
        for payment in payments:
            nb_errors += 1
            self.stdout.write(
                f"Payment {build_url(reverse('payment_detail', args=[payment.id]))} : "
                f"amount={payment.amount} instead of sum(allocations)={payment.sum}"
            )

        # Check online contribution + sum(transfer) = rate
        adhesions = Adhesion.objects.with_effective_rate(
        ).filter(
            season__gte=2024,
            canceled=False,
        ).annotate(
            online=Coalesce(Subquery(
                Allocation.objects.filter(
                    adhesion=OuterRef('pk'),
                    category=1,
                    payment__online_status=PaymentStatus.SUCCESS,
                ).values(
                    'adhesion_id',
                ).annotate(
                    sum=Sum('amount'),
                ).values(
                    'sum',
                )[:1]
            ), Decimal(0)),
            transfer=Coalesce(Subquery(
                Entry.objects.filter(
                    adhesion=OuterRef('pk'),
                    category=1,
                ).values(
                    'adhesion_id',
                ).annotate(
                    sum=Sum('amount'),
                ).values(
                    'sum',
                )[:1]
            ), Decimal(0)),
        ).exclude(
            qs_effective_rate=F('transfer') + F('online'),
        ).exclude(
            id__in=settings.EXCLUDE_CHECK.get('adhesion_contribution', []),
        ).exclude(
            structure_id=1,
        ).order_by(
            'id',
        )
        for adhesion in adhesions:
            nb_errors += 1
            self.stdout.write(
                f"Adhesion {build_url(reverse('adhesion_detail', args=[adhesion.id]))} : "
                f"online={adhesion.online} + transfer={adhesion.transfer} instead of rate={adhesion.qs_effective_rate}"
            )

        # Check online donation = sum(transfer)
        adhesions = Adhesion.objects.filter(
            season__gte=2024,
            canceled=False,
        ).annotate(
            online=Coalesce(Subquery(
                Allocation.objects.filter(
                    adhesion=OuterRef('pk'),
                    category=2,
                    payment__online_status=PaymentStatus.SUCCESS,
                ).values(
                    'adhesion_id',
                ).annotate(
                    sum=Sum('amount'),
                ).values(
                    'sum',
                )[:1]
            ), Decimal(0)),
            transfer=Coalesce(Subquery(
                Entry.objects.filter(
                    adhesion=OuterRef('pk'),
                    category=2,
                ).values(
                    'adhesion_id',
                ).annotate(
                    sum=Sum('amount'),
                ).values(
                    'sum',
                )[:1]
            ), Decimal(0)),
        ).exclude(
            transfer=-F('online'),
        ).exclude(
            id__in=settings.EXCLUDE_CHECK.get('adhesion_donation', []),
        ).order_by(
            'id',
        )
        for adhesion in adhesions:
            nb_errors += 1
            self.stdout.write(
                f"Adhesion {build_url(reverse('adhesion_detail', args=[adhesion.id]))} : "
                f"donation transfer={-adhesion.transfer} instead of online={adhesion.online}"
            )

        # Check adhesion end date
        adhesions = Adhesion.objects.filter(
            rate__category=RateCategory.DISCOVERY,
        ).exclude(
            dates__endswith=F('dates__startswith') + F('rate__duration'),
        ).exclude(
            id__in=settings.EXCLUDE_CHECK.get('adhesion_end', []),
        ).order_by('id')
        for adhesion in adhesions:
            nb_errors += 1
            self.stdout.write(
                f"Adhesion {build_url(reverse('adhesion_detail', args=[adhesion.id]))} : dates.end={adhesion.dates} "
                f"instead of {adhesion.begin + TimeDelta(days=adhesion.rate.duration)}"
            )

        # Check is_discovery
        adhesions = Adhesion.objects.filter(rate__category=RateCategory.DISCOVERY, is_discovery=False)
        for adhesion in adhesions:
            nb_errors += 1
            self.stdout.write(
                f"Adhesion {build_url(reverse('adhesion_detail', args=[adhesion.id]))} : "
                f"is_discovery={adhesion.is_discovery} instead of True"
            )
        adhesions = Adhesion.objects.exclude(rate__category=RateCategory.DISCOVERY).filter(is_discovery=True)
        for adhesion in adhesions:
            nb_errors += 1
            self.stdout.write(
                f"Adhesion {build_url(reverse('adhesion_detail', args=[adhesion.id]))} : "
                f"is_discovery={adhesion.is_discovery} instead of False"
            )

        # Check is_paid
        adhesions = Adhesion.objects.annotate(
            qs_to_pay=Case(
                When(rate__rate_max=None, then=F('rate__rate')),
                default=F('free_rate')
            ) + F('donation'),
            qs_paid=Coalesce(Sum(
                'allocation__amount',
                filter=Q(allocation__payment__online_status__in=(PaymentStatus.MANUAL, PaymentStatus.SUCCESS))
            ), Value(Decimal(0))),
            qs_is_paid=Case(
                When(qs_paid__gte=F('qs_to_pay'), then=Value(True)),
                default=Value(False)
            )
        ).exclude(is_paid=F('qs_is_paid'))
        for adhesion in adhesions:
            nb_errors += 1
            self.stdout.write(
                f"Adhesion {build_url(reverse('adhesion_detail', args=[adhesion.id]))} : "
                f"is_paid={adhesion.is_paid} instead of {adhesion.qs_is_paid}"
            )

        # Check last_invitation
        season = current_season()
        persons = Person.objects.filter(
            invitation__season=season,
            invitation__declined=False,
        ).exclude(
            adherent__adhesions__season=2024,
        ).exclude(
            last_invitation=season,
        ).order_by('id')
        for person in persons:
            nb_errors += 1
            self.stdout.write(
                f"Person {build_url(reverse('person_detail', args=[person.id]))} : "
                f"last_invitation={person.last_invitation} instead of {season}"
            )
        persons = Person.objects.filter(
            ~Q(invitation__season=season) |
            Q(invitation__season=season, invitation__declined=True) |
            Q(adherent__adhesions__dates__contains=today(), adherent__adhesions__canceled=False),
        ).filter(
            last_invitation=season,
        ).order_by('id')
        for person in persons:
            nb_errors += 1
            self.stdout.write(
                f"Person {build_url(reverse('person_detail', args=[person.id]))} : "
                f"last_invitation={person.last_invitation} instead of != {season}"
            )

        # Check has_payment
        persons = Person.objects.filter(payment__isnull=False, has_payment=False)
        for person in persons:
            nb_errors += 1
            self.stdout.write(
                f"Person {build_url(reverse('person_detail', args=[person.id]))} : "
                f"has_payment={person.has_payment} instead of True"
            )
        persons = Person.objects.filter(payment__isnull=True, has_payment=True)
        for person in persons:
            nb_errors += 1
            self.stdout.write(
                f"Person {build_url(reverse('person_detail', args=[person.id]))} : "
                f"has_payment={person.has_payment} instead of False"
            )

        # Check has_protected
        persons = Person.objects.filter(Q(protected1__isnull=False) | Q(protected2__isnull=False), has_protected=False)
        for person in persons:
            nb_errors += 1
            self.stdout.write(
                f"Person {build_url(reverse('person_detail', args=[person.id]))} : "
                f"has_protected={person.has_protected} instead of True"
            )
        persons = Person.objects.filter(Q(protected1__isnull=True) & Q(protected2__isnull=True), has_protected=True)
        for person in persons:
            nb_errors += 1
            self.stdout.write(
                f"Person {build_url(reverse('person_detail', args=[person.id]))} : "
                f"has_protected={person.has_protected} instead of False"
            )

        self.stdout.flush()
        if nb_errors == 1:
            raise CommandError("One error detected")
        elif nb_errors:
            raise CommandError(f"{nb_errors} errors detected")

        self.stdout.write(self.style.SUCCESS("Success"))
