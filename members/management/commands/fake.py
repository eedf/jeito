# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from faker import Faker
from ...models import Person


class Command(BaseCommand):
    def handle(self, *args, **options):
        if not settings.TEST_SITE:
            raise CommandError("Ce n'est pas un site de test. Refus de modifier les données.")

        fake = Faker((settings.LANGUAGE_CODE, ))
        persons = Person.objects.exclude(is_superuser=True)
        nb = persons.count()

        for i, person in enumerate(persons):
            person.username = None
            person.set_unusable_password()
            person.gender = fake.random_element(('M', 'F'))
            if person.gender == 'M':
                person.first_name = fake.first_name_male()
            if person.gender == 'F':
                person.first_name = fake.first_name_female()
            person.last_name = fake.last_name()
            if person.birthdate:
                if person.birthdate.year >= 2004:
                    person.birthdate = fake.date_of_birth(
                        minimum_age=max(2022 - person.birthdate.year, 0),
                        maximum_age=2024 - person.birthdate.year,
                    )
                else:
                    person.birthdate = fake.date_of_birth(minimum_age=18)
            if person.email:
                person.email = fake.email()
                person.email_as_login = False
            person.address1 = fake.street_address()
            person.address2 = ""
            person.address3 = ""
            person.postal_code = fake.postcode()
            person.city = fake.city()
            person.country = "France"
            person.home_phone = fake.phone_number()
            person.mobile_phone = '06' + fake.phone_number()[2:]
            if person.birth_place:
                person.birth_place = fake.city()
            if person.profession:
                person.profession = fake.job()[:100]
            if person.image_rights is not None:
                person.image_rights = fake.boolean(chance_of_getting_true=90)
            # print(
            #     person.gender,
            #     person.first_name,
            #     person.last_name,
            #     person.birth_date,
            #     person.email,
            #     person.address1,
            #     person.address2,
            #     person.address3,
            #     person.postal_code,
            #     person.city,
            #     person.country,
            #     person.home_phone,
            #     person.mobile_phone,
            #     person.birth_place,
            #     person.profession,
            #     person.image_right,
            # )
            person.save()
            if (i + 1) % (nb // 100) == 0:
                self.stdout.write(f"{(i + 1) * 100 // nb + 1} %")
