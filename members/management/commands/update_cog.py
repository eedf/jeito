import csv
from datetime import date
import requests
from psycopg2.extras import DateRange
from django.core.management.base import BaseCommand
from members.models import Country, Commune


class Command(BaseCommand):
    def handle(self, *args, **options):
        # Countries
        self.stdout.write("Update countries:")
        response = requests.get('https://www.insee.fr/fr/statistiques/fichier/7766585/v_pays_territoire_2024.csv')
        response.encoding = 'utf8'
        reader = csv.DictReader(response.text.splitlines())
        for row in reader:
            country, created = Country.objects.update_or_create(
                id=row['COG'],
                defaults={
                    'name': row['LIBCOG'],
                    'iso2': row['CODEISO2'],
                    'iso3': row['CODEISO3'],
                },
            )
            self.stdout.write(f"- {'Create' if created else 'Update'} {country.id} {country.name}")

        # Communes
        self.stdout.write("Update communes:")
        response = requests.get('https://www.insee.fr/fr/statistiques/fichier/7766585/v_commune_depuis_1943.csv')
        response.encoding = 'utf8'
        reader = csv.DictReader(response.text.splitlines())
        for row in reader:
            if row['LIBELLE'] in ('Paris', 'Lyon', 'Marseille'):
                continue
            begin = date.fromisoformat(row['DATE_DEBUT']) if row['DATE_DEBUT'] else None
            if begin == date(1943, 1, 1):
                begin = None
            end = date.fromisoformat(row['DATE_FIN']) if row['DATE_FIN'] else None
            commune, created = Commune.objects.update_or_create(
                code=row['COM'],
                dates__startswith=begin,
                defaults={
                    'name': row['LIBELLE'],
                    'dates': DateRange(begin, end),
                },
            )
            self.stdout.write(f"- {'Create' if created else 'Update'} {commune.code} {commune.name}")
        for name, i, n in [('Paris', 75101, 20), ('Lyon', 69381, 9), ('Marseille', 13201, 16)]:
            for code in range(i, i + n):
                commune, created = Commune.objects.update_or_create(
                    code=code,
                    dates__startswith=None,
                    defaults={
                        'name': f"{name} {code - i + 1}",
                        'dates': DateRange(None, None),
                    },
                )
                self.stdout.write(f"- {'Create' if created else 'Update'} {commune.code} {commune.name}")

        self.stdout.write(self.style.SUCCESS("Done"))
