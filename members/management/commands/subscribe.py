# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from django.conf import settings
from django.core.management.base import BaseCommand
import pika
import ssl


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('key', nargs='?', default='#')
        parser.add_argument('--exchange', default='synchro_v4')

    def handle(self, *args, **options):
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=settings.BROKER_HOST,
                port=settings.BROKER_PORT,
                ssl_options=pika.SSLOptions(ssl.create_default_context()) if settings.BROKER_TLS else None,
                virtual_host=settings.BROKER_VIRTUAL_HOST,
                credentials=pika.credentials.PlainCredentials(
                    username=settings.BROKER_USERNAME,
                    password=settings.BROKER_PASSWORD,
                ),
            ),
        )
        channel = connection.channel()
        channel.exchange_declare(exchange=options['exchange'], exchange_type='topic')
        result = channel.queue_declare(queue='', exclusive=True)
        queue_name = result.method.queue
        channel.queue_bind(exchange=options['exchange'], queue=queue_name, routing_key=options['key'])
        print(' [*] Waiting for logs. To exit press CTRL+C')

        def callback(ch, method, properties, body):
            print(f" [x] {body} {method} {properties}")

        channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)
        try:
            channel.start_consuming()
        except KeyboardInterrupt:
            connection.close()
            self.stdout.write(self.style.SUCCESS(" Quit"))
