# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from base64 import b64encode
from collections import OrderedDict

from dal import autocomplete
from datetime import date, datetime, timedelta
from decimal import Decimal
from functools import reduce
from hashlib import md5
import hmac
import json
import logging
import openpyxl
from operator import and_, or_
from psycopg2.extras import DateRange
import re
import requests
from sentry_sdk import capture_message
from tempfile import NamedTemporaryFile
from urllib.parse import urlencode, urlparse, parse_qs
from weasyprint import HTML
from zipfile import BadZipFile

from django.contrib.auth import login
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin, PermissionRequiredMixin, AccessMixin
from django.contrib.auth.views import LoginView as BaseLoginView, LogoutView as BaseLogoutView, \
    PasswordResetView as BasePasswordResetView
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.search import TrigramSimilarity
from django.conf import settings
from django.contrib import messages
from django.core import mail
from django.core.exceptions import PermissionDenied, ValidationError, ObjectDoesNotExist
from django.core.paginator import Paginator
from django.db import models, transaction, IntegrityError
from django.db.models import Count, F, Q, Max, Sum, Avg, Case, When, Value, Func, Subquery
from django.db.models.functions import ExtractMonth, ExtractYear, Abs, Left
from django.core.exceptions import BadRequest
from django import forms
from django.http import JsonResponse, HttpResponse, HttpResponseForbidden, HttpResponseRedirect, Http404, \
    HttpResponseBadRequest
from django.shortcuts import get_object_or_404, render
from django.template.loader import get_template
from django.urls import reverse, reverse_lazy, resolve
from django.utils.decorators import method_decorator
from django.utils.encoding import force_bytes
from django.utils.formats import date_format, get_format_lazy
from django.utils.html import escape, format_html
from django.utils.http import urlsafe_base64_encode
from django.utils.safestring import mark_safe
from django.utils.timezone import now, make_aware
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View, TemplateView, DetailView, ListView, UpdateView, FormView, CreateView, \
    DeleteView, RedirectView
from auditlog.diff import model_instance_diff
from auditlog.models import LogEntry
from django_filters.views import FilterView
from formtools.wizard.views import SessionWizardView
from oauth2_provider.models import Application
from sequences import get_next_value
from .card import generate_card
from .filters import (StructureFilter, AdhesionFilter, StatsAdhesionFilter, StatsSfFilter, CommuneFilter, CountryFilter,
                      InconsistenciesFilter, InvitationFilter, EmploymentFilter, EmployeeFilter, PersonFilter,
                      CreateInvitationFilter, UnpaidFilter, TeamFilter, FunctionFilter, AdherentFilter, RoleFilter,
                      ReceiptStatsFilter, LogEntryFilter, PaymentFilter, AccountingFilter, AccountingDetailFilter)
from .forms import (AdhesionsFilterForm, GuardianForm, FunctionForm, ContributionForm, SelectInvitationsForm,
                    AdhesionPreCreateForm, PersonForm, AdhesionForm, PaymentForm, PayerForm, AdhesionFunctionForm,
                    SearchGuardianForm, SearchPayerForm, StructureForm, PermissionForm,
                    StatusForm, PersonMergeForm, EmploymentForm, EmployeeForm, RoleForm, RoleDelegationForm,
                    PersonDeduplicateForm, InvitationCreateNewForm, InvitationCreateOldForm, Permission,
                    InvitationCreateListFormSet, InvitationUpdateForm, PreConfirmForm, PostConfirmForm, TeamForm,
                    SearchPersonForm, RoleConfigForm, EmailAsLoginForm, AdhesionImportForm, EmploymentImportForm,
                    CONTACT_MESSAGE, PasswordResetForm)
from .models import (Adhesion, Adherent, Allocation, Entry, Function, Payment, Person, Structure, Rate, PersonTokenPart,
                     age, TransferOrder, Transfer, StructureType, Receipt, Invitation, Season, Employee, EmploymentType,
                     AdherentStatus, Team, TeamConfig, TeamType, FunctionConfig, Employment, Commune, Country,
                     FunctionStatus, RoleConfig, Role, Echelon, FunctionCategory, User, RateCategory, PaymentStatus)
from .signals import force_publish_save, force_publish_delete
from .utils import current_season, today, yesterday, remove_accents, replace_year, tokenize_name, normalize_name
from .widget import widgets


IPN_LOGGER = logging.getLogger('ipn')


class SuperuserMixin(UserPassesTestMixin):
    def test_func(self):
        return self.request.user.is_superuser


class WizardCacheMixin:
    def get_cleaned_data_for_step(self, step):
        if not hasattr(self, '_cleaned_data_cache'):
            self._cleaned_data_cache = {}
        if step not in self._cleaned_data_cache:
            self._cleaned_data_cache[step] = super().get_cleaned_data_for_step(step)
        return self._cleaned_data_cache[step]


class StructureAutocomplete(LoginRequiredMixin, autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Structure.objects.all()
        if 'for_perm' in self.forwarded:
            qs = qs.for_perm(self.forwarded['for_perm'], self.request.user)
        if self.forwarded.get('is_active'):
            qs = qs.is_active()
        if self.forwarded.get('can_import'):
            qs = qs.filter(type__can_import=True)
        if 'child__type' in self.forwarded:
            child__type_pk = self.forwarded['child__type']
            if not child__type_pk.isdigit():
                raise BadRequest("child_type parameter should be a positive integer")
            child__type = get_object_or_404(StructureType, pk=child__type_pk)
            qs = qs.filter(type__echelon__lt=child__type.echelon)
        if 'echelon' in self.forwarded:
            echelon = self.forwarded['echelon']
            if not echelon.isdigit():
                raise BadRequest("echelon parameter should be a positive integer")
            qs = qs.filter(type__echelon=echelon)
        type_pk = self.forwarded.get('type')
        if isinstance(type_pk, str) and type_pk.isdigit():
            qs = qs.filter(type=type_pk)
        if self.q:
            tokens = re.split('( |-)', self.q)
            qs = qs.filter(reduce(and_, (Q(name__unaccent__icontains=token) for token in tokens)))
        return qs.order_by('name')


class TeamAutocomplete(LoginRequiredMixin, autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Team.objects
        if self.forwarded.get('is_active'):
            qs = qs.is_active()
        if 'for_perm' in self.forwarded:
            qs = qs.for_perm(self.forwarded['for_perm'], self.request.user)
        if 'for_adherent' in self.forwarded:
            qs = qs.for_adherent()
        structure_pk = self.forwarded.get('structure')
        if not isinstance(structure_pk, str) or not structure_pk.isdigit():
            return qs.none()
        qs = qs.filter(structure=structure_pk)
        type_pk = self.forwarded.get('type')
        if isinstance(type_pk, str) and type_pk.isdigit():
            qs = qs.filter(type=type_pk)
        if self.q:
            tokens = re.split('( |-)', self.q)
            qs = qs.filter(reduce(and_, (Q(name__unaccent__icontains=token) for token in tokens)))
        qs = qs.distinct().order_by('type__order', 'name')
        return qs


class PersonAutocomplete(LoginRequiredMixin, autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Person.objects
        if 'for_perm' in self.forwarded:
            qs = qs.for_perm(self.forwarded['for_perm'], self.request.user)
        if self.forwarded.get('current', False):
            qs = qs.current()
        if 'exclude' in self.forwarded:
            qs = qs.exclude(id=self.forwarded['exclude'])
        if 'structure' in self.forwarded and not self.forwarded.get('outside', True):
            structure_pk = self.forwarded['structure']
            if structure_pk.isdigit():
                qs = qs.filter(adherent__last_adhesion__structure=structure_pk)
            elif structure_pk != '':
                raise BadRequest("structure parameter should be a positive integer")
        if self.q:
            qs = qs.search(self.q).order_by('-similarity')
        return qs.distinct().select_related(
            'adherent',
            'employee',
            'employee__last_employment',
        ).only(
            'last_name',
            'family_name',
            'first_name',
            'nickname',
            'adherent__id',
            'employee__id',
            'employee__last_employment__type',
        )


class AdherentAutocomplete(LoginRequiredMixin, autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Adherent.objects
        if 'for_perm' in self.forwarded:
            qs = qs.for_perm(self.forwarded['for_perm'], self.request.user)
        if 'structure' in self.forwarded:
            structure_pk = self.forwarded['structure']
            if structure_pk == '':
                pass
            elif not structure_pk.isdigit():
                raise BadRequest("structure parameter should be a positive integer")
            else:
                qs = qs.filter(adhesions__structure=structure_pk)
        if self.q:
            qs = qs.search(self.q).order_by('-similarity')
        return qs.distinct().select_related(
            'person',
        ).only(
            'person__last_name',
            'person__family_name',
            'person__first_name',
            'person__nickname',
            'id',
        )


class EmployeeAutocomplete(LoginRequiredMixin, autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Employee.objects
        if self.q:
            qs = qs.search(self.q)
        return qs.distinct().select_related(
            'person',
            'last_employment',
        ).only(
            'person__last_name',
            'person__family_name',
            'person__first_name',
            'person__nickname',
            'id',
            'last_employment__type',
        )


class FunctionConfigAutocomplete(LoginRequiredMixin, autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = FunctionConfig.objects.filter(subject_to_nomination=False)
        team_pk = self.forwarded.get('team')
        if not isinstance(team_pk, str) or not team_pk.isdigit():
            return qs.none()
        team = get_object_or_404(Team, pk=team_pk)
        qs = qs.filter(
            Q(team_type=team.type, structure_type=team.structure.type) |
            Q(team_type=team.type, structure_type=None)
        )
        qs = qs.filter(reduce(and_, [Q(name__unaccent__icontains=word) for word in self.q.split(" ")]))
        return qs.distinct()

    def get_results(self, context):
        begin = self.forwarded.get('begin')
        try:
            begin = datetime.strptime(begin, '%d/%m/%Y')
        except TypeError:
            begin = today()
        return [
            {
                'id': self.get_result_value(result),
                'text': self.get_result_label(result),
                'selected_text': self.get_selected_result_label(result),
                'default_end': result.default_end(begin, format='%d/%m/%Y'),
            } for result in context['object_list']
        ]


def get_ban_label(feature):
    properties = feature['properties']
    label = properties['label']
    if properties['type'] == 'municipality':
        label += f" ({properties['context']})"
    return label


class PlaceAutocomplete(LoginRequiredMixin, autocomplete.Select2ListView):
    def get_list(self):
        q = self.q.strip()
        if not q or len(q) < 3:
            return []

        response = requests.get('https://api-adresse.data.gouv.fr/search/', {'q': q})
        if response.status_code != 200:
            capture_message(f"BANO returned HTTP {response.status_code}")
            return []
        return [get_ban_label(feature) for feature in response.json()['features']]


class CommuneAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Commune.objects.all()
        value = self.forwarded.get('birthdate').strip()
        birthdate = None
        for format in get_format_lazy("DATE_INPUT_FORMATS"):
            try:
                birthdate = datetime.strptime(value, format).date()
            except (ValueError, TypeError):
                continue
        if not birthdate:
            return qs.none()
        qs = qs.filter(dates__contains=birthdate)
        if self.q:
            qs = qs.search(self.q).order_by('-similarity')
        return qs


class RedirectFromV1View(RedirectView):
    def get_redirect_url(self, path):
        return f'/{path}'


class HomeView(LoginRequiredMixin, TemplateView):
    template_name = 'members/home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['season'] = Season.objects.get(year=current_season())
        context['news_banner'] = settings.NEWS_BANNER
        context['nb_unpaid'] = Adhesion.objects \
            .for_perm('members.view_unpaid_adhesion', self.request.user) \
            .filter(is_paid=False, canceled=False, season=current_season()) \
            .count()
        if self.request.user.person:
            context['persons'] = Person.objects.filter(
                Q(user=self.request.user) |
                Q(legal_guardian1=self.request.user.person) |
                Q(legal_guardian2=self.request.user.person)
            ).order_by('birthdate')
        context['structures'] = set()
        if self.request.user.person:
            if self.request.user.person.last_adhesion:
                context['structures'].add(self.request.user.person.last_adhesion.structure)
            for protected in self.request.user.person.protecteds:
                if protected.last_adhesion:
                    context['structures'].add(protected.last_adhesion.structure)
        context['SUPPORT_EMAIL'] = settings.SUPPORT_EMAIL
        return context


class StructureListView(PermissionRequiredMixin, FilterView):
    permission_required = 'members.view_structure'
    filterset_class = StructureFilter
    template_name = 'members/structure_list.html'
    paginate_by = 25

    def get_queryset(self):
        qs = Structure.objects.for_perm('members.view_structure', self.request.user)
        if not self.request.user.has_perm('members.view_inactive_structure'):
            qs = qs.filter(status__in=(Structure.STATUS_AUTONOMOUS, Structure.STATUS_GUARDIANSHIP))
        return qs.select_related(
            'type',
            'parent',
        ).order_by('name', 'pk')


class StructureDetailView(PermissionRequiredMixin, LoginRequiredMixin, DetailView):
    permission_required = 'members.view_structure'

    def get_queryset(self):
        qs = Structure.objects.for_perm('members.view_structure', self.request.user)
        if not self.request.user.has_perm('members.view_inactive_structure'):
            qs = qs.filter(status__in=(Structure.STATUS_AUTONOMOUS, Structure.STATUS_GUARDIANSHIP))
        return qs.order_by('name', 'pk')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['teams'] = self.object.teams.order_by('type__order', 'name')
        context['can_delete'] = self.request.user.has_perm('members.delete_structure', self.object)
        context['can_change'] = self.request.user.has_perm('members.change_structure', self.object)
        context['can_view_iban'] = self.request.user.has_perm('members.view_structure_iban', self.object)
        context['can_view_function'] = self.request.user.has_perm('members.view_function', self.object)
        context['can_view_team'] = self.request.user.has_perm('members.view_team', self.object)
        context['content_type'] = ContentType.objects.get_for_model(Structure)
        return context


class StructureSigningView(PermissionRequiredMixin, LoginRequiredMixin, DetailView):
    permission_required = 'members.view_adhesion'
    model = Structure
    template_name = 'members/structure_signing.html'

    def render_to_response(self, context, **response_kwargs):
        template = get_template(self.template_name)
        adhesions = Adhesion.objects.for_perm(
            'members.view_adhesion', self.request.user,
        ).filter_parent_structure(
            self.object,
        ).with_age(
        ).filter(
            season=current_season(),
            qs_age__gte=Value(16),
        ).select_related(
            'adherent',
            'adherent__person',
            'structure',
        ).order_by(
            'adherent__person__last_name',
            'adherent__person__first_name',
        )
        context = {
            'structure': self.object,
            'today': today(),
            'adhesions': adhesions,
        }
        markup = template.render(context)
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = f'attachment; filename=Liste d´émargement {self.object}.pdf'
        HTML(string=markup, base_url=settings.BASE_DIR).write_pdf(response)
        return response


class BackSuccessUrl:
    def get_success_url(self):
        url = self.request.GET.get('back')
        if not url:
            model = getattr(self, 'model', None) or self.get_queryset().model
            url = reverse(f'{model.__name__.lower()}_list')
        if '#' in url:
            url, anchor = url.split('#', maxsplit=1)
        else:
            anchor = ''
        if issubclass(resolve(url).func.view_class, FilterView):
            url += self.request.POST.get('filters', '')
        if anchor:
            url += f'#{anchor}'
        return url


class StructureCreateView(PermissionRequiredMixin, BackSuccessUrl, CreateView):
    permission_required = 'members.add_structure'
    model = Structure
    form_class = StructureForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        response = super().form_valid(form)
        # Create teams
        configs = TeamConfig.objects.filter(
            structure_type=self.object.type,
            by_default=True,
        )
        for config in configs:
            Team.objects.create(
                structure=self.object,
                type=config.team_type,
                name=config.team_name,
                creation_date=now(),
            )
        return response


class StructureUpdateView(PermissionRequiredMixin, BackSuccessUrl, UpdateView):
    permission_required = 'members.change_structure'
    form_class = StructureForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def get_queryset(self):
        return Structure.objects.for_perm('members.change_structure', self.request.user)


class StructureDeleteView(PermissionRequiredMixin, BackSuccessUrl, DeleteView):
    permission_required = 'members.delete_structure'
    model = Structure


class StructureDormantView(PermissionRequiredMixin, BackSuccessUrl, DeleteView):
    permission_required = 'members.change_structure'
    template_name = 'members/structure_confirm_dormant.html'
    model = Structure

    def get_queryset(self):
        return Structure.objects.filter(status__in=(Structure.STATUS_AUTONOMOUS, Structure.STATUS_GUARDIANSHIP))

    def form_valid(self, form):
        self.object.status = Structure.STATUS_DORMANT
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())


class StructureClosedView(StructureDormantView):
    template_name = 'members/structure_confirm_closed.html'

    def get_queryset(self):
        return Structure.objects.filter(status=Structure.STATUS_DORMANT)

    def form_valid(self, form):
        self.object.status = Structure.STATUS_CLOSED
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())


class StructureGuardianshipView(StructureDormantView):
    template_name = 'members/structure_confirm_guardianship.html'

    def get_queryset(self):
        return Structure.objects.filter(status=Structure.STATUS_DORMANT)

    def form_valid(self, form):
        self.object.status = Structure.STATUS_GUARDIANSHIP
        self.object.save()
        return HttpResponseRedirect(reverse('structure_detail', args=[self.object.pk]))


class ExportView():
    paginate_by = None

    def add_row(self, i, values):
        formats = []
        row = []
        for value in values:
            if callable(value):
                value = value()
            if value is None:
                formats.append(None)
                row.append("")
            elif isinstance(value, datetime):
                formats.append('DD/MM/YYYY')
                row.append(value.date())
            elif isinstance(value, date):
                formats.append('DD/MM/YYYY')
                row.append(value)
            elif isinstance(value, bool):
                formats.append(None)
                row.append(("Non", "Oui")[value])
            elif isinstance(value, Decimal):
                formats.append('0.00')
                row.append(value)
            elif isinstance(value, int):
                formats.append('0')
                row.append(value)
            else:
                formats.append(None)
                row.append(str(value))
        self.ws.append(row)
        for j, format in enumerate(formats):
            if format:
                self.ws.cell(row=i + 2, column=j + 1).number_format = format

    def render_to_response(self, context, **response_kwargs):
        LogEntry.objects.create(
            content_type=ContentType.objects.get_for_model(context['object_list'].model),
            action=LogEntry.Action.ACCESS,
            changes_text=f"Export {self.request.get_full_path()}"
        )
        wb = openpyxl.Workbook()
        self.ws = wb.active
        header, attributes, widths = zip(*self.get_fields())
        self.ws.append(header)
        for i, obj in enumerate(context['object_list']):
            values = []
            for attribute in attributes:
                value = obj
                for sub_attribute in attribute.split('.'):
                    if value is not None:
                        try:
                            value = getattr(value, sub_attribute)
                        except ObjectDoesNotExist:
                            value = None
                values.append(value)
            self.add_row(i, values)
        if hasattr(self, 'get_total'):
            self.add_row(i, self.get_total(context))
        for i, width in enumerate(widths):
            self.ws.column_dimensions[openpyxl.utils.get_column_letter(i + 1)].width = width
        response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response['Content-Disposition'] = f"attachment; filename={self.filename}.xlsx"
        with NamedTemporaryFile() as tmp:
            wb.save(tmp.name)
            tmp.seek(0)
            response.write(tmp.read())
        return response


class StructureExportView(ExportView, StructureListView):
    filename = "Structures"

    def get_fields(self):
        fields = [
            ("Nom", 'name', 40),
            ("Type", 'type', 20),
            ("Parent", 'parent', 40),
            ("Statut", 'status_display', 12),
            ("Localisation", 'place', 50),
            ("Email public", 'public_email', 40),
            ("Téléphone public", 'public_phone_display', 20),
        ]
        if self.request.user.has_perm('members.view_structure_iban'):
            fields.append(("IBAN", 'iban', 40))
        return fields


class AdherentListView(LoginRequiredMixin, FilterView):
    filterset_class = AdherentFilter
    template_name = 'members/adherent_list.html'
    paginate_by = 25

    def get_queryset(self):
        return Adherent.objects \
            .for_perm('members.view_adherent', self.request.user) \
            .select_related(
                'person',
                'last_adhesion',
                'last_adhesion__rate',
                'last_adhesion__structure',
                'last_adhesion__structure__region',
                'person__legal_guardian1',
                'person__legal_guardian2',
            )


class AdherentExportView(ExportView, AdherentListView):
    filename = "Adhérent·es"

    def get_fields(self, strict=False):
        fields = PersonExportView.get_fields(self, strict=True)
        fields = [(name, f"person.{attribute}", width) for name, attribute, width in fields]
        fields.extend([
            ("Numéro", 'id_str', 12),
            ("Statut", 'status_text', 15),
        ])
        if 'full' in self.request.GET:
            fields.extend([
                ("Autorisation légale", 'person.permission_display', 20),
                ("Nom resp. légal 1", 'person.legal_guardian1.last_name', 30),
                ("Prénom", 'person.legal_guardian1.first_name', 30),
                ("Email", 'person.legal_guardian1.email', 30),
                ("Tél. fixe", 'person.legal_guardian1.home_phone_display', 15),
                ("Tél. mobile", 'person.legal_guardian1.mobile_phone_display', 15),
                ("Nom resp. légal 2", 'person.legal_guardian2.last_name', 30),
                ("Prénom", 'person.legal_guardian2.first_name', 30),
                ("Email", 'person.legal_guardian2.email', 30),
                ("Tél. fixe", 'person.legal_guardian2.home_phone_display', 15),
                ("Tél. mobile", 'person.legal_guardian2.mobile_phone_display', 15),
            ])
            if not strict:
                fields.extend([
                    ("Dernière adhésion", 'last_adhesion.begin', 12),
                    ("Tarif", 'last_adhesion.rate.name', 30),
                    ("Structure", 'last_adhesion.structure.name', 25),
                    ("Région", 'last_adhesion.structure.region.name', 25),
                ])
        return fields


class AdhesionListView(LoginRequiredMixin, FilterView):
    filterset_class = AdhesionFilter
    template_name = 'members/adhesion_list.html'
    paginate_by = 25

    def get_queryset(self):
        return Adhesion.objects \
            .filter(canceled=False) \
            .for_perm('members.view_adhesion', self.request.user) \
            .select_related(
                'adherent',
                'adherent__last_adhesion',
                'adherent__person',
                'adherent__person__legal_guardian1',
                'adherent__person__legal_guardian2',
                'structure',
                'structure__region',
                'rate',
            )


class EmployeeListView(PermissionRequiredMixin, FilterView):
    permission_required = 'members.view_employee'
    filterset_class = EmployeeFilter
    template_name = 'members/employee_list.html'
    paginate_by = 25

    def get_queryset(self):
        return Employee.objects \
            .for_perm('members.view_employee', self.request.user) \
            .select_related(
                'person',
                'last_employment',
            )


class EmployeeExportView(ExportView, EmployeeListView):
    filename = "Salarié·es"

    def get_fields(self, strict=False):
        fields = PersonExportView.get_fields(self, strict=True)
        fields = [(name, f"person.{attribute}", width) for name, attribute, width in fields]
        fields.extend([
            ("Email pro", 'email', 30),
            ("Tél. pro", 'phone_display', 15),
            ("Numéro", 'id_str', 12),
            ("Status", 'status_text', 15),
        ])
        if 'full' in self.request.GET:
            if not strict:
                fields.extend([
                    ("Dernier contrat", 'last_employment.name', 40),
                    ("Type", 'last_employment.type_display', 15),
                    ("Du", 'last_employment.begin', 12),
                    ("Au", 'last_employment.end', 12),
                ])
        return fields


class EmployeeDetailView(PermissionRequiredMixin, DetailView):
    permission_required = 'members.view_employee'

    def get_queryset(self):
        return Employee.objects \
            .for_perm('members.view_employee', self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class EmployeeUpdateView(PermissionRequiredMixin, BackSuccessUrl, UpdateView):
    permission_required = 'members.change_employee'
    form_class = EmployeeForm

    def get_queryset(self):
        return Employee.objects.for_perm('members.change_employee', self.request.user)


class EmployeeDeleteView(PermissionRequiredMixin, BackSuccessUrl, DeleteView):
    permission_required = 'members.delete_employee'
    success_url = reverse_lazy('employee_list')

    def get_queryset(self):
        return Employee.objects.for_perm('members.delete_employee',  self.request.user)

    @transaction.atomic
    def form_valid(self, form):
        response = super().form_valid(form)
        # Reset last employment
        self.object.employee.last_employment = self.object.employee.employments.last()
        self.object.employee.save()
        return response


class EmploymentListView(PermissionRequiredMixin, FilterView):
    permission_required = 'members.view_employment'
    filterset_class = EmploymentFilter
    template_name = 'members/employment_list.html'
    paginate_by = 25

    def get_queryset(self):
        return Employment.objects \
            .for_perm('members.view_employment', self.request.user) \
            .select_related('employee', 'employee__person') \
            .only('name', 'type', 'dates', 'employee__person__last_name', 'employee__person__first_name')


class EmploymentExportView(ExportView, EmploymentListView):
    filename = "Contrats de travail"

    def get_fields(self):
        fields = EmployeeExportView.get_fields(self, strict=True)
        fields = [(name, f"employee.{attribute}", width) for name, attribute, width in fields]
        fields.extend([
            ("Intitulé de poste", 'name', 40),
            ("Type", 'type_display', 15),
            ("Du", 'begin', 12),
            ("Au", 'end', 12),
        ])
        return fields


class EmploymentDetailView(PermissionRequiredMixin, DetailView):
    permission_required = 'members.view_employment'

    def get_queryset(self):
        return Employment.objects \
            .for_perm('members.view_employment', self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['content_type'] = ContentType.objects.get_for_model(Employment)
        context['can_change'] = self.request.user.has_perm('members.change_employment', self.object)
        context['can_delete'] = self.request.user.has_perm('members.delete_employment', self.object)
        return context


class EmploymentUpdateView(PermissionRequiredMixin, BackSuccessUrl, UpdateView):
    permission_required = 'members.change_employment'
    form_class = EmploymentForm

    def get_queryset(self):
        return Employment.objects.for_perm('members.change_employment', self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['employee'] = self.object.employee
        return context

    def form_valid(self, form):
        response = super().form_valid(form)
        self.object.employee.last_employment = self.object.employee.employments.last()
        self.object.employee.save()
        messages.success(self.request, f"Le contrat de travail de {self.object.employee} a bien été modifié.",
                         extra_tags="alert-success")
        upper = self.object.employee.last_employment.dates.upper
        for function in Function.objects.filter(
            Q(dates__endswith=None) | Q(dates__endswith__gt=upper),
            person=self.object.employee.person,
            function_config__function_type__category=FunctionCategory.EMPLOYEE,
        ):
            if function.dates.lower >= upper:
                messages.warning(
                    self.request,
                    f"La fonction {function} sur l'équipe {function.team} {function.team.structure} a été supprimée.",
                    extra_tags="alert-warning",
                )
                function.delete()
            else:
                messages.warning(
                    self.request,
                    f"La fonction {function} sur l'équipe {function.team} {function.team.structure} "
                    f"a été stoppée au {(upper - timedelta(days=1)).strftime('%d/%m/%Y')}.",
                    extra_tags="alert-warning",
                )
                function.dates = DateRange(function.dates.lower, upper)
                function.save()
        return response


class EmploymentDeleteView(PermissionRequiredMixin, BackSuccessUrl, DeleteView):
    permission_required = 'members.delete_employment'
    success_url = reverse_lazy('employment_list')

    def get_queryset(self):
        return Employment.objects.for_perm('members.delete_employment', self.request.user)

    @transaction.atomic
    def form_valid(self, form):
        self.object = self.get_object()
        # Reset last employment
        self.object.employee.last_employment = self.object.employee.employments.exclude(pk=self.object.pk).last()
        self.object.employee.save()
        if self.object.employee.last_employment:
            upper = self.object.employee.last_employment.dates.upper or today()
        else:
            upper = date(2023, 9, 1)
        for function in Function.objects.filter(
            Q(dates__endswith=None) | Q(dates__endswith__gt=upper),
            person=self.object.employee.person,
            function_config__function_type__category=FunctionCategory.EMPLOYEE,
        ):
            if function.dates.lower >= upper:
                messages.warning(
                    self.request,
                    f"La fonction {function} sur l'équipe {function.team} {function.team.structure} a été supprimée.",
                    extra_tags="alert-warning",
                )
                function.delete()
            else:
                messages.warning(
                    self.request,
                    f"La fonction {function} sur l'équipe {function.team} {function.team.structure} "
                    f"a été stoppée au {(upper - timedelta(days=1)).strftime('%d/%m/%Y')}.",
                    extra_tags="alert-warning",
                )
                function.dates = DateRange(function.dates.lower, upper)
                function.save()
        messages.success(self.request, f"Le contrat de travail de {self.object.employee} a bien été supprimé.",
                         extra_tags="alert-success")
        return super().form_valid(form)


class UnpaidListView(LoginRequiredMixin, FilterView):
    filterset_class = UnpaidFilter
    template_name = 'members/unpaid_list.html'
    paginate_by = 25

    def get_queryset(self):
        return Adhesion.objects \
            .for_perm('members.view_unpaid_adhesion', self.request.user) \
            .filter(is_paid=False, canceled=False) \
            .select_related('adherent', 'adherent__person', 'structure') \
            .only('adherent__person__last_name', 'adherent__person__first_name', 'adherent__id',
                  'adherent__status', 'canceled', 'season', 'structure__name')


class PersonOKView(DetailView):
    model = Person
    template_name = 'members/person_ok.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        adhesion = self.object.last_adhesion
        context['can_read'] = adhesion and self.request.user.has_perm('members.view_adhesion', adhesion)
        return context


class PasswordResetMessageView(SuperuserMixin, DetailView):
    model = Person
    template_name = 'members/password_reset.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user, _ = User.objects.get_or_create(person=self.object)
        url = reverse('password_reset_confirm', kwargs={
            'uidb64': urlsafe_base64_encode(force_bytes(user.pk)),
            'token': PasswordResetTokenGenerator().make_token(user),
        })
        context['url'] = self.request.build_absolute_uri(url)
        return context


class AdhesionDetailView(LoginRequiredMixin, DetailView):
    def get_queryset(self):
        return Adhesion.objects.filter(canceled=False).for_perm('members.view_adhesion', self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['adherent'] = self.object.adherent
        context['person'] = self.object.adherent.person
        context['payments'] = Payment.objects.filter(
            allocation__adhesion=self.object,
            online_status__in=(PaymentStatus.MANUAL, PaymentStatus.SUCCESS),
        ).distinct()
        for perm in ('view_person_email', 'view_person_phone', 'view_person_birth', 'view_person_image_rights',
                     'view_person_guardian', 'delete_payment', 'change_payment'):
            context[f'can_{perm}'] = self.request.user.has_perm(
                f'members.{perm}', self.object.adherent.person
            )
        context['can_change_adherent_status'] = self.request.user.has_perm('members.change_adherent_status',
                                                                           self.object.adherent)
        for perm in ('view_adhesion', 'change_adhesion', 'download_adhesion_card', 'change_adherent_status',
                     'delete_adhesion'):
            context[f'can_{perm}'] = self.request.user.has_perm(f'members.{perm}', self.object)
        return context


class PersonDetailView(PermissionRequiredMixin, DetailView):
    permission_required = 'members.view_person'

    def get_queryset(self):
        return Person.objects.for_perm('members.view_person', self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            adherent = self.object.adherent
        except Adherent.DoesNotExist:
            adherent = None
        try:
            employee = self.object.employee
        except Employee.DoesNotExist:
            employee = None
        adhesion = self.object.current_adhesion
        context['functions'] = self.object.functions.current().for_perm('members.view_function', self.request.user)
        try:
            context['employments'] = self.object.employee.employments.current().order_by('-dates__startswith')
        except Employee.DoesNotExist:
            pass
        context['payments'] = Payment.objects.filter(
            payer=self.object,
            online_status__in=(PaymentStatus.MANUAL, PaymentStatus.SUCCESS),
        ).for_season(current_season()).order_by('-date')
        context['invitations'] = Invitation.objects.filter(
            person=self.object,
            season=current_season(),
        ).order_by('-date')
        for perm in ('view_person_email', 'view_person_phone', 'view_person_birth', 'view_person_image_rights',
                     'view_person_guardian', 'change_person', 'masquerade_person',
                     'add_person_guardian', 'change_person_guardian', 'delete_person_guardian',
                     'view_function', 'add_function',
                     'view_role', 'add_role', 'delete_role',
                     'view_invitation', 'delete_invitation', 'change_invitation',
                     'view_payment', 'delete_payment', 'change_payment'):
            context[f'can_{perm}'] = self.request.user.has_perm(f'members.{perm}', self.object)
        for perm in ('add', 'delete'):
            if self.request.user.person == self.object:
                context[f'can_{perm}_delegation'] = self.object.has_perm(f'members.{perm}_delegation', self.object)
            else:
                context[f'can_{perm}_delegation'] = self.request.user.has_perm(f'members.{perm}_role', self.object)
        if adherent:
            context['adherent'] = adherent
            for perm in ('view_adherent', 'change_adherent', 'change_adherent_status'):
                context[f'can_{perm}'] = self.request.user.has_perm(f'members.{perm}', adherent)
        if adhesion:
            context['adhesion'] = adhesion
            context['adhesion_payments'] = Payment.objects.filter(
                allocation__adhesion=adhesion,
                online_status__in=(PaymentStatus.MANUAL, PaymentStatus.SUCCESS),
            ).distinct()
            for perm in ('view_adhesion', 'change_adhesion', 'download_adhesion_card', 'delete_adhesion'):
                context[f'can_{perm}'] = self.request.user.has_perm(f'members.{perm}', adhesion)
        else:
            context['can_view_adhesion'] = self.request.user.has_perm('members.view_adhesion')
        if employee:
            context['employee'] = employee
            for perm in ('view_employee', 'change_employee', 'view_employment', 'add_employment'):
                context[f'can_{perm}'] = self.request.user.has_perm(f'members.{perm}', employee)
        context['logentries'] = LogEntry.objects.filter(
            Q(content_type=ContentType.objects.get_for_model(Person), object_id=self.object.pk) |
            Q(additional_data__person_id__contains=self.object.pk)
        ).filter(timestamp__gte=datetime(current_season() - 1, 9, 1), timestamp__lt=datetime(current_season(), 9, 1))
        context['season_display'] = f"{current_season() - 1}/{current_season()}"
        context['content_type'] = ContentType.objects.get_for_model(Person)
        return context


class AdhesionCancelView(PermissionRequiredMixin, BackSuccessUrl, DeleteView):
    permission_required = 'members.delete_adhesion'

    def get_queryset(self):
        return Adhesion.objects.filter(season=current_season(), canceled=False)

    def get_object(self):
        adhesion = super().get_object()
        self.amount = (
            sum(adhesion.allocation_set.filter(
                payment__online_status=PaymentStatus.SUCCESS,
                category=Allocation.CATEGORY_CONTRIBUTION
            ).values_list('amount', flat=True), 0) +
            sum(adhesion.entry_set.filter(
                category=Entry.CATEGORY_CONTRIBUTION
            ).values_list('amount', flat=True), 0)
        )
        return adhesion

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['amount'] = self.amount
        return context

    @transaction.atomic
    def form_valid(self, form):
        self.object = self.get_object()
        # Cancel adhesion
        self.object.canceled = True
        self.object.save()
        # Reset last adhesion
        self.object.adherent.last_adhesion = self.object.adherent.adhesions.filter(canceled=False).last()
        self.object.adherent.save()
        # Functions
        Function.objects.filter(
            person=self.object.adherent.person,
            dates__contained_by=self.object.dates,
            function_config__subject_to_nomination=False,
            function_config__function_type__category__in=(FunctionCategory.PARTICIPANT, FunctionCategory.VOLUNTEER),
        ).delete()
        # Invitations
        try:
            self.object.invitation.delete()
        except Invitation.DoesNotExist:
            pass
        # Payment
        assert not self.object.successful_allocations
        # Entry : bank transfer of the difference between paid amount and 0
        if self.amount:
            Entry.create(self.object, -self.amount, category=Entry.CATEGORY_CONTRIBUTION)
        # Message
        messages.success(self.request, f"L'adhésion de {self.object.adherent} a bien été annulée.",
                         extra_tags="alert-success")
        return HttpResponseRedirect(self.get_success_url())


class PersonUpdateView(LoginRequiredMixin, BackSuccessUrl, UpdateView):
    model = Person

    def dispatch(self, request, *args, **kwargs):
        person = self.get_object()
        if not request.user.has_perm('members.change_person', person):
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)

    def get_form_class(self):
        if self.object.is_adherent or self.object.is_employee:
            return PersonForm
        if self.object.protecteds:
            return GuardianForm
        return PayerForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.object.is_adherent or self.object.is_employee:
            kwargs['required_birth_place'] = False
        kwargs['user'] = self.request.user
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        today_ = today()
        birthdate = replace_year(today_, today_.year - 18)
        context['protecteds'] = Person.objects.filter(
            Q(legal_guardian1=self.object) | Q(legal_guardian2=self.object),
            birthdate__gt=birthdate
        ).order_by('last_name', 'first_name')
        context['payments'] = Payment.objects.filter(
            payer=self.object,
            online_status__in=(PaymentStatus.MANUAL, PaymentStatus.SUCCESS),
        ).exists()
        return context


class GuardianCreateView(PermissionRequiredMixin, BackSuccessUrl, WizardCacheMixin, SessionWizardView):
    permission_required = 'members.add_person_guardian'
    template_name = 'members/guardian_wizard.html'
    form_list = (
        ('search_guardian', SearchGuardianForm),
        ('guardian', GuardianForm),
    )

    def dispatch(self, request, pk, n):
        self.person = get_object_or_404(Person, pk=pk)
        if not request.user.has_perm('members.add_person_guardian', self.person):
            return self.handle_no_permission()
        self.guardian = getattr(self.person, f'legal_guardian{n}')
        self.n = n
        return super().dispatch(request)

    def get_context_data(self, form, **kwargs):
        context = super().get_context_data(form=form, **kwargs)
        context['n'] = self.n
        context['person'] = self.person
        return context

    def get_form_instance(self, step):
        if step == 'guardian':
            cleaned_data = self.get_cleaned_data_for_step('search_guardian')
            if cleaned_data and cleaned_data['mode'] == 'Sel':
                return cleaned_data['person']
            else:
                return None

    def get_form_initial(self, step):
        # search_guardian:
        if step == 'search_guardian' and self.guardian:
            return {
                'person': self.guardian,
                'mode': 'Sel',
            }
        # guardian
        if step == 'guardian':
            search_guardian_data = self.get_cleaned_data_for_step('search_guardian')
            if search_guardian_data and search_guardian_data.get('mode') == 'New':
                person_data = self.get_cleaned_data_for_step('person')
                return {
                    'last_name': person_data and person_data['last_name'],
                }
        return None

    def get_form_kwargs(self, step):
        kwargs = {
            'user': self.request.user
        }
        if step == 'search_guardian':
            kwargs.update({
                'protected': self.person,
                'other_guardian': self.person.legal_guardian1 if self.n == 2 else self.person.legal_guardian2,
            })
        return kwargs

    def done(self, form_list_, form_dict, **kwargs):
        with transaction.atomic():
            # Save guardian
            if 'guardian' in form_dict:
                guardian = form_dict['guardian'].save(commit=False)
                guardian.has_protected = True
                guardian.expired = False
                guardian.no_sync = True
                guardian.save()
            else:
                guardian = None
            # Save person
            old_guardian = getattr(self.person, f'legal_guardian{self.n}')
            setattr(self.person, f'legal_guardian{self.n}', guardian)
            self.person.no_sync = True
            self.person.save()
            # Save old guardian
            if old_guardian and not old_guardian.protecteds.exists():
                old_guardian.has_protected = False
                old_guardian.save()
        if guardian:
            force_publish_save(guardian)
        force_publish_save(self.person)
        return HttpResponseRedirect(self.get_success_url())


class GuardianUpdateView(PermissionRequiredMixin, BackSuccessUrl, UpdateView):
    permission_required = 'members.change_person_guardian'

    form_class = GuardianForm
    template_name = 'members/guardian_form.html'

    def get_object(self):
        self.person = get_object_or_404(Person, pk=self.kwargs['pk'])
        if not self.request.user.has_perm('members.change_person_guardian', self.person):
            return self.handle_no_permission()
        return getattr(self.person, f'legal_guardian{self.kwargs["n"]}')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['n'] = self.kwargs['n']
        context['person'] = self.person
        today_ = today()
        birthdate = replace_year(today_, today_.year - 18)
        context['protecteds'] = Person.objects.filter(
            Q(legal_guardian1=self.object) | Q(legal_guardian2=self.object),
            birthdate__gt=birthdate
        ).exclude(pk=self.person.pk).order_by('last_name', 'first_name')
        context['payments'] = Payment.objects.filter(
            payer=self.object,
            online_status__in=(PaymentStatus.MANUAL, PaymentStatus.SUCCESS),
        ).exists()
        return context


class GuardianDeleteView(PermissionRequiredMixin, BackSuccessUrl, DeleteView):
    permission_required = 'members.delete_person_guardian'
    template_name = 'members/guardian_confirm_delete.html'
    context_object_name = 'guardian'

    def get_object(self):
        self.n = self.kwargs['n']
        self.person = get_object_or_404(Person, pk=self.kwargs['pk'])
        if not self.request.user.has_perm('members.delete_person_guardian', self.person):
            return self.handle_no_permission()
        if self.kwargs["n"] == 1 and not self.person.legal_guardian2:
            raise PermissionDenied("Au moins un représentant légal est requis.")
        return getattr(self.person, f'legal_guardian{self.n}')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['n'] = self.n
        context['person'] = self.person
        return context

    def form_valid(self, form):
        self.object = self.get_object()
        old_guardian = getattr(self.person, f'legal_guardian{self.n}')
        if self.n == 1:
            self.person.legal_guardian1 = self.person.legal_guardian2
        self.person.legal_guardian2 = None
        self.person.save()
        if not old_guardian.protecteds.exists():
            old_guardian.has_protected = False
            old_guardian.save()
        return HttpResponseRedirect(self.get_success_url())


class AdhesionCardView(DetailView):
    model = Adhesion

    def render_to_response(self, context, **response_kwargs):
        token = self.request.GET.get('token')
        if token:
            if token != self.object.token:
                return HttpResponseForbidden("Le lien n'est pas valide.")
        else:
            if not self.request.user.has_perm('members.download_adhesion_card', self.object):
                return HttpResponseForbidden("Tu n'es pas autorisé·e à télécharger cette carte d'adhésion.")
        if self.object.canceled:
            return HttpResponseForbidden("Cette adhésion a été annulée.")
        if self.object.adherent.status != AdherentStatus.OK:
            return HttpResponseForbidden("Cette adhésion est actuellement bloquée.")
        response = HttpResponse(content_type='application/pdf')
        filename = f'carte-adhesion-{self.object.season}-{self.object.adherent.id_str}.pdf'
        response['Content-Disposition'] = f'attachment; filename={filename}'
        generate_card(self.object, response)
        return response


class AdhesionCertificateView(DetailView):
    model = Adhesion

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['adhesions'] = self.object.adherent.adhesions.filter(
            season__lte=self.object.season,
        ).order_by('-season')
        return context

    def render_to_response(self, context, **response_kwargs):
        token = self.request.GET.get('token')
        if token:
            if token != self.object.token:
                return HttpResponseForbidden("Le lien n'est pas valide.")
        else:
            if not self.request.user.has_perm('members.download_adhesion_card', self.object):
                return HttpResponseForbidden("Tu n'es pas autorisé·e à télécharger cette attestation d'adhésion.")
        if self.object.canceled:
            return HttpResponseForbidden("Cette adhésion a été annulée.")
        if self.object.adherent.status != AdherentStatus.OK:
            return HttpResponseForbidden("Cette adhésion est actuellement bloquée.")
        template = get_template('members/adhesion_certificate.html')
        markup = template.render(context)
        filename = f'attestation-adhesion-{self.object.season}-{self.object.adherent.id_str}.pdf'
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = f'attachment; filename={filename}'
        HTML(string=markup, base_url=settings.BASE_DIR).write_pdf(response)
        return response


class PersonDeduplicateView(PermissionRequiredMixin, WizardCacheMixin, SessionWizardView):
    permission_required = 'members.merge_person'
    template_name = 'members/person_deduplicate.html'
    form_list = (
        ('choice', PersonDeduplicateForm),
        ('merge', PersonMergeForm),
    )

    def dispatch(self, request, pk, *args, **kwargs):
        self.remove = get_object_or_404(
            Person,
            pk=pk,
        )
        return super().dispatch(request, *args, **kwargs)

    # Prevent collisions when two wizards are opened in parallel
    def get_prefix(self, request):
        return f'deduplicate{self.remove.pk}'

    def get_context_data(self, form, **kwargs):
        context = super().get_context_data(form=form, **kwargs)
        context['remove'] = self.remove
        context['step_title'] = {
            'choice': "Choix de la personne à conserver",
            'merge': "Fusion des données",
        }[self.steps.current]
        if self.steps.current == 'merge':
            choice_data = self.get_cleaned_data_for_step('choice')
            keep = choice_data['keep']
            keep_seasons = keep.adhesions.filter(canceled=False).values_list('season', flat=True)
            duplicate_adhesions = self.remove.adhesions.filter(canceled=False, season__in=keep_seasons)
            context['duplicate_adhesions'] = duplicate_adhesions
        return context

    def get_form_kwargs(self, step):
        if step == 'choice':
            return {
                'remove': self.remove,
            }
        elif step == 'merge':
            choice_data = self.get_cleaned_data_for_step('choice')
            return {
                'person1': self.remove,
                'person2': choice_data and choice_data['keep'],
            }

    def get_initial(self, step):
        if step == 'choice':
            keep = Person.objects.filter(
                normalized_last_name=normalize_name(self.remove.last_name),
                normalized_first_name=normalize_name(self.remove.first_name),
                birthdate=self.remove.birthdate,
            ).exclude(id=self.remove.id).last()
            return {
                'keep': keep,
            }

    def set_duplicate_of(self, remove, keep):
        with transaction.atomic():
            # Adherent
            try:
                remove_adherent = remove.adherent
            except Adherent.DoesNotExist:
                remove_adherent = None
            try:
                keep_adherent = keep.adherent
            except Adherent.DoesNotExist:
                keep_adherent = None
            if remove_adherent and keep_adherent:
                adhesions = remove_adherent.adhesions.all()
                for adhesion in adhesions:
                    if not adhesion.canceled:
                        if keep_adherent.adhesions.filter(season=adhesion.season, canceled=False).exists():
                            adhesion.canceled = True
                    adhesion.adherent = keep_adherent
                    adhesion.no_sync = True
                    adhesion.save()
                    if not adhesion.canceled and (
                        not keep_adherent.last_adhesion or
                        keep_adherent.last_adhesion.season < adhesion.season
                    ):
                        keep_adherent.last_adhesion = adhesion
                keep_adherent.duplicates.append(remove_adherent.id)
                keep_adherent.merged_with.append(remove_adherent.uuid)
                keep_adherent.no_sync = True
                keep_adherent.save()
                remove_adherent.no_sync = True
                remove_adherent.delete()
            elif remove_adherent:
                remove_adherent.person = keep
                remove_adherent.no_sync = True
                remove_adherent.save()
            # Employee
            try:
                remove_employee = remove.employee
            except Employee.DoesNotExist:
                remove_employee = None
            try:
                keep_employee = keep.employee
            except Employee.DoesNotExist:
                keep_employee = None
            if remove_employee and keep_employee:
                employments = remove_employee.employments.all()
                for employment in employments:
                    assert not keep_employee.employments.filter(dates__overlap=employment.dates).exists()
                    employment.employee = keep_employee
                    employment.no_sync = True
                    employment.save()
                keep_employee.merged_with.append(remove_employee.uuid)
                keep_employee.no_sync = True
                keep_employee.save()
                remove_employee.no_sync = True
                remove_employee.delete()
            elif remove_employee:
                remove_employee.person = keep
                remove_employee.no_sync = True
                remove_employee.save()
            # Protecteds
            protecteds = []
            for protected in remove.protected1.all():
                protected.legal_guardian1 = keep
                protected.no_sync = True
                protecteds.append(protected)
                protected.save()
            for protected in remove.protected2.all():
                protected.legal_guardian2 = keep
                protected.no_sync = True
                protecteds.append(protected)
                protected.save()
            # Payments
            for payment in remove.payment_set.all():
                payment.payer = keep
                payment.save()
            # Functions
            functions = remove.functions.all()
            for function in functions:
                function.person = keep
                function.no_sync = True
                function.save()
            # Invitations
            for invitation in remove.invitation_set.all():
                if Invitation.objects.filter(person=keep, season=invitation.season).exists():
                    invitation.delete()
                else:
                    invitation.person = keep
                    invitation.save()
            for invitation in Invitation.objects.filter(host=remove):
                invitation.host = keep
                invitation.save()
            # User
            try:
                remove_user = remove.user
            except User.DoesNotExist:
                remove_user = None
            try:
                keep_user = keep.user
            except User.DoesNotExist:
                keep_user = None
            if not remove_user:
                pass
            elif not keep_user:
                remove_user.person = keep
                remove_user.save()
            elif remove_user.last_login and keep_user.last_login and remove_user.last_login > keep_user.last_login:
                remove_user.person = keep
                remove_user.save()
                keep_user.delete()
            else:
                remove_user.delete()
            # Roles
            deleted_roles = []
            roles = []
            for role in Role.objects.filter(holder=remove):
                role.no_sync = True
                if Role.objects.exclude(pk=role.pk).filter(
                    config=role.config,
                    team=role.team,
                    holder=remove,
                    proxy=role.proxy,
                ).exists():
                    deleted_roles.append(role)
                    role.delete()
                else:
                    roles.append(role)
                    role.holder = keep
                    role.save()
            for role in Role.objects.filter(proxy=remove):
                role.no_sync = True
                if Role.objects.exclude(pk=role.pk).filter(
                    config=role.config,
                    team=role.team,
                    holder=role.holder,
                    proxy=remove,
                ).exists():
                    deleted_roles.append(role)
                    role.delete()
                else:
                    roles.append(role)
                    role.proxy = keep
                    role.save()
        if remove_adherent and keep_adherent:
            for adhesion in adhesions:
                force_publish_save(adhesion)
            force_publish_save(keep_adherent)
            force_publish_delete(remove_adherent)
        elif remove_adherent:
            force_publish_save(remove_adherent)
        if remove_employee and keep_employee:
            for employment in employments:
                force_publish_save(employment)
            force_publish_save(keep_employee)
            force_publish_delete(remove_employee)
        elif remove_employee:
            force_publish_save(remove_employee)
        for protected in protecteds:
            force_publish_save(protected)
        for function in functions:
            force_publish_save(function)
        for role in deleted_roles:
            force_publish_delete(deleted_roles)
        for role in roles:
            force_publish_save(role)

    @transaction.atomic()
    def done(self, form_list_, form_dict, **kwargs):
        keep = form_dict['choice'].cleaned_data['keep']
        remove = self.remove
        for fieldname, value in form_dict['merge'].cleaned_data.items():
            if value == '1':
                if fieldname.startswith('adherent_'):
                    setattr(keep.adherent, fieldname[9:], getattr(remove.adherent, fieldname[9:]))
                elif fieldname.startswith('employee_'):
                    setattr(keep.employee, fieldname[9:], getattr(remove.employee, fieldname[9:]))
                else:
                    setattr(keep, fieldname, getattr(remove, fieldname))
        if remove.email_as_login and keep.email == remove.email:
            remove.email_as_login = False
            remove.no_sync = True
            remove.save()
            keep.email_as_login = True
        assert not keep.last_adhesion or keep.last_adhesion.season < 2021 or keep.image_rights is not None
        self.set_duplicate_of(remove, keep)
        keep.update_has_protected(commit=False)
        keep.has_payment = keep.payment_set.exists()
        season = current_season()
        if Invitation.objects.filter(person=keep, season=season, declined=False).exists() and \
                not Adhesion.objects.filter(adherent__person=keep, season=season).exists():
            keep.last_invitation = season
        keep.merged_with.append(remove.uuid)
        remove.no_sync = True
        remove.delete()
        keep.save()
        force_publish_delete(remove)
        return HttpResponseRedirect(reverse('person_detail', args=[keep.pk]))


class RateListView(ListView):
    def get_queryset(self):
        return Rate.objects.filter(season=current_season())


class AdhesionPreCreateView(PermissionRequiredMixin, FormView):
    permission_required = 'members.add_adhesion'
    form_class = AdhesionPreCreateForm
    template_name = 'members/adhesion_pre_create.html'

    def form_valid(self, form):
        id = form.cleaned_data['id']
        last_name = form.cleaned_data['last_name']
        first_name = form.cleaned_data['first_name']
        if id:
            person = Person.objects.get(adherent__id=id)
        else:
            person = None
        if person:
            adhesion = person.last_adhesion
            if adhesion and adhesion.season == current_season():
                success_url = reverse('person_ok', kwargs={'pk': person.pk})
            else:
                success_url = reverse('adhesion_confirm_renew', kwargs={'pk': person.pk})
        else:
            base_url = reverse('adhesion_confirm_create')
            params = urlencode({'last_name': last_name, 'first_name': first_name})
            success_url = f'{base_url}?{params}'
        return HttpResponseRedirect(success_url)


# FIXME: permissions
class AdhesionConfirmRenewView(PermissionRequiredMixin, DetailView):
    permission_required = 'members.add_adhesion'
    model = Person
    template_name = 'members/adhesion_confirm_renew.html'

    def post(self, request, pk):
        return HttpResponseRedirect(reverse('adhesion_renew', kwargs={'person_pk': pk}))

    def get_context_data(self, **kwargs):
        adhesion = self.object.last_adhesion
        context = super().get_context_data(**kwargs)
        context['adhesion'] = adhesion
        try:
            context['blocked'] = self.object.adherent.is_blocked()
        except Adherent.DoesNotExist:
            context['blocked'] = False
        return context


# FIXME: permissions
class AdhesionConfirmCreateView(PermissionRequiredMixin, TemplateView):
    permission_required = 'members.add_adhesion'
    model = Person
    template_name = 'members/adhesion_confirm_create.html'

    def post(self, request):
        url = reverse('adhesion_create')
        url += '?' + request.META['QUERY_STRING']
        return HttpResponseRedirect(url)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        last_name = self.request.GET.get('last_name')
        first_name = self.request.GET.get('first_name')
        if not last_name:
            raise BadRequest("Missing last_name parameter")
        if not first_name:
            raise BadRequest("Missing first_name parameter")
        tokens_last = tokenize_name(last_name)
        tokens_first = tokenize_name(first_name)
        qs = Person.objects.filter(
            reduce(or_, [Q(tokens__token__trigram_similar=token) for token in tokens_last]),
            tokens__part=PersonTokenPart.LAST,
        ).filter(
            reduce(or_, [Q(tokens__token__trigram_similar=token) for token in tokens_first]),
            tokens__part=PersonTokenPart.FIRST,
        ).values('pk')
        context['persons'] = Person.objects.filter(pk__in=Subquery(qs)).annotate(
            similarity_last=sum([
                Avg(TrigramSimilarity('tokens__token', token)) for token in tokens_last
            ]) / len(tokens_last),
            similarity_first=sum([
                Avg(TrigramSimilarity('tokens__token', token)) for token in tokens_first
            ]) / len(tokens_first),
            similarity=F('similarity_last') + F('similarity_first'),
        ).order_by('-similarity')
        context['last_name'] = last_name
        context['first_name'] = first_name
        return context


class PaymentCreateView(LoginRequiredMixin, BackSuccessUrl, WizardCacheMixin, SessionWizardView):
    template_name = 'members/payment_wizard.html'
    form_list = (
        ('search_payer', SearchPayerForm),
        ('payer', PayerForm),
        ('payment', PaymentForm),
    )
    model = Payment

    def dispatch(self, request, adhesion_pk):
        self.adhesion = get_object_or_404(Adhesion, pk=adhesion_pk)
        if self.adhesion.season != current_season():
            if not request.user.has_perm('members.add_old_payment', self.adhesion.structure):
                raise PermissionDenied(
                    "Merci de contacter un·e CAFT ou le secrétariat national "
                    "pour ajouter un paiement pour une saison terminée."
                )
        if not request.user.has_perm('members.add_payment', self.adhesion.structure):
            return self.handle_no_permission()
        self.person = self.adhesion.adherent.person
        return super().dispatch(request)

    def payer_condition(self):
        search_payer_data = self.get_cleaned_data_for_step('search_payer')
        return not search_payer_data or search_payer_data.get('mode') in ('New', 'Sel')

    def payment_condition(self):
        search_payer_data = self.get_cleaned_data_for_step('search_payer')
        return not search_payer_data or search_payer_data.get('mode') != 'EEDF'

    condition_dict = {
        'payer': payer_condition,
        'payment': payment_condition,
    }

    def get_context_data(self, form, **kwargs):
        context = super().get_context_data(form=form, **kwargs)
        context['step_title'] = {
            'search_payer': "Le payeur",
            'payer': "Le payeur",
            'payment': "Le paiement",
        }[self.steps.current]
        context['adhesion'] = self.adhesion
        context['person'] = self.person
        return context

    def get_form_instance(self, step):
        if step == 'payer':
            cleaned_data = self.get_cleaned_data_for_step('search_payer')
            if cleaned_data and cleaned_data['mode'] == 'Sel':
                return cleaned_data['person']
            else:
                return None

    def get_form_initial(self, step):
        # payment
        if step == 'payment':
            return {
                'amount': max(self.adhesion.balance, 0),
                'date': self.adhesion.begin,
            }

        # others
        return None

    def get_form_kwargs(self, step):
        kwargs = {
            'user': self.request.user,
        }
        if step == 'search_payer':
            kwargs.update({
                'eedf': True,
                'person': {
                    'first_name': self.person.first_name,
                    'last_name': self.person.last_name
                },
                'guardian1': {
                    'first_name': self.person.legal_guardian1.first_name,
                    'last_name': self.person.legal_guardian1.last_name
                } if self.person.legal_guardian1 else None,
                'guardian2': {
                    'first_name': self.person.legal_guardian2.first_name,
                    'last_name': self.person.legal_guardian2.last_name
                } if self.person.legal_guardian2 else None,
                'rate': self.adhesion.rate,
                'donation': self.adhesion.donation,
            })
        if step == 'payment':
            kwargs['season'] = self.adhesion.season
            kwargs['amount_max'] = max(self.adhesion.balance, 0)
        return kwargs

    def done(self, form_list_, form_dict, **kwargs):
        with transaction.atomic():
            # Save payer
            if 'payer' in form_dict:
                payer = form_dict['payer'].save(commit=False)
                payer.no_sync = True
                payer.save()
            else:
                payer = None
            # Save payment
            if 'payment' in form_dict:
                payment = form_dict['payment'].save(commit=False)
                assert payment.online_status == PaymentStatus.MANUAL
                payment.payer = {
                    'Sel': payer,
                    'New': payer,
                    'Adh': self.person,
                    'G1': self.person.legal_guardian1,
                    'G2': self.person.legal_guardian2,
                }[form_dict['search_payer'].cleaned_data['mode']]
                payment.save()
            elif 'search_payer' in form_dict and form_dict['search_payer'].cleaned_data['mode'] == 'EEDF':
                payment = Payment.objects.create(
                    payer=None,
                    method=5,
                    amount=self.adhesion.effective_rate - self.adhesion.paid,
                    date=self.adhesion.begin,
                    reference="",
                )
            else:
                payment = None
            if payment:
                payment.create_allocations(self.adhesion)
            # Save payer denormalization
            if payment.payer:
                payment.payer.has_payment = True
                payment.payer.expired = False
                payment.payer.save()
            # Log payment (after allocation creation to set persons in additional data)
            LogEntry.objects.log_create(
                payment,
                action=LogEntry.Action.CREATE,
                changes=model_instance_diff(None, payment),
                additional_data=payment.get_additional_data(),
            )
        if payer:
            force_publish_save(payer)
        if self.adhesion.is_paid:
            self.adhesion.send_email(self.request)
        messages.success(
            self.request,
            mark_safe("Le paiement a bien été enregistré."),
            extra_tags="alert-success"
        )
        return HttpResponseRedirect(self.get_success_url())


class PaymentUpdateView(LoginRequiredMixin, BackSuccessUrl, WizardCacheMixin, SessionWizardView):
    template_name = 'members/payment_wizard.html'
    form_list = (
        ('search_payer', SearchPayerForm),
        ('payer', PayerForm),
        ('payment', PaymentForm),
    )
    model = Payment

    def dispatch(self, request, pk):
        self.payment = get_object_or_404(Payment, pk=pk)
        adhesions = self.payment.allocation_set.values_list('adhesion', flat=True).distinct()
        assert len(adhesions) == 1, "Payment update for several adhesions. Not yet supported."
        self.adhesion = Adhesion.objects.get(pk=adhesions[0])
        if self.adhesion.season != current_season():
            if not request.user.has_perm('members.add_old_payment', self.adhesion.structure):
                raise PermissionDenied(
                    "Merci de contacter un·e CAFT ou le secrétariat national "
                    "pour ajouter un paiement pour une saison terminée."
                )
        if not request.user.has_perm('members.change_payment', self.payment):
            return self.handle_no_permission()
        self.person = self.adhesion.adherent.person
        return super().dispatch(request)

    def payer_condition(self):
        search_payer_data = self.get_cleaned_data_for_step('search_payer')
        return not search_payer_data or search_payer_data.get('mode') != 'EEDF'

    def payment_condition(self):
        if self.payment.online_status != PaymentStatus.MANUAL:
            return False
        search_payer_data = self.get_cleaned_data_for_step('search_payer')
        return not search_payer_data or search_payer_data.get('mode') != 'EEDF'

    condition_dict = {
        'payer': payer_condition,
        'payment': payment_condition,
    }

    def get_context_data(self, form, **kwargs):
        context = super().get_context_data(form=form, **kwargs)
        context['step_title'] = {
            'search_payer': "Le payeur",
            'payer': "Le payeur",
            'payment': "Le paiement",
        }[self.steps.current]
        context['payment'] = self.payment
        context['adhesion'] = self.adhesion
        context['person'] = self.person
        return context

    def get_form_instance(self, step):
        # payer
        if step == 'payer':
            cleaned_data = self.get_cleaned_data_for_step('search_payer')
            if not cleaned_data:
                return None
            return {
                'Sel': cleaned_data['person'],
                'New': None,
                'Adh': self.person,
                'G1': self.person.legal_guardian1,
                'G2': self.person.legal_guardian2,
            }[cleaned_data['mode']]

        # payment
        if step == 'payment':
            return self.payment

    def get_form_initial(self, step):
        # payment
        person = self.adhesion.adherent.person
        if not self.payment.payer:
            mode = 'EEDF'
        elif self.payment.payer == person:
            mode = 'Adh'
        elif self.payment.payer == person.legal_guardian1:
            mode = 'G1'
        elif self.payment.payer == person.legal_guardian2:
            mode = 'G2'
        else:
            mode = 'Sel'
        if step == 'search_payer':
            return {
                'mode': mode,
                'person': self.payment.payer if mode == 'Sel' else None,
            }

        # others
        return None

    def get_form_kwargs(self, step):
        kwargs = {
            'user': self.request.user,
        }
        if step == 'search_payer':
            kwargs.update({
                'eedf': self.payment.online_status == PaymentStatus.MANUAL,
                'person': {
                    'first_name': self.person.first_name,
                    'last_name': self.person.last_name
                },
                'guardian1': {
                    'first_name': self.person.legal_guardian1.first_name,
                    'last_name': self.person.legal_guardian1.last_name
                } if self.person.legal_guardian1 else None,
                'guardian2': {
                    'first_name': self.person.legal_guardian2.first_name,
                    'last_name': self.person.legal_guardian2.last_name
                } if self.person.legal_guardian2 else None,
                'rate': self.adhesion.rate,
                'donation': self.adhesion.donation,
            })
        if step == 'payment':
            kwargs['season'] = self.adhesion.season
            kwargs['amount_max'] = max(self.adhesion.balance, 0) + self.payment.amount
        return kwargs

    def done(self, form_list_, form_dict, **kwargs):
        old_is_paid = self.adhesion.is_paid
        with transaction.atomic():
            # Cancel receipt
            try:
                receipt = self.payment.receipt
            except Receipt.DoesNotExist:
                pass
            else:
                receipt.cancel()
            # Save payer
            if 'payer' in form_dict:
                payer = form_dict['payer'].save(commit=False)
                payer.no_sync = True
                payer.save()
            else:
                payer = None
            # Save payment
            if 'payment' in form_dict:
                payment = form_dict['payment'].save(commit=False)
                assert payment.online_status == PaymentStatus.MANUAL
            else:
                payment = self.payment
            if form_dict['search_payer'].cleaned_data['mode'] == 'EEDF':
                payment.method = 5
                payment.amount = self.adhesion.effective_rate
                payment.date = self.adhesion.begin
                payment.reference = ""
            payment.payer = {
                'Sel': payer,
                'New': payer,
                'Adh': self.person,
                'G1': self.person.legal_guardian1,
                'G2': self.person.legal_guardian2,
                'EEDF': None,
            }[form_dict['search_payer'].cleaned_data['mode']]
            old_payment = Payment.objects.get(id=payment.id)
            payment.save()
            payment.allocation_set.all().delete()
            payment.create_allocations(self.adhesion)
            # Save payer denormalization
            if payment.payer:
                payment.payer.has_payment = True
                payment.payer.expired = False
                payment.payer.save()
            # Save old payer denormalization
            if old_payment.payer and not old_payment.payer.payment_set.exists():
                old_payment.payer.has_payment = False
                old_payment.payer.save()
            # Log payment (after allocation creation to set persons in additional data)
            LogEntry.objects.log_create(
                payment,
                action=LogEntry.Action.UPDATE,
                changes=model_instance_diff(old_payment, payment),
                additional_data=payment.get_additional_data(),
            )
        if payer:
            force_publish_save(payer)
        if not old_is_paid and self.adhesion.is_paid:
            self.adhesion.send_email(self.request)
        messages.success(
            self.request,
            mark_safe("Le paiement a bien été modifié."),
            extra_tags="alert-success"
        )
        return HttpResponseRedirect(self.get_success_url())


class AdhesionWizardView(AccessMixin, BackSuccessUrl, WizardCacheMixin, SessionWizardView):
    template_name = 'members/adhesion_wizard.html'
    form_list = (
        ('pre_confirm', PreConfirmForm),
        ('person', PersonForm),
        ('search_guardian1', SearchGuardianForm),
        ('guardian1', GuardianForm),
        ('search_guardian2', SearchGuardianForm),
        ('guardian2', GuardianForm),
        ('permission', PermissionForm),
        ('adhesion', AdhesionForm),
        ('function', AdhesionFunctionForm),
        ('contribution', ContributionForm),
        ('search_payer', SearchPayerForm),
        ('payer', PayerForm),
        ('payment', PaymentForm),
        ('post_confirm', PostConfirmForm),
    )
    permission_denied_message = "Tu n'es pas autorisé à gérer les adhésions"
    model = Adhesion

    def dispatch(self, request, person_pk=None, invitation_token=None):
        """
        3 modes:
        - without person and adhesion pk : create a new person
        - with person pk : create a new adhesion for an existing person
        - with invitation token : online registration
        """

        # Check access
        if not invitation_token:
            if not request.user.has_perm('members.add_adhesion'):
                return self.handle_no_permission()

        # person
        if person_pk:
            self.person = get_object_or_404(
                Person,
                Q(adherent=None) | Q(adherent__status__in=(AdherentStatus.OK, AdherentStatus.LEFT)),
                pk=person_pk,
            )
            self.invitation = None
        elif invitation_token:
            self.invitation = get_object_or_404(Invitation, token=invitation_token, season=current_season())
            if self.invitation.adhesion and not self.invitation.adhesion.is_paid:
                return HttpResponseRedirect(reverse('adhesion_pre_pay', args=[self.invitation.token]))
            invitation_pks = set(self.request.session.get('invitations', []))
            invitation_pks.add(self.invitation.pk)
            self.request.session['invitations'] = list(invitation_pks)
            self.invitations = Invitation.objects.filter(pk__in=invitation_pks, season=current_season())
            if self.invitation.adhesion:
                self.person = self.invitation.adhesion.adherent.person
            else:
                self.person = self.invitation.person
        else:
            self.invitation = None
            self.person = None
        self.transform = False
        if self.person:
            last_adhesion = self.person.last_adhesion
            if last_adhesion and last_adhesion.season == current_season():
                if last_adhesion.rate.category != RateCategory.DISCOVERY or \
                        self.invitation and self.invitation.rate:
                    return HttpResponseRedirect(reverse('person_ok', args=[self.person.pk]))
                self.transform = True

        # adherent
        if self.person:
            try:
                self.adherent = self.person.adherent
            except Adherent.DoesNotExist:
                self.adherent = None
        else:
            self.adherent = None

        # guardians
        if self.person:
            self.guardian1 = self.person.legal_guardian1
            self.guardian2 = self.person.legal_guardian2
        else:
            self.guardian1 = None
            self.guardian2 = None

        # search persons
        if self.invitation:
            # Invited persons
            pks = set(invitation.person.pk for invitation in self.invitations if invitation.person)
            pks |= set(invitation.adhesion.adherent.person.pk for invitation in self.invitations if invitation.adhesion)
            # Invited emails (persons with these adresses as personal email)
            emails = [invitation.email for invitation in self.invitations if invitation.email != 'anonymise@eedf.fr']
            pks |= set(Person.objects.filter(email__in=emails).values_list('pk', flat=True))
            if request.user.is_authenticated and request.user.person:
                pks.add(request.user.person.pk)
            self.search_persons = Person.objects.filter(
                # Person
                Q(pk__in=pks) |
                # Legal guardians
                Q(protected1__in=pks) |
                Q(protected2__in=pks) |
                # Other parent
                Q(protected1__legal_guardian1__in=pks) |
                Q(protected1__legal_guardian2__in=pks) |
                Q(protected2__legal_guardian1__in=pks) |
                Q(protected2__legal_guardian2__in=pks) |
                # Payers
                Q(payment__allocation__adhesion__adherent__person__in=pks)
            )
            if self.invitation.person:
                self.search_persons = self.search_persons.exclude(pk=self.invitation.person.pk)
            self.search_persons = self.search_persons.distinct().order_by('last_name', 'first_name')
        else:
            self.search_persons = None

        return super().dispatch(request, person_pk=person_pk)

    # Prevent collisions when two wizards are opened in parallel
    def get_prefix(self, request, person_pk):
        if person_pk:
            return f'renew{person_pk}'
        hash = md5(json.dumps(request.GET).encode('utf8')).hexdigest()
        return f'create-{hash}'

    def person_condition(self):
        return not self.transform

    def person_is_minor(self):
        cleaned_data = self.get_cleaned_data_for_step('person')
        if cleaned_data:
            birthdate = cleaned_data.get('birthdate')
        elif self.person:
            birthdate = self.person.birthdate
        else:
            try:
                birthdate = date.fromisoformat(self.request.GET.get('birthdate', ''))
            except ValueError:
                birthdate = None
        if not birthdate:
            return True
        return age(birthdate) < 18

    def guardian_condition(self):
        return not self.transform and self.person_is_minor()

    def confirm_condition(self):
        return self.invitation

    def guardian2_condition(self):
        if not self.guardian_condition():
            return False
        cleaned_data = self.get_cleaned_data_for_step('search_guardian2')
        return not cleaned_data or cleaned_data.get('mode') != 'N/A'

    def permission_condition(self):
        return self.guardian_condition()

    def adhesion_condition(self):
        return not self.invitation

    def function_condition(self):
        return not self.invitation

    def search_payer_condition(self):
        if self.invitation:
            return False
        contribution_data = self.get_cleaned_data_for_step('contribution')
        rate = contribution_data and contribution_data.get('rate')
        if rate and rate.rate_max:
            effective_rate = contribution_data and contribution_data.get('free_rate')
        else:
            effective_rate = rate and rate.rate
        donation = contribution_data and contribution_data.get('donation')
        return effective_rate is None or donation is None or effective_rate + donation != 0

    def payer_condition(self):
        if not self.search_payer_condition():
            return False
        search_payer_data = self.get_cleaned_data_for_step('search_payer')
        return not search_payer_data or search_payer_data.get('mode') in ('New', 'Sel')

    def payment_condition(self):
        if not self.search_payer_condition():
            return False
        search_payer_data = self.get_cleaned_data_for_step('search_payer')
        return not search_payer_data or search_payer_data.get('mode') != 'EEDF'

    condition_dict = {
        'pre_confirm': confirm_condition,
        'person': person_condition,
        'search_guardian1': guardian_condition,
        'guardian1': guardian_condition,
        'search_guardian2': guardian_condition,
        'guardian2': guardian2_condition,
        'permission': permission_condition,
        'adhesion': adhesion_condition,
        'function': function_condition,
        'search_payer': search_payer_condition,
        'payer': payer_condition,
        'payment': payment_condition,
        'post_confirm': confirm_condition,
    }

    def recap_person(self, data):
        nom = f"Nom : {data['first_name']}"
        if data['family_name']:
            nom += f" ({data['family_name']})"
        nom += f" {data['last_name']}"
        if data['nickname']:
            nom += f" ({data['nickname']})"
        recap = [
            nom,
            f"Genre : {dict(Person.GENDER_CHOICES)[data['gender']]}",
        ]
        if data['birthdate']:
            if not data['birth_country']:
                birth_place_display = data['birth_place']
            elif data['birth_country'].id == 99100:
                birth_place_display = f"{data['birth_commune']}, France"
            else:
                birth_place_display = f"{data['birth_place']}, {data['birth_country']}"
            recap.append(f"Naissance : le {data['birthdate'].strftime('%d/%m/%Y')} à {birth_place_display}")
        address = f"Adresse : {data['address1']}"
        if data['address2']:
            address += f" - {data['address2']}"
        if data['address3']:
            address += f" - {data['address3']}"
        address += f" - {data['postal_code']} {data['city']}"
        if data['country']:
            address += f" - {data['country']}"
        recap.append(address)
        contacts = []
        if data['email']:
            contacts.append(f"{data['email']} (email)")
        if data['home_phone']:
            contacts.append(f"{data['home_phone']} (tél. fixe)")
        if data['mobile_phone']:
            contacts.append(f"{data['mobile_phone']} (tél. mobile)")
        if contacts:
            recap.append("Contacts : " + ", ".join(contacts))
        if data['profession']:
            recap.append(f"Profession : {data['profession']}")
        if 'image_rights' in data:
            recap.append(f"Droit à l'image : {'oui' if data['image_rights'] else 'non'}")
        return recap

    def get_context_data(self, form, **kwargs):
        context = super().get_context_data(form=form, **kwargs)
        context['step_title'] = {
            'pre_confirm': "Validation de l'invitation",
            'person': "L'adhérent·e",
            'search_guardian1': "Le ou la responsable légal·e 1",
            'guardian1': "Le ou la responsable légal·e 1",
            'search_guardian2': "Le ou la responsable légal·e 2",
            'guardian2': "Le ou la responsable légal·e 2",
            'permission': "L'autorisation légale",
            'adhesion': "L'adhésion",
            'function': "La fonction",
            'contribution': "La cotisation",
            'search_payer': "Le ou la payeur·se",
            'payer': "Le ou la payeur·se",
            'payment': "Le paiement",
            'post_confirm': "Confirmation finale",
        }[self.steps.current]
        context['person'] = self.person
        context['invitation'] = self.invitation
        context['transform'] = self.transform
        if self.steps.current == 'pre_confirm':
            context['paper_form'] = Season.objects.get(year=current_season()).form
        if self.steps.current == 'post_confirm':
            recap = ["<b>L'adhérent·e</b>"]
            if self.transform:
                recap.append(f"Nom : {self.person.full_name}")
            data = self.get_cleaned_data_for_step('person')
            if data:
                recap += self.recap_person(data)
            if self.guardian_condition():
                data = self.get_cleaned_data_for_step('guardian1')
                recap.append("<b>Le ou la responsable légal·e 1</b>")
                recap += self.recap_person(data)
            if self.guardian2_condition():
                data = self.get_cleaned_data_for_step('guardian2')
                recap.append("<b>Le ou la responsable légal·e 2</b>")
                recap += self.recap_person(data)
            recap.append("<b>L'adhésion</b>")
            data = self.get_cleaned_data_for_step('contribution')
            structure = self.invitation.structure
            if data['rate'].rate_max:
                effective_rate = data['free_rate']
            else:
                effective_rate = data['rate'].rate
            recap += [
                f"En tant que {self.invitation.functions_display} sur la structure locale de {structure}",
                f"Au tarif {data['rate'].name} de {effective_rate} €" +
                (f" + un don de {data['donation']} € à la structure locale {structure}" if data['donation'] else ""),
            ]
            if self.permission_condition():
                data = self.get_cleaned_data_for_step('permission')
                recap.append(
                    "J'autorise l'enfant à rentrer seul·e à son domicile : " +
                    ("oui" if data.get('permission_alone') else "non")
                )
                if data.get('permission_with'):
                    recap.append(
                        "J'autorise l'enfant à rentrer accompagné·e par : "
                        f"{data['permission_with']}"
                    )
            context['recap'] = "<br>".join(recap)
        return context

    def get_form_instance(self, step):
        if step == 'person':
            return self.person
        if step == 'permission':
            return self.person
        if step == 'guardian1':
            cleaned_data = self.get_cleaned_data_for_step('search_guardian1')
            if cleaned_data and cleaned_data['mode'] == 'Sel':
                return cleaned_data['person']
            else:
                return None
        if step == 'guardian2':
            cleaned_data = self.get_cleaned_data_for_step('search_guardian2')
            if cleaned_data and cleaned_data['mode'] == 'Sel':
                return cleaned_data['person']
            else:
                return None
        if step == 'payer':
            cleaned_data = self.get_cleaned_data_for_step('search_payer')
            if cleaned_data and cleaned_data['mode'] == 'Sel':
                return cleaned_data['person']
            else:
                return None

    def get_form_initial(self, step):
        # person
        if step == 'person':
            if self.person:
                return None
            if self.invitation:
                pre_confirm_data = self.get_cleaned_data_for_step('pre_confirm')
                if not pre_confirm_data:
                    return {}
                return {
                    'last_name': pre_confirm_data['last_name'],
                    'first_name': pre_confirm_data['first_name'],
                    'birthdate': pre_confirm_data['birthdate'],
                }
            try:
                birthdate = date.fromisoformat(self.request.GET.get('birthdate', ''))
            except ValueError:
                birthdate = None
            return {
                'last_name': self.request.GET.get('last_name'),
                'first_name': self.request.GET.get('first_name'),
                'birthdate': birthdate,
            }

        # search_guardian1
        if step == 'search_guardian1' and self.person and self.person.legal_guardian1:
            if self.request.user.has_perm('members.add_adhesion'):
                return {
                    'person': self.person.legal_guardian1,
                    'mode': 'Sel',
                }
            else:
                return {
                    'mode': str(self.person.legal_guardian1.pk),
                }

        # guardian1
        if step == 'guardian1':
            search_guardian1_data = self.get_cleaned_data_for_step('search_guardian1')
            if search_guardian1_data and search_guardian1_data.get('mode') == 'New':
                person_data = self.get_cleaned_data_for_step('person')
                return {
                    'last_name': person_data and person_data['last_name'],
                }

        # search_guardian2
        if step == 'search_guardian2':
            if self.person and self.person.legal_guardian2:
                if self.request.user.has_perm('members.add_adhesion'):
                    return {
                        'person': self.person.legal_guardian2,
                        'mode': 'Sel',
                    }
                else:
                    return {
                        'mode': str(self.person.legal_guardian2.pk),
                    }
            else:
                return {
                    'mode': 'N/A',
                }

        # guardian2
        if step == 'guardian2':
            search_guardian2_data = self.get_cleaned_data_for_step('search_guardian2')
            if search_guardian2_data and search_guardian2_data.get('mode') == 'New':
                person_data = self.get_cleaned_data_for_step('person')
                return {
                    'last_name': person_data and person_data['last_name'],
                }

        # adhesion
        if step == 'adhesion':
            last_adhesion = self.person and self.person.last_adhesion
            if last_adhesion and last_adhesion.structure.is_active:
                structure = last_adhesion.structure
            elif self.request.user.is_authenticated:
                structure = self.request.user.structure
            else:
                structure = None
            initial = {
                'begin': self.person.last_adhesion.begin if self.transform else now(),
                'structure': structure,
            }
            return initial

        # function
        if step == 'function' and self.person:
            team, function_config = self.person.guess_main_function()
            return {
                'team': team,
                'function_config': function_config,
            }

        # payment
        if step == 'payment':
            adhesion_data = self.get_cleaned_data_for_step('adhesion')
            contribution_data = self.get_cleaned_data_for_step('contribution')
            if contribution_data and contribution_data.get('rate'):
                if contribution_data['rate'].rate_max:
                    amount = contribution_data['free_rate'] + contribution_data['donation']
                else:
                    amount = contribution_data['rate'].rate + contribution_data['donation']
                if self.transform:
                    amount -= self.person.last_adhesion.effective_rate
            else:
                amount = None
            return {
                'amount': amount,
                'date': adhesion_data and min(adhesion_data['begin'], today()),
            }

        # others
        return None

    def get_form_kwargs(self, step):
        kwargs = {
            'user': self.request.user,
        }
        if step == 'pre_confirm':
            kwargs.update({
                'invitation': self.invitation,
                'force_age': self.request.POST.get('force_age') == 'on',
            })
        if step == 'person':
            kwargs.update({
                'invitation': self.invitation,
                'required_birth_place': True,
            })
        if step == 'search_guardian1':
            kwargs.update({
                'protected': self.person,
                'other_guardian': None,
                'persons': self.search_persons,
            })
        if step == 'search_guardian2':
            search_guardian1_data = self.get_cleaned_data_for_step('search_guardian1')
            if search_guardian1_data and search_guardian1_data['mode'] == 'Sel':
                other_guardian = search_guardian1_data['person']
            else:
                other_guardian = None
            kwargs.update({
                'protected': self.person,
                'other_guardian': other_guardian,
                'allow_empty': True,
                'persons': self.search_persons,
            })
        if step == 'function':
            person_data = self.get_cleaned_data_for_step('person')
            adhesion_data = self.get_cleaned_data_for_step('adhesion')
            structure = adhesion_data and adhesion_data['structure']
            if self.person and structure:
                current_functions = self.person.functions.filter(
                    team__structure=structure,
                ).exclude(
                    function_config__function_type__category=FunctionCategory.EMPLOYEE,
                ).current()
            else:
                current_functions = Function.objects.none()
            kwargs.update({
                'structure': structure,
                'current_functions': current_functions,
                'birthdate': person_data and person_data['birthdate'],
                'force_age': self.request.POST.get('force_age') == 'on',
            })
        if step == 'contribution':
            person_data = self.get_cleaned_data_for_step('person')
            kwargs.update({
                'invitation': self.invitation,
                'person': self.person,
                'birthdate': person_data and person_data['birthdate'],
                'force_age': self.request.POST.get('force_age') == 'on',
                'transform': self.transform,
            })
            if self.invitation:
                kwargs.update({
                    'adhesion_date': self.invitation.date,
                    'structure': self.invitation.structure,
                    'function_config': self.invitation.function_config,
                })
            else:
                adhesion_data = self.get_cleaned_data_for_step('adhesion')
                function_data = self.get_cleaned_data_for_step('function')
                kwargs.update({
                    'adhesion_date': adhesion_data and adhesion_data['begin'],
                    'structure': adhesion_data and adhesion_data['structure'],
                    'function_config': function_data and function_data['function_config'],
                })
        if step == 'search_payer':
            contribution_data = self.get_cleaned_data_for_step('contribution')
            kwargs.update({
                'persons': self.search_persons,
                'eedf': True,
                'person': {
                    'first_name': self.person.first_name,
                    'last_name': self.person.last_name,
                } if self.transform else self.get_cleaned_data_for_step('person'),
                'guardian1': {
                    'first_name': self.person.legal_guardian1.first_name,
                    'last_name': self.person.legal_guardian1.last_name,
                } if self.transform else self.get_cleaned_data_for_step('guardian1'),
                'guardian2': {
                    'first_name': self.person.legal_guardian2.first_name,
                    'last_name': self.person.legal_guardian2.last_name,
                } if self.transform and self.person.legal_guardian2 else self.get_cleaned_data_for_step('guardian2'),
                'rate': contribution_data and contribution_data['rate'],
                'donation': contribution_data and contribution_data['donation'],
            })
        if step == 'payment':
            contribution_data = self.get_cleaned_data_for_step('contribution')
            if contribution_data and contribution_data.get('rate'):
                if contribution_data['rate'].rate_max:
                    amount = contribution_data['free_rate'] + contribution_data['donation']
                else:
                    amount = contribution_data['rate'].rate + contribution_data['donation']
                if self.transform:
                    amount -= self.person.last_adhesion.effective_rate
            else:
                amount = None
            kwargs['amount_max'] = amount
            kwargs['season'] = current_season()
        return kwargs

    def save_payment(self, form_dict, person, guardian1, guardian2, adhesion):
        # Save payer
        if 'payer' in form_dict:
            payer = form_dict['payer'].save()
        else:
            payer = None
        # Save payment
        if 'payment' in form_dict:
            payment = form_dict['payment'].save(commit=False)
            assert payment.online_status == PaymentStatus.MANUAL
            payment.payer = {
                'Sel': payer,
                'New': payer,
                'Adh': person,
                'G1': guardian1,
                'G2': guardian2,
            }[form_dict['search_payer'].cleaned_data['mode']]
            payment.save()
        elif 'search_payer' in form_dict and form_dict['search_payer'].cleaned_data['mode'] == 'EEDF':
            payment = Payment.objects.create(
                payer=None,
                method=5,
                amount=adhesion.effective_rate,
                date=adhesion.begin,
                reference="",
            )
        else:
            payment = None
        # Save payer denormalisation
        if payment.payer:
            payment.payer.has_payment = True
            payment.payer.expired = False
            payment.payer.save()
        # Save allocations
        if payment:
            payment.create_allocations(adhesion)
        # Log payment (after allocation creation to set persons in additional data)
        LogEntry.objects.log_create(
            payment,
            action=LogEntry.Action.CREATE,
            changes=model_instance_diff(None, payment),
            additional_data=payment.get_additional_data(),
        )

    def save_transfer(self, adhesion):
        # Save entry
        amount = (
            adhesion.effective_rate -
            sum(adhesion.allocation_set.filter(
                payment__online_status=PaymentStatus.SUCCESS,
                category=Allocation.CATEGORY_CONTRIBUTION
            ).values_list('amount', flat=True), 0) -
            sum(adhesion.entry_set.filter(
                category=Entry.CATEGORY_CONTRIBUTION
            ).values_list('amount', flat=True), 0)
        )
        if amount:
            Entry.create(adhesion, amount, category=Entry.CATEGORY_CONTRIBUTION)

    def done(self, form_list_, form_dict, **kwargs):
        try:
            return self.atomic_done(form_dict)
        except IntegrityError as exception:
            # Check there is no duplicate (in case of double click for exemple)
            if Adhesion.objects.filter(
                adherent__person=form_dict['person'].instance.pk,
                season=current_season(),
                canceled=False
            ).exists():
                return HttpResponseRedirect(self.get_success_url())
            raise exception

    def atomic_done(self, form_dict):
        with transaction.atomic():
            if self.transform:
                guardian1 = None
                guardian2 = None
                adherent = None
                # Save person
                person = self.person
                person.last_invitation = None
                person.no_sync = True
                person.save()
                # Save adhesion
                adhesion = person.last_adhesion
                if self.invitation:
                    adhesion.structure = self.invitation.structure
                adhesion.rate = form_dict['contribution'].cleaned_data['rate']
                adhesion.set_dates(adhesion.begin)
                if adhesion.rate.rate_max:
                    adhesion.free_rate = form_dict['contribution'].cleaned_data['free_rate']
                adhesion.donation += form_dict['contribution'].cleaned_data['donation']
                adhesion.is_discovery = False
                adhesion.update_is_paid(commit=False)
                adhesion.no_sync = True
                adhesion.save()
                # Save function
                if self.invitation and self.invitation.function_config:
                    function = Function(
                        team=self.invitation.team,
                        function_config=self.invitation.function_config,
                        name=self.invitation.function_config.name,
                    )
                elif not self.invitation and form_dict['function'].cleaned_data['function_config']:
                    function = Function(
                        team=form_dict['function'].cleaned_data['team'],
                        function_config=form_dict['function'].cleaned_data['function_config'],
                        name=form_dict['function'].cleaned_data['function_config'].name,
                    )
                else:
                    function = None
                if function:
                    function.person = person
                    function.dates = adhesion.dates
                    function.no_sync = True
                    function.save()
                # Save payer / payment / entry
                if not self.invitation:
                    self.save_payment(form_dict, person, person.legal_guardian1, person.legal_guardian2, adhesion)
                self.save_transfer(adhesion)
            else:
                # Save guardians
                if 'guardian1' in form_dict:
                    guardian1 = form_dict['guardian1'].save(commit=False)
                    guardian1.has_protected = True
                    guardian1.expired = False
                    guardian1.no_sync = True
                    guardian1.save()
                else:
                    guardian1 = None
                if 'guardian2' in form_dict:
                    guardian2 = form_dict['guardian2'].save(commit=False)
                    guardian2.has_protected = True
                    guardian2.expired = False
                    guardian2.no_sync = True
                    guardian2.save()
                else:
                    guardian2 = None
                # Save person
                person = form_dict['person'].save(commit=False)
                old_guardian1 = person.legal_guardian1
                person.legal_guardian1 = guardian1
                old_guardian2 = person.legal_guardian2
                person.legal_guardian2 = guardian2
                person.expired = False
                person.last_invitation = None
                if 'permission' in form_dict:
                    person.permission_alone = form_dict['permission'].cleaned_data['permission_alone']
                    person.permission_with = form_dict['permission'].cleaned_data['permission_with']
                person.no_sync = True
                person.save()
                if old_guardian1:
                    old_guardian1.update_has_protected()
                if old_guardian2:
                    old_guardian2.update_has_protected()
                form_dict['person'].save_m2m()
                # Save adherent
                if self.adherent:
                    adherent = self.adherent
                else:
                    adherent = Adherent(person=person)
                if not adherent.id:
                    adherent.id = get_next_value('person_number', initial_value=settings.PERSON_NUMBER_INITIAL_VALUE)
                if adherent.status == AdherentStatus.LEFT:
                    adherent.status = AdherentStatus.OK
                assert adherent.status == AdherentStatus.OK
                assert adherent.person == person
                adherent.no_sync = True
                adherent.save()
                # Save adhesion
                if 'adhesion' in form_dict:
                    adhesion = form_dict['adhesion'].save(commit=False)
                else:
                    adhesion = Adhesion()
                adhesion.entry_date = today()
                adhesion.adherent = adherent
                adhesion.rate = form_dict['contribution'].cleaned_data['rate']
                if adhesion.rate.rate_max:
                    adhesion.free_rate = form_dict['contribution'].cleaned_data['free_rate']
                adhesion.donation = form_dict['contribution'].cleaned_data['donation']
                adhesion.season = current_season()
                if self.invitation:
                    adhesion.structure = self.invitation.structure
                    adhesion.set_dates(self.invitation.date or today())
                    adhesion.signatory = form_dict['post_confirm'].cleaned_data['signatory']
                else:
                    adhesion.set_dates(adhesion.begin)
                adhesion.is_discovery = adhesion.rate.category == RateCategory.DISCOVERY
                adhesion.no_sync = True
                adhesion.save()
                # Save last adhesion
                adherent.last_adhesion = adhesion
                adherent.save()
                # Save function
                if self.invitation and self.invitation.function_config:
                    function = Function(
                        team=self.invitation.team,
                        function_config=self.invitation.function_config,
                        name=self.invitation.function_config.name,
                    )
                elif not self.invitation and form_dict['function'].cleaned_data['function_config']:
                    function = Function(
                        team=form_dict['function'].cleaned_data['team'],
                        function_config=form_dict['function'].cleaned_data['function_config'],
                        name=form_dict['function'].cleaned_data['function_config'].name,
                    )
                else:
                    function = None
                if function:
                    function.person = person
                    function.dates = adhesion.dates
                    function.no_sync = True
                    function.save()
                # Save payer / payment / entry
                if not self.invitation:
                    self.save_payment(form_dict, person, guardian1, guardian2, adhesion)
                self.save_transfer(adhesion)
            # Save invitation
            if self.invitation:
                self.invitation.person = person
                self.invitation.adhesion = adhesion
                self.invitation.save()
            others_invitations = Invitation.objects.filter(person=person, season=current_season())
            if self.invitation:
                others_invitations.exclude(pk=self.invitation.pk)
            others_invitations.update(declined=True)
        # Realtime sync
        if guardian1:
            force_publish_save(guardian1)
        if guardian2:
            force_publish_save(guardian2)
        force_publish_save(person)
        if adherent:
            force_publish_save(adherent)
        force_publish_save(adhesion)
        if function:
            force_publish_save(function)
        if not self.invitation:
            url = reverse('person_detail', args=[person.pk])
            messages.success(
                self.request,
                mark_safe(f"L'adhésion de <a href=\"{url}\">{escape(person)}</a> a bien été enregistrée"),
                extra_tags="alert-success"
            )
        if adhesion.is_paid:
            adhesion.send_email(self.request)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        if self.invitation:
            return reverse('adhesion_pre_pay', args=[self.invitation.token])
        else:
            return super().get_success_url()


class EmploymentWizardView(AccessMixin, BackSuccessUrl, WizardCacheMixin, SessionWizardView):
    template_name = 'members/employment_wizard.html'
    form_list = (
        ('search_person', SearchPersonForm),
        ('person', PersonForm),
        ('employee', EmployeeForm),
        ('employment', EmploymentForm),
    )
    permission_denied_message = "Tu n'es pas autorisé à gérer les salarié·es, CEE, SC"
    model = Employment

    def dispatch(self, request, person_pk=None):
        # Check access
        if not request.user.has_perm('members.add_employment'):
            return self.handle_no_permission()

        # person
        if person_pk:
            self.person = get_object_or_404(Person, pk=person_pk)
        else:
            self.person = None
        if self.person:
            if Employment.objects.current().filter(employee__person=self.person).exists():
                messages.warning(
                    self.request,
                    f"{self.person} a déjà un contrat en cours.",
                    extra_tags="alert-warning",
                )
                return HttpResponseRedirect(reverse('person_detail', args=[self.person.pk]))

        # employee
        if self.person:
            try:
                self.employee = self.person.employee
            except Employee.DoesNotExist:
                self.employee = None
        else:
            self.employee = None

        return super().dispatch(request, person_pk)

    def search_person_condition(self):
        return not self.person

    condition_dict = {
        'search_person': search_person_condition,
    }

    # Prevent collisions when two wizards are opened in parallel
    def get_prefix(self, request, person_pk):
        if person_pk:
            return f'employment{person_pk}'
        hash = md5(json.dumps(request.GET).encode('utf8')).hexdigest()
        return f'create-{hash}'

    def get_form_initial(self, step):
        # employment
        if step == 'employment':
            return {
                'begin': today(),
            }

    def get_context_data(self, form, **kwargs):
        context = super().get_context_data(form=form, **kwargs)
        context['step_title'] = {
            'search_person': "Chercher la personne",
            'person': "La personne",
            'employee': "Le ou la salarié·e, CEE, SC",
            'employment': "Le contrat de travail",
            'function': "La fonction",
            'post_confirm': "Confirmation finale",
        }[self.steps.current]
        context['person'] = self.person
        return context

    def get_form_instance(self, step):
        if step == 'person':
            if self.person:
                return self.person
            cleaned_data = self.get_cleaned_data_for_step('search_person')
            if cleaned_data and cleaned_data['mode'] == 'Sel':
                return cleaned_data['person']
            else:
                return None
        if step == 'employee':
            if self.employee:
                return self.employee
            cleaned_data = self.get_cleaned_data_for_step('search_person')
            if cleaned_data and cleaned_data['mode'] == 'Sel':
                try:
                    return cleaned_data['person'].employee
                except Employee.DoesNotExist:
                    return None
            else:
                return None

    def get_form_kwargs(self, step):
        kwargs = {}
        if step in ('search_person', 'person'):
            kwargs['user'] = self.request.user
        if step == 'person':
            kwargs['required_birth_place'] = True
        return kwargs

    def done(self, form_list_, form_dict, **kwargs):
        with transaction.atomic():
            # Save person
            person = form_dict['person'].save(commit=False)
            person.expired = False
            person.no_sync = True
            person.save()
            # Save employee
            employee = form_dict['employee'].save(commit=False)
            if not employee.id:
                employee.id = get_next_value('person_number', initial_value=settings.PERSON_NUMBER_INITIAL_VALUE)
            employee.person = person
            employee.no_sync = True
            employee.save()
            # Save employment
            employment = form_dict['employment'].save(commit=False)
            employment.employee = employee
            employment.no_sync = True
            employment.save()
            # Save last employment
            employee.last_employment = employee.employments.last()
            employee.save()
        force_publish_save(person)
        force_publish_save(employee)
        force_publish_save(employment)
        messages.success(
            self.request,
            f"Le contrat de travail de {person} a bien été enregistrée",
            extra_tags="alert-success"
        )
        employment.send_email(self.request)
        return HttpResponseRedirect(self.get_success_url())


class AdhesionUpdateView(LoginRequiredMixin, BackSuccessUrl, UpdateView):
    model = Adhesion
    form_class = AdhesionForm

    def dispatch(self, request, pk):
        adhesion = self.get_object()
        if adhesion.season != current_season():
            return self.handle_no_permission()
        if not request.user.is_authenticated or not request.user.has_perm('members.change_adhesion', adhesion):
            return self.handle_no_permission()
        return super().dispatch(request, pk)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'user': self.request.user,
        })
        return kwargs


class ContributionUpdateView(LoginRequiredMixin, BackSuccessUrl, UpdateView):
    model = Adhesion
    form_class = ContributionForm
    template_name = 'members/contribution_form.html'

    def dispatch(self, request, pk):
        adhesion = self.get_object()
        if not request.user.has_perm('members.change_adhesion', adhesion):
            return self.handle_no_permission()
        if adhesion.season != current_season() and not request.user.has_perm('members.change_old_adhesion', adhesion):
            raise PermissionDenied(
                "Merci de contacter un·e CAFT ou le secrétariat national "
                "pour modifier le tarif ou le don pour une saison terminée."
            )
        return super().dispatch(request, pk)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'user': self.request.user,
            'invitation': None,
            'person': self.object.adherent.person,
            'birthdate': self.object.adherent.person.birthdate,
            'adhesion_date': self.object.begin,
            'force_age': self.request.POST.get('force_age') == 'on',
            'structure': self.object.structure,
            'function_config': None,
        })
        return kwargs

    def form_valid(self, form):
        adhesion = form.save(commit=False)
        with transaction.atomic():
            # Bank transfer of the difference between paid amount and new contribution rate
            old = Adhesion.objects.get(pk=adhesion.pk)
            amount = (
                adhesion.effective_rate -
                sum(old.allocation_set.filter(
                    payment__online_status=PaymentStatus.SUCCESS,
                    category=Allocation.CATEGORY_CONTRIBUTION
                ).values_list('amount', flat=True), 0) -
                sum(old.entry_set.filter(
                    category=Entry.CATEGORY_CONTRIBUTION
                ).values_list('amount', flat=True), 0)
            )
            if amount:
                Entry.create(adhesion, amount, category=Entry.CATEGORY_CONTRIBUTION)
            adhesion.update_is_paid(commit=False)
            adhesion.save()
        return HttpResponseRedirect(self.get_success_url())


class RateOptionsView(ListView):
    template_name = 'members/rate_options.html'

    def get_queryset(self):
        structure = get_object_or_404(Structure, pk=self.kwargs['structure_pk'])
        return Rate.objects.filter(season=current_season(), structure_types=structure.type)


class ReceiptDownloadView(DetailView):
    queryset = Receipt.objects.filter(canceled=False)

    def render_to_response(self, context, **response_kwargs):
        with open(self.object.filename, 'rb') as f:
            data = f.read()
        filename = f"Reçu_fiscal_EEDF_J-{self.object.number}.pdf"
        response = HttpResponse(data, content_type='application/pdf')
        response['Content-Disposition'] = f'attachment; filename="{filename}"'
        return response


class ReceiptDownloadByPkView(ReceiptDownloadView):
    def get_queryset(self):
        return super().get_queryset().for_perm('members.view_receipt', self.request.user)


class ReceiptDownloadByTokenView(ReceiptDownloadView):
    slug_field = 'token'
    slug_url_kwarg = 'token'

    def render_to_response(self, context, **response_kwargs):
        self.object.download = now()
        self.object.save()
        return super().render_to_response(context, **response_kwargs)


class DashboardView(LoginRequiredMixin, TemplateView):
    template_name = 'members/dashboard.html'

    def get_context_data(self, **kwargs):
        filter = StatsAdhesionFilter(self.request.GET)
        ref_filter = StatsAdhesionFilter(self.request.GET, ref=True)
        context = super().get_context_data(**kwargs)
        context['widget_templates'] = [widget.template_name for widget in widgets]
        context['filter'] = filter
        for widget in widgets:
            context.update(widget.get_context_data(filter, ref_filter))
        return context


# TODO: use StatsAdhesionFilter
class ChartJsonView(LoginRequiredMixin, View):
    def serie(self, season, GET):
        start = date(season - 1, 9, 1)
        end = min(date(season, 8, 31), yesterday())
        qs = Adhesion.objects.filter(season=season, canceled=False)
        if GET.get('branch'):
            qs = qs.filter(functions__function__branch=GET['branch'], functions__main=True)
        if GET.get('function'):
            qs = qs.filter(functions__function__category=GET['function'], functions__main=True)
        if GET.get('rate'):
            qs = qs.filter(rate__category=GET['rate'])
        qs = qs.order_by('-entry_date').values('entry_date').annotate(headcount=Count('id'))
        qs = list(qs)
        data = OrderedDict()
        dates = [start + timedelta(days=n) for n in
                 range((end - start).days + 1)]
        acc = 0
        for d in dates:
            if qs and qs[-1]['entry_date'] == d:
                acc += qs.pop()['headcount']
            if d.month == 2 and d.day == 29:
                continue
            data[d] = acc
        return data

    def get(self, request):
        if 'season' not in self.request.GET or not self.request.GET['season'].isdigit():
            return HttpResponseBadRequest("Missing or wrong season parameter.")
        season = int(self.request.GET['season'])
        reference = season - 1
        data = self.serie(season, self.request.GET)
        if not data:
            return JsonResponse({
                'comment': "La courbe n'est visible qu'à partir du 2 septembre."
            })
        ref_data = self.serie(reference, self.request.GET)
        date_max = max(data.keys())
        ref_date_max = date_max.replace(year=reference if date_max.month <= 8 else reference - 1,
                                        month=date_max.month, day=date_max.day)
        date1 = ref_date_max.strftime('%d/%m/%Y')
        date2 = date_max.strftime('%d/%m/%Y')
        nb1 = ref_data[ref_date_max]
        nb2 = data[date_max]
        diff = nb2 - nb1
        if nb1:
            percent = 100 * diff / nb1
            comment = """Au <strong>{}</strong> : <strong>{}</strong> adhérent·es<br>
                         Au <strong>{}</strong> : <strong>{}</strong> adhérent·es,
                         c'est-à-dire <strong>{:+d}</strong> adhérent·es
                         (<strong>{:+0.1f} %</strong>)
                      """.format(date1, nb1, date2, nb2, diff, percent)
        else:
            comment = """Au <strong>{}</strong> : <strong>{}</strong> adhérent·es
                      """.format(date2, nb2)
        data = {
            'labels': [date_format(x, 'b') if x.day == 1 else '' for x in ref_data.keys()],
            'series': [
                list(ref_data.values()),
                list(data.values()),
            ],
            'comment': comment,
        }
        return JsonResponse(data)


class ChartView(LoginRequiredMixin, TemplateView):
    def get_template_names(self):
        if 'print' in self.request.GET:
            return 'members/chart_print.html'
        else:
            return 'members/chart.html'

    def get_context_data(self, **kwargs):
        season = self.request.GET.get('season', current_season())
        branch = self.request.GET.get('branch', 0)
        initial = self.request.GET.dict()
        initial.update({
            'season': season,
            'branch': branch,
        })
        form = AdhesionsFilterForm(initial=initial)
        context = super().get_context_data(**kwargs)
        context['form'] = form
        context.update(initial)
        return context


class TableauAmountView(LoginRequiredMixin, TemplateView):
    template_name = 'members/tableau_amount.html'

    def get_context_data(self, **kwargs):
        filter = StatsAdhesionFilter(self.request.GET)
        qs = filter.qs.with_effective_rate().values('rate__name', 'qs_effective_rate')
        qs = qs.annotate(headcount=Count('id'))
        qs = qs.annotate(amount=Sum('qs_effective_rate'))
        qs = qs.order_by('-qs_effective_rate')
        context = {
            'filter': filter,
            'date': filter.date.strftime('%d/%m/%Y') if hasattr(filter, 'date') and filter.date else "",
            'data': qs,
            'total_headcount': sum([obj['headcount'] or 0 for obj in qs]),
            'total_amount': sum([obj['amount'] or 0 for obj in qs]),
        }
        return context


class ReceiptStatsView(LoginRequiredMixin, TemplateView):
    template_name = 'members/receipt_stats.html'

    def get_context_data(self, **kwargs):
        filter = ReceiptStatsFilter(self.request.GET)
        qs = filter.qs.filter(
            canceled=False,
        )
        receipt_qs = qs.aggregate(
            amount=Sum('amount'),
            nb=Count('id'),
        )
        allocation_qs = qs.aggregate(
            amount_contribution=Sum(
                'payment__allocation__amount',
                filter=Q(payment__allocation__category=Allocation.CATEGORY_CONTRIBUTION),
            ),
            amount_donation=Sum(
                'payment__allocation__amount',
                filter=Q(payment__allocation__category=Allocation.CATEGORY_DONATION),
            ),
        )
        return {
            'filter': filter,
            'year': filter.form.cleaned_data['year'],
            'receipt_qs': receipt_qs,
            'allocation_qs': allocation_qs,
        }


class TableauView(LoginRequiredMixin, TemplateView):
    template_name = 'members/tableau.html'

    def format_diff(self, row):
        diff = row[1] - row[0]
        if diff > 0:
            return "+ {}".format(diff)
        elif diff == 0:
            return "="
        else:
            return "- {}".format(-diff)

    def format_percent(self, row):
        diff = row[1] - row[0]
        if diff == 0:
            return "="
        elif not row[0]:
            return "∞"
        elif diff > 0:
            return "+ {:0.1f} %".format(100 * diff / row[0])
        else:
            return "- {:0.1f} %".format(-100 * diff / row[0])

    def aggregate(self, qs, filter):
        qs = qs.values(*self.values).annotate(headcount=Count('id'))
        qs = qs.order_by('-headcount')
        return qs

    def graph_label(self, key, values, total):
        if values[1] * 50 < total[1]:
            return None
        if not total[1]:
            return key
        return "{} ({:0.1f}%)".format(key, 100 * values[1] / total[1])

    def get_context_data(self, **kwargs):
        filter = StatsAdhesionFilter(self.request.GET)
        if filter.form.errors:
            return {
                'title': self.title,
                'filter': filter,
            }
        self.filter = filter
        ref_filter = StatsAdhesionFilter(self.request.GET, ref=True)
        data = OrderedDict()
        total = [0, 0]
        for i, _filter in ((1, filter), (0, ref_filter)):
            qs = self.aggregate(_filter.qs, _filter)
            for obj in qs:
                data.setdefault(self.row_key(obj), [0, 0])[i] = obj['headcount']
                total[i] += obj['headcount']
        graph = {
            'labels': [self.graph_label(key, values, total) for key, values in data.items()],
            'series': [values[1] for values in data.values()]
        }
        data['TOTAL'] = total
        for row in data.values():
            row.append(self.format_diff(row))
            row.append(self.format_percent(row))
        context = {
            'title': self.title,
            'filter': filter,
            'header': (
                "Au {}".format(ref_filter.date.strftime('%d/%m/%Y')) if ref_filter.date else "",
                "Au {}".format(filter.date.strftime('%d/%m/%Y')),
                "Variation",
                "Variation %",
            ),
            'data': data,
            'graph': json.dumps(graph),
        }
        return context


class TableauStructureTypeView(TableauView):
    values = ('structure__type__name', )
    title = "type de structure"

    def row_key(self, obj):
        return obj['structure__type__name']


class TableauStructureView(TableauView):
    values = ('structure__name', )
    title = "structure"

    def row_key(self, obj):
        return obj['structure__name']


class TableauFunctionsView(TableauView):
    values = ('adherent__person__functions__function_config__name', )
    title = "fonction"

    def aggregate(self, qs, filter):
        season = int(filter.form.cleaned_data['season'])
        value = filter.form.cleaned_data['date']
        begin = date(season - 1, 9, 1)
        if value == 'ajd':
            end = yesterday()
            leap_day = end.month == 2 and end.day == 29
            end = end.replace(
                year=season if end.month <= 8 else season - 1,
                day=28 if leap_day else end.day
            )
        else:
            end = date(season, 8, 31)
        qs = qs.filter(
            adherent__person__functions__status=FunctionStatus.OK,
            adherent__person__functions__dates__overlap=DateRange(begin, end, '[]'),
        )
        return super().aggregate(qs, filter)

    def row_key(self, obj):
        return obj['adherent__person__functions__function_config__name']


class TableauRateView(TableauView):
    values = ('rate__category', )
    title = "tarif"

    def row_key(self, obj):
        if obj['rate__category'] is None:
            return "Autre"
        return RateCategory(obj['rate__category']).label


class TableauGenderView(TableauView):
    values = ('adherent__person__gender', )
    title = "genre"

    def row_key(self, obj):
        return {
            'M': "Masculin",
            'F': "Féminin",
            '-': "Autre",
            '': "Inconnu",
        }[obj['adherent__person__gender']]


class TableauRegionsView(TableauView):
    values = ('region', )
    title = "région"

    def aggregate(self, qs, filter):
        regions = Structure.objects.filter(type__echelon=Echelon.REGIONAL).order_by('name')
        res = []
        for region in regions:
            headcount = qs.filter(structure__in=region.get_descendants(include_self=True)).count()
            if headcount:
                res.append({'region': region, 'headcount': headcount})
        res.sort(key=lambda obj: obj['headcount'], reverse=True)
        return res

    def row_key(self, obj):
        return obj['region'].name.replace('EEDF ', '')


class TableauDepartementsView(TableauView):
    values = ('departement', )
    title = "département"

    def aggregate(self, qs, filter):
        qs = qs.annotate(departement=Left('adherent__person__postal_code', 2))
        return super().aggregate(qs, filter)

    def row_key(self, obj):
        return obj['departement']


# TODO: calcule le prix moyen de la cotisation (avant et après déduction)
class TranchesView(TableauView):
    values = ('rate__bracket', )
    title = "tranche d'imposition"

    def aggregate(self, qs, filter):
        qs = super().aggregate(qs, filter)
        qs = qs.filter(rate__category=1)
        return qs

    def row_key(self, obj):
        return obj['rate__bracket']


class StatsSfView(LoginRequiredMixin, TemplateView):
    template_name = 'members/stats_sf.html'

    def get_context_data(self, **kwargs):
        filter = StatsSfFilter(self.request.GET)
        filter.form.is_valid()
        context = {
            'filter': filter,
            'headcounts': {},
        }
        season = int(filter.form.cleaned_data['season'])
        for team_type in TeamType.objects.all():
            qs = filter.qs.filter(
                functions__function_config__function_type__category=FunctionCategory.PARTICIPANT,
                functions__team__type=team_type,
                functions__status=FunctionStatus.OK,
                functions__dates__overlap=DateRange(date(season - 1, 9, 1), date(season, 8, 31), '[]'),
            ).distinct()
            count_m = qs.filter(gender='M').aggregate(count=Count('pk'))['count']
            count_f = qs.exclude(gender='M').aggregate(count=Count('pk'))['count']
            if count_m or count_f:
                context['headcounts'][team_type.name] = (count_m, count_f, count_m + count_f)
        for category, name in FunctionCategory.choices:
            if category == FunctionCategory.PARTICIPANT:
                continue
            qs = filter.qs.filter(
                functions__function_config__function_type__category=category,
            ).distinct()
            count_m = qs.filter(gender='M').aggregate(count=Count('pk'))['count']
            count_f = qs.exclude(gender='M').aggregate(count=Count('pk'))['count']
            if count_m or count_f:
                context['headcounts'][name] = (count_m, count_f, count_m + count_f)
        # Total
        context['headcounts']['Total'] = [sum(col) for col in zip(*context['headcounts'].values())]
        return context


class AdhesionExportView(ExportView, AdhesionListView):
    filename = "Adhésions"

    def get_fields(self):
        fields = AdherentExportView.get_fields(self, strict=True)
        fields = [(name, f"adherent.{attribute}", width) for name, attribute, width in fields]
        if 'full' in self.request.GET:
            fields.extend([
                ("Début", 'begin', 12),
                ("Fin", 'end', 12),
                ("Tarif", 'rate.name', 30),
                ("Structure", 'structure.name', 25),
                ("Région", 'structure.region.name', 25),
            ])
        return fields


class AdhesionImportView(LoginRequiredMixin, FormView):
    form_class = AdhesionImportForm
    template_name = 'members/adhesion_import.html'
    success_url = reverse_lazy('adhesion_list')
    header = None
    FIELDS = {
        'last_name': "nom",
        'first_name': "prénom",
        'gender': "genre",
        'address1': "adresse ligne 1",
        'address2': "adresse ligne 2",
        'address3': "adresse ligne 3",
        'postal_code': "code postal",
        'city': "ville",
        'country': "pays",
        'home_phone': "tél. fixe",
        'mobile_phone': "tél. mobile",
        'email': "email",
        'birthdate': "date de naissance",
        'birth_country': "pays de naissance",
        'birth_commune': "commune de naissance",
        'team': "équipe",
        'function_config': "fonction",
        'image_rights': "droit à l'image",
    }

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['structures'] = Structure.objects.for_perm("members.import_adhesion", self.request.user) \
            .is_active().filter(type__can_import=True)
        return kwargs

    def form_valid(self, form):
        try:
            wb = openpyxl.load_workbook(form.cleaned_data['file'])
        except (BadZipFile, KeyError, IOError):
            form.add_error('file', "Le fichier n'est pas au format Excel 2007 XLSX.")
            return super().form_invalid(form)
        ws = wb.worksheets[0]
        header = None
        nb = 0
        errors = []
        adhesions = []
        functions = []
        rate = Rate.objects.get(season=current_season(), name=settings.IMPORT_RATE_NAME)
        date_field = forms.DateTimeField()
        transaction.set_autocommit(False)
        for line, row in enumerate(ws.values):
            if not header:
                header = [remove_accents(cell.lower().replace('’', '\'').strip()) if cell else "" for cell in row]
                missing = [value for value in self.FIELDS.values() if remove_accents(value) not in header]
                extra = [
                    cell for cell in row
                    if cell and remove_accents(cell.lower().replace('’', '\'').strip()) not in [
                        remove_accents(value) for value in self.FIELDS.values()
                    ]
                ]
                if len(missing) == 1:
                    errors.append(f"Il manque la colonne intitulée : « {missing[0]} ».")
                elif missing:
                    errors.append(f"Il manque les colonnes intitulées : « {' », « '.join(missing)} ».")
                if len(extra) == 1:
                    errors.append(f"Il y a une colonne inutile intitulée : « {extra[0]} ».")
                elif extra:
                    errors.append(f"Il y a des colonnes inutiles intitulées : « {' », « '.join(extra)} ».")
                if errors:
                    break
                continue
            data = {
                name: row[header.index(remove_accents(title))]
                for name, title in self.FIELDS.items()
            }
            # Remove ="…" formulas
            for name, value in data.items():
                if isinstance(value, str):
                    data[name] = re.sub(r'^="(.*)"$', r'\1', value)
            # Ignore blank lines
            if not any(data.values()):
                continue
            nb += 1
            if not data['birthdate']:
                errors.append(f"Ligne {line + 1} : La date de naissance est manquante.")
                continue
            if isinstance(data['birthdate'], str):
                try:
                    data['birthdate'] = date_field.to_python(data['birthdate'])
                except ValidationError:
                    errors.append(f"Ligne {line + 1} : La date de naissance n'est valide.")
                    continue
            elif not isinstance(data['birthdate'], date):
                errors.append(f"Ligne {line + 1} : La date de naissance n'est valide.")
                continue
            if isinstance(data['gender'], str):
                data['gender'] = data['gender'].upper()
            if isinstance(data['image_rights'], str) and data['image_rights'].lower() == "oui":
                data['image_rights'] = True
            elif isinstance(data['image_rights'], str) and data['image_rights'].lower() == "non":
                data['image_rights'] = False
            else:
                data['image_rights'] = None
            if data['home_phone'] is not None:
                data['home_phone'] = str(data['home_phone'])
            if data['mobile_phone'] is not None:
                data['mobile_phone'] = str(data['mobile_phone'])
            try:
                team = Team.objects.is_active().get(
                    name__unaccent__iexact=data['team'],
                    structure=form.cleaned_data['structure'],
                )
            except Team.DoesNotExist:
                team = None
                possible = Team.objects.is_active().filter(
                    structure=form.cleaned_data['structure'],
                )
                errors.append(
                    f"Ligne {line + 1}, colonne équipe : le choix « {data['team'] or ''} » n'est pas valide. "
                    f"Les choix possibles sont : {', '.join(str(f) for f in possible)}"
                )
                continue
            try:
                data['birth_country'] = Country.objects.get(
                    name__unaccent__iexact=data['birth_country'],
                )
            except Country.DoesNotExist:
                errors.append(mark_safe(
                    f"Ligne {line + 1}, colonne pays de naissance : le choix "
                    f"« {escape(data['birth_country']) or ''} » n'est pas valide. "
                    f"Voir les <a href=\"{reverse('country_list')}?q={escape(data['birth_country'])}\"> "
                    f"choix possibles</a>."
                ))
                continue
            if data['birth_country'].id == 99100:
                match = re.fullmatch(r'(.*) \((\d{2})\)', data['birth_commune'])
                try:
                    if match:
                        data['birth_commune'] = Commune.objects.get(
                            dates__contains=data['birthdate'],
                            name__unaccent__iexact=match.group(1),
                            code__startswith=match.group(2),
                        )
                    else:
                        data['birth_commune'] = Commune.objects.get(
                            dates__contains=data['birthdate'],
                            name__unaccent__iexact=data['birth_commune'],
                        )
                except Commune.DoesNotExist:
                    errors.append(mark_safe(
                        f"Ligne {line + 1}, colonne commune de naissance : le choix "
                        f"« {escape(data['birth_commune']) or ''} » n'est pas valide. "
                        f"Voir les <a href=\"{reverse('commune_list')}?q={escape(data['birth_commune'])}\"> "
                        f"choix possibles</a>."
                    ))
                    continue
                except Commune.MultipleObjectsReturned:
                    errors.append(
                        f"Ligne {line + 1}, colonne commune de naissance : il existe plusieurs communes nommées "
                        f"« {data['birth_commune'] or ''} ». Précisez le département (ex. « Verrières (12) »)."
                    )
                    continue
            else:
                data['birth_place'] = data['birth_commune']
                data['birth_commune'] = ''
            try:
                function_config = FunctionConfig.objects.get(
                    name__unaccent__iexact=data['function_config'],
                )
            except FunctionConfig.DoesNotExist:
                function_config = None
                possible = FunctionConfig.objects.filter(
                    Q(structure_type=team.structure.type) | Q(structure_type=None),
                    team_type=team.type,
                ).exclude(
                    function_type__category=FunctionCategory.EMPLOYEE,
                )
                errors.append(
                    f"Ligne {line + 1}, colonne fonction : le choix « {data['function_config'] or ''} » "
                    f"n'est pas valide. Les choix possibles sont : {', '.join(str(f) for f in possible)}"
                )
                continue
            person = Person.objects.filter(
                normalized_last_name=normalize_name(data['last_name']),
                normalized_first_name=normalize_name(data['first_name']),
                birthdate=data['birthdate'],
            ).order_by('id').last()
            if not person:
                person = Person()
            data['birthdate'] = data['birthdate'].date().isoformat()
            person_form = PersonForm(instance=person, data=data, user=self.request.user, required_birth_place=True)
            person_form.is_valid()
            if person_form.errors:
                for field, msgs in person_form.errors.items():
                    for msg in msgs:
                        if msg == person_form.SAME_PERSON_ERROR:
                            pass
                        elif field == '__all__':
                            errors.append(f"Ligne {line + 1} : " + msg)
                        else:
                            errors.append(f"Ligne {line + 1}, colonne {self.FIELDS[field]} : " + msg)
                continue
            else:
                person_form.save(commit=False)
            old_guardian1 = person.legal_guardian1
            old_guardian2 = person.legal_guardian2
            person.legal_guardian1 = None
            person.legal_guardian2 = None
            person.expired = False
            person.last_invitation = None
            person.no_sync = True
            person.save()
            if old_guardian1:
                old_guardian1.update_has_protected()
            if old_guardian2:
                old_guardian2.update_has_protected()
            try:
                adherent = person.adherent
            except Adherent.DoesNotExist:
                adherent = Adherent(
                    id=get_next_value('person_number', initial_value=settings.PERSON_NUMBER_INITIAL_VALUE),
                    person=person,
                )
            adherent.no_sync = True
            if adherent.status == AdherentStatus.LEFT:
                adherent.status = AdherentStatus.OK
            if adherent.status != AdherentStatus.OK:
                errors.append(mark_safe(
                    f"Ligne {line + 1}, l'adhésion de « {escape(person)} » est actuellement bloquée. "
                    "Merci de contacter le <a href=\"/aide/assistance.html\" target=\"_blank\">siège national</a> "
                    "pour plus d'information."
                ))
                continue
            adherent.save()
            adhesion = adherent.adhesion
            if adhesion:
                adhesion.created_by_import = False  # In memory only
            else:
                adhesion = Adhesion(
                    adherent=adherent,
                    season=current_season(),
                    dates=DateRange(today(), date(current_season(), 9, 1)),
                    entry_date=today(),
                    rate=rate,
                    structure=form.cleaned_data['structure'],
                )
                adhesion.no_sync = True
                adhesion.save()
                adhesion.created_by_import = True  # In memory only
                function = Function(
                    person=person,
                    team=team,
                    function_config=function_config,
                    dates=DateRange(today(), date(current_season(), 9, 1)),
                    name=str(function_config),
                )
                function.no_sync = True
                function.save()
                adherent.last_adhesion = adhesion
                adherent.save()
                if rate.rate:
                    payment = Payment.objects.create(
                        payer=None,
                        method=5,
                        amount=rate.rate,
                        date=adhesion.begin,
                        reference="",
                    )
                    payment.create_allocations(adhesion)
                    # Log payment (after allocation creation to set persons in additional data)
                    LogEntry.objects.log_create(
                        payment,
                        action=LogEntry.Action.CREATE,
                        changes=model_instance_diff(None, payment),
                        additional_data=payment.get_additional_data(),
                    )
                    Entry.create(adhesion, rate.rate, Entry.CATEGORY_CONTRIBUTION)
                functions.append(function)
            adhesions.append(adhesion)
            Invitation.objects.filter(person=person, season=current_season()).update(declined=True)
        if errors:
            transaction.rollback()
            transaction.set_autocommit(True)
            for error in errors:
                form.add_error(None, error)
            return super().form_invalid(form)
        else:
            for adhesion in adhesions:
                force_publish_save(adhesion.adherent.person)
                force_publish_save(adhesion.adherent)
                force_publish_save(adhesion)
            for function in functions:
                force_publish_save(function)
            msg = format_html("{} adhésions ont bien été importées :", nb)
            for adhesion in adhesions:
                msg += format_html("\n<br>- {}", adhesion.adherent)
                if adhesion.created_by_import:
                    msg += mark_safe(" (Créée)")
                else:
                    msg += mark_safe(" (Existante)")
            messages.success(self.request, msg, extra_tags="alert-success")
            transaction.commit()
            transaction.set_autocommit(True)
            with mail.get_connection() as connection:
                for adhesion in adhesions:
                    if adhesion.created_by_import:
                        adhesion.send_email(self.request, connection)
            return super().form_valid(form)


class EmploymentImportView(LoginRequiredMixin, FormView):
    form_class = EmploymentImportForm
    template_name = 'members/employment_import.html'
    success_url = reverse_lazy('employment_list')
    header = None
    FIELDS = {
        'last_name': "nom",
        'first_name': "prénom",
        'gender': "genre",
        'address1': "adresse ligne 1",
        'address2': "adresse ligne 2",
        'address3': "adresse ligne 3",
        'postal_code': "code postal",
        'city': "ville",
        'country': "pays",
        'home_phone': "tél. fixe",
        'mobile_phone': "tél. mobile",
        'email': "email",
        'birthdate': "date de naissance",
        'birth_country': "pays de naissance",
        'birth_commune': "commune de naissance",
        'type': "type de contrat",
        'name': "intitulé de poste",
        'begin': "date de début",
        'end': "date de fin",
        'team': "équipe",
        'function_name': "fonction",
        'image_rights': "droit à l'image",
    }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['employment_types'] = EmploymentType
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['structures'] = Structure.objects.for_perm("members.import_employment", self.request.user) \
            .is_active().filter(type__can_import=True)
        return kwargs

    def form_valid(self, form):
        try:
            wb = openpyxl.load_workbook(form.cleaned_data['file'])
        except (BadZipFile, KeyError, IOError):
            form.add_error('file', "Le fichier n'est pas au format Excel 2007 XLSX.")
            return super().form_invalid(form)
        ws = wb.worksheets[0]
        header = None
        nb = 0
        errors = []
        employments = []
        functions = []
        date_field = forms.DateTimeField()
        transaction.set_autocommit(False)
        for line, row in enumerate(ws.values):
            if not header:
                header = [remove_accents(cell.lower().replace('’', '\'').strip()) if cell else "" for cell in row]
                missing = [value for value in self.FIELDS.values() if remove_accents(value) not in header]
                extra = [
                    cell for cell in row
                    if cell and remove_accents(cell.lower().replace('’', '\'').strip()) not in [
                        remove_accents(value) for value in self.FIELDS.values()
                    ]
                ]
                if len(missing) == 1:
                    errors.append(f"Il manque la colonne intitulée : « {missing[0]} ».")
                elif missing:
                    errors.append(f"Il manque les colonnes intitulées : « {' », « '.join(missing)} ».")
                if len(extra) == 1:
                    errors.append(f"Il y a une colonne inutile intitulée : « {extra[0]} ».")
                elif extra:
                    errors.append(f"Il y a des colonnes inutiles intitulées : « {' », « '.join(extra)} ».")
                if errors:
                    break
                continue
            data = {
                name: row[header.index(remove_accents(title))]
                for name, title in self.FIELDS.items()
            }
            # Remove ="…" formulas
            for name, value in data.items():
                if isinstance(value, str):
                    data[name] = re.sub(r'^="(.*)"$', r'\1', value)
            # Ignore blank lines
            if not any(data.values()):
                continue
            nb += 1
            if not data['birthdate']:
                errors.append(f"Ligne {line + 1} : La date de naissance est manquante.")
                continue
            if isinstance(data['birthdate'], str):
                try:
                    data['birthdate'] = date_field.to_python(data['birthdate'])
                except ValidationError:
                    errors.append(f"Ligne {line + 1} : La date de naissance n'est valide.")
                    continue
            elif not isinstance(data['birthdate'], date):
                errors.append(f"Ligne {line + 1} : La date de naissance n'est valide.")
                continue
            if isinstance(data['gender'], str):
                data['gender'] = data['gender'].upper()
            if isinstance(data['image_rights'], str) and data['image_rights'].lower() == "oui":
                data['image_rights'] = True
            elif isinstance(data['image_rights'], str) and data['image_rights'].lower() == "non":
                data['image_rights'] = False
            else:
                data['image_rights'] = None
            if data['home_phone'] is not None:
                data['home_phone'] = str(data['home_phone'])
            if data['mobile_phone'] is not None:
                data['mobile_phone'] = str(data['mobile_phone'])
            try:
                team = Team.objects.is_active().get(
                    name__unaccent__iexact=data['team'],
                    structure=form.cleaned_data['structure'],
                )
            except Team.DoesNotExist:
                possible = Team.objects.is_active().filter(
                    structure=form.cleaned_data['structure'],
                )
                errors.append(
                    f"Ligne {line + 1}, colonne équipe : le choix « {data['team'] or ''} » n'est pas valide. "
                    f"Les choix possibles sont : {', '.join(str(f) for f in possible)}"
                )
                continue
            try:
                data['birth_country'] = Country.objects.get(
                    name__unaccent__iexact=data['birth_country'],
                )
            except Country.DoesNotExist:
                errors.append(mark_safe(
                    f"Ligne {line + 1}, colonne pays de naissance : le choix "
                    f"« {escape(data['birth_country']) or ''} » n'est pas valide. "
                    f"Voir les <a href=\"{reverse('country_list')}?q={escape(data['birth_country'])}\"> "
                    f"choix possibles</a>."
                ))
                continue
            if data['birth_country'].id == 99100:
                if not data['birth_commune']:
                    errors.append(f"Ligne {line + 1} : La commune de naissance est manquante.")
                    continue
                match = re.fullmatch(r'(.*) \((\d{2})\)', data['birth_commune'])
                try:
                    if match:
                        data['birth_commune'] = Commune.objects.get(
                            dates__contains=data['birthdate'],
                            name__unaccent__iexact=match.group(1),
                            code__startswith=match.group(2),
                        )
                    else:
                        data['birth_commune'] = Commune.objects.get(
                            dates__contains=data['birthdate'],
                            name__unaccent__iexact=data['birth_commune'],
                        )
                except Commune.DoesNotExist:
                    errors.append(mark_safe(
                        f"Ligne {line + 1}, colonne commune de naissance : le choix "
                        f"« {escape(data['birth_commune']) or ''} » n'est pas valide. "
                        f"Voir les <a href=\"{reverse('commune_list')}?q={escape(data['birth_commune'])}\"> "
                        f"choix possibles</a>."
                    ))
                    continue
                except Commune.MultipleObjectsReturned:
                    errors.append(
                        f"Ligne {line + 1}, colonne commune de naissance : il existe plusieurs communes nommées "
                        f"« {data['birth_commune'] or ''} ». Précisez le département (ex. « Verrières (12) »)."
                    )
                    continue
            else:
                data['birth_place'] = data['birth_commune']
                data['birth_commune'] = ''
            try:
                data['type'] = {
                    remove_accents(label).lower(): value for value, label in EmploymentType.choices
                }[remove_accents(data['type']).lower()]
            except KeyError:
                possible = [type.label for type in EmploymentType]
                errors.append(
                    f"Ligne {line + 1}, colonne type de contrat : le choix « {data['type'] or ''} » "
                    f"n'est pas valide. Les choix possibles sont : {', '.join(str(f) for f in possible)}"
                )
                continue
            try:
                function_config = FunctionConfig.objects.get(
                    Q(structure_type=team.structure.type) | Q(structure_type=None),
                    team_type=team.type,
                    function_type__category=FunctionCategory.EMPLOYEE,
                )
            except FunctionConfig.DoesNotExist:
                errors.append(
                    f"Ligne {line + 1} : il n'existe pas de fonction salarié·e pour l'équipe « {data['team']} »."
                )
                continue
            person = Person.objects.filter(
                normalized_last_name=normalize_name(data['last_name']),
                normalized_first_name=normalize_name(data['first_name']),
                birthdate=data['birthdate'],
            ).order_by('id').last()
            if not person:
                person = Person()
            data['birthdate'] = data['birthdate'].date().isoformat()
            person_form = PersonForm(instance=person, data=data, user=self.request.user, required_birth_place=True)
            person_form.is_valid()
            if person_form.errors:
                for field, msgs in person_form.errors.items():
                    for msg in msgs:
                        if msg == person_form.SAME_PERSON_ERROR:
                            pass
                        elif field == '__all__':
                            errors.append(f"Ligne {line + 1} : " + msg)
                        elif field == 'birth_place':
                            errors.append(f"Ligne {line + 1}, colonne commune de naissance : " + msg)
                        else:
                            errors.append(f"Ligne {line + 1}, colonne {self.FIELDS[field]} : " + msg)
                continue
            else:
                person_form.save(commit=False)
            person.no_sync = True
            person.save()
            try:
                employee = person.employee
            except Employee.DoesNotExist:
                employee = Employee(
                    id=get_next_value('person_number', initial_value=settings.PERSON_NUMBER_INITIAL_VALUE),
                    person=person,
                )
            employee.no_sync = True
            employee.save()
            employment = employee.employment
            if employment:
                employment.created_by_import = False  # In memory only
            else:
                employment = Employment(employee=employee)
                employment.created_by_import = True  # In memory only
            employment_form = EmploymentForm(instance=employment, data=data)
            employment_form.is_valid()
            if employment_form.errors:
                for field, msgs in employment_form.errors.items():
                    for msg in msgs:
                        if field == '__all__':
                            errors.append(f"Ligne {line + 1} : " + msg)
                        else:
                            errors.append(f"Ligne {line + 1}, colonne {self.FIELDS[field]} : " + msg)
                continue
            else:
                employment_form.save(commit=False)
            employment.no_sync = True
            employment.save()
            if employment.created_by_import:
                employee.last_employment = employment
                employee.save()
                function = Function(
                    person=person,
                    team=team,
                    function_config=function_config,
                    dates=employment.dates,
                    name=data['function_name'] or str(function_config),
                )
                function.no_sync = True
                function.save()
                functions.append(function)
            employments.append(employment)
        if errors:
            transaction.rollback()
            transaction.set_autocommit(True)
            for error in errors:
                form.add_error(None, error)
            return super().form_invalid(form)
        else:
            for employment in employments:
                force_publish_save(employment.employee.person)
                force_publish_save(employment.employee)
                force_publish_save(employment)
            for function in functions:
                force_publish_save(function)
            msg = format_html("{} contrats de travail ont bien été importés :", nb)
            for employment in employments:
                msg += format_html("\n<br>- {}", employment.employee)
                if employment.created_by_import:
                    msg += mark_safe(" (Créée)")
                else:
                    msg += mark_safe(" (Existant)")
            messages.success(self.request, msg, extra_tags="alert-success")
            transaction.commit()
            transaction.set_autocommit(True)
            with mail.get_connection() as connection:
                for employment in employments:
                    if employment.created_by_import:
                        employment.send_email(self.request, connection)
            return super().form_valid(form)


class TransferOrderListView(PermissionRequiredMixin, ListView):
    permission_required = 'members.view_transferorder'
    model = TransferOrder
    paginate_by = 25


class TransferOrderDetailView(PermissionRequiredMixin, DetailView):
    permission_required = 'members.view_transferorder'
    model = TransferOrder

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['transfers'] = self.object.transfer_set \
            .for_perm('members.view_transferorder', self.request.user) \
            .select_related('structure') \
            .order_by('structure__name')
        return context


class TransferOrderXlsxView(PermissionRequiredMixin, DetailView):
    permission_required = 'members.export_transferorder'
    model = TransferOrder

    def render_to_response(self, context, **response_kwargs):
        if self.object.outstanding:
            raise Http404
        wb = openpyxl.Workbook()
        ws = wb.active
        ws.append([
            "Structure",
            "IBAN",
            "Cotisations",
            "Dons",
            "Débit",
            "Crédit",
        ])
        transfers = context['object'].transfer_set \
            .filter(amount__gt=0) \
            .for_perm('members.view_transfer',  self.request.user) \
            .order_by('structure__name') \
            .select_related('structure')
        for i, transfer in enumerate(transfers):
            ws.append([
                transfer.structure.name,
                transfer.iban,
                transfer.contribution or "",
                transfer.donation_positive or "",
                transfer.debit or "",
                transfer.credit or "",
            ])
            for column in range(3, 7):
                ws.cell(row=i + 2, column=column).number_format = '0.00'
        for i, width in enumerate([50, 35, 15, 15, 15, 15]):
            ws.column_dimensions[openpyxl.utils.get_column_letter(i + 1)].width = width
        response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        filename = f"Virements et prélèvements adhésions {context['object'].date.strftime('%d-%m-%Y')}.xlsx"
        response['Content-Disposition'] = f'attachment; filename={filename}'
        with NamedTemporaryFile() as tmp:
            wb.save(tmp.name)
            tmp.seek(0)
            response.write(tmp.read())
        return response


class DirectDebitSEPAView(PermissionRequiredMixin, DetailView):
    permission_required = 'members.export_transferorder'
    model = TransferOrder

    def render_to_response(self, context, **response_kwargs):
        if self.object.outstanding:
            raise Http404
        content = self.object.sepa_debit()
        filename = 'Prélèvements adhésions {}.xml'.format(self.object.date.strftime('%d-%m-%Y'))
        response = HttpResponse(content, content_type='application/xml')
        response['Content-Disposition'] = 'attachment; filename={}'.format(filename)
        return response


class CreditTransferSEPAView(PermissionRequiredMixin, DetailView):
    permission_required = 'members.export_transferorder'
    model = TransferOrder

    def render_to_response(self, context, **response_kwargs):
        if self.object.outstanding:
            raise Http404
        content = self.object.sepa_credit()
        filename = 'Virements adhésions {}.xml'.format(self.object.date.strftime('%d-%m-%Y'))
        response = HttpResponse(content, content_type='application/xml')
        response['Content-Disposition'] = 'attachment; filename={}'.format(filename)
        return response


class TransferDetailView(PermissionRequiredMixin, DetailView):
    permission_required = 'members.view_transfer'

    def get_queryset(self):
        return Transfer.objects.for_perm('members.view_transfer', self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['entries'] = self.object.entry_set.order_by(
            'adhesion__dates',
            'adhesion__adherent__person__last_name',
            'adhesion__adherent__person__first_name'
        ).select_related('adhesion', 'adhesion__adherent', 'adhesion__adherent__person')
        return context


class StatusUpdateView(PermissionRequiredMixin, BackSuccessUrl, UpdateView):
    permission_required = 'members.change_adherent_status'
    model = Adherent
    form_class = StatusForm
    template_name = 'members/status_form.html'


class PermissionUpdateView(PermissionRequiredMixin, BackSuccessUrl, UpdateView):
    permission_required = 'members.change_person'
    form_class = PermissionForm
    template_name = 'members/permission_form.html'

    def get_queryset(self):
        return Person.objects.for_perm('members.change_person', self.request.user)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs


class FunctionListView(LoginRequiredMixin, FilterView):
    filterset_class = FunctionFilter
    template_name = 'members/function_list.html'
    paginate_by = 25

    def get_queryset(self):
        return Function.objects \
            .for_perm("members.view_function", self.request.user) \
            .select_related(
                'person',
                'person__adherent',
                'person__adherent__last_adhesion',
                'person__employee',
                'team',
                'team__structure',
                'team__structure__region',
            )


class FunctionExportView(ExportView, FunctionListView):
    filename = "Fonctions"

    def get_fields(self, strict=False):
        fields = PersonExportView.get_fields(self, strict=True)
        fields = [(name, f"person.{attribute}", width) for name, attribute, width in fields]
        fields.extend([
            ("Numéro", 'person.id_str', 12),
            ("Statut", 'person.status_text', 15),
        ])
        if 'full' in self.request.GET:
            fields.extend([
                ("Région", 'team.structure.region.name', 30),
                ("Structure", 'team.structure.name', 30),
                ("Équipe", 'team.name', 30),
                ("Fonction", 'name', 30),
                ("Du", 'begin', 15),
                ("Au", 'end', 15),
            ])
        return fields


class FunctionDetailView(LoginRequiredMixin, DetailView):
    filterset_class = FunctionFilter
    template_name = 'members/function_detail.html'

    def get_queryset(self):
        return Function.objects \
            .for_perm("members.view_function", self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['can_change'] = not self.object.owner \
            and self.request.user.has_perm('members.change_function', self.object)
        context['can_delete'] = not self.object.owner \
            and self.request.user.has_perm('members.delete_function', self.object)
        context['content_type'] = ContentType.objects.get_for_model(Function)
        return context


class FunctionCreateView(PermissionRequiredMixin, BackSuccessUrl, CreateView):
    permission_required = 'members.add_function'
    model = Function
    form_class = FunctionForm

    def get_initial(self):
        initial = {}
        structure_pk = self.request.GET.get('structure', '')
        if structure_pk.isdigit():
            structure = get_object_or_404(
                Structure.objects.for_perm("members.add_function", self.request.user),
                pk=structure_pk,
            )
            initial['structure'] = structure
        else:
            initial['structure'] = self.request.user.structure
        adhesion_pk = self.request.GET.get('adhesion', '')
        if adhesion_pk.isdigit():
            adhesion = get_object_or_404(
                Adhesion.objects.for_perm('members.view_adhesion', self.request.user).distinct(),
                season=current_season(),
                canceled=False,
                pk=adhesion_pk,
            )
            initial['person'] = adhesion.adherent.person
            initial['structure'] = adhesion.structure
        team_pk = self.request.GET.get('team', '')
        if team_pk.isdigit():
            team = get_object_or_404(
                Team.objects.for_perm('members.add_function', self.request.user).is_active(),
                pk=team_pk,
            )
            initial['team'] = team
            initial['structure'] = initial['team'].structure
        config_pk = self.request.GET.get('config', '')
        if config_pk.isdigit():
            config = get_object_or_404(
                FunctionConfig.objects.all(),
                pk=config_pk,
            )
            initial['function_config'] = config
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'user': self.request.user,
            'force_age': self.request.POST.get('force_age') == 'on',
        })
        return kwargs

    def form_valid(self, form):
        response = super().form_valid(form)
        if self.object.status == FunctionStatus.REQUESTED:
            messages.warning(
                self.request,
                f"Un message a été envoyé à {self.object.person} afin qu'il ou elle donne son accord.",
                extra_tags="alert-warning",
            )
            self.object.send_email(self.request)
        return response


class FunctionUpdateView(PermissionRequiredMixin, BackSuccessUrl, UpdateView):
    permission_required = 'members.change_function'
    form_class = FunctionForm

    def get_queryset(self):
        return Function.objects.filter(owner=None).for_perm('members.change_function', self.request.user)

    def get_initial(self):
        try:
            adherent = self.object.person.adherent
        except Adherent.DoesNotExist:
            outside = True
        else:
            outside = not adherent.last_adhesion or adherent.last_adhesion.structure != self.object.team.structure
        return {
            'structure': self.object.team.structure,
            'outside': outside,
        }

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'user': self.request.user,
            'force_age': self.request.POST.get('force_age') == 'on',
        })
        return kwargs

    def form_valid(self, form):
        response = super().form_valid(form)
        if self.object.status == FunctionStatus.REQUESTED:
            messages.warning(
                self.request,
                f"Un message a été envoyé à {self.object.person} afin qu'il ou elle donne son accord.",
                extra_tags="alert-warning",
            )
            self.object.send_email(self.request)
        return response


class FunctionReplyView(DetailView):
    model = Function
    slug_field = 'token'
    slug_url_kwarg = 'token'
    template_name = 'members/function_reply.html'

    def get(self, request, token):
        function = self.get_object()
        if function.status == FunctionStatus.REQUESTED:
            return super().get(request, token)
        if function.status == FunctionStatus.OK:
            reply = "acceptée"
        elif function.status == FunctionStatus.REFUSED:
            reply = "refusée"
        return render(self.request, 'members/function_reply_done.html', {'function': function, 'reply': reply})

    def post(self, request, token):
        function = self.get_object()
        if self.request.POST['reply'] == 'accept':
            function.status = FunctionStatus.OK
            reply = "acceptée"
        elif self.request.POST['reply'] == 'refuse':
            function.status = FunctionStatus.REFUSED
            reply = "refusée"
        else:
            raise "Unknown reply"
        function.save()
        return render(self.request, 'members/function_reply_done.html', {'function': function, 'reply': reply})


class FunctionDeleteView(PermissionRequiredMixin, BackSuccessUrl, DeleteView):
    permission_required = 'members.delete_function'

    def get_queryset(self):
        return Function.objects.filter(owner=None).for_perm('members.delete_function', self.request.user)

    def form_valid(self, form):
        function = self.get_object()
        if self.request.POST.get('choice') == 'finish':
            function.dates = DateRange(function.begin, today(), '[)')
            function.save()
        elif self.request.POST.get('choice') == 'delete':
            function.delete()
        return HttpResponseRedirect(self.get_success_url())


class InconsistenciesView(PermissionRequiredMixin, FilterView):
    permission_required = 'members.view_inconsistencies'
    filterset_class = InconsistenciesFilter
    template_name = 'members/inconsistencies.html'

    def get_queryset(self):
        return Function.objects.current().filter(
            person__adherent__status=AdherentStatus.OK,
            person__birthdate__isnull=False,
        ).annotate(
            birthmonth=ExtractMonth('person__birthdate'),
            birthyear=ExtractYear('person__birthdate'),
            age_begin=ExtractYear(
                Func(F('dates__startswith'), F('person__birthdate'), function='age'),
                output_field=models.IntegerField(),
            ),
            age_end=ExtractYear(
                Func(F('dates__endswith'), F('person__birthdate'), function='age'),
                output_field=models.IntegerField(),
            ),
            gap=Case(
                When(
                    age_begin__lt=F('function_config__age_min'),
                    then=F('age_begin') - F('function_config__age_min')
                ),
                When(
                    age_end__gt=F('function_config__age_max') - 1,
                    then=F('age_end') - F('function_config__age_max')
                ),
                default=0
            ),
            abs_gap=Abs('gap')
        ).exclude(
            gap=0
        ).select_related(
            'person',
            'team__structure',
        )


class MasqueradeView(PermissionRequiredMixin, DeleteView):
    permission_required = 'members.masquerade_person'
    template_name = 'members/person_confirm_masquerade.html'

    def get_queryset(self):
        actual_user = self.request.actual_user or self.request.user
        return User.objects.for_perm('members.masquerade_person', actual_user)

    def form_valid(self, form):
        fake_user = self.get_object()
        actual_user = self.request.actual_user or self.request.user
        login(self.request, fake_user, backend='django.contrib.auth.backends.ModelBackend')
        self.request.session['actual_user_pk'] = actual_user.pk
        return HttpResponseRedirect(reverse('home'))


class UnmasqueradeView(DeleteView):
    template_name = 'members/person_confirm_unmasquerade.html'

    def get_object(self):
        return self.request.user

    def form_valid(self, form):
        if self.request.actual_user:
            login(self.request, self.request.actual_user, backend='django.contrib.auth.backends.ModelBackend')
        return HttpResponseRedirect(reverse('home'))


class PaymentDeleteView(LoginRequiredMixin, BackSuccessUrl, DeleteView):
    def get_queryset(self):
        return Payment.objects.for_perm('members.delete_payment', self.request.user)

    def form_valid(self, form):
        payment = self.get_object()
        if payment.online_status != PaymentStatus.MANUAL:
            raise PermissionDenied("Il n'est pas possible de supprimer un paiement en ligne.")
        try:
            receipt = payment.receipt
        except Receipt.DoesNotExist:
            pass
        else:
            receipt.cancel()
        LogEntry.objects.log_create(
            payment,
            action=LogEntry.Action.DELETE,
            changes=model_instance_diff(payment, None),
            additional_data=payment.get_additional_data(),
        )
        adhesions = list(Adhesion.objects.filter(allocation__payment=payment).distinct())
        payment.allocation_set.all().delete()
        payment.delete()
        if payment.payer and not payment.payer.payment_set.exists():
            payment.payer.has_payment = False
            payment.payer.save()
        for adhesion in adhesions:
            adhesion.update_is_paid()
        return HttpResponseRedirect(self.get_success_url())


class AccountView(LoginRequiredMixin, RedirectView):
    def get_redirect_url(self):
        return reverse('person_detail', args=[self.request.user.person.pk])


class InvitationListView(LoginRequiredMixin, FilterView):
    filterset_class = InvitationFilter
    template_name = 'members/invitation_list.html'
    paginate_by = 25

    def get_queryset(self):
        return Invitation.objects \
            .filter(season=current_season()) \
            .for_perm('members.view_invitation', self.request.user) \
            .order_by('email') \
            .select_related('person')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['current_season'] = current_season()
        return context


class InvitationDetailView(LoginRequiredMixin, DetailView):
    def get_queryset(self):
        qs = Invitation.objects \
            .filter(season=current_season()) \
            .for_perm('members.view_invitation', self.request.user)
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['url'] = self.object.url(self.request)
        context['content_type'] = ContentType.objects.get_for_model(Invitation)
        return context


class InvitationSendReminderView(LoginRequiredMixin, DetailView):
    template_name = 'members/invitation_send_reminder.html'

    def get_queryset(self):
        qs = Invitation.objects \
            .filter(season=current_season()) \
            .for_perm('members.change_invitation', self.request.user)
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(self.object.get_reminder_email_context(self.request))
        return context

    def post(self, request, pk):
        invitation = self.get_object()
        invitation.send_reminder_email(request)
        messages.success(request, "Le message de relance a bien été envoyé.", extra_tags="alert-success")
        return HttpResponseRedirect(reverse('invitation_detail', args=[invitation.pk]))


class InvitationCreateView(PermissionRequiredMixin, TemplateView):
    permission_required = 'members.add_invitation'
    template_name = 'members/invitation_create.html'


class InvitationCreateNewView(PermissionRequiredMixin, BackSuccessUrl, WizardCacheMixin, SessionWizardView):
    permission_required = 'members.add_invitation'
    template_name = 'members/invitation_new_form.html'
    model = Invitation
    form_list = (
        ('adhesion', InvitationCreateNewForm),
        ('function', AdhesionFunctionForm),
    )

    def get_form_kwargs(self, step):
        kwargs = {
            'user': self.request.user,
        }
        if step == 'function':
            data = self.get_cleaned_data_for_step('adhesion')
            kwargs.update({
                'structure': data and data['structure'],
                'current_functions': None,
                'birthdate': None,
                'force_age': False,
                'is_invitation': True,
                'rate': data and data['rate'],
            })
        return kwargs

    def done(self, form_list_, form_dict, **kwargs):
        with mail.get_connection() as connection:
            for email in form_dict['adhesion'].cleaned_data['emails']:
                invitation = Invitation.objects.create(
                    host=self.request.user.person,
                    season=current_season(),
                    email=email,
                    structure=form_dict['adhesion'].cleaned_data['structure'],
                    team=form_dict['function'].cleaned_data['team'],
                    function_config=form_dict['function'].cleaned_data['function_config'],
                    rate=form_dict['adhesion'].cleaned_data['rate'],
                    date=form_dict['adhesion'].cleaned_data['date'],
                    entry_date=today(),
                    modification_date=today(),
                )
                invitation.send_email(self.request, connection)
        if len(form_dict['adhesion'].cleaned_data['emails']) > 1:
            msg = "Les invitations ont bien été envoyées par email."
        else:
            msg = "L'invitation a bien été envoyée par email."
        messages.success(self.request, msg, extra_tags="alert-success")
        return HttpResponseRedirect(self.get_success_url())


class InvitationCreateOldView(PermissionRequiredMixin, BackSuccessUrl, WizardCacheMixin, SessionWizardView):
    permission_required = 'members.add_invitation'
    template_name = 'members/invitation_old_form.html'
    model = Invitation
    form_list = (
        ('adhesion', InvitationCreateOldForm),
        ('function', AdhesionFunctionForm),
    )

    def get_form_initial(self, step):
        if step == 'adhesion' and 'person' in self.request.GET:
            person = get_object_or_404(Person, pk=self.request.GET['person'])
            return {
                'person': person,
                'email': person.guardian_email,
                'structure': self.request.user.structure,
            }
        elif step == 'function':
            data = self.get_cleaned_data_for_step('adhesion')
            if not data:
                return
            team, function_config = data['person'].guess_main_function()
            if function_config and not function_config.subject_to_nomination:
                return {
                    'team': team,
                    'function_config': function_config,
                }

    def get_form_kwargs(self, step):
        kwargs = {
            'user': self.request.user,
        }
        if step == 'function':
            data = self.get_cleaned_data_for_step('adhesion')
            kwargs.update({
                'force_age': self.request.POST.get('force_age') == 'on',
                'is_invitation': True,
            })
            if data:
                kwargs.update({
                    'structure': data['structure'],
                    'current_functions': data['person'].functions.filter(
                        team__structure=data['structure']
                    ).exclude(
                        function_config__function_type__category=FunctionCategory.EMPLOYEE,
                    ).current(),
                    'birthdate': data['person'].birthdate,
                    'rate': data['rate'],
                })
            else:
                kwargs.update({
                    'structure': None,
                    'current_functions': Function.objects.none(),
                    'birthdate': None,
                    'rate': None,
                })
        return kwargs

    def done(self, form_list_, form_dict, **kwargs):
        defaults = {
            'host': self.request.user.person,
            'email': form_dict['adhesion'].cleaned_data['email'],
            'team': form_dict['function'].cleaned_data['team'],
            'function_config': form_dict['function'].cleaned_data['function_config'],
            'rate': form_dict['adhesion'].cleaned_data['rate'],
            'date': form_dict['adhesion'].cleaned_data['date'],
            'modification_date': today(),
            'adhesion': None,
        }
        create_defaults = defaults.copy()
        create_defaults['entry_date'] = today()
        invitation, created = Invitation.objects.update_or_create(
            season=current_season(),
            person=form_dict['adhesion'].cleaned_data['person'],
            structure=form_dict['adhesion'].cleaned_data['structure'],
            defaults=defaults,
            create_defaults=create_defaults,
        )
        invitation.send_email(self.request)
        form_dict['adhesion'].cleaned_data['person'].last_invitation = current_season()
        form_dict['adhesion'].cleaned_data['person'].save(update_fields=('last_invitation', ))
        messages.success(self.request, "L'invitation a bien été envoyée par email.", extra_tags="alert-success")
        return HttpResponseRedirect(self.get_success_url())


class InvitationCreateListView(PermissionRequiredMixin, BackSuccessUrl, FormView):
    permission_required = 'members.add_invitation'
    form_class = InvitationCreateListFormSet
    success_url = reverse_lazy('invitation_list')
    template_name = 'members/invitation_list_form.html'
    model = Invitation

    def dispatch(self, request, *args, **kwargs):
        qs = Person.objects.for_perm('members.add_invitation', self.request.user) \
            .select_related(
                'adherent__last_adhesion',
                'legal_guardian1',
                'legal_guardian2',
                'adherent__last_adhesion__structure',
            ) \
            .filter(adherent__last_adhesion__season=current_season() - 1) \
            .exclude(invitation__season=current_season()) \
            .only(
                'last_name', 'first_name', 'adherent__id', 'birthdate', 'email',
                'legal_guardian1__email', 'legal_guardian2__email',
                'adherent__last_adhesion__season', 'adherent__last_adhesion__structure',
            )
        self.filter = CreateInvitationFilter(
            queryset=qs,
            data=self.request.GET,
            request=self.request
        )
        self.paginator = Paginator(self.filter.qs, 25)
        self.page = self.paginator.get_page(int(self.request.GET.get('page', '1')))
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.filter
        context['paginator'] = self.paginator
        context['page_obj'] = self.page
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['form_kwargs'] = {
            'user': self.request.user,
            'persons': Person.objects.for_perm('members.add_invitation', self.request.user),
            'force_age': [
                person.pk for person in self.page.object_list
                if f'force_age{person.pk}' in self.request.POST
            ],
        }
        if self.filter.form.is_valid():
            kwargs['form_kwargs']['structure'] = self.filter.form.cleaned_data['structure']
        else:
            kwargs['form_kwargs']['structure'] = None

        return kwargs

    def get_initial(self):
        initial = []
        for person in self.page.object_list:
            team, function_config = person.guess_main_function()
            initial.append({
                'person': person,
                'email': person.guardian_email,
                'team': team,
                'function_config': function_config,
            })
        return initial

    def form_valid(self, formset):
        n = 0
        with mail.get_connection() as connection:
            for form in formset:
                if not form.cleaned_data['invite']:
                    continue
                invitation = Invitation.objects.create(
                    host=self.request.user.person,
                    season=current_season(),
                    person=form.cleaned_data['person'],
                    email=form.cleaned_data['email'],
                    structure=self.filter.form.cleaned_data['structure'],
                    team=form.cleaned_data['team'],
                    function_config=form.cleaned_data['function_config'],
                    entry_date=today(),
                    modification_date=today(),
                )
                invitation.send_email(self.request, connection)
                form.cleaned_data['person'].last_invitation = current_season()
                form.cleaned_data['person'].save(update_fields=('last_invitation', ))
                n += 1
        if n > 1:
            msg = "Les invitations ont bien été envoyées par email."
        else:
            msg = "L'invitation a bien été envoyée par email."
        messages.success(self.request, msg, extra_tags="alert-success")
        return super().form_valid(formset)


class InvitationCreateSelfView(View):
    def get(self, request):
        try:
            person = request.user.person
        except Person.DoesNotExist:
            raise PermissionDenied("Cet utilisateur n'est pas lié à une personne.")
        if not person.can_invite_itself:
            raise PermissionDenied("Vous n'avez pas la permission de vous invitez vous-même.")
        try:
            adherent = person.adherent
        except Adherent.DoesNotExist:
            raise PermissionDenied("Vous n'êtes pas ancien·ne adhérent·e.")
        if adherent.is_blocked():
            raise PermissionDenied(mark_safe(f"Votre adhésion est actuellement bloquée. {CONTACT_MESSAGE}"))
        invitation, _ = Invitation.objects.get_or_create(
            season=current_season(),
            person=person,
            structure=person.last_adhesion.structure,
            defaults={
                'host': person,
                'email': person.email,
                'team': None,
                'function_config': None,
                'entry_date': today(),
                'modification_date': today(),
            }
        )
        person.last_invitation = current_season()
        person.save(update_fields=('last_invitation', ))
        return HttpResponseRedirect(reverse('adhesion_create', args=[invitation.token]))


class InvitationUpdateView(LoginRequiredMixin, BackSuccessUrl, UpdateView):
    form_class = InvitationUpdateForm
    success_url = reverse_lazy('invitation_list')

    def get_queryset(self):
        qs = Invitation.objects \
            .filter(season=current_season()) \
            .for_perm('members.change_invitation', self.request.user)
        return qs

    def get_object(self):
        invitation = super().get_object()
        if invitation.is_adherent:
            raise PermissionDenied("Déjà adhérent·e")
        return invitation

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        kwargs['force_age'] = self.request.POST.get('force_age') == 'on'
        return kwargs

    def form_valid(self, form):
        invitation = form.save(commit=False)
        invitation.declined = False
        invitation.modification_date = today()
        invitation.save()
        self.object.send_email(self.request)
        url = reverse('invitation_detail', args=[self.object.pk])
        msg = mark_safe(
            f"L'<a href=\"{url}\">invitation</a> de {self.object.person or 'Nouvel·le adhérent·e'} "
            f"a bien été envoyée à <a href=\"mailto:{self.object.email}\">{self.object.email}</a>."
        )
        messages.success(self.request, msg, extra_tags="alert-success")
        return HttpResponseRedirect(self.get_success_url())


class InvitationDeleteView(LoginRequiredMixin, BackSuccessUrl, DeleteView):
    success_url = reverse_lazy('invitation_list')

    def get_queryset(self):
        qs = Invitation.objects \
            .filter(season=current_season()) \
            .for_perm('members.delete_invitation', self.request.user)
        return qs

    def get_object(self):
        invitation = super().get_object()
        if invitation.is_adherent:
            raise PermissionDenied("Déjà adhérent·e")
        return invitation

    def form_valid(self, form):
        if self.object.person and not Invitation.objects.filter(
            person=self.object.person,
            season=current_season(),
            declined=False,
        ).exclude(pk=self.object.pk).exists():
            self.object.person.last_invitation = None
            self.object.person.save(update_fields=('last_invitation', ))
        return super().form_valid(form)


class InvitationSendView(LoginRequiredMixin, BackSuccessUrl, DeleteView):
    success_url = reverse_lazy('invitation_list')
    template_name = 'members/invitation_confirm_send.html'

    def get_queryset(self):
        qs = Invitation.objects \
            .filter(season=current_season()) \
            .for_perm('members.change_invitation', self.request.user)
        return qs

    def get_object(self):
        invitation = super().get_object()
        if invitation.is_adherent:
            raise PermissionDenied("Déjà adhérent·e")
        return invitation

    def form_valid(self, form):
        self.object.declined = False
        self.object.modification_date = today()
        self.object.save()
        self.object.send_email(self.request)
        url = reverse('invitation_detail', args=[self.object.pk])
        msg = mark_safe(
            f"L'<a href=\"{url}\">invitation</a> de {self.object.person or 'Nouvel·le adhérent·e'} "
            f"a bien été envoyée à <a href=\"mailto:{self.object.email}\">{self.object.email}</a>."
        )
        messages.success(self.request, msg, extra_tags="alert-success")
        return HttpResponseRedirect(self.get_success_url())


class InvitationDeclineView(DeleteView):
    success_url = reverse_lazy('invitation_list')
    template_name = 'members/invitation_confirm_decline.html'
    slug_field = 'token'
    slug_url_kwarg = 'token'
    model = Invitation

    def get_object(self):
        invitation = super().get_object()
        if invitation.is_adherent:
            raise PermissionDenied("Déjà adhérent·e")
        return invitation

    def form_valid(self, form):
        self.object.declined = True
        self.object.save()
        if self.object.person and not Invitation.objects.filter(
            person=self.object.person,
            season=current_season(),
            declined=False,
        ).exists():
            self.object.person.last_invitation = None
            self.object.person.save(update_fields=('last_invitation', ))
        return HttpResponseRedirect('')


class OnlinePrePayView(WizardCacheMixin, SessionWizardView):
    form_list = (
        ('select_invitations', SelectInvitationsForm),
        ('search_payer', SearchPayerForm),
        ('payer', PayerForm),
    )
    template_name = 'members/online_pre_pay.html'

    def payer_condition(self):
        search_payer_data = self.get_cleaned_data_for_step('search_payer')
        return not search_payer_data or search_payer_data.get('mode') in ('New', 'Sel')

    condition_dict = {
        'payer': payer_condition,
    }

    def dispatch(self, request, invitation_token, *args, ):
        self.invitation = get_object_or_404(Invitation, token=invitation_token, season=current_season())
        assert self.invitation.adhesion
        assert not self.invitation.adhesion.canceled
        invitation_pks = set(self.request.session.get('invitations', []))
        invitation_pks.add(self.invitation.pk)
        for invitation in Invitation.objects.filter(pk__in=invitation_pks):
            if invitation.season != current_season() \
                    or not invitation.adhesion \
                    or invitation.adhesion.canceled \
                    or invitation.adhesion.is_paid:
                invitation_pks.remove(invitation.pk)
        self.request.session['invitations'] = list(invitation_pks)
        self.invitations = Invitation.objects.filter(pk__in=invitation_pks)

        # Invited persons
        pks = set(invitation.person.pk for invitation in self.invitations if invitation.person)
        pks |= set(invitation.adhesion.adherent.person.pk for invitation in self.invitations if invitation.adhesion)
        # Invited emails (persons with these adresses as personal email)
        emails = [invitation.email for invitation in self.invitations if invitation.email != 'anonymise@eedf.fr']
        pks |= set(Person.objects.filter(email__in=emails).values_list('pk', flat=True))
        if request.user.is_authenticated and request.user.person:
            pks.add(request.user.person.pk)
        self.search_persons = Person.objects.filter(
            # Person
            Q(pk__in=pks) |
            # Legal guardians
            Q(protected1__in=pks) |
            Q(protected2__in=pks) |
            # Other parent
            Q(protected1__legal_guardian1__in=pks) |
            Q(protected1__legal_guardian2__in=pks) |
            Q(protected2__legal_guardian1__in=pks) |
            Q(protected2__legal_guardian2__in=pks) |
            # Payers
            Q(payment__allocation__adhesion__adherent__person__in=pks)
        )
        if self.invitation.person:
            self.search_persons = self.search_persons.exclude(pk=self.invitation.person.pk)
        self.search_persons = self.search_persons.distinct().order_by('last_name', 'first_name')

        return super().dispatch(request)

    def get_form_kwargs(self, step):
        kwargs = {
            'user': self.request.user,
        }
        if step == 'select_invitations':
            kwargs['invitations'] = self.invitations
        if step == 'search_payer':
            adhesion = self.invitation.adhesion
            person = adhesion.adherent.person
            kwargs.update({
                'persons': self.search_persons,
                'eedf': False,
                'person': {
                    'first_name': person.first_name,
                    'last_name': person.last_name
                },
                'guardian1': person.legal_guardian1 and {
                    'first_name': person.legal_guardian1.first_name,
                    'last_name': person.legal_guardian1.last_name
                },
                'guardian2': person.legal_guardian2 and {
                    'first_name': person.legal_guardian2.first_name,
                    'last_name': person.legal_guardian2.last_name
                },
                'rate': adhesion.rate,
                'donation': adhesion.donation,
            })
        return kwargs

    def get_form_initial(self, step):
        if step == 'select_invitations':
            return {'invitations': [invitation.pk for invitation in self.invitations]}

    def get_form_instance(self, step):
        if step == 'payer':
            cleaned_data = self.get_cleaned_data_for_step('search_payer')
            if cleaned_data and cleaned_data['mode'] == 'Sel':
                return cleaned_data['person']

    def get_context_data(self, form, **kwargs):
        context = super().get_context_data(form=form, **kwargs)
        context['step_title'] = {
            'select_invitations': "Les adhésions à payer",
            'search_payer': "Le payeur",
            'payer': "Le payeur",
        }[self.steps.current]
        context['structure'] = self.invitation.adhesion.structure
        return context

    def done(self, form_list_, form_dict, **kwargs):
        with transaction.atomic():
            # Save payer
            if 'payer' in form_dict:
                payer = form_dict['payer'].save(commit=False)
                payer.no_sync = True
                payer.save()
            else:
                payer = None
            # Save payment
            selected = form_dict['select_invitations'].cleaned_data['invitations']
            for invitation in selected:
                assert not invitation.adhesion.canceled
            amount = sum([
                invitation.adhesion.effective_rate + invitation.adhesion.donation - invitation.adhesion.paid
                for invitation in selected
            ])
            person = self.invitation.adhesion.adherent.person
            payment = Payment.objects.create(
                payer={
                    'Sel': payer,
                    'New': payer,
                    'Adh': person,
                    'G1': person.legal_guardian1,
                    'G2': person.legal_guardian2,
                }[form_dict['search_payer'].cleaned_data['mode']],
                method=4,  # CB
                amount=amount,
                date=now(),
                reference="",
                online_status=PaymentStatus.IN_PROGRESS,
            )
            for invitation in selected:
                payment.create_allocations(invitation.adhesion)
            # Log payment (after allocation creation to set persons in additional data)
            LogEntry.objects.log_create(
                payment,
                action=LogEntry.Action.CREATE,
                changes=model_instance_diff(None, payment),
                additional_data=payment.get_additional_data(),
            )
            # Save payer denormalization
            payment.payer.has_payment = True
            payment.payer.expired = False
            payment.payer.save()
        if payer:
            force_publish_save(payer)

        token = b64encode(f'{settings.BRED_API_USER}:{settings.BRED_API_PWD}'.encode('ascii')).decode('ascii')
        url = self.request.build_absolute_uri(reverse('IPN'))
        request_data = {
            'customer': {
                'reference': str(payment.payer.pk),
                'email': payment.payer.email,
            },
            'metadata': {
                'adhesions': [allocation.adhesion.pk for allocation in payment.allocation_set.all()],
            },
            'amount': int(payment.amount * 100),
            'currency': 'EUR',
            'orderId':  str(payment.pk),
            'ipnTargetUrl': url,
        }
        IPN_LOGGER.debug(json.dumps(request_data))
        response = requests.post(
            'https://api.systempay.fr/api-payment/V4/Charge/CreatePayment',
            headers={'Authorization': f'Basic {token}'},
            json=request_data
        )
        assert response.status_code == 200
        response_data = response.json()
        IPN_LOGGER.debug(json.dumps(response_data))
        if response_data['status'] == 'ERROR' and response_data['answer']['errorCode'] == 'INT_015':
            return render(self.request, 'members/online_pay_error.html', {'payment': payment})
        assert response_data['status'] == 'SUCCESS'
        context = {
            'payment': payment,
            'BRED_JS_PUBKEY': settings.BRED_JS_PUBKEY,
            'bred_js_token': response_data['answer']['formToken'],
        }
        return render(self.request, 'members/online_pay.html', context)


@method_decorator(csrf_exempt, name='dispatch')
class OnlinePostPayView(DetailView):
    model = Payment
    template_name = 'members/online_post_pay.html'

    def post(self, request, pk):
        IPN_LOGGER.debug(self.request.POST['kr-answer'])
        assert hmac.compare_digest(
            hmac.new(
                settings.BRED_HMAC_KEY.encode('ascii'),
                self.request.POST['kr-answer'].encode('utf8'),
                'sha256'
            ).hexdigest(),
            self.request.POST['kr-hash']
        )
        answer = json.loads(self.request.POST['kr-answer'])
        assert answer['orderStatus'] == 'PAID'
        return super().get(request, pk)


@method_decorator(csrf_exempt, name='dispatch')
class IPNView(View):
    def post(self, request):
        IPN_LOGGER.debug(self.request.POST['kr-answer'])
        assert hmac.compare_digest(
            hmac.new(
                settings.BRED_API_PWD.encode('ascii'),
                self.request.POST['kr-answer'].encode('utf8'),
                'sha256'
            ).hexdigest(),
            self.request.POST['kr-hash']
        )
        answer = json.loads(self.request.POST['kr-answer'])
        payment = get_object_or_404(Payment, pk=answer['orderDetails']['orderId'])
        assert payment.method == 4
        if answer['orderCycle'] == 'OPEN':
            return HttpResponse("OK")
        assert answer['orderCycle'] == 'CLOSED'
        if answer['orderStatus'] == 'PAID':
            assert payment.online_status in (PaymentStatus.IN_PROGRESS, PaymentStatus.SUCCESS)
            payment.online_status = PaymentStatus.SUCCESS
        else:
            assert answer['orderStatus'] in ('UNPAID', 'ABANDONED')
            assert payment.online_status in (PaymentStatus.IN_PROGRESS, PaymentStatus.FAILURE)
            payment.online_status = PaymentStatus.FAILURE
        payment.save()
        if payment.online_status == PaymentStatus.SUCCESS:
            for allocation in payment.allocation_set.all():
                Entry.create(allocation.adhesion, -allocation.amount, category=allocation.category)
            for adhesion in Adhesion.objects.filter(allocation__payment=payment).distinct():
                adhesion.update_is_paid()
                if adhesion.is_paid:
                    adhesion.send_email(self.request)
        return HttpResponse("OK")


class RGPDView(TemplateView):
    template_name = 'members/rgpd.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['SUPPORT_EMAIL'] = settings.SUPPORT_EMAIL
        return context


class RemittanceListView(PermissionRequiredMixin, TemplateView):
    permission_required = 'members.view_remittance'
    template_name = 'members/remittance_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        payments = Payment.objects.filter(online_status=PaymentStatus.SUCCESS)
        remittances = payments.values('date__year', 'date__month').annotate(
            amount=Sum('allocation__amount'),
            contribution=Sum('allocation__amount', filter=Q(allocation__category=Allocation.CATEGORY_CONTRIBUTION)),
            donation=Sum('allocation__amount', filter=Q(allocation__category=Allocation.CATEGORY_DONATION)),
        ).order_by('-date__year', '-date__month')
        context['remittances'] = remittances
        return context


class RemittanceDetailView(PermissionRequiredMixin, TemplateView):
    permission_required = 'members.view_remittance'
    template_name = 'members/remittance_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        year = int(self.kwargs['year'])
        month = int(self.kwargs['month'])
        begin = make_aware(datetime(year, month, 1))
        if month < 12:
            end = begin.replace(month=month + 1)
        else:
            end = begin.replace(year=year + 1, month=1)
        today_ = today()
        if end.date() > today_:
            end = today_
        payments = Payment.objects.filter(
            online_status=PaymentStatus.SUCCESS,
            date__gte=begin,
            date__lt=end,
        )
        remittances = payments.values('date').annotate(
            amount=Sum('allocation__amount'),
            contribution=Sum('allocation__amount', filter=Q(allocation__category=Allocation.CATEGORY_CONTRIBUTION)),
            donation=Sum('allocation__amount', filter=Q(allocation__category=Allocation.CATEGORY_DONATION)),
        )
        totals = payments.aggregate(
            amount=Sum('allocation__amount'),
            contribution=Sum('allocation__amount', filter=Q(allocation__category=Allocation.CATEGORY_CONTRIBUTION)),
            donation=Sum('allocation__amount', filter=Q(allocation__category=Allocation.CATEGORY_DONATION)),
        )
        context['remittances'] = remittances
        context['totals'] = totals
        context['begin'] = begin
        return context


class PersonListView(PermissionRequiredMixin, FilterView):
    filterset_class = PersonFilter
    permission_required = 'members.view_person'
    template_name = 'members/person_list.html'
    paginate_by = 25

    def get_queryset(self):
        return Person.objects.for_perm('members.view_person', self.request.user).select_related(
            'adherent',
            'adherent__last_adhesion',
            'employee',
        )


class PersonExportView(ExportView, PersonListView):
    filename = "Personnes"

    def get_fields(self, strict=False):
        fields = [
            ("Nom d'usage", 'last_name', 30),
            ("Prénom", 'first_name', 20),
            ("Nom de naissance", 'family_name', 30),
            ("Surnom", 'nickname', 20),
            ("Genre", 'gender', 6),
            ("Email", 'email', 40),
            ("Tél. fixe", 'home_phone_display', 15),
            ("Tél. mobile", 'mobile_phone_display', 15),
        ]
        if 'full' in self.request.GET:
            fields.extend([
                ("Adresse ligne 1", 'address1', 25),
                ("Adresse ligne 2", 'address2', 25),
                ("Adresse ligne 3", 'address3', 25),
                ("Code postal", 'postal_code', 10),
                ("Ville", 'city', 20),
                ("Pays", 'country', 10),
                ("Date de naissance", 'birthdate', 15),
                ("Age", 'age', 5),
                ("Lieu de naissance", 'birth_place_display', 20),
                ("Profession", 'profession', 20),
                ("Droit à l'image", 'image_rights', 15),
            ])
            if not strict:
                fields.extend([
                    ("Statut", 'status_text', 15),
                ])
        return fields


class DuplicateListView(PermissionRequiredMixin, ListView):
    permission_required = 'members.merge_person'
    template_name = 'members/duplicate_list.html'
    paginate_by = 25

    def get_queryset(self):
        return Person.objects.values(
            'normalized_last_name',
            'normalized_first_name',
        ).annotate(
            nb=Count('id'),
            max_id=Max('id'),
        ).filter(
            nb__gt=1,
        ).order_by(
            '-max_id',
            '-nb',
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        for duplicate in context['page_obj']:
            duplicate['persons'] = Person.objects.filter(
                normalized_last_name=duplicate['normalized_last_name'],
                normalized_first_name=duplicate['normalized_first_name'],
            ).order_by('pk')
        return context


class TeamConfigAutocomplete(LoginRequiredMixin, autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = TeamConfig.objects.all()
        structure_pk = self.forwarded.get('structure')
        if not isinstance(structure_pk, str) or not structure_pk.isdigit():
            return qs.none()
        qs = qs.filter(structure_type__structure=structure_pk)
        qs = qs.with_team_name()
        if self.q:
            qs = qs.search(self.q).order_by('qs_team_name')
        else:
            qs = qs.order_by('team_type__order')
        return qs


class TeamListView(LoginRequiredMixin, FilterView):
    filterset_class = TeamFilter
    template_name = 'members/team_list.html'
    paginate_by = 25

    def get_queryset(self):
        return Team.objects \
            .for_perm('members.view_team', self.request.user) \
            .with_headcount(current_season()) \
            .select_related('structure', 'type') \
            .order_by('name', 'pk')


class TeamDetailView(LoginRequiredMixin, DetailView):
    model = Team

    def get_queryset(self):
        return super().get_queryset().for_perm('members.view_team', self.request.user).select_related('structure')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        members = {}
        for config in FunctionConfig.objects.filter(
            Q(structure_type=self.object.structure.type) | Q(structure_type=None),
            team_type=self.object.type,
        ).order_by('name'):
            members[config] = self.object.function_set.current().filter(function_config=config).order_by(
                'person__last_name',
                'person__first_name'
            )
        context['members'] = members
        context['can_change'] = self.request.user.has_perm('members.change_team', self.object)
        context['can_delete'] = self.request.user.has_perm('members.delete_team', self.object)
        context['can_view_function'] = self.request.user.has_perm('members.view_function', self.object.structure)
        context['content_type'] = ContentType.objects.get_for_model(Team)
        return context


class TeamCreateView(PermissionRequiredMixin, BackSuccessUrl, CreateView):
    permission_required = 'members.add_team'
    model = Team
    form_class = TeamForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def get_initial(self):
        return {
            'structure': Structure.objects.is_active().for_perm('members.add_team', self.request.user).first()
        }

    def form_valid(self, form):
        team = form.save(commit=False)
        team.creation_date = now()
        team.save()
        return HttpResponseRedirect(self.get_success_url())


class TeamUpdateView(LoginRequiredMixin, BackSuccessUrl, UpdateView):
    model = Team
    form_class = TeamForm

    def get_queryset(self):
        return super().get_queryset().for_perm('members.change_team', self.request.user).is_active()

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs


class TeamDeleteView(LoginRequiredMixin, BackSuccessUrl, DeleteView):
    model = Team
    success_url = reverse_lazy('team_list')

    def get_queryset(self):
        return super().get_queryset().for_perm('members.delete_team', self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['errors'] = []
        if self.object.type.config.get(structure_type=self.object.structure.type).by_default:
            same_type = Team.objects.filter(type=self.object.type, structure=self.object.structure)
            if not same_type.exclude(id=self.object.id).exists():
                context['errors'].append("Au moins une équipe de ce type est obligatoire dans la structure.")
        if self.object.function_set.exists():
            context['errors'].append("Il existe des membres dans cette équipe.")
        if self.object.invitation_set.exists():
            context['errors'].append("Il existe des invitation dans cette équipe.")
        if self.object.roles.exists():
            context['errors'].append("Il existe des délégations de rôle sur cette équipe.")
        return context

    def form_valid(self, form):
        self.object.function_set.all().delete()
        return super().form_valid(form)


class TeamDeactivateView(LoginRequiredMixin, BackSuccessUrl, DeleteView):
    model = Team
    template_name = 'members/team_confirm_deactivate.html'

    def get_queryset(self):
        return super().get_queryset().for_perm(
            'members.change_team',
            self.request.user
        ).exclude(
            type__management=True
        ).filter(
            deactivation_date=None
        )

    def form_valid(self, form):
        self.object = self.get_object()
        self.object.deactivation_date = now()
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())


class TeamActivateView(LoginRequiredMixin, BackSuccessUrl, DeleteView):
    model = Team
    template_name = 'members/team_confirm_activate.html'

    def get_queryset(self):
        return super().get_queryset().for_perm(
            'members.change_team',
            self.request.user
        ).exclude(
            deactivation_date=None
        )

    def form_valid(self, form):
        self.object = self.get_object()
        self.object.deactivation_date = None
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())


class TeamExportView(ExportView, TeamListView):
    filename = "Équipes"

    def get_fields(self):
        return [
            ("Nom", 'name', 40),
            ("Type", 'type_display', 20),
            ("Structure", 'structure.name', 40),
            ("Statut", 'status_text', 10),
            ("Date de création", 'creation_date', 20),
            ("Date de désactivation", 'deactivation_date', 20),
        ]


class RoleConfigListView(PermissionRequiredMixin, ListView):
    permission_required = 'members.view_roleconfig'
    paginate_by = 25

    def get_queryset(self):
        return RoleConfig.objects.for_perm(
            'members.view_roleconfig', self.request.user
        ).order_by('name')


class RoleConfigDetailView(PermissionRequiredMixin, DetailView):
    permission_required = 'members.view_roleconfig'

    def get_queryset(self):
        return RoleConfig.objects.for_perm(
            'members.view_roleconfig', self.request.user
        ).order_by('name')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        for perm in ('change', 'delete'):
            context[f'can_{perm}_roleconfig'] = self.request.user.has_perm(f'members.{perm}_roleconfig', self.object)
        context['permissions'] = self.object.permissions.order_by('name')
        return context


class RoleConfigCreateView(PermissionRequiredMixin, BackSuccessUrl, CreateView):
    permission_required = 'members.add_roleconfig'
    form_class = RoleConfigForm
    model = RoleConfig


class RoleConfigUpdateView(PermissionRequiredMixin, BackSuccessUrl, UpdateView):
    permission_required = 'members.change_roleconfig'
    form_class = RoleConfigForm

    def get_queryset(self):
        return RoleConfig.objects.for_perm(
            'members.change_roleconfig', self.request.user
        ).order_by('name')


class RoleConfigDeleteView(PermissionRequiredMixin, BackSuccessUrl, DeleteView):
    permission_required = 'members.delete_roleconfig'
    success_url = reverse_lazy('roleconfig_list')

    def get_queryset(self):
        return RoleConfig.objects.for_perm(
            'members.delete_roleconfig', self.request.user
        ).order_by('name')


class RoleListView(PermissionRequiredMixin, FilterView):
    template_name = 'members/role_list.html'
    permission_required = 'members.view_role'
    filterset_class = RoleFilter
    paginate_by = 25

    def get_queryset(self):
        return Role.objects.for_perm('members.view_role', self.request.user).select_related(
            'config', 'team', 'team__structure', 'holder', 'proxy',
        )


class RoleDetailView(PermissionRequiredMixin, DetailView):
    permission_required = 'members.view_role'

    def get_queryset(self):
        return Role.objects.for_perm('members.view_role', self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['can_delete'] = self.request.user.has_perm('members.delete_role', self.object)
        context['content_type'] = ContentType.objects.get_for_model(Role)
        return context


class RoleCreateView(PermissionRequiredMixin, BackSuccessUrl, CreateView):
    permission_required = 'members.add_role'
    model = Role
    form_class = RoleForm

    def dispatch(self, request, proxy_pk=None):
        if proxy_pk:
            self.proxy = get_object_or_404(Person.objects.for_perm('members.add_role', self.request.user), pk=proxy_pk)
        else:
            self.proxy = None
        return super().dispatch(request)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        kwargs['proxy'] = self.proxy
        kwargs['force'] = self.request.POST.get('force') == 'on'
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['proxy'] = self.proxy
        return context


class RoleDelegationCreateView(LoginRequiredMixin, BackSuccessUrl, CreateView):
    model = Role
    form_class = RoleDelegationForm
    template_name = 'members/delegation_form.html'

    def dispatch(self, request, holder_pk, config_pk, team_pk):
        self.holder = get_object_or_404(Person, pk=holder_pk)
        if request.user.person == self.holder:
            if not request.user.has_perm('members.add_delegation', self.holder):
                raise PermissionError("Vous n'avez pas la permissions de déléguer vos rôles.")
        else:
            if not request.user.has_perm('members.add_role', self.holder):
                raise PermissionError("Vous n'avez pas la permissions d'attribuer des rôles.")
        self.config = get_object_or_404(RoleConfig, pk=config_pk)
        if not self.config.delegation:
            raise PermissionDenied("Ce rôle ne peut pas être délégué.")
        self.team = get_object_or_404(Team, pk=team_pk)
        if (self.config, self.team, True) not in self.holder.roles \
                and (self.config, self.team, False) not in self.holder.roles:
            raise PermissionDenied(f"{self.holder} n'a pas le rôle qu'il ou elle souhaite déléguer.")
        return super().dispatch(request)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        kwargs['holder'] = self.holder
        kwargs['config'] = self.config
        kwargs['team'] = self.team
        kwargs['force'] = self.request.POST.get('force') == 'on'
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['holder'] = self.holder
        context['config'] = self.config
        context['team'] = self.team
        return context


class RoleDeleteView(PermissionRequiredMixin, BackSuccessUrl, DeleteView):
    permission_required = 'members.delete_role'

    def get_queryset(self):
        return Role.objects.for_proxy_perm('members.delete_role', self.request.user).distinct()


class RoleDelegationDeleteView(LoginRequiredMixin, BackSuccessUrl, DeleteView):
    template_name = 'members/delegation_confirm_delete.html'
    model = Role

    def get_object(self, queryset=None):
        role = super().get_object(queryset)
        if self.request.user.person == role.holder:
            if not self.request.user.has_perm('members.delete_delegation', role.holder):
                raise PermissionError("Vous n'avez pas la permissions de supprimer vos délégations de rôles.")
        else:
            if not self.request.user.has_perm('members.delete_role', role.holder):
                raise PermissionError("Vous n'avez pas la permissions de supprimer des rôles.")
        return role


class PermissionListView(PermissionRequiredMixin, ListView):
    permission_required = 'members.view_roleconfig'

    def get_queryset(self):
        return Permission.objects.exclude(
            name__startswith="Can ",
        ).order_by(
            'content_type__model',
            'name',
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['role_configs'] = RoleConfig.objects.order_by('name').prefetch_related('permissions')
        return context


class LogEntryListView(PermissionRequiredMixin, FilterView):
    permission_required = 'members.view_logentry'
    filterset_class = LogEntryFilter
    template_name = 'members/logentry_list.html'
    paginate_by = 25

    def get_queryset(self):
        return LogEntry.objects.select_related('actor')


class PaymentListView(PermissionRequiredMixin, FilterView):
    permission_required = 'members.view_payment'
    filterset_class = PaymentFilter
    template_name = 'members/payment_list.html'
    paginate_by = 25

    def get_queryset(self):
        return Payment.objects \
            .for_perm("members.view_payment", self.request.user) \
            .filter(online_status__in=(PaymentStatus.MANUAL, PaymentStatus.SUCCESS)) \
            .select_related(
                'payer',
                'receipt',
            ).distinct()


class PaymentDetailView(PermissionRequiredMixin, DetailView):
    permission_required = 'members.view_payment'

    def get_queryset(self):
        return Payment.objects \
            .for_perm("members.view_payment", self.request.user) \
            .filter(online_status__in=(PaymentStatus.MANUAL, PaymentStatus.SUCCESS))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['content_type'] = ContentType.objects.get_for_model(Payment)
        context['can_change'] = self.request.user.has_perm('members.change_payment', self.object)
        context['can_delete'] = self.request.user.has_perm('members.delete_payment', self.object)
        return context


class PaymentExportView(ExportView, PaymentListView):
    filename = "Paiements"

    def get_fields(self):
        return [
            ("Montant", 'amount', 12),
            ("Date", 'date', 12),
            ("Mode", 'method_display', 15),
            ("Payeur·se", 'payer', 50),
            ("Adhérent·e", 'adherents_display', 50),
            ("Saison", 'seasons_display', 15),
            ("Reçu fiscal", 'receipt', 15),
        ]


class AccountingView(PermissionRequiredMixin, FilterView):
    permission_required = 'members.view_payment'
    filterset_class = AccountingFilter
    template_name = 'members/accounting.html'
    paginate_by = 25

    def get_queryset(self):
        qs = Structure.objects.for_perm('members.view_transferorder', self.request.user)
        if not self.request.user.has_perm('members.view_inactive_structure'):
            qs = qs.filter(status__in=(Structure.STATUS_AUTONOMOUS, Structure.STATUS_GUARDIANSHIP))
        return qs.order_by('name', 'pk')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        year = self.filterset.form.cleaned_data['year']
        context['year'] = year
        total = {
            'allocation_contribution': 0,
            'allocation_donation': 0,
            'transfer_contribution': 0,
            'transfer_donation': 0,
        }
        for structure in context['page_obj'] or context['object_list']:
            allocations = Allocation.objects.filter(
                payment__date__year=year,
                payment__payer__isnull=False,
            )
            if structure.id == 1:
                allocations = allocations.filter(
                    Q(adhesion__structure_id=1, payment__online_status=PaymentStatus.MANUAL) |
                    Q(payment__online_status=PaymentStatus.SUCCESS),
                )
            else:
                allocations = allocations.filter(
                    adhesion__structure=structure,
                    payment__online_status=PaymentStatus.MANUAL,
                )
            allocations = allocations.aggregate(
                contribution=Sum('amount', filter=Q(category=Allocation.CATEGORY_CONTRIBUTION)),
                donation=Sum('amount', filter=Q(category=Allocation.CATEGORY_DONATION)),
            )
            structure.allocation_contribution = allocations['contribution']
            structure.allocation_donation = allocations['donation']
            total['allocation_contribution'] += allocations['contribution'] or 0
            total['allocation_donation'] += allocations['donation'] or 0
            transfers = Transfer.objects.filter(
                structure=structure,
                order__date__year=year,
            ).aggregate(
                contribution=Sum('contribution'),
                donation=-Sum('donation'),
            )
            structure.transfer_contribution = transfers['contribution']
            structure.transfer_donation = transfers['donation']
            total['transfer_contribution'] += transfers['contribution'] or 0
            total['transfer_donation'] += transfers['donation'] or 0
        context['total'] = total
        return context


class AccountingExportView(ExportView, AccountingView):
    filename = "Comptabilisation des adhésions"

    def get_fields(self):
        return [
            ("Structure", 'name', 50),
            ("Cotisations perçues (7560000)", 'allocation_contribution', 30),
            ("Cotisations prélevées (6561009)", 'transfer_contribution', 30),
            ("Dons directs (7540000)", 'allocation_donation', 30),
            ("Dons reversés (7540009)", 'transfer_donation', 30),
        ]

    def get_total(self, context):
        return [
            "Total",
            context['total']['allocation_contribution'],
            context['total']['allocation_donation'],
            context['total']['transfer_contribution'],
            context['total']['transfer_donation'],
        ]


class AccountingDetailView(PermissionRequiredMixin, TemplateView):
    permission_required = 'members.view_transferorder'
    template_name = 'members/accounting_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        form = AccountingDetailFilter(self.request.GET, self.request)
        context['form'] = form
        if not form.is_valid():
            return context
        year = form.cleaned_data['year']
        structure = form.cleaned_data['structure']
        context['year'] = year
        context['structure'] = structure
        allocations = Allocation.objects.filter(
            payment__date__year=year,
            payment__payer__isnull=False,
        )
        if structure.id == 1:
            allocations = allocations.filter(
                Q(adhesion__structure_id=1, payment__online_status=PaymentStatus.MANUAL) |
                Q(payment__online_status=PaymentStatus.SUCCESS),
            )
        else:
            allocations = allocations.filter(
                adhesion__structure=structure,
                payment__online_status=PaymentStatus.MANUAL,
            )
        allocations = allocations.select_related(
            'payment',
            'payment__payer',
            'payment__payer__adherent',
            'payment__payer__employee',
            'adhesion',
            'adhesion__adherent',
            'adhesion__adherent__person',
        ).order_by(
            'payment__date',
            'id',
        )
        context['allocations_contribution'] = allocations.filter(category=Allocation.CATEGORY_CONTRIBUTION)
        context['total_allocations_contribution'] = sum(
            (allocation.amount for allocation in context['allocations_contribution'])
        )
        context['allocations_donation'] = allocations.filter(category=Allocation.CATEGORY_DONATION)
        context['total_allocations_donation'] = sum(
            (allocation.amount for allocation in context['allocations_donation'])
        )
        entries = Entry.objects.filter(
            transfer__structure=structure,
            transfer__order__date__year=year,
        ).select_related(
            'transfer',
            'transfer__order',
            'adhesion',
            'adhesion__adherent',
            'adhesion__adherent__person',
        ).order_by(
            'adhesion__entry_date',
        )
        context['entries_contribution'] = entries.filter(category=Entry.CATEGORY_CONTRIBUTION)
        context['total_entries_contribution'] = sum((entry.amount for entry in context['entries_contribution']))
        context['entries_donation'] = entries.filter(category=Entry.CATEGORY_DONATION)
        context['total_entries_donation'] = sum((entry.donation_positive for entry in context['entries_donation']))
        return context


class CommuneListView(FilterView):
    filterset_class = CommuneFilter
    template_name = 'members/commune_list.html'
    paginate_by = 25
    model = Commune


class CountryListView(FilterView):
    filterset_class = CountryFilter
    template_name = 'members/country_list.html'
    paginate_by = 25
    model = Country


class LoginView(BaseLoginView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        next = self.request.GET.get('next')
        url = urlparse(next)
        query = parse_qs(url.query)
        client_id = query.get('client_id', [''])[0]
        try:
            context['oauth2_application'] = Application.objects.get(client_id=client_id)
        except Application.DoesNotExist:
            pass
        context['SUPPORT_EMAIL'] = settings.SUPPORT_EMAIL
        return context


class LogoutView(BaseLogoutView):
    http_method_names = ["get", "head", "post", "options"]

    def get(self, request):
        return render(request, 'registration/logout.html')


class PasswordResetView(BasePasswordResetView):
    form_class = PasswordResetForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['SUPPORT_EMAIL'] = settings.SUPPORT_EMAIL
        return context


class EmailAsLoginView(LoginRequiredMixin, BackSuccessUrl, FormView):
    form_class = EmailAsLoginForm
    template_name = 'members/email_as_login.html'
    model = Person

    def dispatch(self, request, pk):
        person = get_object_or_404(Person, pk=pk)
        if not request.user.has_perm('members.change_person', person):
            return self.handle_no_permission()
        self.email = person.email
        return super().dispatch(request, pk)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['email'] = self.email
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['email'] = self.email
        return context

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)
