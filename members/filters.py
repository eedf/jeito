# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout
import datetime
from dal import autocomplete, forward
from functools import reduce
from operator import and_
from psycopg2.extras import DateRange
from django import forms
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.core.validators import EMPTY_VALUES
from django.db.models import Q, Value, Min
from auditlog.models import LogEntry
import django_filters
from .utils import first_season, current_season, today, yesterday, current_year, replace_year, remove_accents
from .models import Adhesion, Structure, Function, Person, Invitation, Team, TeamType, FunctionType, Commune, Country, \
    StructureType, Employment, FunctionCategory, Employee, FunctionStatus, Adherent, Receipt, RateCategory, Payment, \
    Role


class NotEmptyStringFilter(django_filters.BooleanFilter):
    def filter(self, qs, value):
        if value in EMPTY_VALUES:
            return qs

        exclude = self.exclude ^ (value is True)
        method = qs.exclude if exclude else qs.filter

        return method(**{self.field_name: ""})


class SearchFilter(django_filters.CharFilter):
    def filter(self, qs, value):
        if value:
            return qs.search(value)
        else:
            return qs.annotate(similarity=Value(1))


class MandatorySearchFilter(django_filters.CharFilter):
    def filter(self, qs, value):
        if value:
            return qs.search(value)
        else:
            return qs.annotate(similarity=Value(1)).none()


class FilterForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.attrs = {'id': 'filter'}
        self.helper.form_class = 'form-inline'
        self.helper.form_method = 'get'
        self.helper.layout = Layout(*self.fields.keys())


class StructureFilter(django_filters.FilterSet):
    status = django_filters.ChoiceFilter(label="Statut", choices=(), method='filter_status', empty_label='Toutes')
    q = SearchFilter(label="Nom")

    class Meta:
        model = Structure
        fields = ('status', 'type', 'q')
        form = FilterForm

    def __init__(self, data, *args, **kwargs):
        data = data.copy() if data else {}
        data.setdefault('status', '100')
        super().__init__(data, *args, **kwargs)
        choices = (
            (100, "Actives"),
            (Structure.STATUS_AUTONOMOUS, "⤷ Autonomes"),
            (Structure.STATUS_GUARDIANSHIP, "⤷ Rattachées"),
        )
        if self.request.user.has_perm('members.view_inactive_structure'):
            choices += (
                (101, "Inactives"),
                (Structure.STATUS_DORMANT, "⤷ En sommeil"),
                (Structure.STATUS_CLOSED, "⤷ Fermées"),
            )
        self.filters['status'].field.choices = choices

    def filter_status(self, qs, name, value):
        if value == '100':
            qs = qs.is_active()
        elif value == '101':
            qs = qs.is_inactive()
        elif value:
            qs = qs.filter(status=value)
        return qs


class PersonOrderingFilter(django_filters.OrderingFilter):
    def __init__(self, *args, **kwargs):
        kwargs['choices'] = (
            ('-similarity', "Pertinence"),
            ('last_name', "Nom"),
            ('first_name', "Prénom"),
        )
        kwargs['fields'] = {
            'similarity': 'similarity',
            'last_name': 'last_name',
            'first_name': 'first_name',
        }
        super().__init__(*args, **kwargs)

    def filter(self, qs, value):
        if not value or value == ['-similarity']:
            value = ['-similarity', 'last_name', 'first_name']
        if value == ['last_name']:
            value = ['last_name', 'first_name']
        if value == ['first_name']:
            value = ['first_name', 'last_name']
        return super().filter(qs, value)


class PersonFilter(django_filters.FilterSet):
    q = MandatorySearchFilter(label="Nom/Prénom/Numéro/Email")
    o = PersonOrderingFilter(label="Tri", empty_label=None)

    class Meta:
        model = Person
        fields = ('q', 'o')
        form = FilterForm

    def __init__(self, data, *args, **kwargs):
        data = data.copy() if data else {}
        data.setdefault('q', '')
        super().__init__(data, *args, **kwargs)


class CommuneOrderingFilter(django_filters.OrderingFilter):
    def __init__(self, *args, **kwargs):
        kwargs['choices'] = (
            ('-similarity', "Pertinence"),
            ('name', "Nom"),
            ('code', "Département"),
        )
        kwargs['fields'] = {
            'similarity': 'similarity',
            'name': 'name',
            'code': 'code',
        }
        super().__init__(*args, **kwargs)

    def filter(self, qs, value):
        if not value or value == ['-similarity']:
            value = ['-similarity', 'name']
        return super().filter(qs, value)


class CommuneFilter(django_filters.FilterSet):
    q = MandatorySearchFilter(label="Nom")
    o = CommuneOrderingFilter(label="Tri", empty_label=None)

    class Meta:
        model = Commune
        fields = ('q', 'o')
        form = FilterForm

    def __init__(self, data, *args, **kwargs):
        data = data.copy() if data else {}
        data.setdefault('q', '')
        super().__init__(data, *args, **kwargs)


class CountryOrderingFilter(django_filters.OrderingFilter):
    def __init__(self, *args, **kwargs):
        kwargs['choices'] = (
            ('-similarity', "Pertinence"),
            ('name', "Nom"),
        )
        kwargs['fields'] = {
            'similarity': 'similarity',
            'name': 'name',
        }
        super().__init__(*args, **kwargs)

    def filter(self, qs, value):
        if not value or value == ['-similarity']:
            value = ['-similarity', 'name']
        return super().filter(qs, value)


class CountryFilter(django_filters.FilterSet):
    q = MandatorySearchFilter(label="Nom")
    o = CountryOrderingFilter(label="Tri", empty_label=None)

    class Meta:
        model = Country
        fields = ('q', 'o')
        form = FilterForm

    def __init__(self, data, *args, **kwargs):
        data = data.copy() if data else {}
        data.setdefault('q', '')
        super().__init__(data, *args, **kwargs)


class AdherentOrderingFilter(django_filters.OrderingFilter):
    def __init__(self, *args, **kwargs):
        kwargs['choices'] = (
            ('-similarity', "Pertinence"),
            ('last_name', "Nom"),
            ('first_name', "Prénom"),
            ('id', "Numéro"),
            ('structure', "Structure"),
        )
        kwargs['fields'] = {
            'similarity': 'similarity',
            'person__last_name': 'last_name',
            'person__first_name': 'first_name',
            'id': 'id',
            'last_adhesion__structure__name': 'structure',
        }
        super().__init__(*args, **kwargs)

    def filter(self, qs, value):
        if not value or value == ['-similarity']:
            value = ['-similarity', 'last_name', 'first_name', 'id']
        if value == ['last_name']:
            value = ['last_name', 'first_name', 'id']
        if value == ['first_name']:
            value = ['first_name', 'last_name', 'id']
        if value == ['structure']:
            value = ['structure', 'last_name', 'first_name', 'id']
        return super().filter(qs, value)


class AdherentFilter(django_filters.FilterSet):
    season = django_filters.ChoiceFilter(label="Saison", choices=(), empty_label="Toutes",
                                         field_name='adhesions__season')
    structure = django_filters.ModelChoiceFilter(label="Structure", method='filter_structure',
                                                 queryset=Structure.objects.none())
    q = SearchFilter(label="Nom/Prénom/Numéro/Email")
    o = AdherentOrderingFilter(label="Tri", empty_label=None)

    class Meta:
        model = Adherent
        fields = ('season', 'structure', 'q', 'o')
        form = FilterForm

    def __init__(self, data, *args, **kwargs):
        data = data.copy() if data else {}
        data.setdefault('season', current_season())

        super().__init__(data, *args, **kwargs)

        # season
        end_season = current_season()
        begin_season = first_season()
        self.filters['season'].field.choices = [
            (season, "{}/{}".format(season - 1, season))
            for season in range(end_season, begin_season - 1, -1)
        ]

        self.user = kwargs['request'].user
        self.filters['structure'].field.queryset = self.get_structures()
        self.filters['structure'].field.widget = autocomplete.ModelSelect2(
            url='structure-autocomplete',
            attrs={'data-placeholder': 'Toutes'},
            forward=[
                forward.Const('members.view_adherent', 'for_perm'),
            ]
        )

    def get_structures(self):
        return Structure.objects \
            .for_perm('members.view_adhesion', self.user) \
            .is_active()

    def filter_structure(self, qs, name, value):
        if value:
            qs = qs.filter(
                last_adhesion__structure__lft__gte=value.lft,
                last_adhesion__structure__lft__lte=value.rght
            )
        return qs


class AdhesionOrderingFilter(django_filters.OrderingFilter):
    def __init__(self, *args, **kwargs):
        kwargs['choices'] = (
            ('last_name', "Nom"),
            ('first_name', "Prénom"),
            ('id', "Numéro"),
            ('structure', "Structure"),
            ('date', "Date"),
            ('entry_date', "Saisie"),
        )
        kwargs['fields'] = {
            'adherent__person__last_name': 'last_name',
            'adherent__person__first_name': 'first_name',
            'adherent__id': 'id',
            'structure__name': 'structure',
            '-dates__startswith': 'date',
            '-entry_date': 'entry_date',
        }
        super().__init__(*args, **kwargs)

    def filter(self, qs, value):
        if not value or value == ['last_name']:
            value = ['last_name', 'first_name', 'id']
        if value == ['first_name']:
            value = ['first_name', 'last_name', 'id']
        if value == ['structure']:
            value = ['structure', 'last_name', 'first_name', 'id']
        if value == ['date']:
            value = ['date', 'last_name', 'first_name', 'id']
        if value == ['entry_date']:
            value = ['entry_date', 'last_name', 'first_name', 'id']
        return super().filter(qs, value)


class AdhesionFilter(django_filters.FilterSet):
    season = django_filters.ChoiceFilter(label="Saison", choices=(), empty_label="Toutes",
                                         field_name='season')
    structure = django_filters.ModelChoiceFilter(label="Structure", method='filter_structure',
                                                 queryset=Structure.objects.none())
    is_paid = django_filters.BooleanFilter(label="Payé", method='filter_is_paid')
    adherent = django_filters.ModelChoiceFilter(label="Adhérent·e", field_name='adherent',
                                                queryset=Adherent.objects.none())
    above16 = django_filters.BooleanFilter(label="≥ 16 ans", method='filter_above16', widget=forms.CheckboxInput())
    o = AdhesionOrderingFilter(label="Tri", empty_label=None)

    class Meta:
        model = Adhesion
        fields = ('season', 'structure', 'is_paid', 'adherent', 'o', 'above16')
        form = FilterForm

    def __init__(self, data, *args, **kwargs):
        data = data.copy() if data else {}
        data.setdefault('season', current_season())
        super().__init__(data, *args, **kwargs)
        self.user = kwargs['request'].user

        # season
        end_season = current_season()
        begin_season = first_season()
        self.filters['season'].field.choices = [
            (season, "{}/{}".format(season - 1, season))
            for season in range(end_season, begin_season - 1, -1)
        ]

        # structure
        self.filters['structure'].field.queryset = self.get_structures()
        self.filters['structure'].field.widget = autocomplete.ModelSelect2(
            url='structure-autocomplete',
            attrs={'data-placeholder': 'Toutes'},
            forward=[
                forward.Const('members.view_adhesion', 'for_perm'),
            ]
        )

        # adherent
        self.filters['adherent'].field.queryset = Adherent.objects.for_perm('members.view_adhesion', self.user)
        self.filters['adherent'].field.widget = autocomplete.ModelSelect2(
            url='adherent-autocomplete',
            forward=[
                'structure',
                forward.Const('members.view_adhesion', 'for_perm')
            ],
            attrs={'data-placeholder': 'Tou·tes'}
        )

    def get_structures(self):
        return Structure.objects \
            .for_perm('members.view_adhesion', self.user) \
            .is_active()

    def filter_structure(self, qs, name, value):
        if value:
            qs = qs.filter(
                structure__lft__gte=value.lft,
                structure__lft__lte=value.rght
            )
        return qs

    def filter_is_paid(self, qs, name, value):
        if value is not None:
            return qs.filter(is_paid=value)
        return qs

    def filter_above16(self, qs, name, value):
        if value:
            today_ = today()
            qs = qs.filter(adherent__person__birthdate__lte=replace_year(today_, today_.year - 16))
        return qs


class EmployeeOrderingFilter(django_filters.OrderingFilter):
    def __init__(self, *args, **kwargs):
        kwargs['choices'] = (
            ('-similarity', "Pertinence"),
            ('last_name', "Nom"),
            ('first_name', "Prénom"),
            ('id', "Numéro")
        )
        kwargs['fields'] = {
            'similarity': 'similarity',
            'person__last_name': 'last_name',
            'person__first_name': 'first_name',
            'id': 'id',
        }
        super().__init__(*args, **kwargs)

    def filter(self, qs, value):
        if not value or value == ['-similarity']:
            value = ['-similarity', 'last_name', 'first_name', 'id']
        if value == ['last_name']:
            value = ['last_name', 'first_name', 'id']
        if value == ['first_name']:
            value = ['first_name', 'last_name', 'id']
        return super().filter(qs, value)


class EmployeeFilter(django_filters.FilterSet):
    season = django_filters.ChoiceFilter(label="Saison", choices=(), empty_label="Toutes",
                                         field_name='season', method='filter_season')
    q = SearchFilter(label="Nom/Prénom/Numéro/Email")
    o = EmployeeOrderingFilter(label="Tri", empty_label=None)

    class Meta:
        model = Employee
        fields = ('season', 'q', 'o')
        form = FilterForm

    def __init__(self, data, *args, **kwargs):
        data = data.copy() if data else {}
        data.setdefault('season', current_season())
        super().__init__(data, *args, **kwargs)
        self.user = kwargs['request'].user

        # season
        end_season = current_season()
        begin_season = first_season()
        self.filters['season'].field.choices = [
            (season, "{}/{}".format(season - 1, season))
            for season in range(end_season, begin_season - 1, -1)
        ]

    def filter_season(self, qs, name, value):
        if value:
            qs = qs.for_season(int(value))
        return qs


class EmploymentOrderingFilter(django_filters.OrderingFilter):
    def __init__(self, *args, **kwargs):
        kwargs['choices'] = (
            ('last_name', "Nom"),
            ('first_name', "Prénom"),
            ('type', "Type"),
            ('begin', "Du"),
            ('end', "Au"),
        )
        kwargs['fields'] = {
            'employee__person__last_name': 'last_name',
            'employee__person__first_name': 'first_name',
            'type': 'type',
            'dates__startswith': 'begin',
            'dates__endswith': 'end',
        }
        super().__init__(*args, **kwargs)

    def filter(self, qs, value):
        if not value or value == ['last_name']:
            value = ['last_name', 'first_name']
        if value == ['first_name']:
            value = ['first_name', 'last_name']
        if value == ['type']:
            value = ['type', 'last_name', 'first_name']
        if value == ['begin']:
            value = ['begin', 'last_name', 'first_name']
        if value == ['end']:
            value = ['end', 'last_name', 'first_name']
        return super().filter(qs, value)


class EmploymentFilter(django_filters.FilterSet):
    season = django_filters.ChoiceFilter(label="Saison", choices=(), empty_label="Toutes",
                                         field_name='season', method='filter_season')
    employee = django_filters.ModelChoiceFilter(label="Salarié·e", field_name='employee',
                                                queryset=Employee.objects.none())
    name = django_filters.CharFilter(label="Intitulé", method='filter_name')
    o = EmploymentOrderingFilter(label="Tri", empty_label=None)

    class Meta:
        model = Employment
        fields = ('season', 'type', 'employee', 'name', 'o')
        form = FilterForm

    def __init__(self, data, *args, **kwargs):
        data = data.copy() if data else {}
        data.setdefault('season', current_season())
        super().__init__(data, *args, **kwargs)
        self.user = kwargs['request'].user

        # season
        end_season = current_season()
        begin_season = first_season()
        self.filters['season'].field.choices = [
            (season, "{}/{}".format(season - 1, season))
            for season in range(end_season, begin_season - 1, -1)
        ]

        # employee
        self.filters['employee'].field.queryset = Employee.objects.for_perm('members.view_employment', self.user)
        self.filters['employee'].field.widget = autocomplete.ModelSelect2(
            url='employee-autocomplete',
            forward=[
                forward.Const('members.view_employment', 'for_perm')
            ],
            attrs={'data-placeholder': 'Toutes'}
        )

    def filter_season(self, qs, name, value):
        if value:
            qs = qs.for_season(int(value))
        return qs

    def filter_name(self, qs, name, value):
        return qs.filter(name__unaccent__icontains=remove_accents(value))


class UnpaidOrderingFilter(django_filters.OrderingFilter):
    def __init__(self, *args, **kwargs):
        kwargs['choices'] = (
            ('last_name', "Nom"),
            ('first_name', "Prénom"),
            ('id', "Numéro"),
            ('structure', "Structure"),
            ('date', "Date de saisie"),
        )
        kwargs['fields'] = {
            'adherent__person__last_name': 'last_name',
            'adherent__person__first_name': 'first_name',
            'adherent__id': 'id',
            'structure__name': 'structure',
            'entry_date': 'date',
        }
        super().__init__(*args, **kwargs)

    def filter(self, qs, value):
        if not value or value == ['last_name']:
            value = ['last_name', 'first_name', 'id']
        if value == ['first_name']:
            value = ['first_name', 'last_name', 'id']
        if value == ['structure']:
            value = ['structure', 'last_name', 'first_name', 'id']
        if value == ['date']:
            value = ['date', 'last_name', 'first_name', 'id']
        return super().filter(qs, value)


class UnpaidFilter(django_filters.FilterSet):
    season = django_filters.ChoiceFilter(label="Saison", choices=(), empty_label=None,
                                         field_name='season')
    structure = django_filters.ModelChoiceFilter(label="Structure", method='filter_structure',
                                                 queryset=Structure.objects.none())
    q = django_filters.CharFilter(label="Nom/Prénom/Numéro", method='filter_q')
    o = UnpaidOrderingFilter(label="Tri", empty_label=None)

    class Meta:
        model = Adhesion
        fields = ('season', 'structure', 'q', 'o')
        form = FilterForm

    def __init__(self, data, *args, **kwargs):
        data = data.copy() if data else {}
        data.setdefault('season', current_season())
        data.setdefault('o', 'date')
        super().__init__(data, *args, **kwargs)
        self.user = kwargs['request'].user

        # season
        current_season_ = current_season()
        self.filters['season'].field.choices = [
            (season, "{}/{}".format(season - 1, season))
            for season in range(current_season_, current_season_ - settings.RGPD_DELAY - 1, -1)
        ]

        # structure
        self.filters['structure'].field.queryset = self.get_structures()
        self.filters['structure'].field.widget = autocomplete.ModelSelect2(
            url='structure-autocomplete',
            attrs={'data-placeholder': 'Toutes'}
        )

    def get_structures(self):
        return Structure.objects \
            .for_perm('members.view_unpaid_adhesion', self.user) \
            .is_active()

    def filter_structure(self, qs, name, value):
        if value:
            qs = qs.filter(
                structure__lft__gte=value.lft,
                structure__lft__lte=value.rght
            )
        return qs

    def filter_q(self, qs, name, value):
        return qs.search(value)


class AdherentOrderingFilter(django_filters.OrderingFilter):
    def __init__(self, *args, **kwargs):
        kwargs['choices'] = (
            ('last_name', "Nom"),
            ('first_name', "Prénom"),
            ('structure', "Structure"),
        )
        kwargs['fields'] = {
            'person__last_name': 'last_name',
            'person__first_name': 'first_name',
            'last_adhesion__structure__name': 'structure',
        }
        super().__init__(*args, **kwargs)

    def filter(self, qs, value):
        if not value or value == ['last_name']:
            value = ['last_name', 'first_name']
        if value == ['first_name']:
            value = ['first_name', 'last_name']
        if value == ['structure']:
            value = ['structure', 'last_name', 'first_name']
        return super().filter(qs, value)


class StatsAdhesionFilter(django_filters.FilterSet):
    rate_choices = [(None, "Tous")] + RateCategory.choices
    structures = Structure.objects.is_active()

    season = django_filters.ChoiceFilter(label="Saison", choices=(), empty_label=None)
    structure = django_filters.ModelChoiceFilter(label="Structure", empty_label="Toutes", method='filter_structure',
                                                 queryset=structures)
    age_min = django_filters.NumberFilter(label="Age min", method='filter_age_min')
    age_max = django_filters.NumberFilter(label="Age max", method='filter_age_max')
    function_category = django_filters.ChoiceFilter(label="Catégorie de fonction", empty_label="Toutes",
                                                    choices=FunctionCategory.choices, method='filter_function_category')
    rate = django_filters.ChoiceFilter(field_name='rate__category', label="Tarif",
                                       choices=RateCategory.choices, empty_label="Tous")
    date = django_filters.ChoiceFilter(label="Au", empty_label=None, method='filter_date')

    class Meta:
        model = Adhesion
        fields = ('season', 'structure', 'age_min', 'age_max', 'function_category', 'rate', 'date')
        form = FilterForm

    def __init__(self, data, *args, **kwargs):
        data = data.copy()
        if not data.get('season'):
            data['season'] = str(current_season())
        if not data.get('date'):
            data['date'] = 'ajd'
        if kwargs.pop('ref', False):
            data['season'] = int(data['season']) - 1
            _first_season = first_season() - 1
        else:
            _first_season = first_season()
        super().__init__(data, *args, **kwargs)
        self.filters['season'].field.choices = [
            (i, "{}/{}".format(i - 1, i)) for i in range(current_season(), _first_season, -1)
        ]

        # Structures
        self.filters['structure'].field.widget = autocomplete.ModelSelect2(
            url='structure-autocomplete',
            forward=[
                forward.Const('on', 'is_active'),
            ],
            attrs={'data-placeholder': 'Toutes'},
        )

        self.filters['date'].field.choices = (('fin', "31/08"), ('ajd', format(yesterday().strftime('%d/%m'))))

    @property
    def qs(self):
        return super().qs.filter(canceled=False)

    def filter_function_category(self, qs, name, value):
        if not value:
            return qs
        date = self.form.cleaned_data.get('date')
        season = self.form.cleaned_data.get('season')
        if not date or not season:
            return qs.none()
        begin = datetime.date(int(season) - 1, 9, 1)
        if date == 'ajd':
            end = datetime.date(
                int(season) if yesterday().month <= 8 else int(season) - 1,
                yesterday().month,
                yesterday().day,
            )
        elif date == 'fin':
            end = datetime.date(int(season), 8, 31)
        return qs.filter(Q(
            adherent__person__functions__function_config__function_type__category=value,
            adherent__person__functions__dates__overlap=DateRange(begin, end, '[]'),
        ))

    def filter_structure(self, qs, name, value):
        if value:
            qs = qs.filter(
                structure__lft__gte=value.lft,
                structure__lft__lte=value.rght
            )
        return qs

    def filter_date(self, qs, name, value):
        if 'season' not in self.form.cleaned_data:
            self.date = None
            return qs.none()
        season = int(self.form.cleaned_data['season'])
        if value == 'ajd':
            self.date = yesterday()
            leap_day = self.date.month == 2 and self.date.day == 29
            self.date = self.date.replace(
                year=season if self.date.month <= 8 else season - 1,
                day=28 if leap_day else self.date.day
            )
            qs = qs.filter(entry_date__lte=self.date)
        else:
            self.date = datetime.date(season, 8, 31)
        return qs

    def filter_age_min(self, qs, name, value):
        if not value:
            return qs
        date = self.form.cleaned_data.get('date')
        season = self.form.cleaned_data.get('season')
        if not date or not season:
            return qs.none()
        if date == 'ajd':
            qs = qs.with_age(datetime.date(
                int(season) if yesterday().month <= 8 else int(season) - 1,
                yesterday().month,
                yesterday().day,
            ))
        elif date == 'fin':
            qs = qs.with_age(datetime.date(int(season), 8, 31))
        return qs.filter(qs_age__gte=Value(value))

    def filter_age_max(self, qs, name, value):
        if not value:
            return qs
        date = self.form.cleaned_data.get('date')
        season = self.form.cleaned_data.get('season')
        if not date or not season:
            return qs.none()
        if date == 'ajd':
            qs = qs.with_age(datetime.date(
                int(season) if yesterday().month <= 8 else int(season) - 1,
                yesterday().month,
                yesterday().day,
            ))
        elif date == 'fin':
            qs = qs.with_age(datetime.date(int(season), 8, 31))
        return qs.filter(qs_age__lte=Value(value))


class StatsSfFilter(django_filters.FilterSet):
    season = django_filters.ChoiceFilter(label="Saison", choices=(), empty_label=None,
                                         field_name='adherent__adhesions__season')

    class Meta:
        model = Person
        fields = ('season', )
        form = FilterForm

    def __init__(self, data, *args, **kwargs):
        data = data.copy()
        data.setdefault('season', current_season())
        super().__init__(data, *args, **kwargs)
        self.filters['season'].field.choices = [
            (i, "{}/{}".format(i - 1, i)) for i in range(current_season(), first_season() - 1, -1)
        ]


class FunctionOrderingFilter(django_filters.OrderingFilter):
    def __init__(self, *args, **kwargs):
        kwargs['choices'] = (
            ('gap', "Écart"),
            ('name', "Nom"),
            ('structure', "Structure"),
            ('birthdate', "Date de naissance"),
        )
        super().__init__(*args, **kwargs)

    def filter(self, qs, value):
        if not value or value == ['gap']:
            return qs.order_by(
                '-abs_gap',
                'gap',
                'function_config__name',
                'person__last_name'
            )
        if value == ['name']:
            return qs.order_by(
                'person__last_name',
                'person__first_name'
            )
        if value == ['structure']:
            return qs.order_by(
                'structure__left',
                '-abs_gap',
                'gap',
                'person__last_name'
            )
        if value == ['birthdate']:
            return qs.order_by(
                'person__birthdate'
            )


class InconsistenciesFilter(django_filters.FilterSet):
    season = django_filters.ChoiceFilter(label="Saison", choices=(), empty_label="Toutes",
                                         method='filter_season')
    gap = django_filters.NumberFilter(label="Écart min", method='filter_gap')
    o = FunctionOrderingFilter(label="Tri", empty_label=None, required=True)

    class Meta:
        model = Function
        fields = ('season', 'gap', 'o')
        form = FilterForm

    def __init__(self, data, *args, **kwargs):
        data = data.copy() if data else {}
        data.setdefault('season', current_season())
        data.setdefault('o', 'gap')
        data.setdefault('gap', 2)
        super().__init__(data, *args, **kwargs)

        # season
        end_season = current_season()
        begin_season = first_season()
        self.filters['season'].field.choices = [
            (season, "{}/{}".format(season - 1, season))
            for season in range(end_season, begin_season - 1, -1)
        ]

    def filter_season(self, qs, name, value):
        if value:
            qs = qs.for_season(int(value))
        return qs

    def filter_gap(self, queryset, name, value):
        return queryset.filter(abs_gap__gte=value)


class InvitationFilter(django_filters.FilterSet):
    structure = django_filters.ModelChoiceFilter(label="Structure", method='filter_structure',
                                                 queryset=Structure.objects.none())
    team = django_filters.ModelChoiceFilter(label="Équipe", queryset=Team.objects.none(), empty_label="Toutes")
    function_type = django_filters.ModelChoiceFilter(label="Type de fonction",
                                                     field_name='function_config__function_type',
                                                     queryset=FunctionType.objects.all())

    def __init__(self, data, *args, **kwargs):
        super().__init__(data or {}, *args, **kwargs)
        self.user = kwargs['request'].user

        # Structures
        self.filters['structure'].field.queryset = Structure.objects \
            .for_perm('members.add_invitation', self.user) \
            .is_active()
        self.filters['structure'].field.widget = autocomplete.ModelSelect2(
            url='structure-autocomplete',
            forward=[
                forward.Const('members.add_invitation', 'for_perm'),
                forward.Const('on', 'is_active'),
            ],
            attrs={'data-placeholder': 'Toutes'},
        )

        # team
        self.filters['team'].field.queryset = Team.objects \
            .for_perm('members.add_invitation', self.user) \
            .is_active()
        self.filters['team'].field.widget = autocomplete.ModelSelect2(
            url='team-autocomplete',
            forward=[
                'structure', forward.Field('team_type', 'type'),
                forward.Const('members.add_invitation', 'for_perm'),
                forward.Const('on', 'is_active'),
            ],
            attrs={'data-placeholder': 'Toutes'},
        )

        # person
        self.filters['person'].field.queryset = Person.objects.for_perm('members.view_invitation', self.user)
        self.filters['person'].field.widget = autocomplete.ModelSelect2(
            url='person-autocomplete',
            forward=[
                'structure',
                forward.Const(False, 'outside'),
                forward.Const('members.view_invitation', 'for_perm')
            ],
            attrs={'data-placeholder': 'Toutes'}
        )

    class Meta:
        model = Invitation
        fields = ('structure', 'team', 'function_type', 'person', 'email')
        form = FilterForm

    def filter_structure(self, qs, name, value):
        if value:
            qs = qs.filter(
                structure__lft__gte=value.lft,
                structure__lft__lte=value.rght
            )
        return qs


class CreateInvitationFilter(django_filters.FilterSet):
    structure = django_filters.ModelChoiceFilter(label="Structure", method='filter_structure', required=True,
                                                 queryset=Structure.objects.none())
    team = django_filters.ModelChoiceFilter(label="Ancienne équipe", queryset=Team.objects.none(), empty_label="Toutes",
                                            method='filter_team')
    function_type = django_filters.ModelChoiceFilter(label="Ancienne fonction", queryset=FunctionType.objects.all(),
                                                     method='filter_function_type')
    q = SearchFilter(label="Nom/Prénom/Numéro/Email")
    o = PersonOrderingFilter(label="Tri", empty_label=None)

    def __init__(self, data, *args, **kwargs):
        self.user = kwargs['request'].user
        structures = Structure.objects \
            .for_perm('members.add_invitation', self.user) \
            .is_active()
        data = data.copy()

        # Structures
        if structures:
            data.setdefault('structure', structures[0].pk)
        super().__init__(data, *args, **kwargs)
        self.filters['structure'].field.queryset = structures
        self.filters['structure'].field.widget = autocomplete.ModelSelect2(
            url='structure-autocomplete',
            attrs={'allowClear': False}
        )

        # Teams
        self.filters['team'].field.queryset = Team.objects \
            .for_perm('members.add_invitation', self.user) \
            .is_active()
        self.filters['team'].field.widget = autocomplete.ModelSelect2(
            url='team-autocomplete', forward=['structure'],
            attrs={'data-placeholder': 'Toutes'}
        )

    class Meta:
        model = Person
        fields = ('structure', 'team', 'function_type', 'q', 'o')
        form = FilterForm

    def filter_structure(self, qs, name, value):
        if not value:
            return qs.none()
        return qs.filter(adherent__last_adhesion__structure=value)

    def filter_team(self, qs, name, value):
        last_season = current_season() - 1
        if value:
            qs = qs.filter(
                functions__team=value,
                functions__dates__overlap=(datetime.date(last_season - 1, 9, 1), datetime.date(last_season, 9, 1)),
            )
        return qs

    def filter_function_type(self, qs, name, value):
        last_season = current_season() - 1
        if value:
            qs = qs.filter(
                functions__function_config__function_type=value,
                functions__dates__overlap=(datetime.date(last_season - 1, 9, 1), datetime.date(last_season, 9, 1)),
            )
        return qs


class TeamOrderingFilter(django_filters.OrderingFilter):
    def __init__(self, *args, **kwargs):
        kwargs['choices'] = (
            ('name', "Nom"),
            ('type', "Type d'équipe"),
            ('structure', "Structure"),
            ('qs_headcount', "Effectif"),
            ('-qs_headcount', "Effectif (décroissant)"),
        )
        kwargs['fields'] = {
            'name': 'name',
            'type': 'type',
            'structure__name': 'structure',
            'structure__type': 'structure_type',
            'qs_headcount': 'qs_headcount',
        }
        super().__init__(*args, **kwargs)

    def filter(self, qs, value):
        if not value or value == ['type']:
            value = ['type', 'structure_type', 'structure']
        if value == ['name']:
            value = ['name', 'structure_type', 'structure']
        if value == ['structure']:
            value = ['structure', 'type', 'name']
        if value == ['headcount']:
            value = ['headcount', 'structure_type', 'structure']
        return super().filter(qs, value)


class TeamFilter(django_filters.FilterSet):
    status = django_filters.ChoiceFilter(label="Statut", choices=((0, "----------"), (1, "Active"), (2, "Inactive")),
                                         method='filter_status', empty_label=None)
    structure_type = django_filters.ModelChoiceFilter(label="Type de structure", field_name='structure__type',
                                                      queryset=StructureType.objects.all())
    structure = django_filters.ModelChoiceFilter(label="Structure", method='filter_structure',
                                                 queryset=Structure.objects.none())
    q = django_filters.CharFilter(label="Nom", method='filter_q')
    o = TeamOrderingFilter(label="Tri", empty_label=None)

    class Meta:
        model = Team
        fields = ('status', 'type', 'structure_type', 'structure', 'q')
        form = FilterForm

    def __init__(self, data, *args, **kwargs):
        self.user = kwargs['request'].user
        data = data.copy() if data else {}
        data.setdefault('status', '1')
        super().__init__(data, *args, **kwargs)
        self.filters['structure'].field.queryset = self.get_structures()
        self.filters['structure'].field.widget = autocomplete.ModelSelect2(
            url='structure-autocomplete',
            forward=[
                forward.Const('members.view_team', 'for_perm'),
                forward.Const('on', 'is_active'),
            ],
            attrs={'data-placeholder': 'Toutes'}
        )

    def get_structures(self):
        return Structure.objects \
            .for_perm('members.view_team', self.user) \
            .is_active()

    def filter_status(self, qs, name, value):
        if value == '1':
            qs = qs.is_active()
        elif value == '2':
            qs = qs.is_inactive()
        return qs

    def filter_structure(self, qs, name, value):
        if value:
            qs = qs.filter(
                structure__lft__gte=value.lft,
                structure__lft__lte=value.rght
            )
        return qs

    def filter_q(self, qs, name, value):
        return qs.filter(reduce(and_, [
            Q(name__unaccent__icontains=word)
            for word in value.split(" ")
        ]))


class FunctionOrderingFilter(django_filters.OrderingFilter):
    def __init__(self, *args, **kwargs):
        kwargs['choices'] = (
            ('last_name', "Nom"),
            ('first_name', "Prénom"),
            ('team', "Équipe"),
            ('function', "Fonction"),
            ('date', "Date"),
        )
        kwargs['fields'] = {
            'person__last_name': 'last_name',
            'person__first_name': 'first_name',
            'team__name': 'team',
            'name': 'function',
            '-dates': 'date',
        }
        super().__init__(*args, **kwargs)

    def filter(self, qs, value):
        if not value or value == ['last_name']:
            value = ['last_name', 'first_name']
        if value == ['first_name']:
            value = ['first_name', 'last_name']
        if value == ['team']:
            value = ['team', 'last_name', 'first_name']
        if value == ['function']:
            value = ['function', 'last_name', 'first_name']
        return super().filter(qs, value)


class FunctionFilter(django_filters.FilterSet):
    season = django_filters.ChoiceFilter(label="Saison", choices=(), empty_label="Toutes",
                                         method='filter_season')
    structure_type = django_filters.ModelChoiceFilter(label="Type de structure", field_name='team__structure__type',
                                                      queryset=StructureType.objects.all())
    structure = django_filters.ModelChoiceFilter(label="Structure", method='filter_structure',
                                                 queryset=Structure.objects.none())
    team_type = django_filters.ModelChoiceFilter(label="Type d'équipe", queryset=TeamType.objects.all(),
                                                 empty_label="Tous", field_name='team__type')
    team = django_filters.ModelChoiceFilter(label="Équipe", queryset=Team.objects.none(), empty_label="Toutes")
    function_type = django_filters.ModelChoiceFilter(label="Type de fonction", empty_label="Tous",
                                                     queryset=FunctionType.objects.all(),
                                                     field_name='function_config__function_type')
    category = django_filters.ChoiceFilter(label="Catégorie", empty_label="Toutes", choices=FunctionCategory.choices,
                                           field_name='function_config__function_type__category')
    status = django_filters.ChoiceFilter(label="Statut", empty_label="Tous", choices=FunctionStatus.choices)
    o = FunctionOrderingFilter(label="Tri", empty_label=None)

    class Meta:
        model = Function
        fields = ('season', 'structure_type', 'structure', 'team_type', 'team', 'category', 'function_type',
                  'status', 'person', 'o')
        form = FilterForm

    def __init__(self, data, *args, **kwargs):
        data = data.copy() if data else {}
        data.setdefault('season', current_season())
        super().__init__(data, *args, **kwargs)
        self.user = kwargs['request'].user

        # season
        end_season = current_season()
        begin_season = first_season()
        self.filters['season'].field.choices = [
            (season, "{}/{}".format(season - 1, season))
            for season in range(end_season, begin_season - 1, -1)
        ]

        # structure
        self.filters['structure'].field.queryset = self.get_structures()
        self.filters['structure'].field.widget = autocomplete.ModelSelect2(
            url='structure-autocomplete',
            forward=[
                forward.Field('structure_type', 'type'),
                forward.Const('members.view_function', 'for_perm')
            ],
            attrs={'data-placeholder': 'Toutes'}
        )

        # team
        self.filters['team'].field.queryset = self.get_teams()
        self.filters['team'].field.widget = autocomplete.ModelSelect2(
            url='team-autocomplete',
            forward=[
                'structure',
                forward.Field('team_type', 'type'),
                forward.Const('members.view_function', 'for_perm')
            ],
            attrs={'data-placeholder': 'Toutes'}
        )

        # person
        self.filters['person'].field.queryset = Person.objects.for_perm('members.view_function', self.user)
        self.filters['person'].field.widget = autocomplete.ModelSelect2(
            url='person-autocomplete',
            forward=[
                'structure',
                forward.Const(False, 'outside'),
                forward.Const('members.view_function', 'for_perm')
            ],
            attrs={'data-placeholder': 'Toutes'}
        )

    def get_structures(self):
        return Structure.objects \
            .for_perm('members.view_function', self.user)

    def get_teams(self):
        return Team.objects \
            .for_perm('members.view_function', self.user)

    def filter_season(self, qs, name, value):
        if value:
            qs = qs.for_season(int(value))
        return qs

    def filter_structure(self, qs, name, value):
        if value:
            qs = qs.filter(
                team__structure__lft__gte=value.lft,
                team__structure__lft__lte=value.rght
            )
        return qs


class ReceiptStatsFilter(django_filters.FilterSet):
    year = django_filters.ChoiceFilter(label="Année", choices=(), empty_label=None,
                                       field_name='date__year')

    class Meta:
        model = Receipt
        fields = ('year', )
        form = FilterForm

    def __init__(self, data, *args, **kwargs):
        first_year = Receipt.objects.aggregate(Min('date__year'))['date__year__min']
        last_year = today().year - 1
        data = data.copy()
        data.setdefault('year', last_year)
        super().__init__(data, *args, **kwargs)
        self.filters['year'].field.choices = [
            (year, str(year)) for year in range(last_year, first_year - 1, -1)
        ]


class LogEntryOrderingFilter(django_filters.OrderingFilter):
    def __init__(self, *args, **kwargs):
        kwargs['choices'] = (
            ('timestamp', "Date"),
            ('actor', "Auteur·ice"),
        )
        super().__init__(*args, **kwargs)

    def filter(self, qs, value):
        if not value or value == ['timestamp']:
            return qs.order_by(
                '-timestamp',
            )
        if value == ['actor']:
            return qs.order_by(
                'actor__person__last_name',
                'actor__person__first_name',
            )


class LogEntryFilter(django_filters.FilterSet):
    timestamp = django_filters.DateRangeFilter(label="Dates")
    actor = django_filters.ModelChoiceFilter(label="Auteur·ice", field_name='actor__person',
                                             queryset=Person.objects.none())
    content_type = django_filters.ModelChoiceFilter(
        label="Type d'objet",
        queryset=ContentType.objects.filter(app_label='members').order_by('model'),
    )
    object_id = django_filters.NumberFilter(label="Numéro d'objet")
    o = LogEntryOrderingFilter(label="Tri", empty_label=None)

    class Meta:
        model = LogEntry
        fields = ('timestamp', 'actor', 'content_type', 'object_id', 'o')
        form = FilterForm

    def __init__(self, data, *args, **kwargs):
        super().__init__(data, *args, **kwargs)
        self.user = kwargs['request'].user

        # actor
        self.filters['actor'].field.queryset = Person.objects.for_perm('members.view_person', self.user)
        self.filters['actor'].field.widget = autocomplete.ModelSelect2(
            url='person-autocomplete',
            forward=[
                forward.Const('members.view_person', 'for_perm')
            ],
            attrs={'data-placeholder': 'Tous·tes'}
        )


class PaymentOrderingFilter(django_filters.OrderingFilter):
    def __init__(self, *args, **kwargs):
        kwargs['choices'] = (
            ('-date', "Date"),
            ('amount', "Montant"),
            ('payer', "Payeur"),
        )
        kwargs['fields'] = {
            'similarity': 'similarity',
            'date': '-date',
            'amount': 'amount',
            'payer': 'payer__last_name',
        }
        super().__init__(*args, **kwargs)

    def filter(self, qs, value):
        if not value or value == ['-date']:
            value = ['-date', 'payer__last_name']
        return super().filter(qs, value)


class PaymentFilter(django_filters.FilterSet):
    season = django_filters.ChoiceFilter(label="Saison", choices=(), empty_label="Toutes",
                                         field_name='allocation__adhesion__season')
    structure = django_filters.ModelChoiceFilter(label="Structure", method='filter_structure',
                                                 queryset=Structure.objects.none())
    adherent = django_filters.ModelChoiceFilter(label="Adhérent·e", field_name='allocation__adhesion__adherent',
                                                queryset=Adherent.objects.none())
    o = PaymentOrderingFilter(label="Tri", empty_label=None)

    class Meta:
        model = Payment
        fields = ('season', 'amount', 'date', 'method', 'structure', 'payer', 'adherent', 'o')
        form = FilterForm

    def __init__(self, data, *args, **kwargs):
        data = data.copy() if data else {}
        data.setdefault('season', current_season())
        super().__init__(data, *args, **kwargs)
        self.user = kwargs['request'].user

        # season
        end_season = current_season()
        begin_season = first_season()
        self.filters['season'].field.choices = [
            (season, "{}/{}".format(season - 1, season))
            for season in range(end_season, begin_season - 1, -1)
        ]

        # structure
        self.filters['structure'].field.queryset = self.get_structures()
        self.filters['structure'].field.widget = autocomplete.ModelSelect2(
            url='structure-autocomplete',
            attrs={'data-placeholder': 'Toutes'},
            forward=[
                forward.Const('members.view_payment', 'for_perm'),
            ]
        )

        # payer
        self.filters['payer'].field.queryset = Person.objects.for_perm('members.view_person', self.user)
        self.filters['payer'].field.widget = autocomplete.ModelSelect2(
            url='person-autocomplete',
            attrs={'data-placeholder': 'Tous·tes'},
            forward=[
                forward.Const('members.view_person', 'for_perm'),
            ]
        )

        # adherent
        self.filters['adherent'].field.queryset = \
            Adherent.objects.for_perm('members.view_payment', self.user)
        self.filters['adherent'].field.widget = autocomplete.ModelSelect2(
            url='adherent-autocomplete',
            attrs={'data-placeholder': 'Tous·tes'},
            forward=[
                'structure',
                forward.Const('members.view_payment', 'for_perm'),
            ]
        )

    def get_structures(self):
        return Structure.objects \
            .for_perm('members.view_payment', self.user) \
            .is_active()

    def filter_structure(self, qs, name, value):
        if value:
            qs = qs.filter(
                allocation__adhesion__structure__lft__gte=value.lft,
                allocation__adhesion__structure__lft__lte=value.rght
            )
        return qs


class AccountingFilter(django_filters.FilterSet):
    year = django_filters.ChoiceFilter(label="Exercice", choices=(), required=True, empty_label=None,
                                       method='filter_year')
    q = SearchFilter(label="Nom")

    class Meta:
        model = Structure
        fields = ('year', 'type', 'q')
        form = FilterForm

    def __init__(self, data, *args, **kwargs):
        data = data.copy() if data else {}
        data.setdefault('year', current_year() - 1)
        super().__init__(data, *args, **kwargs)
        self.filters['year'].field.choices = [
            (year, str(year)) for year in range(current_year(), first_season() - 1, -1)
        ]

    def filter_year(self, qs, name, value):
        return qs.filter(
            ~Q(status=Structure.STATUS_CLOSED) |
            Q(adhesions__allocation__payment__date__year=value) |
            Q(adhesions__entry__transfer__order__date__year=value)
        ).distinct()


class AccountingDetailFilter(FilterForm):
    year = forms.ChoiceField(label="Exercice", choices=(), required=True)
    structure = forms.ModelChoiceField(
        label="Structure", queryset=Structure.objects.none(), required=True,
        widget=autocomplete.ModelSelect2(
            url='structure-autocomplete',
            forward=[
                forward.Const('members.view_transferorder', 'for_perm'),
            ],
        )
    )

    def __init__(self, data, request, *args, **kwargs):
        data = data.copy() if data else {}
        data.setdefault('year', current_year() - 1)
        super().__init__(data, *args, **kwargs)

        # year
        self.fields['year'].choices = [
            (year, str(year)) for year in range(current_year(), first_season() - 1, -1)
        ]

        # structure
        self.fields['structure'].queryset = Structure.objects.for_perm('members.view_transferorder', request.user)


class WithHolderWidget(django_filters.widgets.BooleanWidget):
    def __init__(self, attrs=None):
        choices = (("", "Tout"), ("true", "Non"), ("false", "Oui"))
        super(django_filters.widgets.BooleanWidget, self).__init__(attrs, choices)


class RoleFilter(django_filters.FilterSet):
    structure = django_filters.ModelChoiceFilter(label="Structure", method='filter_structure',
                                                 queryset=Structure.objects.none())
    with_holder = django_filters.BooleanFilter(label="Avec mandant", field_name='holder', lookup_expr='isnull',
                                               widget=WithHolderWidget())

    class Meta:
        model = Role
        fields = ('config', 'structure', 'team', 'with_holder', 'holder', 'proxy')
        form = FilterForm

    def __init__(self, data, *args, **kwargs):
        self.user = kwargs['request'].user
        data = data.copy() if data else {}
        super().__init__(data, *args, **kwargs)
        self.filters['config'].label = "Rôle"

        # Structures
        self.filters['structure'].field.queryset = Structure.objects \
            .for_perm('members.view_role', self.user) \
            .is_active()
        self.filters['structure'].field.widget = autocomplete.ModelSelect2(
            url='structure-autocomplete',
            forward=[
                forward.Const('members.view_role', 'for_perm'),
                forward.Const('on', 'is_active'),
            ],
            attrs={'data-placeholder': 'Toutes'},
        )

        # team
        self.filters['team'].field.queryset = Team.objects \
            .for_perm('members.view_role', self.user) \
            .is_active()
        self.filters['team'].field.widget = autocomplete.ModelSelect2(
            url='team-autocomplete',
            forward=[
                'structure',
                forward.Const('members.view_role', 'for_perm'),
                forward.Const('on', 'is_active'),
            ],
            attrs={'data-placeholder': 'Toutes'},
        )

        # holder
        self.filters['holder'].field.queryset = Person.objects.for_perm('members.view_person', self.user)
        self.filters['holder'].field.widget = autocomplete.ModelSelect2(
            url='person-autocomplete',
            forward=[
                'structure',
                forward.Const(False, 'outside'),
                forward.Const('members.view_person', 'for_perm')
            ],
            attrs={'data-placeholder': 'Tout·es'}
        )

        # proxy
        self.filters['proxy'].field.queryset = Person.objects.for_perm('members.view_person', self.user)
        self.filters['proxy'].field.widget = autocomplete.ModelSelect2(
            url='person-autocomplete',
            forward=[
                'structure',
                forward.Const(False, 'outside'),
                forward.Const('members.view_person', 'for_perm')
            ],
            attrs={'data-placeholder': 'Tout·es'}
        )

    def filter_structure(self, qs, name, value):
        if value:
            qs = qs.filter(
                team__structure__lft__gte=value.lft,
                team__structure__lft__lte=value.rght
            )
        return qs
