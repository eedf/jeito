# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from rest_framework import serializers
from members.models import Adhesion, Function, Structure, age


class AdhesionListSerializer(serializers.ModelSerializer):
    number = serializers.SerializerMethodField()
    first_name = serializers.CharField(source='adherent.person.first_name')
    last_name = serializers.CharField(source='adherent.person.last_name')
    birthdate = serializers.CharField(source='adherent.person.birthdate')
    email = serializers.CharField(source='adherent.person.email')
    parent_emails = serializers.SerializerMethodField()
    structure = serializers.CharField(source='structure.name')
    functions = serializers.SerializerMethodField()

    class Meta:
        model = Adhesion
        fields = (
            'number', 'first_name', 'last_name', 'birthdate', 'email', 'parent_emails',
            'structure', 'functions',
        )

    def get_number(self, obj):
        return f"{obj.adherent.id:06d}"

    def get_parent_emails(self, obj):
        emails = set()
        if age(obj.adherent.person.birthdate) < 18:
            if obj.adherent.person.legal_guardian1 and obj.adherent.person.legal_guardian1.email:
                emails.add(obj.adherent.person.legal_guardian1.email)
            if obj.adherent.person.legal_guardian2 and obj.adherent.person.legal_guardian2.email:
                emails.add(obj.adherent.person.legal_guardian2.email)
        return emails

    def get_functions(self, obj):
        functions = obj.adherent.person.functions.for_season(obj.season).order_by('id').select_related(
            'team__structure',
            'function_config__function_type',
        )
        for function in functions:
            if function.team.structure == obj.structure:
                function.main = True
                break
        return [{
            'structure': function.team.structure.name,
            'function': function.name,
            'main': getattr(function, 'main', False),
            'is_responsible': function.function_config.function_type.name == "Responsable",
        } for function in functions]


class AdhesionDetailSerializer(AdhesionListSerializer):
    structure_type = serializers.CharField(source='structure.type.old_name')
    region = serializers.CharField(source='structure.region')
    rate = serializers.CharField(source='rate.name')
    nominations = serializers.SerializerMethodField()
    adhesions_resp_email = serializers.SerializerMethodField(method_name='get_structure_resp_email')
    structure_resp_email = serializers.SerializerMethodField()

    class Meta:
        model = Adhesion
        fields = (
            'number', 'first_name', 'last_name', 'birthdate', 'email',
            'structure', 'structure_type', 'region', 'rate', 'nominations',
            'adhesions_resp_email', 'structure_resp_email',
        )

    def get_structure_resp_email(self, obj):
        functions = Function.objects.current().filter(
            team__structure=obj.structure,
            team__type__management=True,
            function_config__function_type__name="Responsable",
        )
        if functions and functions[0]:
            return functions[0].person.email
        return None

    def get_nominations(self, obj):
        functions = obj.adherent.person.functions.for_season(obj.season).order_by('id').select_related(
            'team__structure',
            'function_config__function_type',
        )
        region = None
        for function in functions:
            if function.team.structure == obj.structure:
                function.main = True
                region = function.team.structure.region
                break
        return [{
            'structure': function.team.structure.name,
            'structure_type': function.team.structure.type.old_name,
            'region': region and region.name,
            'function': str(function.function_config),
            'main': getattr(function, 'main', False),
            'is_responsible': function.function_config.function_type.name == "Responsable",
        } for function in functions]


class StructureSerializer(serializers.ModelSerializer):
    type = serializers.CharField(source='type.old_name')
    subtype = serializers.CharField(source='type.old_subtype_name')
    region = serializers.CharField()
    headcount = serializers.IntegerField()

    class Meta:
        model = Structure
        fields = ('name', 'type', 'subtype', 'region', 'headcount')
