# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

import fintech
from django.apps import AppConfig


fintech.register()


class MembersConfig(AppConfig):
    name = 'members'
    verbose_name = "Adhésions"
    default_auto_field = 'django.db.models.BigAutoField'

    def ready(self):
        from . import signals  # noqa
