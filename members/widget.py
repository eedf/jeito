# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.


from collections import Counter
from datetime import date as Date
from psycopg2.extras import DateRange
from django.db.models import Count, Sum, Case, When, F, Q
from members.utils import yesterday


class Widget():
    template_name = None

    def get_context_data(self):
        return {}


widgets = []


def register(Widget):
    widgets.append(Widget())
    return Widget


@register
class HeadcountWidget(Widget):
    template_name = 'members/headcount_widget.html'

    def get_context_data(self, filter, ref_filter):
        qs = filter.qs.aggregate(headcount=Count('id'))
        ref_qs = ref_filter.qs.aggregate(headcount=Count('id'))
        return {
            'headcount': qs['headcount'],
            'headcount_diff': (100 * (qs['headcount'] - ref_qs['headcount']) / ref_qs['headcount'])
            if ref_qs['headcount'] else 0,
        }


@register
class OnlineWidget(Widget):
    template_name = 'members/online_widget.html'

    def get_context_data(self, filter, ref_filter):
        qs = filter.qs.exclude(invitation=None).aggregate(online=Count('id'))
        ref_qs = ref_filter.qs.exclude(invitation=None).aggregate(online=Count('id'))
        return {
            'online': qs['online'],
            'online_diff': (100 * (qs['online'] - ref_qs['online']) / ref_qs['online'])
            if ref_qs['online'] else None,
        }


@register
class YoungsHeadcountWidget(Widget):
    template_name = 'members/youngs_headcount_widget.html'

    def date_range(self, filter):
        season = int(filter.form.cleaned_data['season'])
        value = filter.form.cleaned_data['date']
        begin = Date(season - 1, 9, 1)
        if value == 'ajd':
            end = yesterday()
            leap_day = end.month == 2 and end.day == 29
            end = end.replace(
                year=season if end.month <= 8 else season - 1,
                day=28 if leap_day else end.day
            )
        else:
            end = Date(season, 8, 31)
        return DateRange(begin, end, '[]')

    def get_context_data(self, filter, ref_filter):
        qs = filter.qs.filter(
            adherent__person__functions__function_config__function_type__name="Participant·e simple",
            adherent__person__functions__dates__overlap=self.date_range(filter),
        )
        qs = qs.aggregate(headcount=Count('id'))
        ref_qs = ref_filter.qs.filter(
            adherent__person__functions__function_config__function_type__name="Participant·e simple",
            adherent__person__functions__dates__overlap=self.date_range(ref_filter),
        )
        ref_qs = ref_qs.aggregate(headcount=Count('id'))
        return {
            'youngs_headcount': qs['headcount'],
            'youngs_headcount_diff': (100 * (qs['headcount'] - ref_qs['headcount']) / ref_qs['headcount'])
            if ref_qs['headcount'] else 0,
        }


@register
class GroupsWidget(Widget):
    template_name = 'members/groups_widget.html'

    def get_nb_groups(self, qs):
        qs1 = qs.filter(structure__parent__type=10)
        qs1 = qs1.values_list('structure__parent__id').annotate(nb_adherents=Count('id'))
        qs2 = qs.filter(structure__type=10)
        qs2 = qs2.values_list('structure__id').annotate(nb_adherents=Count('id'))
        sum_by_group = Counter(dict(qs1)) + Counter(dict(qs2))
        return sum([1 for group, nb in sum_by_group.items() if nb >= 3])

    def get_context_data(self, filter, ref_filter):
        nb_groups = self.get_nb_groups(filter.qs)
        ref_nb_groups = self.get_nb_groups(ref_filter.qs)
        return {
            'nb_groups': nb_groups,
            'nb_groups_diff': (100 * (nb_groups - ref_nb_groups) / ref_nb_groups) if ref_nb_groups else 0,
        }


@register
class RevenueWidget(Widget):
    template_name = 'members/revenue_widget.html'

    def get_context_data(self, filter, ref_filter):
        qs = filter.qs.aggregate(revenue=Sum(Case(
            When(~Q(rate__rate_max=None), F('free_rate')),
            default=F('rate__rate')
        )))
        ref_qs = ref_filter.qs.aggregate(revenue=Sum(Case(
            When(~Q(rate__rate_max=None), F('free_rate')),
            default=F('rate__rate')
        )))
        return {
            'revenue': (qs['revenue'] or 0) / 1000,
            'revenue_diff': (100 * (qs['revenue'] - ref_qs['revenue']) / ref_qs['revenue'])
            if (qs['revenue'] and ref_qs['revenue']) else 0,
        }


@register
class DonationWidget(Widget):
    template_name = 'members/donation_widget.html'

    def get_context_data(self, filter, ref_filter):
        qs = filter.qs.aggregate(donation=Sum('donation'))
        ref_qs = ref_filter.qs.aggregate(donation=Sum('donation'))
        return {
            'donation': (qs['donation'] or 0) / 1000,
            'donation_diff': (100 * (qs['donation'] - ref_qs['donation']) / ref_qs['donation'])
            if (qs['donation'] and ref_qs['donation']) else 0,
        }


# @register
class SVHeadcountWidget(Widget):
    template_name = 'members/sv_headcount_widget.html'

    def get_context_data(self, filter, ref_filter):
        qs = filter.qs.filter(functions__function__category=0, functions__main=True)
        qs = qs.filter(structure__type__name="Service vacances")
        qs = qs.aggregate(headcount=Count('id'))
        ref_qs = ref_filter.qs.filter(functions__function__category=0, functions__main=True)
        ref_qs = ref_qs.filter(structure__type__name="Service vacances")
        ref_qs = ref_qs.aggregate(headcount=Count('id'))
        return {
            'sv_headcount': qs['headcount'],
            'sv_headcount_diff': (100 * (qs['headcount'] - ref_qs['headcount']) / ref_qs['headcount'])
            if ref_qs['headcount'] else 0,
        }


# @register
class CenterHeadcountWidget(Widget):
    template_name = 'members/cp_headcount_widget.html'

    def get_context_data(self, filter, ref_filter):
        qs = filter.qs.filter(functions__function__category=0, functions__main=True)
        qs = qs.filter(structure__type__name="Centre")
        qs = qs.aggregate(headcount=Count('id'))
        ref_qs = ref_filter.qs.filter(functions__function__category=0, functions__main=True)
        ref_qs = ref_qs.filter(structure__type__name="Centre")
        ref_qs = ref_qs.aggregate(headcount=Count('id'))
        return {
            'cp_headcount': qs['headcount'],
            'cp_headcount_diff': (100 * (qs['headcount'] - ref_qs['headcount']) / ref_qs['headcount'])
            if ref_qs['headcount'] else 0,
        }


# @register
class StagiaireHeadcountWidget(Widget):
    template_name = 'members/stagiaire_headcount_widget.html'

    def get_context_data(self, filter, ref_filter):
        qs = filter.qs.filter(functions__function__category=3, functions__main=True)
        qs = qs.aggregate(headcount=Count('id'))
        ref_qs = ref_filter.qs.filter(functions__function__category=3, functions__main=True)
        ref_qs = ref_qs.aggregate(headcount=Count('id'))
        return {
            'stagiaire_headcount': qs['headcount'],
            'stagiaire_headcount_diff': (100 * (qs['headcount'] - ref_qs['headcount']) / ref_qs['headcount'])
            if ref_qs['headcount'] else 0,
        }
