from oauth2_provider.oauth2_validators import OAuth2Validator


class JeitoOAuth2Validator(OAuth2Validator):
    # Extend the standard scopes to add a new "permissions" scope
    # which returns a "permissions" claim:
    oidc_claim_scope = OAuth2Validator.oidc_claim_scope
    oidc_claim_scope.update({
        "person_id": "jeito",
        "person_uuid": "jeito",
    })

    def get_person_attr(self, attr, cast):
        def getter(request):
            if not request.user.person:
                return None
            return cast(getattr(request.user.person, attr))
        return getter

    def get_additional_claims(self):
        return {
            'sub': self.get_person_attr('uuid', str),
            'person_id': self.get_person_attr('id', int),
            'person_uuid': self.get_person_attr('uuid', str),
            'email': self.get_person_attr('email', str),
            'family_name': self.get_person_attr('last_name', str),
            'given_name': self.get_person_attr('first_name', str),
            'nickname': self.get_person_attr('nickname', str),
            'name': self.get_person_attr('full_name', str),
            'gender': self.get_person_attr('gender_oidc', str),
        }
