# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from django.urls import path, include, register_converter
from django.views.generic import RedirectView
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView
from rest_framework.routers import DefaultRouter
from . import views
from . import views_api_v1
from . import views_api_v3
from . import views_api_v4
from . import views_api_v5

app_name = 'members'

router_v3 = DefaultRouter()
router_v3.register('structure_types', views_api_v3.StructureTypeViewSet3)
router_v3.register('structures', views_api_v3.StructureViewSet3)
router_v3.register('team_types', views_api_v3.TeamTypeViewSet3)
router_v3.register('teams', views_api_v3.TeamViewSet3)
router_v3.register('persons', views_api_v3.PersonViewSet3)
router_v3.register('adherents', views_api_v3.AdherentViewSet3)
router_v3.register('adhesions', views_api_v3.AdhesionViewSet3)
router_v3.register('function_types', views_api_v3.FunctionTypeViewSet3)
router_v3.register('functions', views_api_v3.FunctionViewSet3)
router_v3.register('employees', views_api_v3.EmployeeViewSet3)
router_v3.register('employments', views_api_v3.EmploymentViewSet3)

router_v4 = DefaultRouter()
router_v4.register('structure_type', views_api_v4.StructureTypeViewSet4)
router_v4.register('structure', views_api_v4.StructureViewSet4)
router_v4.register('team_type', views_api_v4.TeamTypeViewSet4)
router_v4.register('team', views_api_v4.TeamViewSet4)
router_v4.register('person', views_api_v4.PersonViewSet4)
router_v4.register('adherent', views_api_v4.AdherentViewSet4)
router_v4.register('adhesion', views_api_v4.AdhesionViewSet4)
router_v4.register('function_type', views_api_v4.FunctionTypeViewSet4)
router_v4.register('function', views_api_v4.FunctionViewSet4)
router_v4.register('employee', views_api_v4.EmployeeViewSet4)
router_v4.register('employment', views_api_v4.EmploymentViewSet4)
router_v4.register('permission', views_api_v4.PermissionViewSet4)
router_v4.register('role_config', views_api_v4.RoleConfigViewSet4)
router_v4.register('role_config_permission', views_api_v4.RoleConfigPermissionViewSet4)
router_v4.register('attribution', views_api_v4.AttributionViewSet4)
router_v4.register('role', views_api_v4.RoleViewSet4)

router_v5 = DefaultRouter()
router_v5.register('country', views_api_v5.CountryViewSet5)
router_v5.register('commune', views_api_v5.CommuneViewSet5)
router_v5.register('structure', views_api_v5.StructureViewSet5)
router_v5.register('team', views_api_v5.TeamViewSet5)
router_v5.register('person', views_api_v5.PersonViewSet5)
router_v5.register('adherent', views_api_v5.AdherentViewSet5)
router_v5.register('adhesion', views_api_v5.AdhesionViewSet5)
router_v5.register('invitation', views_api_v5.InvitationViewSet5)
router_v5.register('function', views_api_v5.FunctionViewSet5)
router_v5.register('employee', views_api_v5.EmployeeViewSet5)
router_v5.register('employment', views_api_v5.EmploymentViewSet5)
router_v5.register('permission', views_api_v5.PermissionViewSet5)
router_v5.register('role_config', views_api_v5.RoleConfigViewSet5)
router_v5.register('role_config_permission', views_api_v5.RoleConfigPermissionViewSet5)
router_v5.register('attribution', views_api_v5.AttributionViewSet5)
router_v5.register('role', views_api_v5.RoleViewSet5)


class GuardianConverter:
    regex = '[12]'

    def to_python(self, value):
        return int(value)

    def to_url(self, value):
        return str(value)


class YyyyConverter:
    regex = '[0-9]{4}'

    def to_python(self, value):
        return int(value)

    def to_url(self, value):
        return f'{value:04d}'


class MmConverter:
    regex = '[0-9]{2}'

    def to_python(self, value):
        return int(value)

    def to_url(self, value):
        return f'{value:02d}'


register_converter(GuardianConverter, 'guardian')
register_converter(YyyyConverter, 'yyyy')
register_converter(MmConverter, 'mm')


def preprocessing_api_v1(endpoints):
    return [endpoint for endpoint in endpoints if endpoint[0].startswith("/api/v1/")]


def preprocessing_api_v3(endpoints):
    return [endpoint for endpoint in endpoints if endpoint[0].startswith("/api/v3/")]


def preprocessing_api_v4(endpoints):
    return [endpoint for endpoint in endpoints if endpoint[0].startswith("/api/v4/")]


def preprocessing_api_v5(endpoints):
    return [endpoint for endpoint in endpoints if endpoint[0].startswith("/api/v5/")]


urlpatterns = [
    path('', include('jeito.urls')),
    path('', views.HomeView.as_view(), name='home'),
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('password_reset/', views.PasswordResetView.as_view(), name='password_reset'),
    path('members/<path:path>', views.RedirectFromV1View.as_view()),
    path('dashboard/', views.DashboardView.as_view(), name='dashboard'),
    path('graphique/', views.ChartView.as_view(), name='chart'),
    path('graphique/data/', views.ChartJsonView.as_view(), name='chart_data'),
    path('tranches/', views.TranchesView.as_view(), name='tranches'),
    path('tableau/regions/', views.TableauRegionsView.as_view(), name='tableau_regions'),
    path('tableau/departements/', views.TableauDepartementsView.as_view(), name='tableau_departements'),
    path('tableau/functions/', views.TableauFunctionsView.as_view(), name='tableau_functions'),
    path('tableau/amount/', views.TableauAmountView.as_view(), name='amount'),
    path('tableau/receipt/', views.ReceiptStatsView.as_view(), name='receipt_stats'),
    path('tableau/structures/', views.TableauStructureView.as_view(), name='structures'),
    path('tableau/structure_type/', views.TableauStructureTypeView.as_view(), name='structure_type'),
    path('tableau/rate/', views.TableauRateView.as_view(), name='tableau_rate'),
    path('tableau/gender/', views.TableauGenderView.as_view(), name='tableau_gender'),
    path('tableau/sf/', views.StatsSfView.as_view(), name='stats_sf'),
    path('accounting/', views.AccountingView.as_view(), name='accounting'),
    path('accounting/export/', views.AccountingExportView.as_view(), name='accounting_export'),
    path('accounting/detail/', views.AccountingDetailView.as_view(), name='accounting_detail'),
    path('structure/', views.StructureListView.as_view(), name='structure_list'),
    path('structure/create/', views.StructureCreateView.as_view(), name='structure_create'),
    path('structure/<int:pk>/', views.StructureDetailView.as_view(), name='structure_detail'),
    path('structure/<int:pk>/<int:version_pk>/', views.StructureDetailView.as_view(), name='structure_version'),
    path('structure/<int:pk>/update/', views.StructureUpdateView.as_view(), name='structure_update'),
    path('structure/<int:pk>/delete/', views.StructureDeleteView.as_view(), name='structure_delete'),
    path('structure/<int:pk>/dormant/', views.StructureDormantView.as_view(), name='structure_dormant'),
    path('structure/<int:pk>/closed/', views.StructureClosedView.as_view(), name='structure_closed'),
    path('structure/<int:pk>/signing/', views.StructureSigningView.as_view(), name='structure_signing'),
    path('structure/<int:pk>/guardianship/', views.StructureGuardianshipView.as_view(), name='structure_guardianship'),
    path('structure/export/', views.StructureExportView.as_view(), name='structure_export'),
    path('adhesion/', views.AdhesionListView.as_view(), name='adhesion_list'),
    path('adhesion/import/', views.AdhesionImportView.as_view(), name='adhesion_import'),
    path('adhesion/export/', views.AdhesionExportView.as_view(), name='adhesion_export'),
    path('adhesion/unpaid/', views.UnpaidListView.as_view(), name='unpaid_list'),
    path('adhesion/<int:pk>/', views.AdhesionDetailView.as_view(), name='adhesion_detail'),
    path('adhesion/pre_create/', views.AdhesionPreCreateView.as_view(), name='adhesion_pre_create'),
    path('adhesion/confirm_renew/<int:pk>/', views.AdhesionConfirmRenewView.as_view(),
         name='adhesion_confirm_renew'),
    path('adhesion/confirm_create/', views.AdhesionConfirmCreateView.as_view(), name='adhesion_confirm_create'),
    path('adhesion/create/', views.AdhesionWizardView.as_view(), name='adhesion_create'),
    path('adhesion/create/<str:invitation_token>/', views.AdhesionWizardView.as_view(), name='adhesion_create'),
    path('adhesion/pre-pay/<str:invitation_token>/', views.OnlinePrePayView.as_view(), name='adhesion_pre_pay'),
    path('adhesion/post-pay/<int:pk>/', views.OnlinePostPayView.as_view(), name='adhesion_post_pay'),
    path('adhesion/<int:person_pk>/renew/', views.AdhesionWizardView.as_view(), name='adhesion_renew'),
    path('adhesion/<int:pk>/update/', views.AdhesionUpdateView.as_view(), name='adhesion_update'),
    path('adhesion/<int:pk>/delete/', views.AdhesionCancelView.as_view(), name='adhesion_cancel'),
    path('employee/', views.EmployeeListView.as_view(), name='employee_list'),
    path('employee/export/', views.EmployeeExportView.as_view(), name='employee_export'),
    path('employee/<int:pk>/', views.EmployeeDetailView.as_view(), name='employee_detail'),
    path('employee/<int:pk>/update', views.EmployeeUpdateView.as_view(), name='employee_update'),
    path('employee/<int:pk>/delete/', views.EmployeeDeleteView.as_view(), name='employee_delete'),
    path('employment/', views.EmploymentListView.as_view(), name='employment_list'),
    path('employment/import/', views.EmploymentImportView.as_view(), name='employment_import'),
    path('employment/export/', views.EmploymentExportView.as_view(), name='employment_export'),
    path('employment/<int:pk>/', views.EmploymentDetailView.as_view(), name='employment_detail'),
    path('employment/create/', views.EmploymentWizardView.as_view(), name='employment_create'),
    path('employment/create/<int:person_pk>/', views.EmploymentWizardView.as_view(), name='employment_create'),
    path('employment/<int:pk>/update', views.EmploymentUpdateView.as_view(), name='employment_update'),
    path('employment/<int:pk>/delete/', views.EmploymentDeleteView.as_view(), name='employment_delete'),
    path('contribution/<int:pk>/update/', views.ContributionUpdateView.as_view(), name='contribution_update'),
    path('payment/', views.PaymentListView.as_view(), name='payment_list'),
    path('payment/<int:pk>/', views.PaymentDetailView.as_view(), name='payment_detail'),
    path('payment/export/', views.PaymentExportView.as_view(), name='payment_export'),
    path('payment/create/<int:adhesion_pk>/', views.PaymentCreateView.as_view(), name='payment_create'),
    path('payment/<int:pk>/update/', views.PaymentUpdateView.as_view(), name='payment_update'),
    path('payment/<int:pk>/delete/', views.PaymentDeleteView.as_view(), name='payment_delete'),
    path('person/', views.PersonListView.as_view(), name='person_list'),
    # path('person/export/', views.PersonExportView.as_view(), name='person_export'),
    path('person/<int:pk>/', views.PersonDetailView.as_view(), name='person_detail'),
    path('person/<int:pk>/update/', views.PersonUpdateView.as_view(), name='person_update'),
    path('person/<int:pk>/email_as_login/', views.EmailAsLoginView.as_view(), name='email_as_login'),
    path('person/<int:pk>/ok/', views.PersonOKView.as_view(), name='person_ok'),
    path('person/<int:pk>/password_reset_message/', views.PasswordResetMessageView.as_view(),
         name='password_reset_message'),
    path('person/<int:pk>/deduplicate/', views.PersonDeduplicateView.as_view(), name='person_deduplicate'),
    path('person/<int:pk>/guardian<guardian:n>/create/', views.GuardianCreateView.as_view(),
         name='guardian_create'),
    path('person/<int:pk>/guardian<guardian:n>/update/', views.GuardianUpdateView.as_view(),
         name='guardian_update'),
    path('person/<int:pk>/guardian<guardian:n>/delete/', views.GuardianDeleteView.as_view(),
         name='guardian_delete'),
    path('adhesion/<int:pk>/card/', views.AdhesionCardView.as_view(), name='adhesion_card'),
    path('adhesion/<int:pk>/certificate/', views.AdhesionCertificateView.as_view(), name='adhesion_certificate'),
    path('function/', views.FunctionListView.as_view(), name='function_list'),
    path('function/<int:pk>/', views.FunctionDetailView.as_view(), name='function_detail'),
    path('function/create/', views.FunctionCreateView.as_view(), name='function_create'),
    path('function/<int:pk>/update/', views.FunctionUpdateView.as_view(), name='function_update'),
    path('function/<int:pk>/delete/', views.FunctionDeleteView.as_view(), name='function_delete'),
    path('function/<str:token>/reply/', views.FunctionReplyView.as_view(), name='function_reply'),
    path('function/export/', views.FunctionExportView.as_view(), name='function_export'),
    path('rate/options/<int:structure_pk>/', views.RateOptionsView.as_view(), name='rate_options'),
    path('rate/', views.RateListView.as_view(), name='rate_list'),
    path('structure-autocomplete/', views.StructureAutocomplete.as_view(), name='structure-autocomplete'),
    path('team-autocomplete/', views.TeamAutocomplete.as_view(), name='team-autocomplete'),
    path('person-autocomplete/', views.PersonAutocomplete.as_view(), name='person-autocomplete'),
    path('adherent-autocomplete/', views.AdherentAutocomplete.as_view(), name='adherent-autocomplete'),
    path('employee-autocomplete/', views.EmployeeAutocomplete.as_view(), name='employee-autocomplete'),
    path('functionconfig-autocomplete/', views.FunctionConfigAutocomplete.as_view(),
         name='functionconfig-autocomplete'),
    path('place-autocomplete/', views.PlaceAutocomplete.as_view(), name='place-autocomplete'),
    path('commune-autocomplete/', views.CommuneAutocomplete.as_view(), name='commune-autocomplete'),
    path('transferorder/', views.TransferOrderListView.as_view(), name='transferorder_list'),
    path('transferorder/<int:pk>/', views.TransferOrderDetailView.as_view(), name='transferorder_detail'),
    path('transferorder/<int:pk>/sepa_debit/', views.DirectDebitSEPAView.as_view(), name='sepa_debit'),
    path('transferorder/<int:pk>/sepa_credit/', views.CreditTransferSEPAView.as_view(), name='sepa_credit'),
    path('transferorder/<int:pk>/xlsx/', views.TransferOrderXlsxView.as_view(),
         name='transferorder_xlsx'),
    path('transfer/<int:pk>/', views.TransferDetailView.as_view(), name='transfer_detail'),
    path('remittance/', views.RemittanceListView.as_view(), name='remittance_list'),
    path('remittance/<yyyy:year>/<mm:month>/', views.RemittanceDetailView.as_view(),
         name='remittance_detail'),
    path('adherent/', views.AdherentListView.as_view(), name='adherent_list'),
    path('adherent/export/', views.AdherentExportView.as_view(), name='adherent_export'),
    path('adherent/<int:pk>/update_status/', views.StatusUpdateView.as_view(), name='status_update'),
    path('adherent/<int:pk>/update_permission/', views.PermissionUpdateView.as_view(), name='permission_update'),
    path('inconsistencies/', views.InconsistenciesView.as_view(), name='inconsistencies'),
    path('person/<int:pk>/masquerade/', views.MasqueradeView.as_view(), name='masquerade'),
    path('person/unmasquerade/', views.UnmasqueradeView.as_view(), name='unmasquerade'),
    path('receipt/<int:pk>/download/', views.ReceiptDownloadByPkView.as_view(), name='receipt_download'),
    path('receipt/token/<str:token>/', views.ReceiptDownloadByTokenView.as_view(),
         name='receipt_download_by_token'),
    path('account/', views.AccountView.as_view(), name='account'),
    path('invitation/', views.InvitationListView.as_view(), name='invitation_list'),
    path('invitation/<int:pk>/', views.InvitationDetailView.as_view(), name='invitation_detail'),
    path('invitation/<int:pk>/send_reminder/', views.InvitationSendReminderView.as_view(),
         name='invitation_send_reminder'),
    path('invitation/create/', views.InvitationCreateView.as_view(), name='invitation_create'),
    path('invitation/create/new/', views.InvitationCreateNewView.as_view(), name='invitation_create_new'),
    path('invitation/create/list/', views.InvitationCreateListView.as_view(), name='invitation_create_list'),
    path('invitation/create/old/', views.InvitationCreateOldView.as_view(), name='invitation_create_old'),
    path('invitation/create/self/', views.InvitationCreateSelfView.as_view(), name='invitation_create_self'),
    path('invitation/<int:pk>/update/', views.InvitationUpdateView.as_view(), name='invitation_update'),
    path('invitation/<int:pk>/send/', views.InvitationSendView.as_view(), name='invitation_send'),
    path('invitation/<int:pk>/delete/', views.InvitationDeleteView.as_view(), name='invitation_delete'),
    path('invitation/<str:token>/decline/', views.InvitationDeclineView.as_view(), name='invitation_decline'),
    path('ipn/', views.IPNView.as_view(), name='IPN'),
    path('rgpd/', views.RGPDView.as_view(), name='rgpd'),
    path('duplicate/', views.DuplicateListView.as_view(), name='duplicate_list'),
    path('team/', views.TeamListView.as_view(), name='team_list'),
    path('team/create/', views.TeamCreateView.as_view(), name='team_create'),
    path('team/<int:pk>/', views.TeamDetailView.as_view(), name='team_detail'),
    path('team/<int:pk>/update/', views.TeamUpdateView.as_view(), name='team_update'),
    path('team/<int:pk>/delete/', views.TeamDeleteView.as_view(), name='team_delete'),
    path('team/<int:pk>/deactivate/', views.TeamDeactivateView.as_view(), name='team_deactivate'),
    path('team/<int:pk>/activate/', views.TeamActivateView.as_view(), name='team_activate'),
    path('team/export/', views.TeamExportView.as_view(), name='team_export'),
    path('teamconfig-autocomplete/', views.TeamConfigAutocomplete.as_view(), name='teamconfig-autocomplete'),
    path('roleconfig/', views.RoleConfigListView.as_view(), name='roleconfig_list'),
    path('roleconfig/create/', views.RoleConfigCreateView.as_view(), name='roleconfig_create'),
    path('roleconfig/<int:pk>/', views.RoleConfigDetailView.as_view(), name='roleconfig_detail'),
    path('roleconfig/<int:pk>/update/', views.RoleConfigUpdateView.as_view(), name='roleconfig_update'),
    path('roleconfig/<int:pk>/delete/', views.RoleConfigDeleteView.as_view(), name='roleconfig_delete'),
    path('role/', views.RoleListView.as_view(), name='role_list'),
    path('role/<int:pk>/', views.RoleDetailView.as_view(), name='role_detail'),
    path('role/create_delegation/<int:holder_pk>/<int:config_pk>/<int:team_pk>/',
         views.RoleDelegationCreateView.as_view(), name='role_create_delegation'),
    path('role/create/', views.RoleCreateView.as_view(), name='role_create'),
    path('role/create/<int:proxy_pk>/', views.RoleCreateView.as_view(), name='role_create'),
    path('role/<int:pk>/delete/', views.RoleDeleteView.as_view(), name='role_delete'),
    path('role/<int:pk>/delete_delegation/', views.RoleDelegationDeleteView.as_view(),
         name='role_delete_delegation'),
    path('permission/', views.PermissionListView.as_view(), name='permission_list'),
    path('logentry/', views.LogEntryListView.as_view(), name='logentry_list'),
    path('commune/', views.CommuneListView.as_view(), name='commune_list'),
    path('country/', views.CountryListView.as_view(), name='country_list'),
    path('api/', RedirectView.as_view(url='v5/')),
    path('api/v1/schema/', SpectacularAPIView.as_view(api_version='v1', custom_settings={
        'PREPROCESSING_HOOKS': ['members.urls.preprocessing_api_v1'],
    }), name='schema_v1'),
    path('api/v3/schema/', SpectacularAPIView.as_view(api_version='v3', custom_settings={
        'PREPROCESSING_HOOKS': ['members.urls.preprocessing_api_v3'],
    }), name='schema_v3'),
    path('api/v4/schema/', SpectacularAPIView.as_view(api_version='v4', custom_settings={
        'PREPROCESSING_HOOKS': ['members.urls.preprocessing_api_v4'],
    }), name='schema_v4'),
    path('api/v5/schema/', SpectacularAPIView.as_view(api_version='v5', custom_settings={
        'PREPROCESSING_HOOKS': ['members.urls.preprocessing_api_v5'],
    }), name='schema_v5'),
    path('api/v1/', SpectacularSwaggerView.as_view(url_name='schema_v1'), name='swagger_v1'),
    path('api/v3/', SpectacularSwaggerView.as_view(url_name='schema_v3'), name='swagger_v3'),
    path('api/v4/', SpectacularSwaggerView.as_view(url_name='schema_v4'), name='swagger_v4'),
    path('api/v5/', SpectacularSwaggerView.as_view(url_name='schema_v5'), name='swagger_v5'),
    path('api/v1/adhesions/', views_api_v1.AdhesionListAPIView.as_view()),
    path('api/v1/adhesions/<int:adherent__id>/', views_api_v1.AdhesionRetrieveAPIView.as_view()),
    path('api/v1/structures/', views_api_v1.StructureListAPIView.as_view()),
    path('api/v3/', include(router_v3.urls)),
    path('api/v4/', include(router_v4.urls)),
    path('api/v5/', include(router_v5.urls)),
    path('oauth/', include('oauth2_provider.urls', namespace='oauth2_provider')),
]
