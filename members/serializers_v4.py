# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from datetime import timedelta as TimeDelta
from psycopg2.extras import DateRange
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from rest_framework import serializers
from members.models import Adhesion, FunctionType, Function, Person, AdherentStatus, Structure, StructureType, \
    TeamType, Team, Echelon, Employment, EmploymentType, FunctionCategory, Adherent, Employee, Permission, \
    RoleConfig, Attribution, Role, RoleConfigPermission, ExternalContentType, FunctionConfig, FunctionStatus


class PersonSerializer4(serializers.ModelSerializer):
    gender = serializers.CharField(
        label="Genre",
        help_text=", ".join(f"{key}={val}" for key, val in Person.GENDER_CHOICES),
    )
    legal_guardian1_uuid = serializers.UUIDField(source='legal_guardian1.uuid', default=None)
    legal_guardian2_uuid = serializers.UUIDField(source='legal_guardian2.uuid', default=None)
    birth_place = serializers.CharField(source='birth_place_display', label="Lieu de naissance")
    permission_alone = serializers.SerializerMethodField(label="Permission de rentrer seul·e")
    permission_with = serializers.SerializerMethodField(label="Permission de rentrer accompagné·e")
    permission_last_name = serializers.SerializerMethodField(label="Permission de rentrer accompagné·e par (nom)")
    permission_first_name = serializers.SerializerMethodField(label="Permission de rentrer accompagné·e par (prénom)")

    class Meta:
        model = Person
        fields = (
            'uuid', 'id', 'gender', 'first_name', 'last_name', 'family_name', 'nickname', 'birthdate', 'gender',
            'address1', 'address2', 'address3', 'postal_code', 'city', 'country',
            'email', 'home_phone', 'mobile_phone', 'birth_place', 'profession',
            'legal_guardian1_uuid', 'legal_guardian2_uuid', 'image_rights',
            'permission_alone', 'permission_with', 'permission_last_name', 'permission_first_name',
        )

    def get_permission_alone(self, obj):
        try:
            obj.adherent
        except Adherent.DoesNotExist:
            return None
        return obj.permission_with != ""

    def get_permission_with(self, obj):
        try:
            obj.adherent
        except Adherent.DoesNotExist:
            return None
        return obj.permission_with != ""

    def get_permission_first_name(self, obj):
        try:
            obj.adherent
        except Adherent.DoesNotExist:
            return None
        if not obj.permission_with:
            return ""
        return obj.permission_with.split()[0]

    def get_permission_last_name(self, obj):
        try:
            obj.adherent
        except Adherent.DoesNotExist:
            return None
        try:
            return obj.permission_with.split(maxsplit=1)[1]
        except IndexError:
            return ""


class AdherentSerializer4(serializers.ModelSerializer):
    person_uuid = serializers.UUIDField(source='person.uuid')
    status = serializers.IntegerField(
        label="Statut",
        help_text=", ".join(f"{key}={val}" for key, val in AdherentStatus.choices),
    )

    class Meta:
        model = Adherent
        fields = (
            'uuid', 'id', 'person_uuid', 'status', 'duplicates',
        )


class AdhesionSerializer4(serializers.ModelSerializer):
    adherent_uuid = serializers.UUIDField(source='adherent.uuid')
    begin = serializers.DateField(label="Date de début")
    end = serializers.DateField(label="Date de fin")
    structure_uuid = serializers.UUIDField(source='structure.uuid')

    class Meta:
        model = Adhesion
        fields = (
            'uuid', 'id', 'adherent_uuid', 'season', 'begin', 'end', 'entry_date', 'structure_uuid', 'canceled',
        )


class TeamTypeSerializer4(serializers.ModelSerializer):
    class Meta:
        model = TeamType
        fields = ('uuid', 'id', 'name', 'management')


class TeamSerializer4(serializers.ModelSerializer):
    structure_uuid = serializers.UUIDField(label="Structure", source='structure.uuid')
    type_uuid = serializers.UUIDField(label="Type d'équipe", source="type.uuid", default=None)

    class Meta:
        model = Team
        fields = ('uuid', 'id', 'structure_uuid', 'type_uuid', 'name', 'creation_date', 'deactivation_date')


class StructureTypeSerializer4(serializers.ModelSerializer):
    echelon = serializers.IntegerField(
        label="Échelon",
        help_text=", ".join(f"{key}={val}" for key, val in Echelon.choices),
    )

    class Meta:
        model = StructureType
        fields = ('uuid', 'id', 'name', 'echelon')


class StructureSerializer4(serializers.ModelSerializer):
    parent_uuid = serializers.UUIDField(label="Structure parente", source='parent.uuid', default=None, read_only=True)
    region_uuid = serializers.UUIDField(label="Région", source='region.uuid', default=None, read_only=True)
    type_uuid = serializers.UUIDField(label="Type de structure", source='type.uuid', read_only=True)
    status = serializers.IntegerField(
        label="Statut",
        help_text=", ".join(f"{key}={val}" for key, val in Structure.STATUS_CHOICES),
    )

    class Meta:
        model = Structure
        fields = ('uuid', 'id', 'name', 'parent_uuid', 'region_uuid', 'type_uuid', 'status', 'place',
                  'public_email', 'public_phone')


class StructureWriteSerializer4(serializers.ModelSerializer):
    status = serializers.IntegerField(
        label="Statut",
        help_text=", ".join(f"{key}={val}" for key, val in Structure.STATUS_CHOICES if key in (1, 2)),
    )

    def validate_status(self, value):
        if value not in (Structure.STATUS_AUTONOMOUS, Structure.STATUS_GUARDIANSHIP):
            raise serializers.ValidationError("Seuls les statuts 1 (autonome) et 2 (rattachée) sont autorisés.")
        return value

    class Meta:
        model = Structure
        fields = ('uuid', 'id', 'status')


class FunctionTypeSerializer4(serializers.ModelSerializer):
    category = serializers.IntegerField(
        label="Catégorie",
        help_text=", ".join(f"{key}={val}" for key, val in FunctionCategory.choices),
    )

    class Meta:
        model = FunctionType
        fields = ('uuid', 'id', 'name', 'category')


class FunctionSerializer4(serializers.ModelSerializer):
    person_uuid = serializers.UUIDField(source='person.uuid')
    team_uuid = serializers.UUIDField(source='team.uuid')
    begin = serializers.DateField(label="Date de début")
    end = serializers.DateField(label="Date de fin")
    type_uuid = serializers.UUIDField(source='function_config.function_type.uuid')

    class Meta:
        model = Function
        fields = ('uuid', 'id', 'person_uuid', 'team_uuid', 'begin', 'end', 'name', 'type_uuid')

    def validate(self, data):
        response = {}
        if 'uuid' in data:
            response['uuid'] = data['uuid']
        if 'person' in data:
            try:
                response['person'] = Person.objects.get(uuid=data['person']['uuid'])
            except Person.DoesNotExist:
                raise serializers.ValidationError({'person_uuid': ["Not found."]})
        if 'team' in data:
            try:
                response['team'] = Team.objects.get(uuid=data['team']['uuid'])
            except Team.DoesNotExist:
                raise serializers.ValidationError({'team_uuid': ["Not found."]})
        if 'begin' in data:
            response['begin'] = data['begin']
        if 'end' in data:
            response['end'] = data['end'] + TimeDelta(days=1)
        if 'name' in data:
            response['name'] = data['name']
        if 'function_config' in data:
            try:
                response['type'] = FunctionType.objects.get(uuid=data['function_config']['function_type']['uuid'])
            except FunctionType.DoesNotExist:
                raise serializers.ValidationError({'type_uuid': ["Not found."]})
        return response

    def create(self, validated_data):
        try:
            function_config = FunctionConfig.objects.get(
                Q(structure_type=validated_data['team'].structure.type) | Q(structure_type=None),
                function_type=validated_data['type'],
                team_type=validated_data['team'].type,
            )
        except FunctionConfig.DoesNotExist:
            raise serializers.ValidationError({'type': ["Not config found."]})
        except FunctionConfig.MultipleObjectsReturned:
            raise serializers.ValidationError({'type': ["Multiple configs found."]})
        if validated_data['end'] <= validated_data['begin']:
            raise serializers.ValidationError({'end': ["Should be after begin."]})
        return Function.objects.create(
            person=validated_data['person'],
            team=validated_data['team'],
            function_config=function_config,
            dates=DateRange(validated_data['begin'], validated_data['end']),
            name=validated_data['name'],
            status=FunctionStatus.OK,
            owner=self.context['request'].user
        )

    def update(self, instance, validated_data):
        if 'uuid' in validated_data and validated_data['uuid'] != instance.uuid:
            raise serializers.ValidationError({"uuid": ["Mismatch."]})
        if 'person' in validated_data:
            instance.person = validated_data['person']
        if 'team' in validated_data:
            team = validated_data['team']
            instance.team = team
        else:
            team = self.instance.team
        if 'type' in validated_data:
            try:
                instance.function_config = FunctionConfig.objects.get(
                    Q(structure_type=team.structure.type) | Q(structure_type=None),
                    function_type=validated_data['type'],
                    team_type=team.type,
                )
            except FunctionConfig.DoesNotExist:
                raise serializers.ValidationError({'type': ["Not config found."]})
            except FunctionConfig.MultipleObjectsReturned:
                raise serializers.ValidationError({'type': ["Multiple configs found."]})
        if 'begin' in validated_data:
            begin = validated_data['begin']
        else:
            begin = self.instance.begin
        if 'end' in validated_data:
            end = validated_data['end']
        else:
            end = self.instance.end + TimeDelta(days=1)
        if end <= begin:
            raise serializers.ValidationError({"end": ["Should be after begin."]})
        instance.dates = DateRange(begin, end)
        if 'name' in validated_data:
            instance.name = validated_data['name']
        instance.status = FunctionStatus.OK
        instance.save()
        return instance


class EmployeeSerializer4(serializers.ModelSerializer):
    person_uuid = serializers.UUIDField(source='person.uuid')

    class Meta:
        model = Employee
        fields = (
            'uuid', 'id', 'person_uuid', 'email', 'phone',
        )


class EmploymentSerializer4(serializers.ModelSerializer):
    employee_uuid = serializers.UUIDField(source='employee.uuid')
    begin = serializers.DateField(label="Date de début")
    end = serializers.DateField(label="Date de fin")
    type = serializers.IntegerField(
        label="Type",
        help_text=", ".join(f"{type.value}={type.name}={type.label}" for type in EmploymentType),
    )

    class Meta:
        model = Employment
        fields = ('uuid', 'id', 'employee_uuid', 'begin', 'end', 'name', 'type')


class RoleConfigSerializer4(serializers.ModelSerializer):
    permissions = serializers.SerializerMethodField()

    class Meta:
        model = RoleConfig
        fields = ('uuid', 'id', 'name', 'description', 'delegation', 'for_self', 'for_team', 'for_structure',
                  'for_sub_structures', 'for_all', 'only_electeds', 'only_employees', 'with_guardians',
                  'with_payers', 'permissions')

    def get_permissions(self, obj):
        return (f"{ct}.{name}" for ct, name in obj.permissions.values_list("content_type__app_label", "codename"))


class RoleConfigPermissionSerializer4(serializers.ModelSerializer):
    roleconfig_uuid = serializers.UUIDField(source='roleconfig.uuid')
    app_label = serializers.CharField(source='permission.content_type.app_label')
    model = serializers.CharField(source='permission.content_type.model')
    codename = serializers.CharField(source='permission.codename')

    class Meta:
        model = RoleConfigPermission
        fields = ('uuid', 'id', 'roleconfig_uuid', 'app_label', 'model', 'codename')


class AttributionSerializer4(serializers.ModelSerializer):
    role_config_uuid = serializers.UUIDField(source='role_config.uuid')
    structure_type_uuid = serializers.UUIDField(source='structure_type.uuid', default=None)
    team_type_uuid = serializers.UUIDField(source='team_type.uuid', default=None)
    function_type_uuid = serializers.UUIDField(source='function_type.uuid', default=None)

    class Meta:
        model = Attribution
        fields = ('uuid', 'id', 'role_config_uuid', 'structure_type_uuid', 'team_type_uuid',
                  'function_type_uuid', 'function_category')


class RoleSerializer4(serializers.ModelSerializer):
    config_uuid = serializers.UUIDField(source='config.uuid')
    team_uuid = serializers.UUIDField(source='team.uuid')
    holder_uuid = serializers.UUIDField(source='holder.uuid', default=None)
    proxy_uuid = serializers.UUIDField(source='proxy.uuid')

    class Meta:
        model = Role
        fields = ('uuid', 'id', 'config_uuid', 'team_uuid', 'holder_uuid', 'proxy_uuid')


class PermissionSerializer4(serializers.ModelSerializer):
    app_label = serializers.CharField(source='content_type.app_label')
    model = serializers.CharField(source='content_type.model')
    model_verbose_name = serializers.CharField(source='content_type.external.verbose_name')

    class Meta:
        model = Permission
        fields = ('name', 'app_label', 'model', 'codename', 'model_verbose_name')

    def create(self, validated_data):
        content_type_data = validated_data.pop('content_type')
        external_data = content_type_data.pop('external')
        codename = validated_data.pop('codename')
        content_type, _ = ContentType.objects.get_or_create(**content_type_data)
        ExternalContentType.objects.get_or_create(content_type=content_type, defaults=external_data)
        permission, _ = Permission.objects.update_or_create(
            content_type=content_type,
            codename=codename,
            defaults=validated_data,
        )
        return permission


class StructureResponseSerializer4(serializers.Serializer):
    updated = serializers.IntegerField(label="Nombre d'objets mis à jour")


class FunctionResponseSerializer4(serializers.Serializer):
    created = serializers.IntegerField(label="Nombre d'objets créés")
    updated = serializers.IntegerField(label="Nombre d'objets mis à jour")
