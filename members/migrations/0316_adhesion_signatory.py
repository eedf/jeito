# Generated by Django 5.0.3 on 2024-06-01 10:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0315_remove_teamtype_deletable_teamtype_custom'),
    ]

    operations = [
        migrations.AddField(
            model_name='adhesion',
            name='signatory',
            field=models.CharField(blank=True, max_length=200, verbose_name='Signataire'),
        ),
    ]
