# Generated by Django 4.2 on 2023-07-02 06:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0251_alter_structure_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='structuretype',
            name='old_name',
            field=models.CharField(blank=True, max_length=100, verbose_name='Ancien nom (API v1)'),
        ),
        migrations.AddField(
            model_name='structuretype',
            name='old_subtype_name',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Ancien nom du sous-type (API v1)'),
        ),
    ]
