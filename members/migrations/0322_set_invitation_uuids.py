from members.management.commands.anonymize import batched
from uuid import uuid4
from django.db import migrations


def set_uuids(apps, schema_editor):
    Invitation = apps.get_model('members', 'Invitation')
    for batch in batched(Invitation.objects.only('id'), 100):
        for row in batch:
            row.uuid = uuid4()
        Invitation.objects.bulk_update(batch, fields=["uuid"])


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0321_invitation_uuid'),
    ]

    operations = [
        migrations.RunPython(set_uuids, reverse_code=migrations.RunPython.noop),
    ]
