# Generated by Django 5.0.3 on 2025-02-25 19:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0332_person_unique_name_and_email_and_more'),
    ]

    operations = [
        migrations.AddConstraint(
            model_name='person',
            constraint=models.CheckConstraint(check=models.Q(('legal_guardian1', models.F('id')), _negated=True), name='check_legal_guardian1_not_self'),
        ),
        migrations.AddConstraint(
            model_name='person',
            constraint=models.CheckConstraint(check=models.Q(('legal_guardian2', models.F('id')), _negated=True), name='check_legal_guardian2_not_self'),
        ),
    ]
