# Generated by Django 5.0.3 on 2025-02-24 16:01

from django.db import migrations


def delete_unused_persons(apps, schema_editor):
    Person = apps.get_model('members', 'Person')
    persons = Person.objects.filter(
        adherent=None,
        employee=None,
        payment=None,
        protected1=None,
        protected2=None,
        invitation=None,
        user=None,
    )
    n = persons.count()
    for i, person in enumerate(persons):
        # print(f"{i+1}/{n} {person.last_name} {person.first_name}")
        person.no_sync = True
        person.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0328_alter_commune_uuid_alter_country_uuid'),
    ]

    operations = [
        migrations.RunPython(delete_unused_persons, reverse_code=migrations.RunPython.noop),
    ]
