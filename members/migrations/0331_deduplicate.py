# Generated by Django 5.0.3 on 2025-02-24 16:01

from django.db import migrations
from django.db.models import F, Q, Count, Max
from django.db.models.functions import Greatest, Coalesce


def deduplicate_field(apps, unique_field, empty):
    # print(f"Deduplicate {unique_field}")
    Person = apps.get_model('members', 'Person')
    Adherent = apps.get_model('members', 'Adherent')
    Employee = apps.get_model('members', 'Employee')
    User = apps.get_model('members', 'User')
    Role = apps.get_model('members', 'Role')
    Invitation = apps.get_model('members', 'Invitation')
    persons = Person.objects.exclude(**{unique_field: empty}).values(
        'last_name',
        'first_name',
        unique_field,
    ).annotate(nb=Count('id')).filter(nb__gt=1)
    n = persons.count()
    for i, person in enumerate(persons):
        duplicates = Person.objects.filter(
            last_name=person['last_name'],
            first_name=person['first_name'],
            **{unique_field: person[unique_field]},
        ).annotate(
            season=Max(Greatest(
                'adherent__last_adhesion__season',
                'protected1__adherent__last_adhesion__season',
                'protected2__adherent__last_adhesion__season',
                'payment__allocation__adhesion__season',
            )),
        ).order_by(
            F('season').desc(nulls_last=True),
            '-id',
        )
        # print(f"{i+1}/{n} {person['last_name']} {person['first_name']} {person[unique_field]} : {', '.join([f"#{d.pk}" for d in duplicates])}")
        assert len(duplicates) == person['nb'], f"{duplicates=}"
        keep = duplicates[0]
        for remove in duplicates[1:]:
            # Don't deduplicate a child and its legal guardian
            if (
                remove.legal_guardian1 == keep or
                remove.legal_guardian2 == keep or
                keep.legal_guardian1 == remove or
                keep.legal_guardian2 == remove
            ):
                print(f"{person['last_name']} {person['first_name']} : Can't merge child and legal guardian")
                continue
            # Adherent
            try:
                remove_adherent = remove.adherent
            except Adherent.DoesNotExist:
                remove_adherent = None
            try:
                keep_adherent = keep.adherent
            except Adherent.DoesNotExist:
                keep_adherent = None
            if remove_adherent and keep_adherent:
                adhesions = remove_adherent.adhesions.all()
                for adhesion in adhesions:
                    if not adhesion.canceled:
                        if keep_adherent.adhesions.filter(season=adhesion.season, canceled=False).exists():
                            adhesion.canceled = True
                    adhesion.adherent = keep_adherent
                    adhesion.no_sync = True
                    adhesion.save()
                    if not adhesion.canceled and (
                        not keep_adherent.last_adhesion or
                        keep_adherent.last_adhesion.season < adhesion.season
                    ):
                        keep_adherent.last_adhesion = adhesion
                keep_adherent.status = max(remove_adherent.status, keep_adherent.status)
                keep_adherent.duplicates.append(remove_adherent.id)
                keep_adherent.no_sync = True
                keep_adherent.save()
                remove_adherent.no_sync = True
                remove_adherent.delete()
            elif remove_adherent:
                remove_adherent.person = keep
                remove_adherent.no_sync = True
                remove_adherent.save()
            # Employee
            try:
                remove_employee = remove.employee
            except Employee.DoesNotExist:
                remove_employee = None
            try:
                keep_employee = keep.employee
            except Employee.DoesNotExist:
                keep_employee = None
            if remove_employee and keep_employee:
                employments = remove_employee.employments.all()
                for employment in employments:
                    assert not keep_employee.employments.filter(dates__overlap=employment.dates).exists()
                    employment.employee = keep_employee
                    employment.no_sync = True
                    employment.save()
                keep_employee.no_sync = True
                keep_employee.save()
                remove_employee.no_sync = True
                remove_employee.delete()
            elif remove_employee:
                remove_employee.person = keep
                remove_employee.no_sync = True
                remove_employee.save()
            # Protecteds
            protecteds = []
            for protected in remove.protected1.all():
                protected.legal_guardian1 = keep
                protected.no_sync = True
                protecteds.append(protected)
                protected.save()
            for protected in remove.protected2.all():
                protected.legal_guardian2 = keep
                protected.no_sync = True
                protecteds.append(protected)
                protected.save()
            # Payments
            for payment in remove.payment_set.all():
                payment.payer = keep
                payment.save()
            # Functions
            functions = remove.functions.all()
            for function in functions:
                function.person = keep
                function.no_sync = True
                function.save()
            # Invitations
            for invitation in remove.invitation_set.all():
                if Invitation.objects.filter(person=keep, season=invitation.season).exists():
                    invitation.delete()
                else:
                    invitation.person = keep
                    invitation.save()
            for invitation in Invitation.objects.filter(host=remove):
                invitation.host = keep
                invitation.save()
            # User
            try:
                remove.user.delete()
            except User.DoesNotExist:
                pass
            # Roles
            roles = Role.objects.filter(Q(holder=remove) | Q(proxy=remove))
            for role in roles:
                role.no_sync = True
                role.delete()
            # Self
            for field in (
                'gender',
                'family_name',
                'nickname',
                'birthdate',
                'address1',
                'address2',
                'address3',
                'postal_code',
                'city',
                'country',
                'home_phone',
                'mobile_phone',
                'birth_country',
                'birth_commune',
                'birth_place',
                'profession',
                'image_rights',
                'legal_guardian1',
                'legal_guardian2',
            ):
                if not getattr(keep, field):
                    setattr(keep, field, getattr(remove, field))
            if not keep.email:
                keep.email = remove.email
            if keep.email == remove.email and remove.email_as_login:
                keep.email_as_login = True
            remove.no_sync = True
            remove.delete()
            keep.no_sync = True
            keep.save()


def deduplicate(apps, schema_editor):
    # print("\n")
    deduplicate_field(apps, 'birthdate', None)
    deduplicate_field(apps, 'email', '')
    deduplicate_field(apps, 'mobile_phone', '')
    deduplicate_field(apps, 'home_phone', '')
    deduplicate_field(apps, 'postal_code', '')


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0330_fix_phone_numbers'),
    ]

    operations = [
        migrations.RunPython(deduplicate, reverse_code=migrations.RunPython.noop),
    ]
