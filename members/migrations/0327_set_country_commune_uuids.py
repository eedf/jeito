from members.management.commands.anonymize import batched
from uuid import uuid4
from django.db import migrations


def set_uuids(apps, schema_editor):
    Country = apps.get_model('members', 'Country')
    for batch in batched(Country.objects.only('id'), 100):
        for row in batch:
            row.uuid = uuid4()
        Country.objects.bulk_update(batch, fields=["uuid"])
    Commune = apps.get_model('members', 'Commune')
    for batch in batched(Commune.objects.only('id'), 100):
        for row in batch:
            row.uuid = uuid4()
        Commune.objects.bulk_update(batch, fields=["uuid"])


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0326_commune_uuid_country_uuid'),
    ]

    operations = [
        migrations.RunPython(set_uuids, reverse_code=migrations.RunPython.noop),
    ]
