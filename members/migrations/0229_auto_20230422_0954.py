# Generated by Django 3.2.9 on 2023-04-22 07:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0228_adhesion_date_not_null'),
    ]

    operations = [
        migrations.AlterField(
            model_name='adhesion',
            name='date',
            field=models.DateField(verbose_name="Date d'adhésion"),
        ),
        migrations.AlterField(
            model_name='adhesion',
            name='entry_date',
            field=models.DateField(verbose_name='Date de saisie'),
        ),
    ]
