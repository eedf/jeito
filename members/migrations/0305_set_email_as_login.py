# Generated by Django 5.0.3 on 2024-04-04 09:41

from django.db import migrations
from django.db.models import Count, Q


def set_email_as_login(apps, schema_editor):
    Person = apps.get_model('members', 'Person')
    persons = []
    for email in Person.objects.values('email').annotate(n=Count('id', filter=Q(email_as_login=True))).filter(n=0).values_list('email', flat=True):
        person = Person.objects.filter(email=email).earliest('birthdate')
        person.email_as_login = True
        persons.append(person)
    Person.objects.bulk_update(persons, fields=['email_as_login'])


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0304_alter_function_options'),
    ]

    operations = [
        migrations.RunPython(set_email_as_login, reverse_code=migrations.RunPython.noop),
    ]
