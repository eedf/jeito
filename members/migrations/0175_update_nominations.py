# Generated by Django 3.2.9 on 2022-12-07 15:13

from django.db import migrations
from django.db.models import Subquery, OuterRef
from django.utils.timezone import now
from datetime import date as Date
import json
from psycopg2.extras import DateRange


FRIEND_CODES = (
    'AMSLA',  # Ami
    'AMREG',  # Ami
    'AMING',  # Ami
    'AMSV',  # Ami
    'AMSR',  # Ami
    'GESRA',  # Gestionnaire rassemblement
)

PARTICIPANT_CODES = (
    'AIA',
    'ECE',
    'JAREG',
    'JASLA',
    'LOC',
    'LUO',
    'NON',
    'PAASV',
    'PACP',
    'PACR',
    'PAD',
    'PAREG',
    'PASLA',
    'PASR',
    'PASV',
    'PAV',
    'STCPN',
    'STREG',
    'STSLA',
    'STSR',
    'STSV',
)

VOLUNTEER_CODES = (
    'ANASV',
    'ANREG',
    'ANSLA',
    'ANSR',
    'ANV',
    'ANVCP',
    'ANVSV',
    'CCC',
    'CER',
    'CMN',
    'DIV',
    'EGSLA',
    'EGSR',
    'ENT',
    'FOR',
    'FOREG',
    'FORMA',
    'FOSLA',
    'MCD',
    'MCG',
    'MCGA',
    'MCGSV',
    'MED',
    'MER',
    'MERI',
    'PARSL',
    'PARSR',
    'RAC',
    'RACPN',
    'RAE',
    'RAF',
    'RAO',
    'RAREG',
    'RASLA',
    'RASR',
    'REFP',
    'RCP',
    'SEN',
    'VPR',
    'TRRA',
    'TRNA',
    'TRSA',
    'TRCPA',
    'RESA',  # Responsable de SLA adjoint
    'RRRA',  # Responsable régional adjoint
)

EMPLOYEE_CODES = (
    'ADM',
    'ADSLA',
    'AGENT',
    'ASLAN',
    'ASREG',
    'ASSLA',
    'ASSN',
    'ASSV',
    'ATR',
    'DEG',
    'DEN',
    'DIRSV',
    'DSASV',
    'DSSV',
    'DTN',
    'EAF',
    'PDC',
    'RAFT1',
    'RCT',
    'RFN',
    'SCCPN',
    'SCDEP',
    'SCGCP',
    'SCGDP',
    'SCGR',
    'SCGSL',
    'SCREG',
    'SCSLA',
    'SCSV',
    'SCVCP',
    'SCVN',
    'SCVR',
    'SCVSL',
    'SCVSR',
    'SCVSV',
    'SECS',
    'SGDEP',
    'SGREG',
    'SGSLA',
    'SPR',
    'SSA',
    'SUPER',
    'SVI',
)

LEADER_CODES = (
    'COA',  # Coordinateur de clan (probablement RUA en réalité)
    'PRE',  # Président
    'RCP',  # Responsable de centre permanent
    'RDD',  # Responsable départemental
    'RESLA',  # Responsable de SLA
    'RESR',
    'RESSV',  # Responsable de service vacances
    'RRI',
    'RRR',  # Responsable régional
    'RUA',  # Responsable d'unité aîné
    'RUC',  # Responsable d'unité lutin
    'RUE',  # Responsable d'unité éclé
    'RUO',  # Responsable d'unité louveteau
)

TREASURER_CODES = (
    'TDE',  # Départemental
    'TRCPN',  # CPN
    'TRESV',  # SV
    'TRI',  # Régional intérimaire
    'TRN',  # National
    'TRR',  # Régional
    'TRSLA',  # SLA
    'TRSR',  # Structure rattachée
)

TEAM_CODES = {
    "Activité adaptée": ('ANASV', 'ANV', 'ANVSV', 'PAASV', 'PAD', 'PASV', 'RAF', 'DSASV', 'DSSV', 'DIRSV', 'DIV'),
    "Activités complémentaires": ('ANREG', 'ANVCP', 'PACP', 'PACR', 'PAREG', 'PASLA', 'PASR', 'PAV', 'STCPN', 'STREG', 'STSLA', 'STSR', 'STSV'),
    "CAF": (),
    "Cercle louveteau·tte": ('LOC', 'RAC', 'RUC'),
    "Commission": ('CMN', 'ENT'),
    "Commission de contrôle": ('CCC', ),
    "Équipe de gestion": ('ANSLA', 'ANSR', 'EGSLA', 'EGSR', 'MCG', 'MCGA', 'MCGSV', 'PARSL', 'PARSR', 'RACPN', 'RASLA', 'RASR', 'RCP', 'RESA', 'RESLA', 'RESR', 'RESSV', 'TRCPA', 'TRCPN', 'TRESV', 'TRSA', 'TRSLA', 'TRSR', 'MED', 'RDD', 'TDE', 'MCD', 'PRE', 'SEN', 'TRN', 'TRNA', 'VPR', 'CER', 'MER', 'MERI', 'RAREG', 'REFP', 'RRI', 'RRR', 'RRRA', 'TRI', 'TRR', 'TRRA'),
    "Équipe nationale": ('DEG', 'DEN'),
    "Équipes Support et Développement": ('ADM', 'ASSN', 'DTN', 'EAF', 'GESRA', 'PDC', 'RAFT1', 'RCT', 'RFN', 'SCVN', 'SECS', 'SPR', 'SSA', 'SUPER', 'SVI',
                                         'ADSLA', 'ASREG', 'SCDEP', 'SCGR', 'SCREG', 'SCVR', 'SGREG', 'ATR', 'AGENT', 'SCGSL', 'SGSLA', 'SCGCP', 'SCVCP', 'SCVSL', 'SCVSR', 'SCVSV', 'ASSLA', 'ASLAN', 'ASSV', 'SCCPN', 'SCGDP', 'SCSLA', 'SCSV', 'SGDEP'),   # 2nd line = regional and local
    "Format·eur·rices": ('FOR', 'FOREG', 'FORMA', 'FOSLA'),
    "Ronde Lutin·e": ('LUO', 'RAO', 'RUO'),
    "Unité Aîné·e": ('AIA', 'COA', 'RUA'),
    "Unité Éclé·e": ('ECE', 'RAE', 'RUE'),
    "Unité Nomade": ('JAREG', 'JASLA', 'NON'),
}

def update_nominations(apps, schema_editor):
    FunctionType = apps.get_model('members', 'FunctionType')
    FunctionConfig = apps.get_model('members', 'FunctionConfig')
    Nomination = apps.get_model('members', 'Nomination')
    Adhesion = apps.get_model('members', 'Adhesion')
    Function = apps.get_model('members', 'Function')
    Team = apps.get_model('members', 'Team')
    TeamConfig = apps.get_model('members', 'TeamConfig')
    Structure = apps.get_model('members', 'Structure')
    TeamType = apps.get_model('members', 'TeamType')
    StructureType = apps.get_model('members', 'StructureType')

    # Quit if database is empty
    if not Nomination.objects.exists():
        return

    # Load function types and configs
    with open("members/fixtures/functionconfig.json", "r") as f:
        for obj in json.load(f):
            if obj['model'] == 'members.functiontype':
                FunctionType.objects.create(pk=obj['pk'], **obj['fields'])
            if obj['model'] == 'members.functionconfig':
                obj['fields']['function_type'] = FunctionType.objects.get(pk=obj['fields']['function_type'])
                obj['fields']['team_type'] = TeamType.objects.get(pk=obj['fields']['team_type'])
                if obj['fields']['structure_type']:
                    obj['fields']['structure_type'] = StructureType.objects.get(pk=obj['fields']['structure_type'])
                FunctionConfig.objects.create(pk=obj['pk'], **obj['fields'])

    # Remove friend nominations
    print(" friends,", end="")
    Nomination.objects.filter(function__code__in=FRIEND_CODES).delete()

    # Remove nominations for canceled adhesions
    print(" canceled,", end="")
    Nomination.objects.filter(adhesion__canceled=True).delete()

    # Set functions
    print(" functions,", end="")
    for config in FunctionConfig.objects.all():
        codes = set(TEAM_CODES[config.team_type.name])
        codes &= set({
            "Membre": VOLUNTEER_CODES,
            "Participant·e simple": PARTICIPANT_CODES,
            "Responsable": LEADER_CODES,
            "Trésorier·e": TREASURER_CODES,
            "Délégué·e à l'AG": {},
            "Salarié·e": EMPLOYEE_CODES,
        }[config.function_type.name])
        nominations = Nomination.objects.filter(function__code__in=codes)
        if config.structure_type:
            nominations = nominations.filter(structure__type=config.structure_type)
        nominations.update(function_config=config)
    remain = Nomination.objects.filter(function_config=None)[:50]
    if remain:
        print(remain.values_list('id', flat=True))
        for x in remain:
            print(f"person={x.adhesion.person.last_name} adhesion={x.adhesion.pk}, code={x.function.code} structure type={x.structure.type.name}")
        # assert False

    # Set person
    print(" persons,", end="")
    Nomination.objects.update(person=Subquery(Adhesion.objects.filter(pk=OuterRef('adhesion_id')).values('person')[:1]))
    remain = Nomination.objects.filter(person=None)[:50]
    if remain:
        print(remain.values_list('id', flat=True))
        assert False

    # Set dates
    print(" dates,", end="")
    for season in range(2013, 2024):
        Nomination.objects.filter(adhesion__season=season).update(
            dates=DateRange(Date(season - 1, 9, 1), Date(season, 8, 31), '[]'),
        )
    remain = Nomination.objects.filter(dates=None)[:50]
    if remain:
        print(remain.values_list('id', flat=True))
        assert False

    # Set name
    print(" names,", end="")
    for function in Function.objects.all():
        Nomination.objects.filter(function=function).update(
            name=function.name
        )
    remain = Nomination.objects.filter(name="")[:50]
    if remain:
        print(remain.values_list('id', flat=True))
        assert False

    # Set team
    print(" teams", end="")
    for name, codes in TEAM_CODES.items():
        for structure in Structure.objects.all():
            nominations = structure.nomination_set.filter(function__code__in=codes)
            if not nominations:
                continue
            try:
                team = Team.objects.get(structure=structure, type__name=name)
            except Team.DoesNotExist:
                try:
                    config = TeamConfig.objects.get(structure_type=structure.type, team_type__name=name)
                except TeamConfig.DoesNotExist:
                    print(f"Missing team config for structure type {structure.type.name}, team type {name}")
                    continue
                team = Team.objects.create(
                    type=config.team_type,
                    structure=structure,
                    name=config.structure_type.management_team_name if config.team_type.management else name,
                    creation_date=now(),
                    deactivation_date=None if structure.status in (1, 2) else now(),
                )
            nominations.update(team=team)
    remain = Nomination.objects.filter(team=None)[:50]
    if remain:
        print(remain.values_list('id', flat=True))
        assert False


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0174_auto_20221207_1605'),
    ]

    operations = [
        migrations.RunPython(update_nominations, migrations.RunPython.noop),
    ]
