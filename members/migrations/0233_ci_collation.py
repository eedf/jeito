# Generated by Django 4.2 on 2023-04-22 20:29

from django.contrib.postgres.operations import CreateCollation
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0232_last_employment'),
    ]

    operations = [
        CreateCollation("case_insensitive", provider="icu", locale="und-u-ks-level2", deterministic=False),
    ]
