# Generated by Django 4.2 on 2024-03-08 17:47

from decimal import Decimal
from django.db import migrations
from django.db.models import Case, When, Sum, Value, F, Q
from django.db.models.functions import Coalesce


def set_is_paid(apps, schema_editor):
    Adhesion = apps.get_model('members', 'Adhesion')
    adhesions = Adhesion.objects.annotate(
        qs_to_pay=Case(
            When(rate__rate_max=None, then=F('rate__rate')),
            default=F('free_rate')
        ) + F('donation'),
        qs_paid=Coalesce(Sum(
            'allocation__amount',
            filter=Q(allocation__payment__online_status__in=(1, 4))
        ), Value(Decimal(0))),
        qs_is_paid=Case(
            When(qs_paid__gte=F('qs_to_pay'), then=Value(True)),
            default=Value(False)
        )
    ).filter(
        qs_is_paid=True,
    ).update(
        is_paid=True,
    )


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0282_adhesion_is_paid'),
    ]

    operations = [
        migrations.RunPython(set_is_paid, reverse_code=migrations.RunPython.noop),
    ]
