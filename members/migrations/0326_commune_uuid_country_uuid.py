# Generated by Django 5.0.3 on 2024-10-20 16:32

import uuid
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0325_alter_commune_options_alter_country_options'),
    ]

    operations = [
        migrations.AddField(
            model_name='commune',
            name='uuid',
            field=models.UUIDField(default=uuid.uuid4),
        ),
        migrations.AddField(
            model_name='country',
            name='uuid',
            field=models.UUIDField(default=uuid.uuid4),
        ),
    ]
