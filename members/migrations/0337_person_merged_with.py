# Generated by Django 5.0.3 on 2025-03-01 17:18

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0336_person_check_different_guardians'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='merged_with',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.UUIDField(), blank=True, default=list, size=None, verbose_name='Fusionné avec'),
        ),
    ]
