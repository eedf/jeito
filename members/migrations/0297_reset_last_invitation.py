# Generated by Django 4.2 on 2024-03-15 10:48

from django.db import migrations
from members.utils import current_season


def reset_last_invitation(apps, schema_editor):
    Person = apps.get_model('members', 'Person')
    persons = Person.objects.filter(last_invitation=current_season()).exclude(
        invitation__season=current_season(),
        invitation__adhesion=None,
    )
    for person in persons:
        person.last_invitation = None
    Person.objects.bulk_update(persons, fields=['last_invitation'])


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0296_alter_season_form'),
    ]

    operations = [
        migrations.RunPython(reset_last_invitation, reverse_code=migrations.RunPython.noop),
    ]
