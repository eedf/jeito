# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

import unicodedata
from datetime import timedelta
from django.conf import settings
from django.http import HttpRequest
from django.utils.timezone import now, make_naive


def today():
    return make_naive(now()).date()


def yesterday():
    return make_naive(now() - timedelta(days=1)).date()


def first_season():
    return 2013


def current_season():
    today_ = today()
    if today_.month < 9:
        return today_.year
    else:
        return today_.year + 1


def current_year():
    return today().year


def replace_year(date, year):
    if date.month == 2 and date.day == 29:
        return date.replace(year=year, month=3, day=1)
    return date.replace(year=year)


def next_transfer_date():
    today_ = today()
    if today_.month == 12:
        if today_.day < 20:
            return today_.replace(day=20)
        else:
            return today_.replace(year=today_.year + 1, month=2, day=1)
    return today_.replace(month=today_.month+1, day=1)


def build_url(url, request=None):
    if not request:
        request = HttpRequest()
        request.META = {
            'SERVER_NAME': settings.HOST,
            'SERVER_PORT': 8000 if settings.DEBUG else 80,
            'HTTP_X_FORWARDED_PROTO': 'http' if settings.DEBUG else 'https',
        }
    return request.build_absolute_uri(url)


def remove_accents(input_str):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    only_ascii = nfkd_form.encode('ASCII', 'ignore').decode()
    return only_ascii


def normalize_name(name):
    name = name.translate(str.maketrans({"-": " ", "œ": "oe", "Œ": "OE", "æ": "ae", "Æ": "AE"}))
    name = remove_accents(name)
    name = name.upper()
    return name


def tokenize_name(name):
    name = normalize_name(name)
    tokens = set(name.split())
    assert all((token for token in tokens))
    return tokens
