# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

# -*- coding: utf8 -*-

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.forms import UserChangeForm as BaseUserChangeForm
from rest_framework.authtoken.admin import TokenAdmin
from . import models


@admin.register(models.Season)
class SeasonAdmin(admin.ModelAdmin):
    list_display = ('year', 'card', 'form')


@admin.register(models.StructureType)
class StructureTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'echelon', 'can_import', 'has_iban')
    list_filter = ('echelon', 'can_import', 'has_iban')
    search_fields = ('name', '=uuid')


@admin.register(models.Structure)
class StructureAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'parent', 'iban', 'status')
    search_fields = ('name', 'iban', '=uuid')
    list_filter = ('type', 'status')


@admin.register(models.FunctionType)
class FunctionTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'category')
    list_filter = ('category', )
    search_fields = ('=uuid', )


@admin.register(models.FunctionConfig)
class FunctionConfigAdmin(admin.ModelAdmin):
    list_display = ('name', 'team_type', 'function_type', 'structure_type', 'age_min', 'age_max',
                    'subject_to_nomination', 'duration')
    list_filter = ('team_type', 'function_type', 'structure_type', 'subject_to_nomination', 'duration')
    search_fields = ('name', )


@admin.register(models.Rate)
class RateAdmin(admin.ModelAdmin):
    list_display = ('name', 'season', 'rate', 'bracket', 'qf_min', 'qf_max', 'next_child', 'category',
                    'can_be_free', 'age_min', 'age_max', 'order')
    list_editable = ('category', 'can_be_free', 'age_min', 'age_max', 'order', 'qf_min', 'qf_max', 'next_child')
    list_filter = ('season', 'category', 'function_category', 'bracket', 'can_be_free', 'next_child')
    search_fields = ('name', )


@admin.register(models.Person)
class PersonAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'birthdate', 'email')
    search_fields = ('first_name', 'last_name', 'family_name', 'nickname', '=email', '=uuid')
    ordering = ('last_name', 'first_name')
    list_filter = ('email_as_login', 'gender', 'expired')
    date_hierarchy = 'birthdate'
    autocomplete_fields = ('legal_guardian1', 'legal_guardian2', 'birth_commune')
    fieldsets = (
        ('Identité', {'fields': ('gender', 'first_name', 'last_name', 'family_name', 'nickname',
                                 'birthdate', 'birth_country', 'birth_commune', 'birth_place')}),
        ('Contacts', {'fields': ('email', 'email_as_login', 'home_phone', 'mobile_phone')}),
        ('Adresse', {'fields': ('address1', 'address2', 'address3',
                                'postal_code', 'city', 'country')}),
        ('Autres', {'fields': ('profession', 'image_rights', 'legal_guardian1', 'legal_guardian2', 'uuid')}),
        ('Dénormalisation', {'fields': ('last_invitation', 'has_protected', 'has_payment', 'expired')}),
    )


@admin.register(models.Adherent)
class AdherentAdmin(admin.ModelAdmin):
    list_display = ('id', 'person', 'status')
    search_fields = ('=id', 'person__first_name', 'person__last_name', '=person__email', '=uuid')
    ordering = ('person__last_name', 'person__first_name')
    list_filter = ('status', )
    autocomplete_fields = ('person', 'last_adhesion')


@admin.register(models.Employee)
class EmployeeAdmin(admin.ModelAdmin):
    list_display = ('id', 'person')
    search_fields = ('=id', 'person__first_name', 'person__last_name', '=person__email', '=email', '=uuid')
    ordering = ('person__last_name', 'person__first_name')
    autocomplete_fields = ('person', 'last_employment')


@admin.register(models.Function)
class FunctionAdmin(admin.ModelAdmin):
    list_display = ('person', 'team', 'function_config', 'dates', 'name')
    list_filter = ('function_config', )
    search_fields = ('=person__id', 'person__last_name', 'person__first_name', 'name', '=uuid')
    ordering = ('person__last_name', 'person__first_name')
    autocomplete_fields = ('person', 'team', 'owner')


class AllocationInline(admin.TabularInline):
    model = models.Allocation
    autocomplete_fields = ('adhesion', 'payment', )


class NotificationInline(admin.TabularInline):
    model = models.Notification
    autocomplete_fields = ('adhesion', )


@admin.register(models.Adhesion)
class AdhesionAdmin(admin.ModelAdmin):
    list_display = ('adherent', 'season', 'entry_date', 'rate', 'structure', 'canceled')
    search_fields = ('adherent__id', 'adherent__person__first_name', 'adherent__person__last_name',
                     '=adherent__person__email', '=uuid')
    list_filter = ('canceled', 'season', 'rate__category')
    inlines = (AllocationInline, NotificationInline)
    autocomplete_fields = ('adherent', 'structure')
    date_hierarchy = 'entry_date'


@admin.register(models.Employment)
class EmploymentAdmin(admin.ModelAdmin):
    list_display = ('employee', 'begin', 'end', 'type', 'name')
    search_fields = (
        'employee__person__first_name', 'employee__person__last_name', '=employee__person__email',
        '=employee__email', 'employee__name', '=uuid',
    )
    list_filter = ('type', )
    autocomplete_fields = ('employee', )


@admin.register(models.Payment)
class PaymentAdmin(admin.ModelAdmin):
    list_display = ('payer', 'amount', 'date', 'method', 'online_status')
    date_hierarchy = 'date'
    search_fields = (
        '=allocation__adhesion__adherent__id',
        'allocation__adhesion__adherent__person__last_name', 'allocation__adhesion__adherent__person__first_name',
        'payer__last_name', 'payer__first_name', '=payer__email',
    )
    list_filter = ('method', 'online_status')
    inlines = (AllocationInline, )
    autocomplete_fields = ('payer', )


@admin.register(models.Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = ('payment', 'name', 'amount', 'date', 'number', 'canceled')
    date_hierarchy = 'date'
    search_fields = ('payment__payer__last_name', 'payment__payer__first_name', 'name', '=number')
    list_filter = ('canceled', )
    autocomplete_fields = ('payment', )


@admin.register(models.TransferOrder)
class TransferOrderAdmin(admin.ModelAdmin):
    list_display = ('date', )
    date_hierarchy = 'date'


@admin.register(models.Transfer)
class TransferAdmin(admin.ModelAdmin):
    list_display = ('order', 'structure', 'iban', 'carry', 'contribution', 'donation', 'amount')
    date_hierarchy = 'order__date'
    search_fields = ('structure__name', )
    save_as = True


@admin.register(models.Entry)
class EntryAdmin(admin.ModelAdmin):
    list_display = ('transfer', 'adhesion', 'amount', 'category')
    list_filter = ('category', 'adhesion__season')
    date_hierarchy = 'transfer__order__date'
    search_fields = ('transfer__structure__name', '=adhesion__adherent__id')
    autocomplete_fields = ('adhesion', 'transfer')


@admin.register(models.Invitation)
class InvitationAdmin(admin.ModelAdmin):
    list_display = ('season', 'person', 'email', 'structure', 'team', 'function_config', 'adhesion')
    list_filter = ('season', 'declined')
    date_hierarchy = 'modification_date'
    search_fields = ('=token', '=email', 'person__last_name', 'person__first_name')
    autocomplete_fields = ('person', 'structure', 'team', 'adhesion', 'host')


@admin.register(models.Notification)
class NotificationAdmin(admin.ModelAdmin):
    list_display = ('adhesion', 'date')
    date_hierarchy = 'date'


class TeamConfigInline(admin.TabularInline):
    model = models.TeamConfig


@admin.register(models.TeamType)
class TeamTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'unique', 'management', 'custom', 'order')
    list_filter = ('unique', 'management', 'custom')
    search_fields = ('name', '=uuid')
    inlines = (TeamConfigInline, )


@admin.register(models.Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = ('name', 'structure', 'creation_date', 'deactivation_date')
    list_filter = ('type', )
    date_hierarchy = 'creation_date'
    search_fields = ('name', '=uuid')
    autocomplete_fields = ('structure', )


class AttributionInline(admin.TabularInline):
    model = models.Attribution


class RoleConfigPermissionInline(admin.TabularInline):
    model = models.RoleConfigPermission
    autocomplete_fields = ('permission', )


@admin.register(models.RoleConfig)
class RoleConfigAdmin(admin.ModelAdmin):
    list_display = ('name', 'delegation', 'rgpd',
                    'for_self', 'for_team', 'for_structure', 'for_sub_structures', 'for_all',
                    'only_electeds', 'only_employees', 'with_guardians', 'with_payers')
    list_filter = ('delegation', 'for_self', 'for_team', 'for_structure', 'for_sub_structures', 'for_all',
                   'only_electeds', 'only_employees', 'with_guardians', 'with_payers', 'rgpd')
    search_fields = ('name', '=uuid')
    inlines = (AttributionInline, RoleConfigPermissionInline)


@admin.register(models.Attribution)
class AttributionAdmin(admin.ModelAdmin):
    list_display = ('role_config', 'structure_type', 'team_type', 'function_type', 'function_category')
    list_filter = ('role_config', 'structure_type', 'team_type', 'function_type', 'function_category')
    search_fields = ('role_config__name', '=uuid')


@admin.register(models.Role)
class RoleAdmin(admin.ModelAdmin):
    list_display = ('config', 'team', 'holder', 'proxy')
    list_filter = ('config', )
    search_fields = ('holder__last_name', 'holder__first_name', 'proxy__last_name', 'proxy__first_name', '=uuid')
    autocomplete_fields = ('team', 'holder', 'proxy')


class RoleConfigInline(admin.TabularInline):
    model = models.RoleConfig.permissions.through


@admin.register(models.Permission)
class PermissionAdmin(admin.ModelAdmin):
    list_display = ('content_type', 'codename', 'name')
    list_filter = ('content_type__app_label', )
    search_fields = ('codename', 'name')
    inlines = [RoleConfigInline]


@admin.register(models.ExternalContentType)
class ExternalContentTypeAdmin(admin.ModelAdmin):
    list_display = ('content_type', 'verbose_name')
    search_fields = ('verbose_name', )


class UserChangeForm(BaseUserChangeForm):
    class Meta:
        model = models.User
        fields = "__all__"


@admin.register(models.User)
class UserAdmin(AuthUserAdmin):
    list_display = ("username", "email", "is_staff")
    list_filter = ("is_superuser", "groups")
    search_fields = ("username", "person__first_name", "person__last_name", "=person__email")
    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (
            "Permissions",
            {
                "fields": (
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        ("Important dates", {"fields": ("last_login", )}),
    )
    form = UserChangeForm


@admin.register(ContentType)
class ContentTypeAdmin(admin.ModelAdmin):
    list_display = ('app_label', 'model')
    list_filter = ('app_label', )
    search_fields = ('app_label', 'model')
    ordering = ('app_label', 'model')


@admin.register(models.Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'iso2', 'iso3')
    search_fields = ('name', '=id', '=iso2', '=iso3')


@admin.register(models.Commune)
class CommuneAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'dates')
    search_fields = ('name', '=code')


@admin.register(models.PersonToken)
class PersonTokenAdmin(admin.ModelAdmin):
    list_display = ('token', 'person', 'part')
    list_filter = ('part', )
    search_fields = ('token', )
    autocomplete_fields = ('person', )


TokenAdmin.autocomplete_fields = ('user', )
