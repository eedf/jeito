# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from datetime import date as Date, timedelta as TimeDelta
from psycopg2.extras import DateRange
from django import forms
from django.conf import settings
from django.contrib.auth.forms import PasswordResetForm as PasswordResetBaseForm, UsernameField, SetPasswordForm
from django.contrib.auth.tokens import default_token_generator
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator, MaxValueValidator, EmailValidator
from django.db.models import Q
from django.forms.widgets import RadioSelect
from django.template.defaultfilters import pluralize
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.formats import date_format
from django.utils.http import urlsafe_base64_encode
from django.utils.safestring import mark_safe
from crispy_forms.bootstrap import FormActions, InlineRadios
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, HTML
from dal import autocomplete, forward
from .utils import first_season, current_season, today, replace_year, normalize_name
from .models import Adhesion, Adherent, Function, AdherentStatus, Person, Rate, Structure, Payment, \
    Invitation, Team, TeamConfig, FunctionConfig, FunctionCategory, Employment, Employee, FunctionStatus, age, \
    Role, RoleConfig, Permission, User, RateCategory, NameValidator, ExternalContentType, Echelon


CONTACT_MESSAGE = "Merci de contacter le <a href=\"/aide/assistance.html\" target=\"_blank\">siège national</a> " \
    "pour plus d'information."


class BirthDateField(forms.DateField):
    def widget_attrs(self, widget):
        attrs = super().widget_attrs(widget)
        attrs.setdefault('placeholder', 'jj/mm/aaaa')
        return attrs

    def clean(self, value):
        if value is not None:
            year = value.rsplit('/', 1)[-1]
        value = super().clean(value)
        if value is None:
            return None
        today_ = today()
        if value > today_ and len(year) == 2:
            value = replace_year(value, value.year - 100)
        if value > today_:
            raise ValidationError("La date de naissance ne peut pas être dans le futur.")
        if value < Date(1900, 1, 1):
            raise ValidationError("La date de naissance ne peut pas être antérieure à 1900.")
        return value


class StructureForm(forms.ModelForm):
    class Meta:
        model = Structure
        fields = (
            'name', 'type', 'parent', 'region', 'place', 'public_email', 'public_phone', 'iban',
        )
        widgets = {
            'parent': autocomplete.ModelSelect2(url='structure-autocomplete',
                                                forward=[forward.Field('type', 'child__type')]),
            'region': autocomplete.ModelSelect2(url='structure-autocomplete',
                                                forward=[forward.Const(str(Echelon.REGIONAL), 'echelon')]),
            'place': autocomplete.ListSelect2(
                url='place-autocomplete',
                attrs={'data-tags': 1, 'data-minimum-input-length': 3},
            ),
        }

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['iban'].label += '*'
        if self.instance.pk:
            if not user.has_perm('members.change_structure_core', self.instance):
                del self.fields['name']
                del self.fields['type']
                del self.fields['parent']
                del self.fields['region']
            else:
                self.fields['region'].queryset = Structure.objects.filter(type__echelon=Echelon.REGIONAL)
            if not user.has_perm('members.change_structure_iban', self.instance):
                del self.fields['iban']
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(*self.fields.keys())

    def clean_parent(self):
        parent = self.cleaned_data['parent']
        type = self.cleaned_data.get('type')
        if not parent or not type:
            return parent
        if type.echelon <= parent.type.echelon:
            raise ValidationError(
                f"L'échelon {parent.type.get_echelon_display()} ne peut être parent "
                f"de l'échelon {type.get_echelon_display()}."
            )
        return parent

    def clean_region(self, *args, **kwargs):
        type = self.cleaned_data.get('type')
        parent = self.cleaned_data.get('parent')
        region = self.cleaned_data['region']
        if type and type.echelon != Echelon.LOCAL:
            return None
        if parent and parent.type.echelon == Echelon.NATIONAL:
            return region
        region = None
        ancestor = parent
        while ancestor:
            if ancestor.type.echelon == Echelon.REGIONAL:
                region = ancestor
                break
            ancestor = ancestor.parent
        return region

    def clean_iban(self):
        iban = self.cleaned_data['iban']
        type = self.cleaned_data.get('type') if 'type' in self.fields else self.instance.type
        if not type:
            return iban
        if type.has_iban and not iban:
            self.add_error('iban', f"L'IBAN doit être renseigné pour le type de structure « {type.name} ».")
        if not type.has_iban:
            return ''
        return iban


class AdhesionPreCreateForm(forms.Form):
    id = forms.IntegerField(label="Numéro", required=False)
    first_name = forms.CharField(label="Prénom", max_length=100, required=False)
    last_name = forms.CharField(label="Nom", max_length=100, required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs['class'] = 'uppercase'
        self.fields['last_name'].widget.attrs['class'] = 'uppercase'
        self.helper = FormHelper()
        self.helper.layout = Layout(
            HTML("<h4>Si il s'agit d'un renouvellement et que tu connais le numéro d'adhérent·e :</h4>"),
            'id',
            HTML("<h4>Ou sinon :</h4>"),
            HTML('<div class="row"><div class="col-md-6">'),
            'last_name',
            HTML('</div><div class="col-md-6">'),
            'first_name',
            HTML('</div></div>'),
            FormActions(
                HTML('<a href="/" class="btn btn-default">Annuler</a>'),
                Submit('next', 'Suivant'),
            )
        )

    def clean_last_name(self):
        last_name = self.cleaned_data.get('last_name')
        return last_name and last_name.upper()

    def clean_first_name(self):
        first_name = self.cleaned_data.get('first_name')
        return first_name and first_name.upper()

    def clean_id(self):
        id = self.cleaned_data.get('id')
        if id and not Adherent.objects.filter(id=id).exists():
            raise ValidationError("Aucun·e adhérent·e n'existe avec ce numéro")
        return id

    def clean(self):
        cleaned_data = super().clean()
        if self.errors:
            return cleaned_data
        id = cleaned_data.get('id')
        first_name = cleaned_data.get('first_name')
        last_name = cleaned_data.get('last_name')
        if not id and not (first_name or last_name):
            raise ValidationError("Merci de saisir au choix le numéro ou les nom/prénom.")
        if id and (first_name or last_name):
            raise ValidationError("Merci de ne saisir que le numéro ou que les nom/prénom.")
        if first_name and not last_name:
            self.add_error('last_name', "Merci de saisir également le nom.")
        if last_name and not first_name:
            self.add_error('first_name', "Merci de saisir également le prénom.")
        return cleaned_data


class StatusForm(forms.ModelForm):
    class Meta:
        model = Adherent
        fields = ('status', 'suspension_deadline')
        widgets = {
            'status': RadioSelect(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout('status', 'suspension_deadline')

    def clean_suspension_deadline(self):
        suspension_deadline = self.cleaned_data['suspension_deadline']
        if not suspension_deadline:
            return None
        if suspension_deadline < today():
            raise ValidationError("Cette date est passée.")
        status = self.cleaned_data.get('status')
        if not status:
            return suspension_deadline
        if status != AdherentStatus.SUSPENDED:
            return None
        return suspension_deadline


class PayerForm(forms.ModelForm):
    gender = forms.ChoiceField(
        label="Genre",
        choices=Person.GENDER_CHOICES,
        widget=forms.RadioSelect(),
    )

    class Meta:
        model = Person
        fields = (
            'last_name', 'first_name', 'gender',
            'address1', 'address2', 'address3',
            'postal_code', 'city', 'country',
            'email', 'home_phone', 'mobile_phone',
        )

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs['class'] = 'uppercase'
        self.fields['last_name'].widget.attrs['class'] = 'uppercase'
        self.fields['gender'].required = True
        self.fields['email'].help_text = "Requis si plus de 16 ans."
        for field in ('address1', 'postal_code', 'city'):
            self.fields[field].required = True
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            HTML('<div class="row"><div class="col-md-4">'),
            'last_name',
            HTML('</div><div class="col-md-4">'),
            'first_name',
            HTML('</div><div class="col-md-4">'),
            InlineRadios('gender'),
            HTML('</div></div>'),
            'address1',
            'address2',
            'address3',
            HTML('<div class="row"><div class="col-md-3">'),
            'postal_code',
            HTML('</div><div class="col-md-5">'),
            'city',
            HTML('</div><div class="col-md-4">'),
            'country',
            HTML('</div></div>'),
            HTML('<div class="row"><div class="col-md-4">'),
            'email',
            HTML('</div><div class="col-md-4">'),
            'home_phone',
            HTML('</div><div class="col-md-4">'),
            'mobile_phone',
            HTML('</div></div>'),
        )

    def clean_last_name(self):
        last_name = self.cleaned_data.get('last_name')
        return last_name and last_name.upper()

    def clean_first_name(self):
        first_name = self.cleaned_data.get('first_name')
        return first_name and first_name.upper()

    def clean_email(self):
        birthdate = self.cleaned_data.get('birthdate') or self.instance.birthdate
        email = self.cleaned_data.get('email')
        if birthdate and age(birthdate) >= 16 and not email and not self.errors:
            raise ValidationError("Obligatoire pour les personnes de plus de 16 ans.")
        return email

    def clean_home_phone(self):
        home_phone = self.cleaned_data['home_phone']
        if home_phone and home_phone.country_code == 33 and str(home_phone.national_number)[0] in '67':
            raise ValidationError("Un numéro fixe ne doit pas commencer par 06 ou 07.")
        return home_phone

    def clean_mobile_phone(self):
        mobile_phone = self.cleaned_data['mobile_phone']
        if mobile_phone and mobile_phone.country_code == 33 and str(mobile_phone.national_number)[0] not in '67':
            raise ValidationError("Un numéro mobile doit commencer par 06 ou 07.")
        home_phone = self.cleaned_data.get('home_phone')
        if mobile_phone and mobile_phone == home_phone:
            raise ValidationError("Les deux numéros de téléphone sont identiques.")
        return mobile_phone


class GuardianForm(PayerForm):
    # Same as PayerForm + birthdate, birthplace, profession

    birthdate = BirthDateField(label="Date de naissance", required=False)

    class Meta:
        model = Person
        fields = (
            'last_name', 'family_name', 'gender',
            'first_name', 'nickname', 'profession',
            'birthdate', 'birth_country', 'birth_commune', 'birth_place',
            'address1', 'address2', 'address3',
            'postal_code', 'city', 'country',
            'email', 'home_phone', 'mobile_phone',
        )
        widgets = {
            'birth_commune': autocomplete.ModelSelect2(
                url='commune-autocomplete',
                forward=['birthdate'],
            ),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['first_name'].help_text = "Prénom à l'état civil."
        self.fields['family_name'].help_text = "Nom de naissance à l'état civil, si différent."
        self.fields['family_name'].widget.attrs['class'] = 'uppercase'
        self.fields['nickname'].help_text = "Surnom, prénom d'usage, totem."
        if self.instance.pk:
            try:
                self.instance.adherent
            except Adherent.DoesNotExist:
                pass
            else:
                self.fields['birthdate'].required = True
        self.fields['birth_commune'].label = "Commune et département de naissance"
        self.fields['birth_commune'].help_text = "Dans la configuration de l'époque de la naissance."
        self.helper.layout[0:7] = [
            HTML('<div class="row"><div class="col-md-4">'),
            'last_name',
            HTML('</div><div class="col-md-4">'),
            'first_name',
            HTML('</div><div class="col-md-4">'),
            InlineRadios('gender'),
            HTML('</div></div>'),
            HTML('<div class="row"><div class="col-md-4">'),
            'family_name',
            HTML('</div><div class="col-md-4">'),
            'nickname',
            HTML('</div><div class="col-md-4">'),
            'profession',
            HTML('</div></div>'),
            HTML('<div class="row"><div class="col-md-4">'),
            'birthdate',
            HTML('</div><div class="col-md-4">'),
            'birth_country',
            HTML('</div><div class="col-md-4">'),
            'birth_commune',
            HTML('</div><div class="col-md-4">'),
            'birth_place',
            HTML('</div></div>'),
        ]

    def clean_family_name(self):
        family_name = self.cleaned_data.get('family_name')
        return family_name and family_name.upper()

    def clean_birthdate(self):
        birthdate = self.cleaned_data['birthdate']
        if birthdate and age(birthdate) > 100:
            raise ValidationError("La personne est-elle réellement centenaire ?")
        if birthdate and age(birthdate) < 6:
            raise ValidationError(
                "La réglementation (Article L2324-1 du code de la santé publique) ne permet pas "
                "aux EEDF d'accueillir des enfants de moins de 6 ans."
            )
        return birthdate

    def clean_birth_commune(self):
        birth_commune = self.cleaned_data['birth_commune']
        birthdate = self.cleaned_data.get('birthdate')
        if not birth_commune or not birthdate:
            return birth_commune
        if birth_commune.dates.lower and birth_commune.dates.lower > birthdate:
            raise ValidationError(f"La commune n'existait pas encore au {birthdate.strftime('%d/%m/%Y')}.")
        if birth_commune.dates.upper and birth_commune.dates.upper <= birthdate:
            raise ValidationError(f"La commune n'existait plus au {birthdate.strftime('%d/%m/%Y')}.")
        return birth_commune


class PreConfirmForm(forms.Form):
    first_name = forms.CharField(label="Prénom", max_length=100, validators=[NameValidator()])
    last_name = forms.CharField(label="Nom d'usage", max_length=100, validators=[NameValidator()])
    birthdate = BirthDateField(label="Date de naissance")
    confirm = forms.BooleanField(
        label="Cochez si les informations ci-dessus sont exactes", required=True,
        help_text="Sinon, merci de contacter la personne qui vous a envoyé l'invitation au lieu de poursuivre"
    )

    def __init__(self, user, invitation, force_age, *args, **kwargs):
        self.invitation = invitation
        self.force_age = force_age
        super().__init__(*args, **kwargs)
        if invitation.person:
            del self.fields['first_name']
            del self.fields['last_name']
            del self.fields['birthdate']
        else:
            self.fields['first_name'].widget.attrs['class'] = 'uppercase'
            self.fields['last_name'].widget.attrs['class'] = 'uppercase'
        lines = []
        if invitation.person:
            lines.append(f"<b>{invitation.person.full_name}</b>")
        if invitation.indirect_adhesion:
            lines.append("Déclare prolonger mon adhésion à l'association des Éclaireuses et Éclaireuses de France")
        else:
            lines.append("Déclare adhérer à l'association des Éclaireuses et Éclaireuses de France")
        if invitation.rate:
            lines.append(f"Pour une découverte de {invitation.rate.duration} jours")
        lines.append(f"En tant que <b>{invitation.functions_display}</b>")
        lines.append(f"Sur la structure locale de <b>{invitation.structure}</b>")
        if invitation.indirect_adhesion:
            begin = invitation.indirect_adhesion.begin
        else:
            begin = invitation.begin
        lines.append(f"Du <b>{date_format(begin, 'j F Y')}</b> au <b>{date_format(invitation.end, 'j F Y')}</b>")
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(HTML("<p>L'adhérent·e :</p>"))
        if not invitation.person:
            self.helper.layout.extend([
                HTML('<div class="row"><div class="col-md-4">'),
                'first_name',
                HTML('</div><div class="col-md-4">'),
                'last_name',
                HTML('</div><div class="col-md-4">'),
                'birthdate',
                HTML('</div></div>'),
            ])
        self.helper.layout.extend([
            HTML("".join(f"<p>{line},</p>" for line in lines)),
            'confirm',
        ])

    def clean_last_name(self):
        last_name = self.cleaned_data.get('last_name')
        return last_name and last_name.upper()

    def clean_first_name(self):
        first_name = self.cleaned_data.get('first_name')
        return first_name and first_name.upper()

    def clean_birthdate(self):
        birthdate = self.cleaned_data['birthdate']
        if self.force_age or not self.invitation.function_config:
            return birthdate
        current_age = age(birthdate)
        age_min = self.invitation.function_config.age_min
        age_max = self.invitation.function_config.age_max
        if age_min and current_age < age_min:
            self.has_age_warning = True
            raise ValidationError(f"Trop jeune ({current_age} ans) pour la fonction "
                                  f"{self.invitation.function_config} "
                                  f"({self.invitation.function_config.age_min} ans minimum).")
        if age_max and current_age > age_max:
            self.has_age_warning = True
            raise ValidationError(f"Trop vieux·eille ({current_age} ans) pour la fonction "
                                  f"{self.invitation.function_config} "
                                  f"({self.invitation.function_config.age_max} ans maximum).")
        return birthdate

    def clean(self):
        cleaned_data = super().clean()
        if self.errors:
            return cleaned_data
        first_name = cleaned_data.get('first_name')
        last_name = cleaned_data.get('last_name')
        birthdate = cleaned_data.get('birthdate')
        if first_name and last_name and Person.objects.filter(
            normalized_last_name=normalize_name(last_name),
            normalized_first_name=normalize_name(first_name),
            birthdate=birthdate,
        ).exists():
            raise ValidationError(
                "Une personne avec les mêmes nom, prénom et date de naissance existe déjà dans la base de données."
                " Merci de contacter le responsable des adhésions afin qu'il associe l'invitation à cette personne."
            )
        if not self.invitation.person:
            return cleaned_data
        try:
            adherent = self.invitation.person.adherent
        except Adherent.DoesNotExist:
            pass
        else:
            if adherent.is_blocked():
                raise ValidationError(mark_safe(
                    f"L'adhésion de cette personne est actuellement bloquée. {CONTACT_MESSAGE}"
                ))
        return cleaned_data


class PostConfirmForm(forms.Form):
    signatory = forms.CharField(label="Je soussigné", max_length=200)
    confirm = forms.BooleanField(label="Je certifie sur l'honneur que les informations de ce bulletin sont exactes.",
                                 required=True)
    values = forms.BooleanField(
        label=mark_safe(
            "En adhérant aux EEDF, je m'engage à respecter les <a href=\"https://www.eedf.fr/wp-content/uploads/"
            "2022/08/Statuts-EEDF-du-04-octobre-2020.pdf\" target=\"_blank\">valeurs fondamentales</a> "
            "de l'association."
        ),
        required=True
    )

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            HTML(f"""<p>Pour contacter les E.E.D.F. au sujet de leur politique de confidentialité ou de la collecte
et du traitement de vos données personnelles, vous pouvez envoyer un mail à <a href="mailto:{settings.SUPPORT_EMAIL}">
{settings.SUPPORT_EMAIL}</a> - Tél. : 01 48 15 17 66.<br>
<b>Retrouvez ici <a href=\"{reverse('rgpd')}\" target=\"_blank\">toutes les informations</a>
concernant le RGPD.</b></p>"""),
            'signatory',
            'confirm',
            'values',
        )


class PersonForm(GuardianForm):
    SAME_PERSON_ERROR = "Un·e adhérent·e existe déjà avec les mêmes nom/prénom/date de naissance."
    # Same as GuardianForm + image_rights
    image_rights = forms.ChoiceField(
        label="Droit à l'image",
        choices=((True, "Oui"), (False, "Non")),
        widget=forms.RadioSelect(),
        help_text="""J’autorise l’association des EEDF l'utilisation et l'exploitation de
mon image ou celle de mon enfant dans le cadre de la promotion
de l'association, notamment sur des supports que ce soit papier,
support analogique ou numérique (ex. réseaux sociaux, affiches,
flyers, rapports, site web…)<br>
En conséquence de quoi, je renonce expressément à me prévaloir
d'un quelconque droit à l'image et à toute action à l'encontre de
l'association qui trouverait son origine dans l'exploitation de mon
image dans le cadre précité.""",
    )

    class Meta:
        model = Person
        fields = (
            'last_name', 'family_name', 'gender',
            'first_name', 'nickname', 'profession',
            'birthdate', 'birth_country', 'birth_commune', 'birth_place',
            'address1', 'address2', 'address3',
            'postal_code', 'city', 'country',
            'email', 'home_phone', 'mobile_phone',
            'image_rights',
        )
        widgets = GuardianForm.Meta.widgets

    def __init__(self, required_birth_place, invitation=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance and self.instance.birth_country:
            required_birth_place = True
        self.required_birth_place = required_birth_place
        if required_birth_place:
            self.fields['birth_country'].required = True
            self.fields['birth_place'].label += "*"
        self.fields['birth_commune'].label += "*"
        if invitation:
            self.fields['last_name'].disabled = True
            self.fields['first_name'].disabled = True
        self.fields['birthdate'].required = True
        if invitation and invitation.function_config and \
           invitation.function_config.function_type.name == "Participant·e simple":
            self.fields['profession'].disabled = True
        self.fields['profession'].help_text = ""
        self.helper.layout.append(InlineRadios('image_rights'))

    def clean_birth_commune(self):
        birth_commune = super().clean_birth_commune()
        birth_country = self.cleaned_data.get('birth_country')
        if birth_country and birth_country.id == 99100 and not birth_commune:
            raise ValidationError("La commune est obligatoire pour une naissance en France.")
        return birth_commune

    def clean_birth_place(self):
        birth_place = self.cleaned_data['birth_place']
        birth_country = self.cleaned_data.get('birth_country')
        if self.required_birth_place and birth_country and birth_country.id != 99100 and not birth_place:
            raise ValidationError("La ville de naissance est obligatoire.")
        return birth_place

    def clean(self):
        cleaned_data = super().clean()
        first_name = cleaned_data.get('first_name')
        last_name = cleaned_data.get('last_name')
        birthdate = cleaned_data.get('birthdate')
        if not self.instance.pk and first_name and last_name and Person.objects \
                .filter(
                    normalized_first_name=normalize_name(first_name),
                    normalized_last_name=normalize_name(last_name),
                    birthdate=birthdate
                ) \
                .exists():
            raise ValidationError(self.SAME_PERSON_ERROR)
        return cleaned_data

    def clean_image_rights(self):
        image_rights = self.cleaned_data['image_rights']
        if image_rights is None:
            raise ValidationError("Merci de préciser Oui ou Non.")
        return image_rights


class SearchPersonForm(forms.Form):
    person = forms.ModelChoiceField(
        queryset=Person.objects.none(),
        label="Chercher une personne existante (nom/prénom/numéro)",
        required=False
    )
    mode = forms.ChoiceField(
        choices=(('New', "Une nouvelle personne"), ),
        widget=forms.RadioSelect(),
        label="",
    )

    def __init__(self, user, persons=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'mode',
        )
        if not persons:
            self.fields['person'].widget = autocomplete.ModelSelect2(
                url='person-autocomplete',
                forward=[forward.Const('members.change_person', 'for_perm')],
            )
            self.fields['mode'].choices += (('Sel', "La personne ci-dessous"), )
            self.fields['person'].queryset = Person.objects.for_perm('members.change_person', user)
            self.helper.layout.append('person')
        else:
            self.fields['mode'].choices = [(person.pk, str(person)) for person in persons] + self.fields['mode'].choices
            del self.fields['person']

    def clean(self):
        cleaned_data = super().clean()
        person = cleaned_data.get('person')
        mode = cleaned_data.get('mode')
        if mode and mode.isdigit():
            cleaned_data['person'] = Person.objects.get(pk=mode)
            cleaned_data['mode'] = 'Sel'
        if mode == 'Sel' and not person and 'person' not in self.errors:
            raise ValidationError("Merci de sélectionner une personne.")
        return cleaned_data


class SearchGuardianForm(SearchPersonForm):
    def __init__(self, protected, other_guardian, allow_empty=False, *args, **kwargs):
        self.protected = protected
        self.other_guardian = other_guardian
        super().__init__(*args, **kwargs)
        if allow_empty:
            self.fields['mode'].choices = [('N/A', "Information non disponible")] + self.fields['mode'].choices

    def clean(self):
        cleaned_data = super().clean()
        mode = cleaned_data.get('mode')
        person = cleaned_data.get('person')
        if mode != 'Sel' or person is None:
            return cleaned_data
        if person == self.protected:
            raise ValidationError("Il n'est pas possible d'être responsable légal·e de soi-même.")
        if person.legal_guardian1 or person.legal_guardian2:
            raise ValidationError("Ce·tte responsable légal·e a lui·elle même un·e responsable légal·e.")
        if person == self.other_guardian:
            raise ValidationError("Les deux responsables légaux doivent être différents.")
        return cleaned_data


class PermissionForm(forms.ModelForm):
    permission_with_bool = forms.BooleanField(label="Rentrer accompagné·e par la personne suivante :", required=False)

    class Meta:
        model = Person
        fields = (
            'permission_alone', 'permission_with_bool', 'permission_with',
        )

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['permission_with'].label = ""
        self.fields['permission_with'].widget.attrs['class'] = 'uppercase'
        self.fields['permission_with_bool'].initial = self.instance.permission_with != ""
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            HTML("<p><b>J'autorise l'enfant à :</b></p>"),
            'permission_alone',
            'permission_with_bool',
            'permission_with',
        )

    def clean_permission_with(self):
        permission_with_bool = self.cleaned_data.get('permission_with_bool')
        permission_with = self.cleaned_data.get('permission_with')
        if permission_with_bool and not permission_with:
            raise forms.ValidationError("Ce champ est obligatoire si vous cochez « accompagné·e par ».")
        if not permission_with_bool:
            return ""
        return permission_with.upper()


class AdhesionForm(forms.ModelForm):
    begin = forms.DateField(label="Date", required=True)

    class Meta:
        model = Adhesion
        fields = (
            'structure', 'begin'
        )
        widgets = {
            'structure': autocomplete.ModelSelect2(url='structure-autocomplete'),
        }

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['structure'].queryset = Structure.objects \
            .for_perm('members.change_adhesion' if self.instance.pk else 'members.add_adhesion', user) \
            .is_active()
        self.fields['begin'].validators = [
            MinValueValidator(
                Date(current_season() - 1, 9, 1),
                f"La date d'adhésion ne peut être antérieure au début de la saison (01/09/{current_season() - 1})"
            ),
            MaxValueValidator(
                Date(current_season(), 8, 31),
                f"La date d'adhésion ne peut être postérieure à la fin de la saison (31/08/{current_season()})"
            )
        ]
        if self.instance.pk:
            self.fields['begin'].initial = self.instance.begin
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            HTML('<div class="row"><div class="col-md-6">'),
            'structure',
            HTML('</div><div class="col-md-6">'),
            'begin',
            HTML('</div></div>'),
        )

    def save(self, commit=True):
        adhesion = super().save(commit=False)
        adhesion.set_dates(self.cleaned_data['begin'])
        if commit:
            adhesion.save()
        return adhesion


class AdhesionFunctionForm(forms.ModelForm):
    team = forms.ModelChoiceField(
        label="Équipe",
        required=False,
        widget=autocomplete.ModelSelect2(
            url='team-autocomplete',
            attrs={'data-placeholder': "Aucune"},
            forward=[
                forward.Const('on', 'managed'),
                forward.Const('on', 'is_active'),
                forward.Const('on', 'for_adherent')
            ]),
        queryset=Team.objects.none(),
    )
    function_config = forms.ModelChoiceField(
        label="Fonction",
        required=False,
        widget=autocomplete.ModelSelect2(
            url='functionconfig-autocomplete',
            attrs={'data-placeholder': "Ami·e"},
            forward=['team']
        ),
        queryset=FunctionConfig.objects.none(),
        empty_label="Amie·e",
    )

    class Meta:
        model = Adhesion
        fields = ('team', 'function_config')

    def __init__(self, user, structure, current_functions, birthdate, force_age, is_invitation=False,
                 rate=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.birthdate = birthdate
        self.force_age = force_age
        self.rate = rate
        self.fields['team'].queryset = Team.objects \
            .filter(structure=structure) \
            .for_adherent() \
            .for_perm('members.add_invitation' if is_invitation else 'members.add_adhesion', user) \
            .is_active()
        if structure:
            self.fields['team'].widget.forward.append(forward.Const(str(structure.pk), 'structure'))
        self.fields['function_config'].queryset = FunctionConfig.objects.filter(subject_to_nomination=False)
        self.helper = FormHelper()
        self.helper.form_tag = False
        if current_functions:
            self.helper.layout = Layout(
                HTML(f"Prolongement des fonctions actuelles sur la structure <b>{structure.name}</b> :<ul>")
            )
            for function in current_functions:
                self.helper.layout.append(HTML(
                    f"<li><b>{function}</b> jusqu'au {function.end.strftime('%d/%m/%Y')}</li>"
                ))
            self.helper.layout.append(HTML(
                "</ul><p>Vous pourrez ajouter ou renouveller des fonctions/équipes par la suite.</p>"
            ))
        else:
            self.helper.layout = Layout(
                HTML('<div class="row"><div class="col-md-6">'),
                'team',
                HTML('</div><div class="col-md-6">'),
                'function_config',
                HTML(
                    "</div></div><p>Attention ! Le tarif proposé dépendra de la fonction.</p>"
                    "<p>L'attribution des fonctions électives ne peut se faire que par AGORA dans un second temps. "
                    "Dans ce cas, laissez l'équipe et la fonction vides à ce stade.</p>")
            )

    def clean(self):
        cleaned_data = super().clean()
        team = cleaned_data.get('team')
        function_config = cleaned_data.get('function_config')
        # FIXME vérifier unicité function dans l'équipe (en fonction des dates)
        if team and not function_config and not self.errors:
            self.add_error(
                'function_config',
                "Une fonction autre que « Ami·e » est requise si une équipe est précisée."
            )
        if function_config and not team:
            self.add_error('team', "L'équipe est requise si une fonction autre que « Ami·e » est précisée.")
        if team and function_config and function_config.team_type != team.type:
            self.add_error('function_config', "Cette fonction n'est pas compatible avec le type d'équipe.")
        if team and function_config and function_config.structure_type not in (team.structure.type, None):
            self.add_error('function_config', "Cette fonction n'est pas compatible avec le type de structure.")
        if self.force_age or not self.birthdate or not function_config:
            return cleaned_data
        age_begin = age(self.birthdate)
        age_end = current_season() - self.birthdate.year
        if self.birthdate.month >= 9:
            age_end -= 1
        if function_config.age_min and age_begin < function_config.age_min:
            self.has_age_warning = True
            self.add_error(
                None,
                f"L'adhérent·e est trop jeune pour cette fonction et cette équipe ({age_begin} "
                f"ans au lieu de {function_config.age_min} ans minimum)."
            )
        if function_config.age_max and function_config.age_max and age_end > function_config.age_max:
            self.has_age_warning = True
            self.add_error(
                None,
                f"L'adhérent·e est trop vi·eux·eille pour cette fonction et cette équipe ({age_end} "
                f"ans en fin de saison au lieu de {function_config.age_max} ans maximum)."
            )
        return cleaned_data

    def clean_function_config(self):
        function_config = self.cleaned_data['function_config']
        team = self.cleaned_data.get('team')
        if not self.rate:
            return function_config
        assert self.rate.season == current_season()
        if self.instance.is_discovery and not team and not function_config:
            raise ValidationError(f"Cette fonction n'est pas accessible au tarif {self.rate.name}")
        if not team or not function_config:
            return function_config
        if team.structure.type not in self.rate.structure_types.all() or \
                function_config.function_type.category != self.rate.function_category:
            raise ValidationError(f"Cette fonction n'est pas accessible au tarif {self.rate.name}")
        return function_config


class ContributionForm(forms.ModelForm):
    rate = forms.ModelChoiceField(
        label="Cotisation nationale",
        queryset=Rate.objects.none(),
        widget=forms.RadioSelect()
    )
    free_rate = forms.DecimalField(
        label="Montant libre*",
        help_text="""<span style="color: #f05a24; font-weight: bold;">En fonction de ses revenus
chaque Responsable cotisera, de manière libre et consciente, à hauteur de ses possibilités
(imposable, étudiant·e, salarié·e…) à prix libre.</span>""",
        decimal_places=2,
        required=False,
    )
    donation = forms.DecimalField(
        label="Don libre en complément de la cotisation",
        help_text="Je participe à la vie de l'association, via la structure locale où j'adhère. "
                  "Je soutiens ses actions.",
        min_value=0,
        decimal_places=2,
        initial=0
    )

    class Meta:
        model = Adhesion
        fields = ('rate', 'free_rate', 'donation')

    def __init__(self, user, invitation, person, birthdate, adhesion_date, structure, function_config, force_age,
                 transform=False, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.force_age = force_age
        rates_table = None
        rates = Rate.objects.filter(
            season=self.instance.season if self.instance.pk else current_season(),
        )
        qs_functions = Q()
        if person:
            for function in person.functions.current().filter(team__structure=structure):
                qs_functions |= Q(
                    structure_types=function.team.structure.type,
                    function_category=function.function_config.function_type.category,
                )
        if structure and function_config:
            qs_functions |= Q(
                structure_types=structure.type,
                function_category=function_config.function_type.category,
            )
        if qs_functions:
            rates = rates.exclude(category=RateCategory.SUPPORT).filter(qs_functions).distinct()
        else:
            rates = rates.filter(category__in=(RateCategory.SUPPORT, RateCategory.RESPONSIBLE))
        if birthdate:
            age_begin = age(birthdate, adhesion_date)
            rates = rates.filter(
                Q(age_min=None) | Q(age_min__lte=age_begin),
                Q(age_max=None) | Q(age_max__gte=age_begin),
            )
        qfs = rates.filter(category=RateCategory.CHILD) \
            .order_by('qf_max') \
            .values_list('qf_min', 'qf_max') \
            .distinct()
        next = rates.filter(next_child=True).exists()
        if len(qfs) > 1 or next:
            rates_table = {
                (qf_min, qf_max): {
                    next_child: rates.filter(
                        category=RateCategory.CHILD
                    ).get(qf_min=qf_min, qf_max=qf_max, next_child=next_child)
                    for next_child in (False, True)
                }
                for (qf_min, qf_max) in qfs
            }
        if invitation:
            if invitation.rate:
                rates = rates.filter(pk=invitation.rate.pk)
                rates_table = None
            else:
                rates = rates.exclude(category=RateCategory.DISCOVERY)
        self.fields['rate'].queryset = rates
        self.fields['rate'].label_from_instance = Rate.label
        if len(rates) == 1:
            self.fields['rate'].initial = rates[0]
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(HTML(
            '<div class="alert alert-info"><span class="glyphicon glyphicon-info-sign"></span> '
            'Les cotisations et les dons à l’association des EEDF donnent droit à '
            'l’émission d’un reçu fiscal permettant de déduire 66% du montant du don si vous êtes assujettis à '
            'l’impôt sur le revenu.</div>'
        ))
        if rates_table:
            self.helper.layout.append(HTML(
                '<div class="alert alert-warning"><span class="glyphicon glyphicon-exclamation-sign"></span> '
                'Le quotient familial est le revenu net imposable divisé par le nombre de parts fiscales. '
                'Vous pouvez trouver ces informations sur votre dernier avis d´impôt sur le revenu. '
                'Attention ! Le quotient familial de l´impôt sur le revenu diffère de celui de la CAF.</div>'
            ))
        self.helper.layout.append(HTML('<div class="row"><div class="col-md-4">'))
        if rates_table:
            prefix = f'{self.prefix}-' if self.prefix else ''
            current = int(self.data.get(f'{prefix}rate', '0'))
            error = ', '.join(self.errors.get('rate', []))
            if error:
                self.helper.layout.append(HTML(f'<div id="div_id_{prefix}rate" class="form-group has-error">'))
            else:
                self.helper.layout.append(HTML(f'<div id="div_id_{prefix}rate" class="form-group">'))
            self.helper.layout.append(HTML(
                '<label class="control-label requiredField">Tarif*</label>'
            ))
            if error:
                self.helper.layout.append(HTML(f'<div class="help-block"><strong>{error}</strong></div>'))
            self.helper.layout.append(HTML(
                '<table class="table table-stripped table-bordered"><tr>'
                '<th>Quotient Familial<br>de l’impôt sur le revenu</th>'
            ))
            for qf in rates_table:
                self.helper.layout.append(HTML('<th style="text-align: center;">'))
                if not qf[0]:
                    self.helper.layout.append(HTML(f'inférieur<br>à {qf[1]:,} €</th>'.replace(',', ' ')))
                elif not qf[1]:
                    self.helper.layout.append(HTML(f'supérieur<br>à {qf[0]:,} €</th>'.replace(',', ' ')))
                else:
                    self.helper.layout.append(HTML(f'de {qf[0]:,}<br>à {qf[1]:,} €</th>'.replace(',', ' ')))
            self.helper.layout.append(HTML('<tr><td style="vertical-align: middle;">1er enfant</td>'))
            for qf, col in rates_table.items():
                rate = col[False]
                assert rate.rate % 1 == 0
                try:
                    checked = rate.id == self.instance.rate.id
                except Rate.DoesNotExist:
                    checked = False
                self.helper.layout.append(HTML(
                    '<td style="text-align: center;"><div class="radio">'
                    f'<label for="id_{prefix}rate_{rate.id}">'
                    f'<input id="id_{prefix}rate_{rate.id}" type="radio" '
                    f'name="{prefix}rate" value="{rate.id}"{" checked" if checked else ""}'
                ))
                if rate.id == current:
                    self.helper.layout.append(HTML(' checked'))
                self.helper.layout.append(HTML(
                    f'>{int(rate.rate)} €</label>'
                    '</div></td>'
                ))
            self.helper.layout.append(HTML('</tr><tr><td style="vertical-align: middle;">Enfants suivants</td>'))
            for qf, col in rates_table.items():
                rate = col[True]
                assert rate.rate % 1 == 0
                try:
                    checked = rate.id == self.instance.rate.id
                except Rate.DoesNotExist:
                    checked = False
                self.helper.layout.append(HTML(
                    '<td style="text-align: center;"><div class="radio">'
                    f'<label for="id_{prefix}rate_{rate.id}">'
                    f'<input id="id_{prefix}rate_{rate.id}" type="radio" '
                    f'name="{prefix}rate" value="{rate.id}"{" checked" if checked else ""}>'
                    f'{int(rate.rate)} €</label>'
                    '</div></td>'
                ))
            self.helper.layout.append(HTML('</tr></table>'))
            if not invitation and not transform:
                for rate in rates.exclude(category=RateCategory.CHILD):
                    try:
                        checked = rate.id == self.instance.rate.id
                    except Rate.DoesNotExist:
                        checked = False
                    self.helper.layout.append(HTML(
                        '<div class="radio">'
                        f'<label for="id_{prefix}rate_{rate.id}">'
                        f'<input id="id_{prefix}rate_{rate.id}" type="radio" '
                        f'name="{prefix}rate" value="{rate.id}"{" checked" if checked else ""}>'
                        f'{rate.name} ({rate.rate}) €</label>'
                        '</div>'
                    ))
            if transform:
                self.helper.layout.append(HTML(
                    "La somme déjà versée pour l'adhésion découverte sera déduite de ce montant."
                ))
            self.helper.layout.append(HTML('</div>'))
        elif rates:
            self.helper.layout.append('rate')
        else:
            self.helper.layout.append(HTML(
                '<p class="text-danger"><b>Erreur : aucun tarif ne correspond à l´âge et la fonction.</b></p>'
            ))
        self.helper.layout.extend([
            HTML('</div><div class="col-md-4">'),
            'free_rate',
            HTML('</div><div class="col-md-4">'),
            'donation',
            HTML('</div></div>'),
        ])

    def clean_free_rate(self):
        rate = self.cleaned_data.get('rate')
        free_rate = self.cleaned_data.get('free_rate')
        if rate and rate.rate_max is not None and free_rate is None:
            raise ValidationError("Ce champ est requis.")
        if rate and rate.rate_max is not None and free_rate is not None and free_rate < rate.rate:
            raise ValidationError(f"Le montant minimum est de {rate.rate} €.")
        if rate and rate.rate_max is not None and free_rate is not None and free_rate > rate.rate_max:
            raise ValidationError(
                f"Le montant maximum est de {rate.rate_max} €. "
                "Vous pouvez cependant compléter par un don."
            )
        return free_rate

    def save(self, commit=True):
        adhesion = super().save(commit=False)
        adhesion.is_discovery = adhesion.rate.category == RateCategory.DISCOVERY
        adhesion.set_dates(adhesion.begin)
        if commit:
            adhesion.save()
        return adhesion


class FunctionForm(forms.ModelForm):
    structure = forms.ModelChoiceField(
        label="Structure",
        queryset=Structure.objects.none(),
        widget=autocomplete.ModelSelect2(url='structure-autocomplete', forward=[forward.Const('on', 'is_active')])
    )
    outside = forms.BooleanField(
        label="La personne à ajouter est salariée ou est adhérente sur une autre structure",
        required=False,
    )
    begin = forms.DateField(label="Début", required=True)
    end = forms.DateField(label="Fin", required=False)

    class Meta:
        model = Function
        fields = ('structure', 'team', 'outside', 'person', 'function_config', 'name', 'begin', 'end')
        widgets = {
            'person': autocomplete.ModelSelect2(
                url='person-autocomplete',
                forward=['structure', 'outside']
            ),
            'team': autocomplete.ModelSelect2(
                url='team-autocomplete',
                forward=['structure', forward.Const('on', 'is_active')]
            ),
            'function_config': autocomplete.ModelSelect2(
                url='functionconfig-autocomplete',
                forward=['team', 'begin']
            ),
        }

    def __init__(self, user, force_age, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user
        self.force_age = force_age
        self.fields['person'].queryset = Person.objects.for_perm('members.view_person', user)
        self.fields['person'].widget.forward.append(forward.Const('members.view_person', 'for_perm'))
        self.fields['person'].required = True
        self.fields['structure'].queryset = Structure.objects.for_perm('members.add_function', user).is_active()
        self.fields['structure'].widget.forward.append(forward.Const('members.add_function', 'for_perm'))
        self.fields['team'].queryset = Team.objects.for_perm('members.add_function', user).is_active()
        self.fields['team'].widget.forward.append(forward.Const('members.add_function', 'for_perm'))
        self.fields['team'].required = True
        self.fields['function_config'].queryset = FunctionConfig.objects.filter(subject_to_nomination=False)
        self.fields['function_config'].required = True
        self.fields['function_config'].label = "Fonction"
        self.fields['name'].label = "Fonction spécifique"
        self.fields['name'].help_text = "Optionnel. Par exemple « Responsable matériel »"
        if self.instance.pk:
            self.fields['begin'].initial = self.instance.begin
            self.fields['end'].initial = self.instance.end
        else:
            self.fields['begin'].initial = today()
        if self.instance.pk:
            for field in ('structure', 'team', 'person', 'function_config'):
                self.fields[field].disabled = True
            del self.fields['outside']
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            HTML('<div class="row"><div class="col-md-6">'),
            'structure',
            HTML('</div><div class="col-md-6">'),
            'team',
            HTML('</div></div>'),
        )
        self.helper.layout.append('outside')
        self.helper.layout.extend([
            'person',
            HTML('<div class="row"><div class="col-md-6">'),
            'function_config',
            HTML('</div><div class="col-md-6">'),
            'name',
            HTML('</div></div>'),
            HTML('<div class="row"><div class="col-md-6">'),
            'begin',
            HTML('</div><div class="col-md-6">'),
            'end',
            HTML('</div></div>'),
        ])

    def clean_function_config(self):
        function_config = self.cleaned_data['function_config']
        team = self.cleaned_data.get('team')
        if not team:
            return function_config
        if function_config.team_type != team.type:
            raise ValidationError("Cette fonction ne correspond pas à l'équipe.")
        if function_config.structure_type not in (None, team.structure.type):
            raise ValidationError("Cette fonction ne correspond pas à la structure.")
        return function_config

    def clean_begin(self):
        person = self.cleaned_data.get('person')
        function_config = self.cleaned_data.get('function_config')
        begin = self.cleaned_data['begin']
        if not person or not function_config:
            return begin
        if function_config.function_type.category == FunctionCategory.EMPLOYEE:
            if not Employment.objects.filter(employee__person=person, dates__contains=begin).exists():
                raise ValidationError("Il faut un contrat de travail à cette date.")
        else:
            if not Adhesion.objects.filter(adherent__person=person, canceled=False, dates__contains=begin).exists():
                raise ValidationError("Il faut une adhésion à cette date.")
        return begin

    def clean_end(self):
        person = self.cleaned_data.get('person')
        function_config = self.cleaned_data.get('function_config')
        begin = self.cleaned_data.get('begin')
        end = self.cleaned_data['end']
        if not person or not function_config or not begin:
            return end
        if end and end < begin:
            raise ValidationError("La date de fin ne peut être antérieure à la date de début.")
        if function_config.function_type.category == FunctionCategory.EMPLOYEE:
            employments = Employment.objects.filter(
                employee__person=person,
                dates__overlap=DateRange(begin, end, '[]'),
            ).order_by('dates__startswith')
            for employment in employments:
                if begin < employment.begin:
                    raise ValidationError(f"Les contrats de travail sont interrompus au {begin.strftime('%d/%m/%Y')}.")
                if employment.end:
                    begin = employment.end + TimeDelta(days=1)
            if employment.end and not end:
                raise ValidationError(
                    "La date de fin doit être précisée lorsque le contrat de travail n'est pas un CDI."
                )
            if employment.end and end > employment.end:
                raise ValidationError(
                    "La date de fin doit être antérieure à la date du dernier contrat de travail "
                    f"({employment.end.strftime('%d/%m/%Y')})."
                )
        else:
            if not end:
                end = function_config.default_end(begin)
            if not end:
                raise ValidationError("Une date de fin est obligatoire.")
            max_end = function_config.max_end(begin)
            if end > max_end:
                msg = "Merci de choisir une date "
                if function_config.duration:
                    msg += f"au maximum {function_config.duration} an{pluralize(function_config.duration)} après "
                else:
                    msg += "avant "
                msg += f"la fin de l'adhésion en cours à la prise de fonction ({max_end.strftime('%d/%m/%Y')})."
                raise ValidationError(msg)
        return end

    def clean(self):
        cleaned_data = super().clean()
        person = cleaned_data.get('person')
        team = cleaned_data.get('team')
        begin = cleaned_data.get('begin')
        end = cleaned_data.get('end')
        function_config = cleaned_data.get('function_config')
        if not person or not team or not function_config or not begin or not end:
            return cleaned_data
        if function_config.function_type.category in (FunctionCategory.VOLUNTEER, FunctionCategory.PARTICIPANT):
            season = begin.year
            if begin.month >= 9:
                season += 1
            adhesion = person.adhesions.filter(season=season)
            if not adhesion:
                self.add_error('begin', f"Nécessite une adhésion {season - 1}/{season}.")
            if adhesion and adhesion[0].begin > begin:
                self.add_error(
                    'begin',
                    f"Le début ne peut être antérieur à la date d'adhésion ({adhesion[0].begin.strftime('%d/%m/%Y')})."
                )
        elif function_config.function_type.category == FunctionCategory.EMPLOYEE:
            employment = person.employments.filter(dates__contains=begin)
            if not employment:
                self.add_error(
                    None, f"Cette fonction nécessite un contrat de travail en cours au {begin.strftime('%d/%m/%Y')}.",
                )
        else:
            raise Exception(f"Unknown FunctionCategory {function_config.function_type.category}")
        existing_functions = Function.objects.filter(
            person=person, team=team, function_config=function_config, dates__overlap=DateRange(begin, end, '[]'),
        ).exclude(pk=self.instance.pk)
        if existing_functions:
            self.add_error(
                None,
                f"L'adhérent·e possède déjà cette fonction dans l'équipe {team.name} "
                f"entre le {existing_functions[0].begin.strftime('%d/%m/%Y')} "
                f"et le {existing_functions[0].end.strftime('%d/%m/%Y')}."
            )
        if self.force_age:
            return cleaned_data
        age = person.age(begin)
        if function_config.age_min and age < function_config.age_min:
            self.has_age_warning = True
            self.add_error(
                None,
                f"L'adhérent·e est trop jeune pour ce type de fonction et cette équipe ({age} "
                f"ans au lieu de {function_config.age_min} ans minimum)."
            )
        if function_config.age_max and age > function_config.age_max:
            self.has_age_warning = True
            self.add_error(
                None,
                f"L'adhérent·e est trop vi·eux·eille pour ce type de fonction et cette équipe ({age} "
                f"ans au lieu de {function_config.age_max} ans maximum)."
            )
        return cleaned_data

    def save(self, commit=True):
        function = super().save(commit=False)
        if not function.name:
            function.name = str(function.function_config)
        begin = self.cleaned_data['begin']
        end = self.cleaned_data['end']
        if not end:
            if function.function_config.function_type.category == FunctionCategory.EMPLOYEE:
                end = function.person.employment.end
            else:
                end = begin.replace(month=8, day=31)
                if end < begin:
                    end = end.replace(year=end.year + 1)
        if end:
            end += TimeDelta(days=1)
        function.dates = DateRange(begin, end, '[)')
        if not self.user.has_perm('members.add_function', function.person):
            function.status = FunctionStatus.REQUESTED
        if commit:
            function.save()
        return function


class PaymentForm(forms.ModelForm):
    class Meta:
        model = Payment
        fields = ('amount', 'date', 'method', 'reference')

    def __init__(self, user, season, amount_max, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.season = season
        self.amount_max = amount_max
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'amount',
            'date',
            'method',
            HTML('<p class="text-warning" id="receipt_warning">Attention ! Cette méthode de paiement non numéraire '
                 'ne donne pas droit à un reçu fiscal.</p>'),
            'reference',
        )

    def clean_date(self):
        _date = self.cleaned_data['date']
        if _date < Date(self.season - 1, 9, 1):
            raise ValidationError(f"La date de paiement ne peut être antérieure au 01/09/{self.season - 1}.")
        if _date > Date(self.season, 8, 31):
            raise ValidationError(f"La date de paiement ne peut être postérieure au 31/08/{self.season}.")
        if _date > today():
            raise ValidationError("La date de paiement ne peut être postérieure à la date courante.")
        return _date

    def clean_amount(self):
        amount = self.cleaned_data['amount']
        if amount > self.amount_max:
            raise ValidationError(
                f"Le montant ne doit pas dépasser {self.amount_max} €. "
                "Vérifiez le tarif et, si nécessaire, augmentez le montant du don."
            )
        return amount


class SearchPayerForm(SearchPersonForm):
    confirm = forms.BooleanField(label="Confirmer la prise en charge de l'adhésion par la structure EEDF.",
                                 required=False)

    def __init__(self, user, eedf, person, guardian1, guardian2, rate, donation, *args, **kwargs):
        self.rate = rate
        self.donation = donation
        super().__init__(user, *args, **kwargs)
        if eedf:
            self.fields['mode'].choices = [
                ('EEDF', "La structure EEDF")
            ] + self.fields['mode'].choices
        if guardian2:
            self.fields['mode'].choices = [
                ('G2', f"{guardian2['last_name']} {guardian2['first_name']} (le ou la responsable légal·e 2)")
            ] + [
                (key, val) for key, val in self.fields['mode'].choices
                if not val.startswith(f"{guardian2['last_name']} {guardian2['first_name']}")
            ]
        if guardian1:
            self.fields['mode'].choices = [
                ('G1', f"{guardian1['last_name']} {guardian1['first_name']} (le ou la responsable légal·e 1)")
            ] + [
                (key, val) for key, val in self.fields['mode'].choices
                if not val.startswith(f"{guardian1['last_name']} {guardian1['first_name']}")
            ]
        if person:
            self.fields['mode'].choices = [
                ('Adh', f"{person['last_name']} {person['first_name']} (l'adhérent·e)")
            ] + [
                (key, val) for key, val in self.fields['mode'].choices
                if not val.startswith(f"{person['last_name']} {person['first_name']}")
            ]
        if rate and not rate.can_be_free and self.data.get('search_payer-mode') == 'EEDF':
            self.helper.layout.append('confirm')
        else:
            del self.fields['confirm']

    def clean_mode(self):
        mode = self.cleaned_data['mode']
        if self.donation and mode == 'EEDF':
            raise ValidationError("La structure EEDF ne peut pas prendre en charge une adhésion en cas de don.")
        return mode

    def clean_confirm(self):
        mode = self.cleaned_data.get('mode')
        confirm = self.cleaned_data.get('confirm')
        if not self.rate.can_be_free and mode == 'EEDF' and not confirm:
            raise ValidationError("""Attention ! la prise en charge de l'adhésion des responsables n'est pas recommandée
                                  par l'association. Merci de changer de choix ou de le confirmer malgré tout en cochant
                                  la case ci-dessus.""")


class AdhesionsFilterForm(forms.Form):
    rate_choices = [(None, "Tous")] + RateCategory.choices
    season = forms.ChoiceField(label="Saison", choices=())
    rate = forms.ChoiceField(label="Tarif", choices=rate_choices)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['season'].choices = [
            (i, "{}/{}".format(i - 1, i)) for i in range(current_season(), first_season() - 1, -1)
        ]
        self.helper = FormHelper()
        self.helper.attrs = {'id': 'filter'}
        self.helper.form_class = 'form-inline'
        self.helper.form_method = 'get'
        self.helper.layout = Layout(
            'season',
            'rate',
        )


class PasswordResetForm(PasswordResetBaseForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email'].label = "Email"
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'email',
        )

    def get_users(self, email):
        persons = Person.objects.filter(email=email)[:11]
        users = (User.objects.get_or_create(person=person)[0] for person in persons)
        return [user for user in users if user.has_usable_password() and user.is_active]

    def clean_email(self):
        email = self.cleaned_data['email']
        users = self.get_users(email)
        if not users:
            raise forms.ValidationError(
                "Cette adresse email est inconnue. Si votre saisie est correcte, l'adresse n'a probablement pas été "
                "bien enregistrée dans la base de données. "
                "Contactez votre structure locale ou le secrétariat national."
            )
        if len(users) > 10:
            raise forms.ValidationError("Trop d'utilisateurs différents partagent cette adresse email.")
        return email

    def send_mail(
        self,
        subject_template_name,
        email_template_name,
        context,
        from_email,
        to_email,
        html_email_template_name=None,
    ):
        users = self.get_users(to_email)
        for user in users:
            user_pk_bytes = force_bytes(User._meta.pk.value_to_string(user))
            user.uid = urlsafe_base64_encode(user_pk_bytes)
            user.token = default_token_generator.make_token(user)
        context['users'] = users
        return super().send_mail(
            subject_template_name,
            email_template_name,
            context,
            from_email,
            to_email,
            html_email_template_name or 'registration/password_reset_email_html.html',
        )


class SetUsernamePasswordForm(SetPasswordForm):
    email_as_login = forms.BooleanField(required=False)
    username_as_login = forms.BooleanField(
        required=False,
        label="Je souhaite pouvoir m'identifier avec un nom d'utilisateur·ice",
    )
    username = UsernameField(label="Nom d'utilisateur·ice", required=False)

    def __init__(self, user, *args, **kwargs):
        super().__init__(user, *args, **kwargs)
        self.logins = []
        if user.person and user.person.is_adherent:
            self.logins.append(f'votre numéro d\'adhérent·e : <strong>{user.person.adherent.id}</strong>')
        if user.person and user.person.is_employee:
            self.logins.append(f'votre numéro de salarié·e : <strong>{user.person.employee.id}</strong>')
        if user.person:
            current_email_as_login = Person.objects.filter(email=user.person.email, email_as_login=True)
            if user.person.email_as_login or not current_email_as_login:
                self.fields['email_as_login'].initial = True
            if not user.person.email_as_login and current_email_as_login:
                self.fields['email_as_login'].help_text = "ATTENTION ! Ceci désactivera la possibilité pour " \
                    f"{current_email_as_login[0]} de le faire également."
            self.fields['email_as_login'].label = mark_safe(
                f"Je souhaite pouvoir m'identifier avec mon adresse email : <strong>{user.person.email}</strong>"
            )
        else:
            del self.fields['email_as_login']
        if self.user.username or (not self.logins and not self.fields['email_as_login'].initial):
            self.fields['username_as_login'].initial = True
        self.fields['username'].initial = self.user.username
        message = '<h2>Identifiant</h2>'
        if self.logins:
            message += f'<p>Vous pouvez vous identifier avec {' ou '.join(self.logins)}</p>'
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            HTML(message),
            'email_as_login',
            'username_as_login',
            'username',
            HTML('<h2>Mot de passe</h2>'),
            'new_password1',
            'new_password2',
        )

    def clean_username(self):
        username_as_login = self.cleaned_data.get('username_as_login')
        if username_as_login is False:
            return None
        username = self.cleaned_data['username']
        if username_as_login and not username:
            raise forms.ValidationError("Merci de décocher la case ou de saisir un nom d'utilisateur·ice.")
        if username and User.objects.exclude(pk=self.user.pk).filter(username__iexact=username).exists():
            raise forms.ValidationError("Ce nom est déjà réservé par un·e autre utilisateur·ice.")
        return username

    def clean(self):
        cleaned_data = super().clean()
        if not self.logins and not cleaned_data['email_as_login'] and not cleaned_data['username_as_login']:
            raise ValidationError("Merci de cocher au moins une méthode d'identification.")
        return cleaned_data

    def save(self, commit=True):
        user = super().save(commit=False)
        user.username = self.cleaned_data['username']
        if self.cleaned_data['email_as_login']:
            Person.objects.filter(email=user.email).exclude(pk=user.person.pk).update(email_as_login=False)
            user.person.email_as_login = True
            if commit:
                user.person.save()
        if commit:
            user.save()
        return user


class AdhesionImportForm(forms.Form):
    structure = forms.ModelChoiceField(
        label="Structure",
        queryset=Structure.objects.none(),
        widget=autocomplete.ModelSelect2(url='structure-autocomplete', forward=[
            forward.Const('members.import_adhesion', 'for_perm'),
            forward.Const('on', 'is_active'),
            forward.Const('on', 'can_import'),
        ])
    )
    file = forms.FileField(label="Fichier Excel")

    def __init__(self, structures, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['structure'].queryset = structures
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'structure',
            'file',
            FormActions(
                HTML('<a href="/" class="btn btn-default">Annuler</a>'),
                Submit('save', 'Importer'),
            ),
        )


class EmploymentImportForm(forms.Form):
    structure = forms.ModelChoiceField(
        label="Structure",
        queryset=Structure.objects.none(),
        widget=autocomplete.ModelSelect2(url='structure-autocomplete', forward=[
            forward.Const('members.import_employment', 'for_perm'),
            forward.Const('on', 'is_active'),
            forward.Const('on', 'can_import'),
        ])
    )
    file = forms.FileField(label="Fichier Excel")

    def __init__(self, structures, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['structure'].queryset = structures
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'structure',
            'file',
            FormActions(
                HTML('<a href="/" class="btn btn-default">Annuler</a>'),
                Submit('save', 'Importer'),
            ),
        )


class PersonDeduplicateForm(forms.Form):
    keep1 = forms.ModelChoiceField(
        label="Au profit de la personne",
        queryset=Person.objects.none(),
        widget=forms.RadioSelect(),
        empty_label="Une autre personne",
        required=False,
    )
    keep2 = forms.ModelChoiceField(
        label="",
        queryset=Person.objects.none(),
        widget=autocomplete.ModelSelect2(url='person-autocomplete'),
        required=False
    )

    def __init__(self, remove, *args, **kwargs):
        self.remove = remove
        super().__init__(*args, **kwargs)
        self.fields['keep1'].queryset = Person.objects.filter(
            normalized_last_name=remove.normalized_last_name,
            normalized_first_name=remove.normalized_first_name,
        ).exclude(
            id=remove.id
        ).order_by(
            'id',
        )
        self.fields['keep2'].queryset = Person.objects.exclude(
            id=remove.id
        )
        self.fields['keep2'].widget.forward = (forward.Const(remove.id, 'exclude'), )
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            HTML(f"<p>Supprimer la personne {remove}</p>"),
            'keep1',
            HTML('Chercher une autre personne :'),
            'keep2',
        )

    def clean(self):
        keep1 = self.cleaned_data.get('keep1')
        keep2 = self.cleaned_data.get('keep2')
        if not keep1 and not keep2:
            raise ValidationError("Merci de sélectionner ou rechercher une personne.")
        if keep1 and keep2 and keep1 != keep2:
            raise ValidationError("Merci de ne sélectionner ou rechercher qu'une seule personne.")
        keep = keep1 or keep2
        try:
            for employment in self.remove.employee.employments.all():
                if keep.employee.employments.filter(dates__overlap=employment.dates).exists():
                    raise ValidationError(
                        "Ces deux salarié·es ont des contrats de travail qui se chevauchent. "
                        "Merci d'ajuster les contrats au préalable."
                    )
        except Employee.DoesNotExist:
            pass
        self.cleaned_data = {'keep': keep}
        return self.cleaned_data


class PersonMergeForm(forms.Form):
    last_name = forms.ChoiceField(label="Nom d'usage", widget=forms.RadioSelect)
    first_name = forms.ChoiceField(label="Prénom", widget=forms.RadioSelect)
    family_name = forms.ChoiceField(label="Nom de naissance", widget=forms.RadioSelect)
    nickname = forms.ChoiceField(label="Surnom", widget=forms.RadioSelect)
    gender = forms.ChoiceField(label="Genre", widget=forms.RadioSelect)
    birthdate = forms.ChoiceField(label="Date de naissance", widget=forms.RadioSelect)
    birth_country = forms.ChoiceField(label="Pays de naissance", widget=forms.RadioSelect)
    birth_commune = forms.ChoiceField(label="Commune de naissance", widget=forms.RadioSelect)
    birth_place = forms.ChoiceField(label="Lieu de naissance", widget=forms.RadioSelect)
    profession = forms.ChoiceField(label="Profession", widget=forms.RadioSelect)
    address1 = forms.ChoiceField(label="Adresse ligne 1", widget=forms.RadioSelect)
    address2 = forms.ChoiceField(label="Adresse ligne 2", widget=forms.RadioSelect)
    address3 = forms.ChoiceField(label="Adresse ligne 3", widget=forms.RadioSelect)
    postal_code = forms.ChoiceField(label="Code postal", widget=forms.RadioSelect)
    city = forms.ChoiceField(label="Ville", widget=forms.RadioSelect)
    country = forms.ChoiceField(label="Pays", widget=forms.RadioSelect)
    email = forms.ChoiceField(label="Email", widget=forms.RadioSelect)
    home_phone = forms.ChoiceField(label="Tél. fixe", widget=forms.RadioSelect)
    mobile_phone = forms.ChoiceField(label="Tél. mobile", widget=forms.RadioSelect)
    image_rights = forms.ChoiceField(label="Droit à l'image", widget=forms.RadioSelect)
    legal_guardian1 = forms.ChoiceField(label="Responsable légal 1", widget=forms.RadioSelect)
    legal_guardian2 = forms.ChoiceField(label="Responsable légal 2", widget=forms.RadioSelect)
    adherent_status = forms.ChoiceField(label="Status adhérent", widget=forms.RadioSelect)
    permission_alone = forms.ChoiceField(label="Rentrer seul·e", widget=forms.RadioSelect)
    permission_with = forms.ChoiceField(label="Rentrer accompagné·e par :", widget=forms.RadioSelect)
    employee_email = forms.ChoiceField(label="Email professionel", widget=forms.RadioSelect)
    employee_phone = forms.ChoiceField(label="Tél. professionnel", widget=forms.RadioSelect)

    def __init__(self, person1, person2, *args, **kwargs):
        """"person1 = remove, person2 = keep"""
        super().__init__(*args, **kwargs)
        fieldnames = (
            'last_name', 'first_name', 'family_name', 'nickname', 'gender',
            'birth_country', 'birth_commune', 'birthdate', 'birth_place', 'profession',
            'address1', 'address2', 'address3',
            'postal_code', 'city', 'country',
            'email', 'home_phone', 'mobile_phone', 'image_rights',
        )
        initial = 1 if person1.id > person2.id else 2
        if not person1.birthdate or person1.is_minor or not person2.birthdate or person2.is_minor:
            fieldnames += (
                    'legal_guardian1', 'legal_guardian2',
                    'permission_alone', 'permission_with',
            )
        else:
            del self.fields['legal_guardian1']
            del self.fields['legal_guardian2']
            del self.fields['permission_alone']
            del self.fields['permission_with']
        if person1.is_adherent and person2.is_adherent:
            fieldnames += (
                'adherent_status',
            )
        else:
            del self.fields['adherent_status']
        if person1.is_employee and person2.is_employee:
            fieldnames += (
                'employee_email', 'employee_phone',
            )
        else:
            del self.fields['employee_email']
            del self.fields['employee_phone']
        for fieldname in fieldnames:
            if fieldname == 'gender':
                value1 = person1.get_gender_display()
                value2 = person2.get_gender_display()
            elif fieldname == 'birth_country':
                value1 = person1.birth_country.name if person1.birth_country else ""
                value2 = person2.birth_country.name if person2.birth_country else ""
            elif fieldname == 'birth_commune':
                value1 = person1.birth_commune.name if person1.birth_commune else ""
                value2 = person2.birth_commune.name if person2.birth_commune else ""
            elif fieldname == 'adherent_status':
                value1 = person1.adherent.get_status_display()
                value2 = person2.adherent.get_status_display()
            elif fieldname.startswith('adherent_'):
                value1 = getattr(person1.adherent, fieldname[9:]) if person1.is_adherent else None
                value2 = getattr(person2.adherent, fieldname[9:]) if person2.is_adherent else None
            elif fieldname.startswith('employee_'):
                value1 = getattr(person1.employee, fieldname[9:]) if person1.is_employee else None
                value2 = getattr(person2.employee, fieldname[9:]) if person2.is_employee else None
            else:
                value1 = getattr(person1, fieldname)
                value2 = getattr(person2, fieldname)
            if fieldname == 'birthdate':
                if value1:
                    value1 = value1.strftime('%d/%m/%Y')
                if value2:
                    value2 = value2.strftime('%d/%m/%Y')
            if fieldname == 'image_rights':
                value1 = {False: "Non", True: "Oui", None: None}[value1]
                value2 = {False: "Non", True: "Oui", None: None}[value2]
            if not value1 and not value2:
                self.fields[fieldname].choices = (
                    (1, "---"),
                )
                self.fields[fieldname].initial = 1
                self.fields[fieldname].disabled = True
            elif not value1:
                self.fields[fieldname].choices = (
                    (2, value2),
                )
                self.fields[fieldname].initial = 2
                self.fields[fieldname].disabled = True
            elif not value2:
                self.fields[fieldname].choices = (
                    (1, value1),
                )
                self.fields[fieldname].initial = 1
                self.fields[fieldname].disabled = True
            elif str(value1).upper() == str(value2).upper():
                self.fields[fieldname].choices = (
                    (1, value1),
                )
                self.fields[fieldname].initial = 1
                self.fields[fieldname].disabled = True
            else:
                self.fields[fieldname].choices = (
                    (1, value1),
                    (2, value2),
                )
                self.fields[fieldname].initial = initial
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            HTML('<p>Lorsque les données des deux fiches diffèrent, '
                 'merci de choisir laquelle des deux valeurs est à conserver.<br>'
                 'La valeur du haut est celle de la fiche à supprimer, celle du bas à conserver.</p>'),
            HTML('<div class="row"><div class="col-md-3">'),
            'last_name',
            HTML('</div><div class="col-md-3">'),
            'family_name',
            HTML('</div><div class="col-md-3">'),
            'first_name',
            HTML('</div><div class="col-md-3">'),
            'nickname',
            HTML('</div></div>'),
            HTML('<div class="row"><div class="col-md-3">'),
            'gender',
            HTML('</div><div class="col-md-3">'),
            'birthdate',
            HTML('</div><div class="col-md-3">'),
            'birth_country',
            HTML('</div><div class="col-md-3">'),
            'birth_commune',
            HTML('</div><div class="col-md-3">'),
            'birth_place',
            HTML('</div></div>'),
            HTML('<div class="row"><div class="col-md-6">'),
            'address1',
            HTML('</div><div class="col-md-3">'),
            'postal_code',
            HTML('</div><div class="col-md-3">'),
            'email',
            HTML('</div></div>'),
            HTML('<div class="row"><div class="col-md-6">'),
            'address2',
            HTML('</div><div class="col-md-3">'),
            'city',
            HTML('</div><div class="col-md-3">'),
            'home_phone',
            HTML('</div></div>'),
            HTML('<div class="row"><div class="col-md-6">'),
            'address3',
            HTML('</div><div class="col-md-3">'),
            'country',
            HTML('</div><div class="col-md-3">'),
            'mobile_phone',
            HTML('</div></div>'),
            HTML('<div class="row"><div class="col-md-3">'),
            'profession',
            HTML('</div><div class="col-md-3">'),
            'image_rights',
            HTML('</div></div>'),
        )
        if not person1.birthdate or person1.is_minor or not person2.birthdate or person2.is_minor:
            self.helper.layout.extend([
                HTML('<div class="row"><div class="col-md-3">'),
                'legal_guardian1',
                HTML('</div><div class="col-md-3">'),
                'legal_guardian2',
                HTML('</div><div class="col-md-3">'),
                'permission_alone',
                HTML('</div><div class="col-md-3">'),
                'permission_with',
                HTML('</div></div>'),
            ])
        if person1.is_adherent and person2.is_adherent:
            self.helper.layout.extend([
                'adherent_status',
            ])
        if person1.is_employee and person2.is_employee:
            self.helper.layout.extend([
                HTML('<div class="row"><div class="col-md-6">'),
                'employee_email',
                HTML('</div><div class="col-md-6">'),
                'employee_phone',
                HTML('</div></div>'),
            ])


class RateChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.name


class InvitationCreateBaseForm(forms.Form):
    structure = forms.ModelChoiceField(
        label="Structure",
        required=True,
        widget=autocomplete.ModelSelect2(url='structure-autocomplete', attrs={'allowClear': False}),
        queryset=Structure.objects.none(),
    )
    rate = RateChoiceField(
        label="Tarif",
        required=False,
        empty_label="Plein tarif",
        queryset=Rate.objects.none(),
    )
    date = forms.DateField(label="Date de début de validité de l'adhésion*", required=False)

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['structure'].queryset = Structure.objects \
            .for_perm('members.add_invitation', user) \
            .is_active()
        self.fields['structure'].initial = user.structure
        self.fields['rate'].queryset = Rate.objects.filter(
            season=current_season(),
            duration__isnull=False,
        )
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'structure',
            'rate',
            'date',
        )

    def clean_date(self):
        date = self.cleaned_data['date']
        if date and date < today():
            raise ValidationError("La date de début de validité est déjà dépassée.")
        if date and date > Date(current_season(), 8, 31):
            raise ValidationError("La date de début de validité dépasse la saison en cours.")
        rate = self.cleaned_data.get('rate')
        if not rate:
            return None
        if not date:
            raise ValidationError("La date de début de validité est requise pour les tarifs découverte.")
        return date


class InvitationCreateOldForm(InvitationCreateBaseForm):
    person = forms.ModelChoiceField(
        label="Personne",
        required=True,
        widget=autocomplete.ModelSelect2(url='person-autocomplete', attrs={'allowClear': False}),
        queryset=Person.objects.all(),
    )
    email = forms.EmailField(label="Email")

    def __init__(self, user, *args, **kwargs):
        super().__init__(user, *args, **kwargs)
        self.helper.layout = Layout(
            'person',
            'email',
            'structure',
            'rate',
            'date',
        )

    def clean_person(self):
        person = self.cleaned_data['person']
        try:
            adherent = person.adherent
        except Adherent.DoesNotExist:
            pass
        else:
            if adherent.is_blocked():
                raise ValidationError(mark_safe(
                    f"L'adhésion de cette personne est actuellement bloquée. {CONTACT_MESSAGE}"
                ))
        if person.last_adhesion and person.last_adhesion.is_current_full_rate:
            raise ValidationError(f"{person} est déjà adhérent·e sur la structure {person.last_adhesion.structure}.")
        return person

    def clean(self):
        cleaned_data = super().clean()
        person = cleaned_data.get('person')
        structure = cleaned_data.get('structure')
        rate = cleaned_data.get('rate')
        if person and structure and Invitation.objects.filter(
            person=person,
            structure=structure,
            season=current_season()
        ).exclude(
            adhesion__rate__category=RateCategory.DISCOVERY,
        ).exists():
            raise ValidationError("Cet adhérent·e est déjà invité·e sur cette structure.")
        if not person or not rate:
            return cleaned_data
        if person.last_adhesion and person.last_adhesion.is_current and rate.category == RateCategory.DISCOVERY:
            self.add_error(
                'rate',
                "Il n'est pas possible de prendre plusieurs adhésions découverte sur la même saison.",
            )
        if person.birthdate:
            age_begin = age(person.birthdate)
            if rate.age_min and rate.age_min > age_begin:
                raise ValidationError("La personne est trop jeune pour cette fonction.")
            if rate.age_max and rate.age_max < age_begin:
                raise ValidationError("La personne est trop vieille pour cette fonction.")
        return cleaned_data


class InvitationCreateNewForm(InvitationCreateBaseForm):
    emails = forms.CharField(
        label="Saisir ou copier/coller la liste des adresses email des personnes à inviter",
        help_text="Séparées par des retours à la ligne, des espaces, des virgules ou des points-virgule.",
        widget=forms.Textarea
    )

    def __init__(self, user, *args, **kwargs):
        super().__init__(user, *args, **kwargs)
        self.helper.layout = Layout(
            'emails',
            'structure',
            'rate',
            'date',
        )

    def clean_emails(self):
        emails = self.cleaned_data['emails']
        emails = emails.split(',')
        emails = sum([email.split(';') for email in emails], [])
        emails = sum([email.split() for email in emails], [])
        emails = [email for email in emails if email]
        validate = EmailValidator()
        for email in emails:
            try:
                validate(email)
            except ValidationError:
                raise ValidationError(f"L'adresse email « {email} » n'est pas valide.")
            if Adherent.objects.filter(
                person__email=email,
            ).exclude(
                status__in=(AdherentStatus.OK, AdherentStatus.LEFT),
            ).exists():
                raise ValidationError(mark_safe(
                    f"L'adresse email « {email} » est actuellement bloquée. {CONTACT_MESSAGE}"
                ))
        return emails


class InvitationCreateListForm(forms.Form):
    person = forms.ModelChoiceField(queryset=Person.objects.none(), widget=forms.HiddenInput())
    email = forms.EmailField(label="Email", required=False)
    team = forms.ModelChoiceField(
        label="Équipe",
        required=False,
        widget=autocomplete.ModelSelect2(
            url='team-autocomplete',
            forward=['structure', forward.Const('on', 'managed'), forward.Const('on', 'for_adherent')]
        ),
        queryset=Team.objects.none(),
    )
    function_config = forms.ModelChoiceField(
        label="Fonction",
        required=False,
        widget=autocomplete.ModelSelect2(
            url='functionconfig-autocomplete',
            forward=['team']
        ),
        queryset=FunctionConfig.objects.all(),
    )
    invite = forms.BooleanField(required=False)

    def __init__(self, user, persons, structure, force_age, *args, **kwargs):
        self.structure = structure
        super().__init__(*args, **kwargs)
        person = self.initial.get('person')
        self.force_age = person and person.pk in force_age
        self.fields['person'].queryset = persons
        self.fields['team'].queryset = Team.objects.for_perm('members.add_invitation', user).is_active().for_adherent()
        self.fields['email'].label = ""
        self.fields['team'].label = ""
        self.fields['function_config'].label = ""
        self.fields['invite'].label = ""
        self.current_functions = person and person.functions.filter(team__structure=structure).current()

    def clean_person(self):
        person = self.cleaned_data['person']
        try:
            adherent = person.adherent
        except Adherent.DoesNotExist:
            pass
        else:
            if adherent.is_blocked():
                raise ValidationError(mark_safe(
                    f"L'adhésion de cette personne est actuellement bloquée. {CONTACT_MESSAGE}"
                ))
        if person.last_adhesion and person.last_adhesion.season == current_season():
            raise ValidationError(f"{person} est déjà adhérent·e sur la structure {person.last_adhesion.structure}.")
        return person

    def clean(self):
        cleaned_data = super().clean()
        person = cleaned_data.get('person')
        invite = cleaned_data.get('invite')
        team = cleaned_data.get('team')
        function_config = cleaned_data.get('function_config')
        if invite and not self.cleaned_data.get('email'):
            self.add_error('email', "Ce champ est obligatoire.")
        if invite and team and not function_config:
            self.add_error('function_config', "La fonction est requise si une équipe est précisée.")
        if invite and function_config and not team:
            self.add_error('team', "L'équipe est requise si une fonction est précisée.")
        if team and function_config and function_config.team_type != team.type:
            self.add_error('function_config', "Cette fonction n'est pas compatible avec le type d'équipe.")
        if team and function_config and function_config.structure_type not in (team.structure.type, None):
            self.add_error('function_config', "Cette fonction n'est pas compatible avec le type de structure.")
        if not person or not function_config:
            return cleaned_data
        birthdate = person.birthdate
        if self.force_age or not birthdate:
            return cleaned_data
        age_begin = age(birthdate)
        age_end = current_season() - birthdate.year
        if birthdate.month >= 9:
            age_end -= 1
        if function_config.age_min and age_begin < function_config.age_min:
            self.has_age_warning = True
            self.add_error(
                None,
                f"L'adhérent·e est trop jeune pour ce type de fonction et cette équipe ({age_begin} "
                f"ans au lieu de {function_config.age_min} ans minimum)."
            )
        if function_config.age_max and function_config.age_max and age_end > function_config.age_max:
            self.has_age_warning = True
            self.add_error(
                None,
                f"L'adhérent·e est trop vi·eux·eille pour ce type de fonction et cette équipe ({age_end} "
                f"ans en fin de saison au lieu de {function_config.age_max} ans maximum)."
            )
        return cleaned_data


InvitationCreateListFormSet = forms.formset_factory(InvitationCreateListForm, extra=0)


class InvitationUpdateForm(forms.ModelForm):
    class Meta:
        model = Invitation
        fields = ('email', 'team', 'function_config', 'rate', 'date')
        widgets = {
            'team': autocomplete.ModelSelect2(
                url='team-autocomplete',
                attrs={'data-placeholder': "Aucune"},
                forward=[forward.Const('on', 'managed')],
            ),
            'function_config': autocomplete.ModelSelect2(
                url='functionconfig-autocomplete',
                attrs={'data-placeholder': "Ami·e"},
                forward=['team', forward.Const('on', 'for_adherent')]
            ),
        }

    def __init__(self, user, force_age, *args, **kwargs):
        self.force_age = force_age
        super().__init__(*args, **kwargs)
        self.fields['team'].queryset = Team.objects \
            .filter(structure=self.instance.structure) \
            .for_adherent() \
            .for_perm('members.change_invitation', user) \
            .is_active()
        self.fields['team'].widget.forward.append(forward.Const(str(self.instance.structure.pk), 'structure'))
        self.fields['function_config'].queryset = FunctionConfig.objects.filter(subject_to_nomination=False)
        self.fields['rate'].empty_label = "Plein tarif"
        self.fields['rate'].queryset = Rate.objects.filter(season=current_season(), duration__isnull=False)
        self.fields['date'].label = "Date de début de validité de l'adhésion*"
        self.helper = FormHelper()
        self.helper.form_tag = False
        current_functions = self.instance.person and \
            self.instance.person.functions.filter(team__structure=self.instance.structure).current()
        if current_functions:
            self.helper.layout = Layout(
                'email',
                HTML('<div class="row"><div class="col-md-6">'),
                'rate',
                HTML('</div><div class="col-md-6">'),
                'date',
                HTML('</div></div>'),
                HTML(
                    f"Prolongement des fonctions actuelles sur la structure <b>{self.instance.structure.name}</b> :<ul>"
                )
            )
            for function in current_functions:
                self.helper.layout.append(HTML(
                    f"<li><b>{function}</b> jusqu'au {function.end.strftime('%d/%m/%Y')}</li>"
                ))
            self.helper.layout.append(HTML(
                "</ul><p>Vous pourrez ajouter ou renouveller des fonctions/équipes par la suite.</p>"
            ))
        else:
            self.helper.layout = Layout(
                'email',
                HTML('<div class="row"><div class="col-md-6">'),
                'rate',
                HTML('</div><div class="col-md-6">'),
                'date',
                HTML('</div></div><div class="row"><div class="col-md-6">'),
                'team',
                HTML('</div><div class="col-md-6">'),
                'function_config',
                HTML(
                    "</div></div><p>Pour les bénévoles/amis, vous pouvez laisser ces cases vides et ne pas "
                    "attribuer de fonction.</p><p>L'attribution des fonctions électives ne peut se faire "
                    "que par AGORA dans un second temps. "
                    "Dans ce cas, laissez l'équipe et la fonction vides à ce stade.</p>")
            )

    def clean_date(self):
        date = self.cleaned_data['date']
        if date and date < today():
            raise ValidationError("La date de début de validité est déjà dépassée.")
        if date and date > Date(current_season(), 8, 31):
            raise ValidationError("La date de début de validité dépasse la saison en cours.")
        rate = self.cleaned_data.get('rate')
        if not rate:
            return None
        if not date:
            raise ValidationError("La date de début de validité est requise pour les tarifs découverte.")
        return date

    def clean_function_config(self):
        function_config = self.cleaned_data['function_config']
        team = self.cleaned_data.get('team')
        rate = self.cleaned_data.get('rate')
        if not rate:
            return function_config
        assert rate.season == current_season()
        if rate.category == RateCategory.DISCOVERY and not team and not function_config:
            raise ValidationError(f"Cette fonction n'est pas accessible au tarif {rate.name}")
        if not team or not function_config:
            return function_config
        if team.structure.type not in rate.structure_types.all() or \
                function_config.function_type.category != rate.function_category:
            raise ValidationError(f"Cette fonction n'est pas accessible au tarif {rate.name}")
        return function_config

    def clean(self):
        cleaned_data = super().clean()
        team = cleaned_data.get('team')
        rate = cleaned_data.get('rate')
        function_config = cleaned_data.get('function_config')
        person = self.instance.person
        structure = self.instance.structure
        if person and Invitation.objects.filter(
            person=person,
            structure=structure,
            season=current_season()
        ).exclude(
            pk=self.instance.pk,
        ).exists():
            raise ValidationError(
                "Cet·te adhérent·e est déjà invité·e sur cette structure. "
                "Vous pouvez modifier l'invitation existante si nécessaire."
            )
        if team and not function_config and not self.errors:
            self.add_error(
                'function_config',
                "Une fonction autre que « Ami·e » est requise si une équipe est précisée."
            )
        if function_config and not team:
            self.add_error('team', "L'équipe est requise si une fonction autre que « Ami·e » est précisée.")
        if team and function_config and function_config.team_type != team.type:
            self.add_error('function_config', "Cette fonction n'est pas compatible avec le type d'équipe.")
        if team and function_config and function_config.structure_type not in (team.structure.type, None):
            self.add_error('function_config', "Cette fonction n'est pas compatible avec le type de structure.")
        if person and person.last_adhesion and person.last_adhesion.is_current and \
                rate and rate.category == RateCategory.DISCOVERY:
            self.add_error(
                'rate',
                "Il n'est pas possible de prendre plusieurs adhésions découverte sur la même saison.",
            )
        if self.force_age or not person or not person.birthdate or not team or not function_config:
            return cleaned_data
        birthdate = person.birthdate
        age_begin = age(birthdate)
        age_end = current_season() - birthdate.year
        if birthdate.month >= 9:
            age_end -= 1
        if function_config.age_min and age_begin < function_config.age_min:
            self.has_age_warning = True
            raise ValidationError(
                f"L'adhérent·e est trop jeune pour ce type de fonction et cette équipe ({age_begin} "
                f"ans au lieu de {function_config.age_min} ans minimum)."
            )
        if function_config.age_max and function_config.age_max and age_end > function_config.age_max:
            self.has_age_warning = True
            raise ValidationError(
                f"L'adhérent·e est trop vi·eux·eille pour ce type de fonction et cette équipe ({age_end} "
                f"ans en fin de saison au lieu de {function_config.age_max} ans maximum)."
            )
        return cleaned_data


class InvitationChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return f"{obj.adhesion.adherent.person} : " \
            f"{obj.adhesion.effective_rate + obj.adhesion.donation - obj.adhesion.paid} €"


class SelectInvitationsForm(forms.Form):
    invitations = InvitationChoiceField(
        queryset=Invitation.objects.none(),
        widget=forms.CheckboxSelectMultiple,
        label="Sélectionnez les adhésions que vous souhaitez payer",
    )

    def __init__(self, user, invitations, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['invitations'].queryset = invitations
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'invitations',
            HTML(
                "<p>Si vous souhaitez prendre une adhésion supplémentaire et payer en une seule fois, "
                "ne cliquez pas sur suivant. Fermez cette fenêtre et ouvrez le lien de la nouvelle invitation. "
                "A la fin, il vous sera proposé de payer également les autres adhésions prises lors de la même session "
                "de navigation.<p>"
            )
        )


class TeamForm(forms.ModelForm):
    config = forms.ModelChoiceField(label="Type", queryset=TeamConfig.objects.all(), required=True,
                                    widget=autocomplete.ModelSelect2(url='teamconfig-autocomplete',
                                                                     forward=['structure']))

    class Meta:
        model = Team
        fields = ('structure', 'config', 'name')
        widgets = {
            'structure': autocomplete.ModelSelect2(url='structure-autocomplete', forward=[
                forward.Const('on', 'is_active'),
                forward.Const('members.add_team', 'for_perm'),
            ]),
        }

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        if self.instance.pk:
            del self.fields['structure']
            del self.fields['config']
            self.helper.layout = Layout()
        else:
            self.fields['structure'].queryset = Structure.objects.is_active().for_perm('members.add_team', user)
            self.fields['name'].required = False
            self.helper.layout = Layout(
                'structure',
                'config',
            )
        self.helper.layout.append('name')

    def clean_structure(self):
        structure = self.cleaned_data['structure']
        if not self.user.has_perm("members.add_team", structure):
            raise ValidationError("Non autorisé à gérer cette structure.")
        return structure

    def clean_config(self):
        config = self.cleaned_data['config']
        structure = self.cleaned_data.get('structure')
        if not config or not structure:
            return config
        if config.structure_type != structure.type:
            raise ValidationError(
                f"Le type d'équipe {config.team_type} n'est pas autorisé pour "
                f"le type de structure {structure.type}."
            )
        duplicates = Team.objects.filter(structure=structure, type=config.team_type)
        if config.team_type.unique and duplicates.exists():
            raise ValidationError(
                f"L'équipe {duplicates[0]} existe déjà avec le type {config.team_type} et ce type doit être unique."
            )
        return config

    def clean_name(self):
        name = self.cleaned_data['name']
        config = self.cleaned_data.get('config')
        if config and config.team_type.custom and not name:
            raise ValidationError("Le nom est requis lorsque le type d'équipe est personnalisé.")
        if not name:
            name = config.team_name
        structure = self.cleaned_data.get('structure')
        if not structure:
            return name
        teams = Team.objects.filter(structure=structure, name=name)
        if teams and teams[0].is_active:
            detail_url = reverse('team_detail', args=[teams[0].id])
            raise ValidationError(mark_safe(
                f"Une <a href=\"{detail_url}\">équipe</a> avec ce nom existe déjà dans la structure."
            ))
        if teams and not teams[0].is_active:
            detail_url = reverse('team_detail', args=[teams[0].id])
            activate_url = reverse('team_activate', args=[teams[0].id])
            raise ValidationError(mark_safe(
                f"Une <a href=\"{detail_url}\">équipe</a> inactive avec ce nom existe déjà dans la structure. "
                f"Vous pouvez la <a href=\"{activate_url}\">réactiver</a>."
            ))
        return name

    def save(self, commit=True):
        team = super().save(commit=False)
        if not team.pk:
            config = self.cleaned_data['config']
            team.type = config.team_type
        if commit:
            team.save()
        return team


class EmployeeForm(forms.ModelForm):
    class Meta:
        model = Employee
        fields = ('email', 'phone')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'email',
            'phone',
        )


class EmploymentForm(forms.ModelForm):
    begin = forms.DateField(label="Début", required=True)
    end = forms.DateField(label="Fin", required=False)

    class Meta:
        model = Employment
        fields = ('type', 'name', 'begin', 'end')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields['begin'].initial = self.instance.begin
            self.fields['end'].initial = self.instance.end
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'type',
            'name',
            'begin',
            'end',
        )

    def clean(self):
        begin = self.cleaned_data.get('begin')
        end = self.cleaned_data.get('end')
        if not begin or not self.instance.pk:
            return self.cleaned_data
        existing = Employment.objects.filter(
            employee=self.instance.employee,
            dates__overlap=DateRange(begin, end, '[]'),
        ).exclude(pk=self.instance.pk).order_by('-dates')
        if existing:
            if existing[0].end:
                raise ValidationError(
                    "Ce·tte salarié·e a déjà un contrat de travail "
                    f"entre le {existing[0].begin.strftime('%d/%m/%Y')} "
                    f"et le {existing[0].end.strftime('%d/%m/%Y')}."
                )
            else:
                raise ValidationError(
                    "Ce·tte salarié·e a déjà un contrat de travail "
                    f"à partir du {existing[0].begin.strftime('%d/%m/%Y')}."
                )
        return self.cleaned_data

    def save(self, commit=True):
        employment = super().save(commit=False)
        employment.dates = DateRange(self.cleaned_data['begin'], self.cleaned_data['end'], '[]')
        if commit:
            employment.save()
        return employment


class RoleForm(forms.ModelForm):
    structure = forms.ModelChoiceField(
        label="Structure",
        queryset=Structure.objects.all(),
        widget=autocomplete.ModelSelect2(url='structure-autocomplete')
    )

    class Meta:
        model = Role
        fields = ('config', 'structure', 'team', 'proxy')
        widgets = {
            'team': autocomplete.ModelSelect2(
                url='team-autocomplete',
                forward=['structure', forward.Const('members.add_role', 'for_perm')]
            ),
            'proxy': autocomplete.ModelSelect2(
                url='person-autocomplete',
                forward=[forward.Const('members.add_role', 'for_perm')]
            ),
        }

    def __init__(self, user, force, proxy=None, *args, **kwargs):
        self.proxy = proxy
        self.force = force
        super().__init__(*args, **kwargs)
        if proxy:
            del self.fields['proxy']
        else:
            self.fields['proxy'].label = "Déléguer à"
            self.fields['proxy'].help_text = None
        self.fields['config'].label = "Rôle"
        self.fields['team'].queryset = Team.objects.for_perm('members.add_role', user).distinct()
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'config',
            'structure',
            'team',
        )
        if not proxy:
            self.helper.layout.append('proxy')

    def clean_proxy(self):
        proxy = self.cleaned_data['proxy']
        team = self.cleaned_data['team']
        if not team:
            return proxy
        adhesion = proxy.current_adhesion
        if not self.force and (not adhesion or adhesion.structure != team.structure):
            self.has_warning = True
            raise ValidationError(
                "Cette personne n'appartient pas à la structure pour laquelle vous souhaitez déléguer le rôle."
            )
        config = self.cleaned_data['config']
        if not config:
            return proxy
        if Role.objects.filter(holder=None, config=config, team=team, proxy=proxy).exists():
            raise ValidationError("Ce rôle est déjà attribué à cette personne.")
        return proxy

    def save(self, commit=True):
        delegation = super().save(commit=False)
        if self.proxy:
            delegation.proxy = self.proxy
        if commit:
            delegation.save()
        return delegation


class RoleDelegationForm(forms.ModelForm):
    class Meta:
        model = Role
        fields = ('proxy', )
        widgets = {
            'proxy': autocomplete.ModelSelect2(
                url='person-autocomplete',
                forward=[forward.Const(True, 'current')]
            ),
        }

    def __init__(self, user, holder, config, team, force, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.holder = holder
        self.config = config
        self.team = team
        self.force = force
        self.fields['proxy'].queryset = Person.objects.current().distinct()
        self.fields['proxy'].label = "Déléguer à"
        self.fields['proxy'].help_text = None
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'proxy',
        )

    def clean_proxy(self):
        proxy = self.cleaned_data['proxy']
        adhesion = proxy.current_adhesion
        if not self.force and (not adhesion or adhesion.structure != self.team.structure):
            self.has_warning = True
            raise ValidationError(
                "Cette personne n'appartient pas à la structure pour laquelle vous souhaitez déléguer le rôle."
            )
        if Role.objects.filter(holder=self.holder, config=self.config, team=self.team, proxy=proxy).exists():
            raise ValidationError("Vous avez déjà délégué ce rôle à cette personne.")
        return proxy

    def save(self, commit=True):
        delegation = super().save(commit=False)
        delegation.holder = self.holder
        delegation.config = self.config
        delegation.team = self.team
        if commit:
            delegation.save()
        return delegation


class RoleConfigForm(forms.ModelForm):
    class Meta:
        model = RoleConfig
        fields = ('name', 'description', 'rgpd', 'delegation', 'for_self', 'for_team', 'for_structure',
                  'for_sub_structures', 'for_all', 'only_electeds', 'only_employees', 'with_guardians', 'with_payers')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['description'].widget.attrs['rows'] = 3
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            HTML('<div class="row"><div class="col-md-8">'),
            'name',
            HTML('</div><div class="col-md-4">'),
            'rgpd',
            HTML('</div></div>'),
            'description',
            'delegation',
            HTML('<div class="row"><div class="col-md-4">'),
            'for_self', 'for_team', 'for_structure', 'for_sub_structures', 'for_all',
            HTML('</div><div class="col-md-4">'),
            'only_electeds', 'only_employees',
            HTML('</div><div class="col-md-4">'),
            'with_guardians', 'with_payers',
            HTML('</div></div>'),
        )
        current_ct = None
        active = True
        nav = [HTML('<ul class="nav nav-tabs" role="tablist">')]
        tabs = [HTML('<div class="tab-content">')]
        for permission in Permission.objects.exclude(
            name__startswith="Can ",
        ).order_by(
            '-content_type__external',
            'content_type',
            'name',
        ):
            field_name = f'{permission.content_type.app_label}.{permission.codename}'
            self.fields[field_name] = forms.BooleanField(
                label=permission.name,
                required=False,
                initial=self.instance.pk and permission in self.instance.permissions.all(),
            )
            if permission.content_type != current_ct:
                if current_ct:
                    tabs.append(HTML('</div></div>'))
                current_ct = permission.content_type
                try:
                    name = current_ct.external.verbose_name
                except ExternalContentType.DoesNotExist:
                    name = current_ct.name
                nav.append(HTML(
                    f'''<li><a{' class="active"' if active else ''} href="#tab_ct_{current_ct.pk}"'''
                    f' data-toggle="tab">{name}</a></li>'
                ))
                tabs.append(HTML(
                    f'''<div class="tab-pane{' active' if active else ''}" id="tab_ct_{current_ct.pk}">'''
                    '<div class="row">'
                ))
                active = False
            tabs.append(HTML('<div class="col-md-3">'))
            tabs.append(field_name)
            tabs.append(HTML('</div>'))
        nav.append(HTML('</ul>'))
        tabs.append(HTML('</div></div></div>'))
        self.helper.layout.append(HTML('<div style="min-height: 24em;">'))
        self.helper.layout.extend(nav)
        self.helper.layout.extend(tabs)
        self.helper.layout.append(HTML('</div>'))

    def save(self, commit=True):
        config = super().save(commit=commit)
        if commit:
            config.permissions.set(
                permission
                for permission in Permission.objects.exclude(name__startswith="Can ")
                if self.cleaned_data[f'{permission.content_type.app_label}.{permission.codename}']
            )
        return config


class EmailAsLoginForm(forms.Form):
    person = forms.ModelChoiceField(label="Connexion affectée à", queryset=Person.objects.none(), widget=RadioSelect())

    def __init__(self, email, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.persons = Person.objects.filter(email=email)
        self.fields['person'].queryset = self.persons
        try:
            self.fields['person'].initial = self.persons.get(email_as_login=True)
        except Person.DoesNotExist:
            pass
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            'person',
        )

    def save(self):
        person = self.cleaned_data['person']
        self.persons.exclude(pk=person.pk).update(email_as_login=False)
        person.email_as_login = True
        person.save()
