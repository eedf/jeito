# Copyright 2021 Éclaireuses Éclaireurs de France, Gaël UTARD
#
# This file is part of Jéito.
#
# Jéito is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

from django import template

register = template.Library()


@register.simple_tag(takes_context=True)
def get_params(context, **kwargs):
    query = context['request'].GET.copy()
    for k, v in kwargs.items():
        query[k] = v
    return query.urlencode()


@register.simple_tag(takes_context=True)
def has_perm(context, perm, obj):
    return context['user'].has_perm(perm, obj)


@register.simple_tag
def get_model_verbose_name(object):
    return object._meta.verbose_name
